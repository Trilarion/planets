/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.util;

import java.io.*;

/**
 * Node to be used as part of a linked list. Node contains some
 * arbitrary data and pointer to next node.
 *
 * @author Dodekaedron Software Creations, Inc. -- Wraith
 */

public class LinkedListNode implements Serializable {

  private Object data;
  private LinkedListNode next;

  /**
   * Creates new node with given data. Does not yet link it to anything.
   * 
   * @param o The data to store in this node.
   */

  public LinkedListNode(Object o) {
    data=o;
  }

  /**
   * Gets the data stored in this node.
   * 
   * @return Data stored in this node.
   */

  public Object getData(){ return(data);}

  /**
   * Sets the data stored in this node. Old data is discarded.
   *
   * @param o New data for this node.
   */

  public void setData(Object o) {data=o;}

  /**
   * Gets the next node in the list. If this is the last node of the
   * list, returns null.
   *
   * @return Next node of the list.
   */

  public LinkedListNode getNext() {return(next);}

  /**
   * Sets the pointer to next node.
   *
   * @param l The next node following this one.
   */

  public void setNext(LinkedListNode l) {next=l;} 
}
