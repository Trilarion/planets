/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.util;

/** GarbageCollection contains static methods for easy invocation of
 * runtime's garbage collector.
 *
 * @author Dodekaedron Software Creations, Inc. -- Wraith
 */

public class GarbageCollection {

  /** Invokes garbage collection once. Ie. fast garbage collector.
   */

  public static void once() {
    Runtime rt = Runtime.getRuntime();

    rt.gc();
    rt.runFinalization();
  }

  /** Runs garbage collection until it doesn't free any more memory.
   */
  
  public static void freeAll() {
    Runtime rt = Runtime.getRuntime();
    long isFree = rt.freeMemory();
    long wasFree;
    do {
      //System.out.println("Free memory: " + isFree);
      wasFree=isFree;
      rt.gc();
      isFree = rt.freeMemory();
    } while (isFree > wasFree);
    
    rt.runFinalization();
  }
}
