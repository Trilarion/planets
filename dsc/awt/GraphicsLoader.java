/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.awt;

import java.awt.*;
import java.awt.image.*;


/** GraphicsLoader has static methods to load images. Methods don't 
  return until the image is fully loaded. (In contrast to Toolkit's 
  getImage method's behavior.
  */

public class GraphicsLoader extends Component {

  /** Loads given image and makes color number 0 transparent.
   *
   * @param filename Filename of the image to load.
   * @return Loaded transparent image.
   */

  public static Image getTransparentImage(String filename) {
    return getTransparentImage(filename, 0);
  }

  /** Loads given image and makes given color number transparent.
   *
   * @param filename Filename of the image to load.
   * @param transcolor Transparent color number.
   * @return Loaded transparent image.
   */

  public static Image getTransparentImage(String filename, 
					  int transcolor) {

    MediaTracker mt = new MediaTracker(new GraphicsLoader());
    Image nontrans = Toolkit.getDefaultToolkit().getImage(filename);

    Image i = 
      Toolkit.getDefaultToolkit().createImage(
         new FilteredImageSource(nontrans.getSource(),
	 new TransparencyFilter(transcolor)));

    mt.addImage(i,1);
    
    try {
      mt.waitForAll();
      if(mt.isErrorAny()) {
	System.out.println("Error Loading Image" + filename);
	System.exit(1);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    return i;
  }

  /** Loads given image.
   *
   * @param filename Filename of the image
   * @return Loaded image.
   */

  public static Image getImage(String filename) {

    MediaTracker mt = new MediaTracker(new GraphicsLoader());
    Image i = Toolkit.getDefaultToolkit().getImage(filename);

    mt.addImage(i,1);
    
    try {
      mt.waitForAll();
      if(mt.isErrorAny()) {
	System.out.println("Error Loading Image " + filename);
	System.exit(1);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    return i;
  }
}
