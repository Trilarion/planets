/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.awt;

import java.awt.*;


/** Image component that contains two overlapping images. Smaller image
 * is centered on the top of the bigger one.
 *
 * @author Dodekaedron Software Creations, Inc. -- Wraith
 */

public class DoubleImageComponent extends Canvas {
  private Image smallImage;
  private Image bigImage;
  private int xdiff, ydiff;

  /** Repaints without clearing background. This means that images must
   * not contain invisible pixels when combined.
   */
  
  public void repaint(Graphics g) {
    paint(g);
  }

  /** Paint the images. 
   */

  public void paint(Graphics g) {
    g.drawImage(bigImage,0,0,this);
    g.drawImage(smallImage,xdiff,ydiff,this);
  }
    
  /** Create new image component. 
   *
   * @param small Image to be drawn on the top.
   * @param big Image to be drawn under the small image.
   */

  public DoubleImageComponent (Image small, Image big) {
    super();
    
    bigImage=big;
    setImage(small);
  }

  /** Gets preferred size of the component 
   *
   * @return Preferred size.
   */

  public Dimension getPreferredSize() {
    return getMinimumSize();
  }

  /* Gets mininum size that this component fits into.
   *
   * @return Mininum size dimensions.
   */

  public Dimension getMinimumSize() {
    return new Dimension(bigImage.getWidth(this), bigImage.getHeight(this));
  }

  /** Sets the smaller image.
   *
   * @param i New small image to be shown on the top.
   */

  public void setImage(Image i) {
    smallImage=i;
    xdiff=(bigImage.getWidth(this)-smallImage.getWidth(this))/2;
    ydiff=(bigImage.getHeight(this)-smallImage.getHeight(this))/2;
  }
}
