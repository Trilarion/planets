/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.awt;

import java.awt.*;
import java.awt.event.*;
import java.io.*;


/**
 * HelpWindow is a general purpose help system for AWT-applications. In order 
 * to use it you have to create a separate directory for help files. (for 
 * example data/help). This directory is then known as "help-path". 
 * Help topics are made by creating subdirectories undex help-path. Subtopics
 * are created by placing correctly named file containing the help text in 
 * appropriate subdirectory. <p>
 *
 * This class has a static method callForHelp, which should be used when help
 * is invoked. Method handles all nessecery windowing and makes sure that there
 * is only one help-window open at the time. (Furhter calls simply change 
 * subject) <p>
 *
 * Example call: <p>
 * <code>
 * HelpWindow.callForHelp("data" + File.separator + "help","Introduction",
 * "Short");
 * </code>
 *
 * @author Dodekaedron Software Creations, Inc.
 * @version 1.0
 * @see #callForHelp
 */  

public class HelpWindow extends LayoutableFrame implements ActionListener, 
  ItemListener{

  private TextArea  outputText;
  private Button closeButton;
  private GridBagLayout gb;
  private Choice mainTopic, subTopic;
  private String currentSubTopics;
  private File helpDirectory;

  /** Sets current help subject. If helpfile/directory is not found calls 
   * exits program. 
   *
   * @param m Main help topic
   * @param s Help subtopic
   */

  public void setSubject(String m, String s) {
    mainTopic.select(m);
    recreateSubTopics();
    subTopic.select(s);
    loadHelpFile();
  }

  private void loadHelpFile() {
    recreateSubTopics();

    BufferedReader in=null;
    try {
      File dir = new File(helpDirectory,mainTopic.getSelectedItem());
      File f   = new File(dir,subTopic.getSelectedItem());
      in=new BufferedReader(new FileReader(f));
    } catch (Exception e) {
      System.err.println("Internal error: Help Topic not found:" + 
			 mainTopic.getSelectedItem() + "/" + 
			 subTopic.getSelectedItem()); 
      return;
    }

    outputText.setText("");

    String s;
    try {
      do {
	s = in.readLine();
	if(s!=null)
	  outputText.append(s + "\n");
      } while(s != null);
    } catch (IOException e) {}
    outputText.setCaretPosition(0);

    try {
      in.close();
    } catch (IOException e) {}
  }

  private void scanDirectoryToChoice(Choice c, File d) {
    String s [] =d.list();
    
    for (int i = 0; i < s.length; i++){
      c.add(s[i]);
    }
  }

  private Choice createMainTopics() {
    Choice c = new Choice();

    scanDirectoryToChoice(c,helpDirectory);
    return c;
  }

  private void recreateSubTopics() {
    if(currentSubTopics.equals(mainTopic.getSelectedItem()))
      return;

    subTopic.removeAll();
    File   d = new File(helpDirectory, mainTopic.getSelectedItem());
    scanDirectoryToChoice(subTopic,d);

    currentSubTopics=mainTopic.getSelectedItem();
  }

  private Choice createSubTopics() {
    Choice c = new Choice();

    File   d = new File(helpDirectory, mainTopic.getSelectedItem());
    scanDirectoryToChoice(c,d);
    
    currentSubTopics=mainTopic.getSelectedItem();
    return c;
  }

  private void setHelpDirectory(String helpPath) {
    helpDirectory = new File(helpPath);
    if(! (helpDirectory.exists() && helpDirectory.isDirectory())) {
      System.err.println("Invalid Help Directory");
      System.exit(1);
    }
  }

  /** Creates new HelpWindow and initializes help path.
   *
   * @param helpPath Directory which contains help directories and text files
   */

  public HelpWindow(String helpPath) {
    setTitle("Help");
    setSize(8*40,12*40);

    setHelpDirectory(helpPath);

    gb = new GridBagLayout();
    setLayout(gb);

    Image i=GraphicsLoader.getImage("data/graph/still/icons/help.gif");
    setIconImage(i);
    
    outputText = new TextArea("Help v. 0.00\n\n",20,50,TextArea.SCROLLBARS_VERTICAL_ONLY);
    outputText.setEditable(false);

    gbAdd(outputText,100,100,0,1,8,20);

    closeButton=createButton("Close",this);
    gbAdd(closeButton,100,0,6,21,2,1);

    mainTopic = createMainTopics();
    mainTopic.addItemListener(this);

    subTopic = createSubTopics();
    subTopic.addItemListener(this);
    
    gbAdd(new Label("Topic"),100,0,0,0,1,1);
    gbAdd(new Label("/"),100,0,4,0,1,1);
    gbAdd(mainTopic,100,0,1,0,3,1);
    gbAdd(subTopic,100,0,5,0,3,1);
  }

  /** Method for ActionListener implementation. Do not call */

  public void actionPerformed(ActionEvent evt) {
    String arg = evt.getActionCommand();

    if(arg.equals("Close")) {
      setVisible(false);
    }
  }

  /** Method for ItemListener implementation. Do not call. */    

  public void itemStateChanged(ItemEvent evt) {
    if(evt.getStateChange()==ItemEvent.DESELECTED) return;

    loadHelpFile();
    repaint();
  }

  private static HelpWindow helpWindow=null;

 /** Creates and shows new help window, if needed, and sets subjects 
   * for the window.
   *
   * @param path Help path. Only used in the first calls, so subsequent calls
   * with different paths have no effect in path.
   * @param m Help main topic
   * @param s Help subtopic
   */

  public static void callForHelp(String path, String m, String s) {
    if(helpWindow!=null) {
      helpWindow.setVisible(true);
    } else {
      helpWindow = new HelpWindow(path);
      helpWindow.show();
    }
    helpWindow.setSubject(m,s);
  }
}
