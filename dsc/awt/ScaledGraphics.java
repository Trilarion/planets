/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.awt;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;


public class ScaledGraphics {

  private int scaleFactor;
  private Graphics g;

  public ScaledGraphics(Graphics g, int scale) {
    scaleFactor = scale;
    this.g      = g;
  }

  public int getScaleFactor() {
    return scaleFactor;
  }

  public void clearRect(int x, int y, int width, int height) {
    g.clearRect(x/scaleFactor,
		y/scaleFactor,
		width/scaleFactor,
		height/scaleFactor);
  }

  public void drawRoundRect(int x, int y, int width, int height,
			    int arcWidth, int arcHeight) {

    g.drawRoundRect(x/scaleFactor,
		    y/scaleFactor,
		    width/scaleFactor,
		    height/scaleFactor,
		    arcWidth/scaleFactor,
		    arcHeight/scaleFactor);
  }

  public void fillRoundRect(int x, int y, int width, int height,
			    int arcWidth, int arcHeight) {

    g.fillRoundRect(x/scaleFactor,
		    y/scaleFactor,
		    width/scaleFactor,
		    height/scaleFactor,
		    arcWidth/scaleFactor,
		    arcHeight/scaleFactor);
  }

  public void draw3DRect(int x, int y, int width, int height,
			 boolean raised) {

    g.draw3DRect(x/scaleFactor,
		 y/scaleFactor,
		 width/scaleFactor,
		 height/scaleFactor,
		 raised);
  }

  public void fill3DRect(int x, int y, int width, int height,
			 boolean raised) {

    g.fill3DRect(x/scaleFactor,
		 y/scaleFactor,
		 width/scaleFactor,
		 height/scaleFactor,
		 raised);
  }

  public void drawRect(int x, int y, int width, int height) {
    g.drawRect(x/scaleFactor,
	       y/scaleFactor,
	       width/scaleFactor,
	       height/scaleFactor);
  }

  public void drawOval(int x, int y, int width, int height) {
    
    g.drawOval(x/scaleFactor,
	       y/scaleFactor,
	       width/scaleFactor,
	       height/scaleFactor);
  }

  public void fillOval(int x, int y, int width, int height) {
    
    g.fillOval(x/scaleFactor,
	       y/scaleFactor,
	       width/scaleFactor,
	       height/scaleFactor);
  }

  public void drawArc(int x, int y, int width, int height,
		      int startAngle, int arcAngle) {


    g.drawArc(x/scaleFactor,
	      y/scaleFactor,
	      width/scaleFactor,
	      height/scaleFactor,
	      startAngle,
	      arcAngle);
	      
  }

  public void fillArc(int x, int y, int width, int height,
		      int startAngle, int arcAngle) {


    g.fillArc(x/scaleFactor,
	      y/scaleFactor,
	      width/scaleFactor,
	      height/scaleFactor,
	      startAngle,
	      arcAngle);
  }

  public void drawString(String str, int x, int y) {
    g.drawString(str, x/scaleFactor, y/scaleFactor);
  }

  public boolean drawImage(Image img, int x, int y,
			   int width, int height,
			   ImageObserver observer) {

    return g.drawImage(img, x/scaleFactor, y/scaleFactor,
		width/scaleFactor, height/scaleFactor,
		observer);
  }

  public boolean drawImage(Image img, int x, int y,
			   ImageObserver observer) {

    int height = img.getHeight(observer);
    int width  = img.getWidth(observer);

    return g.drawImage(img, x/scaleFactor, y/scaleFactor,
		width/scaleFactor, height/scaleFactor,
		observer);
  }

  public void setColor(Color c) {
    g.setColor(c);
  }

  public void setFont(Font font) {
    g.setFont(font);
  }

  public FontMetrics getFontMetrics(Font font) {
    return g.getFontMetrics(font);
  }

  public void drawLine(int x1, int y1, int x2, int y2) {
    g.drawLine(x1/scaleFactor,
	       y1/scaleFactor,
	       x2/scaleFactor,
	       y2/scaleFactor);
  }
}
