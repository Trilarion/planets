/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;

import java.io.*;
import java.net.*;
import java.util.*;
import dsc.util.Queue;


public class HostController extends Thread {
  
  private Chatter chatter;
  private PlayerConnect playerConnect[];
  ServerSocket serverSocket;
  private Queue socketQueue;
  private Queue joiningQueue;
  private Queue pendingMessageQueue[];
  private int numberOfPlayers;
  private PlayersInfo playersInfo;
  private boolean gameRunning=false;
  private WorldCreator worldCreator;
  public HostScreen hostWindow;
  private HostWorld world;
  private WorldOptionsWindow worldOptionsWindow;

  private void broadcast(Message m) {
    for(int i=0; i < GlobalDefines.maxPlayers; i++) {
	if(playerConnect[i] != null) {
	    playerConnect[i].putMessage(m);
	} else {
	    if(m instanceof ChatMessage) {
		if(playersInfo.isPlayerInGame(i))
		    pendingMessageQueue[i].add(m);
	    }
	}
    }
  }

  private void chatBroadcast(String s) {
    ChatMessage cm = new ChatMessage(-1,-1,s);
    cm.setSource(-1);
    broadcast(cm);
  }

  public void sendChatMessage(int player, String text) {
      sendChatMessageAs(player, -1, text);
  }

  public void sendChatMessageAs(int toPlayer, int asPlayer, String text) {
    ChatMessage cm = new ChatMessage(toPlayer, asPlayer, text);
    cm.setSource(asPlayer);
    if(playerConnect[toPlayer] != null) {
      playerConnect[toPlayer].putMessage(cm); 
    } else {
	if(playersInfo.isPlayerInGame(toPlayer))
	    pendingMessageQueue[toPlayer].add(cm);
    }
  }

  public void broadcastChatMessage(String text) {
    chatBroadcast(text);
  }

  public void broadcastChatMessageAs(int asPlayer, String s) {
    ChatMessage cm = new ChatMessage(-1,asPlayer,s);
    cm.setSource(asPlayer);
    broadcast(cm);
  }

  private void handleNewConnections() {
    Socket s;

    if(!socketQueue.isEmpty()) {
      s=(Socket) socketQueue.remove();

      PlayerConnect pConnect = new PlayerConnect(s, this);
      pConnect.start();
    }

    if(!joiningQueue.isEmpty()) {
	PlayerConnect pConnect =(PlayerConnect) joiningQueue.remove();

	if(gameRunning) {
	    int newPlayerNumber = 
		playersInfo.getNumberByNameAndPassword(pConnect.getPlayerName(),
						       pConnect.getPassword());
							  

	    if(newPlayerNumber <0 || playerActive(newPlayerNumber)) {
		pConnect.close();
	    } else {
		playerConnect[newPlayerNumber] = pConnect;
		playersInfo.setPlayerActive(newPlayerNumber, true);
		playersInfo.setMaxPlayer();

		playerConnect[newPlayerNumber].putMessage(new PlayerNumberMessage(newPlayerNumber));

		broadcastPlayerInfo();

		StartGameMessage sgm = new StartGameMessage();
		sgm.xSize=world.getXSize(); /***/
		sgm.ySize=world.getYSize();
		pConnect.putMessage(sgm);

		/* send pending messages */
		while(!pendingMessageQueue[newPlayerNumber].isEmpty()) {
		    Message m = (Message) 
			pendingMessageQueue[newPlayerNumber].remove();
		    if(m != null)
			pConnect.putMessage(m);
		}

		chatBroadcast(playersInfo.getPlayerName(newPlayerNumber) + 
			      " has rejoined game.");

		if(world!=null) {
		    ResendTurnMessage etm = new ResendTurnMessage();
		    etm.setSource(newPlayerNumber);
		    etm.setRound(world.getCurrentRound());
		    world.putMessage(etm);
		}

		pConnect.putMessage(new ModeLineTextMessage(newPlayerNumber,
							    "Game rejoined. Please wait for new round to start..."));
	    }
	} else {
	int newPlayerNumber = playersInfo.getNumberByName(pConnect.getPlayerName());
	if(newPlayerNumber == playersInfo.getNumberByNameAndPassword(pConnect.getPlayerName(), pConnect.getPassword())) {
	  if (newPlayerNumber < 0) {
	    newPlayerNumber = playersInfo.findNextFree();
          } else {
	    if(playerConnect[newPlayerNumber] != null) {
	      playerConnect[newPlayerNumber].quitConnection();
	      try {
		playerConnect[newPlayerNumber].socket.close();
	      } catch (Exception e) {}

	      hostWindow.putMessage("Old Connection to " + 
		  playersInfo.getPlayerName(newPlayerNumber) + " dropped");
	      playerConnect[newPlayerNumber]=null;

	    }
	  }
	} else {
	  // incorrect password for an existing player
	  newPlayerNumber = -2;
	}

	if(newPlayerNumber < 0) {
	  try {
	    pConnect.close();
	  } catch (Exception e) {}
	} else {
	  playerConnect[newPlayerNumber] = pConnect;
	  playersInfo.setPlayerActive(newPlayerNumber, true);
	  playersInfo.setNumberOfPlayers(playersInfo.getNumberOfPlayers()+1);
	  playersInfo.setMaxPlayer();
	  
	  if(GlobalDefines.isNameValid(pConnect.getPlayerName()))
	    playersInfo.setPlayerName(newPlayerNumber,
				      pConnect.getPlayerName(),
				      pConnect.getPassword());
	  else
	    playersInfo.setPlayerName(newPlayerNumber,
				      "Unknown-"+newPlayerNumber,
				      pConnect.getPassword());
	  
	  playerConnect[newPlayerNumber].putMessage(new PlayerNumberMessage(newPlayerNumber));
	  broadcastPlayerInfo();
	  chatBroadcast(playersInfo.getPlayerName(newPlayerNumber) + " has joined game.");
	}
      }
    }
  }

  private void handleChatter() {
    if(chatter.isMoreMessages()) {
      ChatMessage cm = chatter.getMessage();
      if(playerConnect[cm.whoTo()] != null) {
	playerConnect[cm.whoTo()].putMessage(cm);
      } else {
	  if(playersInfo.isPlayerInGame(cm.whoTo())) {
	      pendingMessageQueue[cm.whoTo()].add(cm);
	  }
      }
    }
  }

  private void handleWorld() {
    if(world!=null && world.isMoreMessages()) {
      HostWorldMessage m;
      m=world.getMessage();
      if(playerConnect[m.whoTo()]!=null)
	playerConnect[m.whoTo()].putMessage(m);
    }
  }

  private void handlePlayerMessages() {
    for(int i=0; i < playersInfo.getMaxPlayer(); i++) {
      if(playerConnect[i] == null) continue;
      if(playerConnect[i].isMoreMessages()) {
	Message m = playerConnect[i].getMessage();
	m.setSource(i); // Override source to make sure it's correct

	if(m instanceof ControlMessage) {
	  if(m instanceof PlayerQuitMessage) {
	    playerConnect[i].quitConnection();
	    try {
	      playerConnect[i].socket.close();
	    } catch (Exception e) {}

	    hostWindow.putMessage("Connection to " + 
				  playersInfo.getPlayerName(i) + " lost");
	    chatBroadcast(playersInfo.getPlayerName(i) + " has left game.");
	    playerConnect[i]=null;

	    playersInfo.setPlayerActive(i, false);
	    playersInfo.setNumberOfPlayers(playersInfo.getNumberOfPlayers()-1);
	    playersInfo.setMaxPlayer();
	    broadcastPlayerInfo();

	    if(world!=null) {
	      EndTurnMessage etm = new EndTurnMessage();
	      etm.setSource(i);
	      etm.setRound(world.getCurrentRound());
	      world.putMessage(etm);
	    }
	  } 

	}

	if(m instanceof ChatClientMessage) {
	  chatter.putMessage((ChatClientMessage) m);
	}

	if(m instanceof WorldMessage) {
	  if (world!=null) {
	    world.putMessage((WorldMessage) m);
	  }
	}
      }
    }
  }

  private void mainLoop() {
    while(true) {
      handleNewConnections();
      handleChatter();
      handleWorld();
      handlePlayerMessages();

      try {
	sleep(2);
      } catch (Exception e) {}
    }
  }

  public HostController (String name,
			 WorldCreator worldCreator, 
			 boolean awtWindow,
			 String saveGameFile) {
    this.worldCreator=worldCreator;
    worldCreator.getWorldOptionsUI().getOptions().setGameName(name);

    playerConnect = new PlayerConnect[GlobalDefines.maxPlayers];
    pendingMessageQueue = new Queue[GlobalDefines.maxPlayers];

    for(int i=0; i < GlobalDefines.maxPlayers; i++) {
	pendingMessageQueue[i] = new Queue();
    }

    boolean newGame;
    if(saveGameFile == null)
	newGame = true;
    else
	newGame = false;

    if(awtWindow)
      hostWindow = 
	  new HostWindow(this, 
		     worldCreator.getWorldProperties().getName("HostWindow"),
			 newGame);
    else
      hostWindow = 
	  new HostTextScreen(this, 
		     worldCreator.getWorldProperties().getName("HostWindow"),
			     newGame);

    if(newGame)
	playersInfo = new PlayersInfo();
    else {
	world = HostWorld.loadStatus(saveGameFile,
				     this,
				     hostWindow);
	playersInfo = world.getSavedPlayersInfo();
    }

    InetAddress [] addresses;

    for (int i = 0; i < GlobalDefines.maxPlayers; i++) {
      playerConnect[i] = null;
    }

    chatter = new Chatter();
    socketQueue = new Queue();
    joiningQueue = new Queue();

    try {
      serverSocket = new ServerSocket(9090);
    } catch (Exception e) {
      try {
	serverSocket = new ServerSocket(0);
      } catch (Exception e2) {
	System.err.println("Can't reserve TCP socket"); System.exit(1);
      }
    }

    try {
      addresses = InetAddress.getAllByName(
	serverSocket.getInetAddress().getLocalHost().getHostAddress());
      for(int i=0; i< addresses.length ; i++)
	hostWindow.putMessage("Host IP: " + addresses[i]);
    } catch (Exception e) {
      System.err.println("Can't resolve local address"); 
      System.exit(1);
    }
    hostWindow.putMessage("Host Port: " + serverSocket.getLocalPort());
    hostWindow.putMessage("\n\n");

  }

  public void run(){
    ControllerSocket s=
	new ControllerSocket(serverSocket,socketQueue,hostWindow);
    s.start();

    if(hostWindow instanceof HostWindow) {
      ((HostWindow)hostWindow).show();
      WorldOptionsUI worldOptionsUI = worldCreator.getWorldOptionsUI();
      if(worldOptionsUI!=null && world == null) {
	worldOptionsWindow = new WorldOptionsWindow(this, worldOptionsUI);
	worldOptionsWindow.show();
      }
    } else
      ((HostTextScreen)hostWindow).start();

    if(world != null)
	startGame();
    
    mainLoop();
  }

  void startGame() {
    if(worldOptionsWindow!=null) {
      worldOptionsWindow.setVisible(false);
      worldCreator.getWorldOptionsUI().updateOptions();
      worldOptionsWindow=null;
    }

    if(world == null) {
	world=worldCreator.getHostWorld(this,hostWindow);
	world.updatePlayersInfo(playersInfo);
	world.setScoring(worldCreator.getScoreboard(world));
    }

    world.updatePlayersInfo(playersInfo);

    /* Mark which players are members of the game */
    for(int i=0; i<GlobalDefines.maxPlayers; i++) {
	if(playersInfo.isPlayerActive(i)) {
	    playersInfo.setPlayerInGame(i, true);
	}
    }

    new Thread(world).start();

    gameRunning=true;
    StartGameMessage sgm = new StartGameMessage();
    sgm.xSize=world.getXSize();
    sgm.ySize=world.getYSize();
    broadcast(sgm);
    hostWindow.putMessage("\n***************************\n"+
                          "Game Started\n" +
                          "***************************\n");
    hostWindow.putMessage("Selected game options:\n");
    hostWindow.putMessage(world.getWorldOptions().toString());
    chatBroadcast("Selected game options:\n" +
		  world.getWorldOptions().toString());

  }

  public int getMaxPlayer() {
    return playersInfo.getMaxPlayer();
  }

  public boolean playerActive(int n) {
    return (playerConnect[n]!=null);
  }

  public boolean playerInGame(int n) {
    return playersInfo.isPlayerInGame(n);
  }

  public Enumeration getAllPlayers() {
    
    Vector v = new Vector();

    for(int n=0; n<getMaxPlayer(); n++) {
      if(playerActive(n)) {
	v.addElement(new Integer(n));
      }
    }

    return (v.elements());
  }

  public int getNumberOfPlayers() {
    int tot=0;

    for(int n=0; n<getMaxPlayer(); n++) {
      if(playerActive(n)) {
	tot++;
      }
    }

    return tot;
  }

  public String getPlayerName(int n) {
    if(n>=GlobalDefines.maxPlayers)
      return "*Illegal player number*";

    if(n==-1)
      return "Host";
    else
      return playersInfo.getPlayerName(n);    
  }

  public HostWorld getWorld() {
    return world;
  }

  public void resetObjectStreams() {
    for(int i=0; i < playersInfo.getMaxPlayer(); i++) {
      if(playerConnect[i] == null) continue;

      playerConnect[i].resetObjectStreams();
    }
  }

  public void putJoiningPlayer(PlayerConnect pc) {
    joiningQueue.add(pc);
  }

  private void broadcastPlayerInfo() {
      PlayersInfo piClone = (PlayersInfo) playersInfo.clone();
      broadcast(new PlayersInGameMessage(piClone));
  }

    void sendTimeout(TimeoutMessage m) {
	broadcast(m);
    }
}
