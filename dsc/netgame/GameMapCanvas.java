/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;

import java.awt.*;
import java.util.*;
import java.awt.image.*;
import dsc.awt.*;


class GameMapCanvas extends Canvas {

  private GameWindow gameWindow;
  private Vector foregroundComponents;
  private Vector backgroundComponents;

  // Custom mutex to avoid conflicts with AWT synchronized methods
  private Integer mutex = new Integer(0);

  GameMapCanvas(GameWindow w, Color bc) {
    setBackground(bc);
    gameWindow=w;
    foregroundComponents=new Vector();
    backgroundComponents=new Vector();
  }

  public void setComponents(Enumeration e) {
      synchronized (mutex) {
	  foregroundComponents=new Vector();
	  backgroundComponents=new Vector();
	  WorldComponent wc;

	  while(e.hasMoreElements()) {
	      wc=(WorldComponent) e.nextElement();
	      if(wc.paintToForeground())
		  foregroundComponents.addElement(wc);
	      else
		  backgroundComponents.addElement(wc);
	  }
      }
  }

  private Image rotateImage(Image i, 
			    int bearing, 
			    int c1, int n1, int c2, int n2) {
    int height, width;
    PixelGrabber grabber = new PixelGrabber(i, 0, 0, -1, -1, true);
    double cost = Math.cos(((double) bearing)/180.0*Math.PI);
    double sint = Math.sin(((double) bearing)/180.0*Math.PI);
    double xc, yc;
    Image t = null;
    MediaTracker mt = new MediaTracker(this);
    
    try {
      if(grabber.grabPixels()) {
	width = grabber.getWidth();
	height = grabber.getHeight();
	xc = ((double) width)/2.0;
	yc = ((double) height)/2.0;
	int [] pixels = (int []) grabber.getPixels();
	int [] newPixels = new int[height*width];
	
	for(int x=0; x<width; x++) 
          for(int y=0; y<height; y++) {
	    int nx, ny;
	    nx=(int) (((double) x-xc)*cost + ((double) y-yc)*sint + 0.5 + xc);
	    ny=(int) (-((double) x-xc)*sint + ((double) y-yc)*cost + 0.5 + yc);
	    if(nx < 0 || nx >= width || ny < 0 || ny >= height)
	      newPixels[y*height+x] = 0x00000000;
	    else
	      newPixels[y*height+x] = pixels[ny*height+nx];
	  }
	t = createImage(new MemoryImageSource(height, 
						 width,
						 newPixels,
						 0,
						 height));
      }
    } catch(InterruptedException e) {}

    if(t==null)
      return i;

    mt.addImage(t,1);
    
    try {
      mt.waitForAll();
      if(mt.isErrorAny()) {
	System.out.println("Error Creating Image in GameMapCanvas.");
	System.exit(1);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    return t;
  }

  private void paintEnumeration(Enumeration e,ScaledGraphics g) {
    WorldComponent wc;
    Image i,t;
    
    while(e.hasMoreElements()) {
      wc=(WorldComponent) e.nextElement();
      i=gameWindow.getImage(wc.getMapImage());
      int bearing;

      if(wc.isVisible() && i!=null ) {
	bearing = wc.getBearing();
	if(bearing == 0)
	  t=i;
	else {
	  t = rotateImage(i, bearing ,0, 0, 0, 0);
	}
	
	int dxsize = i.getWidth(this)/2;
	int dysize = i.getHeight(this)/2;

	g.drawImage(t,wc.getLocation().getX()-dxsize, 
		    wc.getLocation().getY()-dysize, this);
	wc.drawAdditionalGraphs(g);
      }
    }

    wc = gameWindow.getActiveComponent();
    if(wc!=null) {
      i = gameWindow.getImage(wc.getMapImage());
      int dxsize = i.getWidth(this)/2;
      int dysize = i.getHeight(this)/2;

      g.setColor(Color.red);
      g.drawRect(wc.getLocation().getX()-dxsize-5,
		 wc.getLocation().getY()-dysize-5,
		 i.getWidth(this)+10,
		 i.getHeight(this)+10);
    }

  }

  private void paintPreGraphicsEnumeration(Enumeration e,ScaledGraphics g) {
    WorldComponent wc;
    
    while(e.hasMoreElements()) {
      wc=(WorldComponent) e.nextElement();

      if(wc.isVisible()) {
	wc.drawAdditionalPreGraphs(g);
      }
    }
  }


  private void brightenCircle(Color c, ScaledGraphics g, WorldComponent wc) {
    int playerNumber = gameWindow.getPlayerNumber();

    g.setColor(c);

    int sr = wc.getScanRange();
    if(wc.getOwner()==playerNumber && wc.getScanRange()>0) {
      g.fillOval(wc.getLocation().getX()-sr,
		 wc.getLocation().getY()-sr,
		 sr*2,
		 sr*2);
    }
  }
  

  private void brightenScanRange(Color c, ScaledGraphics g, Enumeration e) {
    WorldComponent wc,active;

    active=gameWindow.getActiveComponent();

    while(e.hasMoreElements()) {
      wc = (WorldComponent) e.nextElement();

      if(active == wc)
	continue;
	
      brightenCircle(c, g, wc);
    }
  }

  private void brightenScanRange(Color c, ScaledGraphics g) {
    WorldComponent active = gameWindow.getActiveComponent();
    
    Enumeration e=backgroundComponents.elements();
    brightenScanRange(c,g,e);
    e=foregroundComponents.elements();
    brightenScanRange(c,g,e);

    if(active != null)
      brightenCircle(c.brighter(), g, active);
  }

    public void update(Graphics g) {
	paint(g);
    }

    public void paint(Graphics g) {
        synchronized (mutex) { 
	    Graphics offgc;
	    Image offscreen = null;
	    Dimension d = size();
      
	    offscreen = createImage(d.width, d.height);
	    offgc = offscreen.getGraphics();
	
	    /*
	      offgc.setColor(getBackground());
	      offgc.fillRect(0, 0, d.width, d.height);
	      offgc.setColor(getForeground());
	    */      

	    ScaledGraphics sg = new ScaledGraphics(offgc, 
						   gameWindow.getScaleFactor());

	    brightenScanRange(new Color(20,20,35),sg);
	    gameWindow.drawBackground(sg);

	    Enumeration e=backgroundComponents.elements();
	    paintPreGraphicsEnumeration(e,sg);
    
	    e=backgroundComponents.elements();
	    paintEnumeration(e,sg);

	    e=foregroundComponents.elements();
	    paintPreGraphicsEnumeration(e,sg);

	    e=foregroundComponents.elements();
	    paintEnumeration(e,sg);

	    g.drawImage(offscreen, 0, 0, this);
	}
    }
}
