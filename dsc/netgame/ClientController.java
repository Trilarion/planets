/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;

import dsc.awt.*;
import dsc.util.Queue;
import java.io.*;
import java.net.*;
import java.awt.*;
import java.util.*;


/**
 * ClientController is the heart of the clientside software. Like the
 * name suggests it controls the whole client. It is also used to start
 * the client in the first place. It froms the connection to the host,
 * opens all relevant windows and handles message traffic between 
 * client and host.
 *
 * @version $Id$
 * @author $Author$
 */

public class ClientController extends Thread {

  /** The socket used to communicate with the host */
  private Socket socket;

  /** The input stream for messages coming from the host */
  private ObjectInputStream inputStream;

  /** The output stream for messages going to host */
  private ObjectOutputStream outputStream;

  /** The clientside chatter object. (Actually also the chat window) */
  private ClientChatter chatter;

  /** Queue for messages going to host */
  private Queue outputQueue;

  /** Thread handling traffic coming from the host. */
  private HostOutput hostOutput;

  /** The game initialization controller. */
  private WorldCreator worldCreator;

  /** Clienside world. Keeps all information about the state of the game. */
  private World world;

  /** The main game window (the one with a map) */
  private GameWindow gameWindow;

  
  /** Table of currently present players */
  public  volatile PlayersInfo playersInfo;

  /** Our player identification number */
  public  int playerNumber=-1;

  /** IP-address or domain name of the host */
  private String serverAddress;

  /** TCP port at the host computer that the host has been attached to */
  private int serverPort;


  /** Computer player for this client. Null is human player. */
  private AI artificialIntelligence = null;

    /** ScreenLayout for this player, may be saved */
    private ScreenLayout screenLayout = new ScreenLayout();

  /** Creates new client for the game. Connects to given host.
   *
   * @param serverAddress IP-address or domain name of the host computer.
   * @param serverPort Port on the server that the host is attached to.
   * @param playerName The name our player wants to have.
   * @param worldCreator Game initialization object.
   */

  public ClientController(String serverAddress, 
		   int serverPort, 
		   String playerName,
		   String password,
		   WorldCreator worldCreator) {
    setDaemon(false);
    loadScreenLayout();

    this.worldCreator=worldCreator;

    this.serverAddress=serverAddress;
    this.serverPort=serverPort;

    outputQueue = new Queue();
    playersInfo = new PlayersInfo();

    ClientHello ch=null;

    ch = new ClientHello(playerName, password);

    try {
      socket=new Socket(this.serverAddress, this.serverPort);
      inputStream=new ObjectInputStream(socket.getInputStream());
      outputStream=new ObjectOutputStream(socket.getOutputStream());

      HostHello hh = (HostHello) inputStream.readObject();
      outputStream.writeObject(ch);
    } catch (Exception e) {
      System.err.println("Unable to connect host (" + e + ")");
      System.exit(1);
    }
  }

  /**
   * This method is called when the connection to host is lost. This
   * typically happens only when the host is shut down or there is 
   * a fatal bug in the connection code. Can also happen if host and
   * client versions are not compatible.
   */

  public synchronized void hostConnectLost() {
    System.err.println("Host connection lost");
    System.exit(1);
  }

  public void run() {
    chatter = new ClientChatter(this, screenLayout);
    chatter.show();

    hostOutput = new HostOutput(inputStream,chatter,this);
    hostOutput.start();

    try {
      while(true) {
	if(!outputQueue.isEmpty()) {
	  Message m = (Message) outputQueue.remove();
	  outputStream.writeObject(m);
	  if(m instanceof EndTurnMessage)
	    outputStream.reset();
	}
	try {
	  sleep(200);
	} catch (Exception e) {}
      }
    } catch(Exception e) {
      System.err.println("(" + e + ")");
      hostConnectLost();
    }
  }

  /**
   * Sends a message to host. If this is worldmessage, the round information
   * must already be set in the message!
   *
   * @param m The message to send to host.
   */

  public void sendToHost(Message m) {
    outputQueue.add(m);
  }


  /**
   * This method is called when host sends StartGameMessage. Starts
   * the game at the client side by opening the game map window.
   *
   * @param m The game starting message received from the host.
   */

  void startGame(StartGameMessage m) {
    gameWindow = new GameWindow(
       m,worldCreator.getWorldProperties().getName("GameWindow") +
       " --- " + playersInfo.getPlayerName(playerNumber),
       screenLayout);
    world = worldCreator.getClientWorld(gameWindow, this);
    gameWindow.setWorld(world);
    gameWindow.show();
  }

  /**
   * Transfers given message to clientside world object.
   *
   * @param m The message to deliver.
   */

  void putWorldMessage(HostWorldMessage m) {
    world.putControlMessage(m);
  }

  /**
   * Gets the properties information of current game.
   * 
   * @return Properties information.
   */

  public WorldProperties getWorldProperties() {
    return worldCreator.getWorldProperties();
  }

  /**
   * Called when player quits the client. Flushes the rejoin information
   * and exists.
   */

  public void playerQuits() {
    System.exit(1);
  }

  /**
   * Gets the main game window object.
   *
   * @return Main game window.
   */

  public GameWindow getGameWindow() {
    return gameWindow;
  }

    /**
     * Sets AI player for this client. Default AI is null (human player).
     *
     */

    public void setAI(dsc.netgame.AI ai) {
	this.artificialIntelligence = ai;
    }

    /**
     * Gets AI player of this client.
     *
     * @return AI instance for this client. Null for human player's client.
     */

    public AI getAI() {
	return artificialIntelligence;
    }


    private String getConfFilename() {
	String filename;

	filename = java.lang.System.getProperty("user.home");
	filename = filename + File.separator + ".phopper.settings";

	return filename;
    }

    public void saveScreenLayout() {
	gameWindow.updateWindowInfo(screenLayout);
	chatter.updateWindowInfo(screenLayout);

	try {
	    OutputStream os = new FileOutputStream(getConfFilename());
	    ObjectOutputStream oos = new ObjectOutputStream(os);

	    oos.writeObject(screenLayout);

	    oos.flush();
	    os.close();
	} catch(Exception e) {
	    System.err.println("Warning: Saving settings failed," +
			       " got exception " + e);
	}

    }

    private void loadScreenLayout() {
	try {
	    InputStream is = new FileInputStream(getConfFilename());
	    ObjectInputStream ois = new ObjectInputStream(is);

	    screenLayout = (ScreenLayout) ois.readObject();
	    
	    is.close();
	} catch(Exception e) {
	    System.err.println("Warning: Loading settings failed," +
			       " got exception " + e + 
			       " (this is probably ok if you run " +
			       "the software for the first time)");
	}
    }

    void timeout(TimeoutMessage tm) {
	gameWindow.incrementClock(tm.getIncrementSeconds());
    }
}
