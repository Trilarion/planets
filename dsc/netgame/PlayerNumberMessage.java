/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;


/** Informs player about it's player ID. Message is sent by
 * hostside controller when player is accepted in the game.
 *
 * @author Dodekaedron Software Creations, Inc. -- Wraith
 */

public class PlayerNumberMessage extends ControlMessage {

  /** Player number assigned to player getting this message */
  private int playerNumber;

  /** 
   * @param n Player number for receiver of this message
   */
  public PlayerNumberMessage(int n) {
    playerNumber=n;
  }

  /** Gets player number of the receiver of this message.
   *
   * @return Player ID for this player
   */
  
  public int getMyPlayerNumber() {
    return playerNumber;
  }
}
