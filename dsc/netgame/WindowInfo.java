/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;

import java.io.*;
/** Stores information on one client window size and location on screen.
 *
 * @author Dodekaedron Software Creations, Inc. -- Wraith
 */

public class WindowInfo implements Serializable {
    private int xSize, ySize;
    private int xCoord, yCoord;
    private Object otherInfo;

    public WindowInfo(int xSize, int ySize,
		      int xCoord, int yCoord) {
	this.xSize = xSize;
	this.ySize = ySize;
	this.xCoord = xCoord;
	this.yCoord = yCoord;
	
	this.otherInfo = null;
    }

    public void setInfo(Object o) {
	this.otherInfo = o;
    }

    public Object getInfo() {
	return this.otherInfo;
    }

    public int getXSize() {
	return this.xSize;
    }

    public int getYSize() {
	return this.ySize;
    }

    public int getXCoord() {
	return this.xCoord;
    }

    public int getYCoord() {
	return this.yCoord;
    }

    public void setSize(int xs, int ys) {
	this.xSize = xs;
	this.ySize = ys;
    }

    public void setCoords(int xc, int yc) {
	this.xCoord = xc;
	this.yCoord = yc;
    }
}
