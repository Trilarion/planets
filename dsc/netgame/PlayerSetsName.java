/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;


/** Player selects name by sending this message to host. Host must
 * check whether the name is already in use and inform all players
 * about this players new name. This message should be one of the first
 * messages from client's controller to the host.
 */

public class PlayerSetsName extends ControlMessage {
  private String playerName;

  /** Create message which sets player's name.
   *
   * @param plaNam Name wanted.
   */

  public PlayerSetsName(String plaNam) {
    playerName=plaNam;
  }

  /** Gets sender of this message.
   *
   * @return Player ID of the player who wants to set name.
   */

  public int getPlayerNumber() {
    return(getSource());
  }

  /** Gets requested name.
   *
   * @return Requested name.
   */

  public String getPlayerName() {
    return playerName;
  }
}
