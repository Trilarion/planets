/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;


/** Message from host telling client what text it should now put 
 * on the modeline. 
 *
 * @author Dodekaedron Software Creations, Inc. -- Wraith
 */

public class ModeLineTextMessage extends HostWorldMessage {

  private String modeLineText;

  /** Create message with text to put on the modeline. 
   * Remember to set round information.
   *
   * @param dest Player ID of the receiver.
   * @param text Modeline text for the client.
   * @see HostWorldMessage#setRound
   */

  public ModeLineTextMessage(int dest, String text) {
    super(dest);
    
    modeLineText=text;
  }

  /** Gets the text that should be put on the modeline.
   *
   * @return Modeline text.
   */

  public String getText() {
    return modeLineText;
  }
}
