/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;

/** WorldCreator links dsc.netgame to current world. WorldCreator
 * is used to create both client and host worlds, properties and
 * options.
 *
 * @author Dodekaedron Software Creations, Inc. -- Wraith
 */

public interface WorldCreator {

  /** Returns clientside world in the current gamesystem.
   *
   * @param gw Playing area.
   * @param c Clientside controller.
   * @return Clientside world.
   */

  public World getClientWorld(GameWindow gw, ClientController c);

  /** Returns Hostside world in the current gamesystem.
   *
   * @param hc Hostside controller.
   * @param hw Hostside information window/stream.
   * @return Hostside world
   */

  public HostWorld getHostWorld(HostController hc, HostScreen hw);

  /** Return options panel for setting options for the world.
   *
   * @return Options with reasonable defaults.
   */
  
  public WorldOptionsUI getWorldOptionsUI();

  /** Return properties for game windows. Properties map classnames to
   * window titles and icons.
   *
   * @return Window properties for current gamesystem.
   */

  public WorldProperties getWorldProperties();

    /** Return scoring system for given world.
     *
     * @param world Pre-initialized game world.
     * @return Scoreboard Scoring system for world.
     */

  public Scoreboard getScoreboard(HostWorld world);
}
