/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;

import java.io.*;
import java.util.*;


public class PlayersInfo implements Serializable, Cloneable {
  private BitSet playerActive;
  private BitSet playerInGame;
  private int numberOfPlayers;
  private int maxPlayer;
  private String [] playerName;
  private String [] password;

  public PlayersInfo() {
    playerActive = new BitSet(GlobalDefines.maxPlayers);
    playerInGame = new BitSet(GlobalDefines.maxPlayers);
    numberOfPlayers=0;
    maxPlayer=0;
    playerName = new String [GlobalDefines.maxPlayers]; 
    password   = new String [GlobalDefines.maxPlayers];
  }

  private PlayersInfo(BitSet b, BitSet p, String [] s) {
    playerActive = b;
    playerInGame = p;
    playerName = s;
    password = null;
  }

  public boolean isPlayerActive(int n) {
    return playerActive.get(n);
  }

  public void setPlayerActive(int n, boolean b) {
    if(b) 
      playerActive.set(n);
    else
      playerActive.clear(n);
  }

  public boolean isPlayerInGame(int n) {
    return (playerInGame.get(n) || playerActive.get(n));
  }

  public void setPlayerInGame(int n, boolean b) {
    if(b) 
      playerInGame.set(n);
    else
      playerInGame.clear(n);
  }

  public int getNumberOfPlayers() {
    return numberOfPlayers;
  }

  public void setNumberOfPlayers(int n) {
    numberOfPlayers=n;
  }

  public String getPlayerName(int n) {
    if(n<0)
      return "*Unknown*";

    if(n==(GlobalDefines.maxPlayers-1)) 
	return "*Thargon Empire*";

    String s=playerName[n];

    if(s==null)
      return ("UNSET-PLAYER-" + n);
    else
      return s;
  }

  public void setPlayerName(int n, String name, String password) {
    playerName[n]=name;
    this.password[n]=password;
  }

  public void setPlayerName(int n, String name) {
    playerName[n]=name;
  }


  public int getMaxPlayer() {
    return maxPlayer;
  }

  void setMaxPlayer() {
    maxPlayer=0;

    for(int i=0; i< GlobalDefines.maxPlayers; i++) {
      if(playerActive.get(i)) {
	maxPlayer=i+1;
      }
    }
  }

  int findNextFree() {
    if(numberOfPlayers == GlobalDefines.maxPlayers) {
      return -1;
    }

    for (int i = 0; i < GlobalDefines.maxPlayers; i++){
      if(!playerActive.get(i)) {
	return i;
      }
    }
    return -1;
  }

    public int getNumberByName(String name) {
	if(name.equals("*Thargon Empire*"))
	    return GlobalDefines.maxPlayers-1;

	for(int i=0; i<GlobalDefines.maxPlayers; i++) {
	    if(isPlayerInGame(i) && name.equals(playerName[i]))
		return i;
	}

	return -2;
    }

  public int getNumberByNameAndPassword(String name, String password) {
      for(int i=0; i<GlobalDefines.maxPlayers; i++) {
	if(playerName[i] != null &&
	   name.equals(playerName[i]) && 
	   password.equals(this.password[i]))
	    return i;
    }

    return -2;
  }


  public Object clone() {
    PlayersInfo pi = new PlayersInfo((BitSet)playerActive.clone(),
				     (BitSet)playerInGame.clone(),
				     (String [])playerName.clone());
    pi.numberOfPlayers=numberOfPlayers;
    pi.maxPlayer = maxPlayer;
    return pi;
  }
    
}
