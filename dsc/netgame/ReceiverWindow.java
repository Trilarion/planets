/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;

import java.awt.*;
import java.awt.event.*;
import dsc.awt.*;
import java.util.*;


public class ReceiverWindow extends LayoutableFrame implements ActionListener, ItemListener {

  private Button closeButton, helpButton;
  private GridBagLayout gb;
  private CheckboxGroup namesGroup, areaGroup;
  private Checkbox oneBox[];
  private Checkbox nameBox1[];
  private Checkbox nameBox2[];
  private Checkbox toOneBox;
  private Label    nameLabels[];
  private int maxName;
  private ClientController controller;
  private ScrollPane scrollPane;
  private Panel namesPanel, topPanel;
  private GridBagConstraints gbc;
  private GridBagLayout gbl;

  private Panel createNamesPanel() {
    maxName=-1;
    namesPanel = new Panel();
    gbc = new GridBagConstraints();
    namesPanel.setLayout(gbl=new GridBagLayout());

    gbc.weighty=0;
    gbc.weightx=100;
    gbc.anchor=GridBagConstraints.NORTH;

    nameBox1 = new Checkbox [GlobalDefines.maxPlayers];
    nameBox2 = new Checkbox [GlobalDefines.maxPlayers];
    oneBox   = new Checkbox [GlobalDefines.maxPlayers];
    nameLabels = new Label [GlobalDefines.maxPlayers];
    namesGroup  = new CheckboxGroup();

    reconstructNamesPanel();
    return namesPanel;
  }

  public void addToPanel(Component c, GridBagConstraints gbc, int x, int y, int w, int h) {
    gbc.gridx=x;
    gbc.gridy=y;
    gbc.gridwidth=w;
    gbc.gridheight=h;
    namesPanel.add(c,gbc);
  }

  public void addToPanel(Panel p,Component c, GridBagConstraints gbc, int x, int y, int w, int h) {
    gbc.gridx=x;
    gbc.gridy=y;
    gbc.gridwidth=w;
    gbc.gridheight=h;
    p.add(c,gbc);
  }


  private void allocateResources() {
    while(maxName < GlobalDefines.maxPlayers-1) {
      maxName++;
      oneBox[maxName]=new Checkbox("",namesGroup,false);
      oneBox[maxName].addItemListener(this);
      nameBox1[maxName]=new Checkbox("",false);
      nameBox2[maxName]=new Checkbox("",false);
      nameLabels[maxName]=new Label("");

      gbc.anchor=GridBagConstraints.WEST;
      addToPanel(oneBox[maxName],gbc,10,maxName,1,1);
      addToPanel(nameBox1[maxName],gbc,16,maxName,1,1);
      addToPanel(nameBox2[maxName],gbc,22,maxName,1,1);
      addToPanel(nameLabels[maxName],gbc,0,maxName,10,1);

      oneBox[maxName].setVisible(false);
      nameBox1[maxName].setVisible(false);
      nameBox2[maxName].setVisible(false);
      nameLabels[maxName].setVisible(false);
    }
  }

  private void reconstructNamesPanel() {
    allocateResources();

    for(int i=0;i <= maxName; i++) {
      if(controller.playerNumber==i)
	continue;

      if(controller.playersInfo.isPlayerInGame(i) 
	 &&  !nameLabels[i].isVisible()) {
	  if(controller.playersInfo.isPlayerActive(i))
	      nameLabels[i].setText("[" + i + "] " + controller.playersInfo.getPlayerName(i));
	  else
	      nameLabels[i].setText("(" + i + ") " + controller.playersInfo.getPlayerName(i));
	nameLabels[i].setForeground(GlobalDefines.getPrimaryColor(i));
	oneBox[i].setVisible(true);
	nameBox1[i].setVisible(true);
	nameBox2[i].setVisible(true);
	nameLabels[i].setVisible(true);
	continue;
      }

      if(!controller.playersInfo.isPlayerInGame(i) &&  nameLabels[i].isVisible()) {
	if(oneBox[i].getState())
	  namesGroup.setSelectedCheckbox(oneBox[controller.playerNumber]);
	oneBox[i].setVisible(false);
	nameBox1[i].setVisible(false);
	nameBox2[i].setVisible(false);
	nameLabels[i].setVisible(false);
	continue;
      }
      
      if(controller.playersInfo.isPlayerActive(i))
	nameLabels[i].setText("[" + i + "] " + controller.playersInfo.getPlayerName(i));
      else
	  nameLabels[i].setText("(" + i + ") " + controller.playersInfo.getPlayerName(i));
    } 
    
    gbl.layoutContainer(namesPanel);
    pack();
    repaint();
  }

  private Panel createTopPanel() {
    Panel p = new Panel();
   
    GridBagConstraints gbc = new GridBagConstraints();
    p.setLayout(new GridBagLayout());

    gbc.fill=GridBagConstraints.BOTH;
    //gbc.anchor=GridBagConstraints.BOTH;
    gbc.weightx=100;
    gbc.weighty=0;

    areaGroup = new CheckboxGroup();
    addToPanel(p,new Checkbox("All",areaGroup,true),gbc,0,0,7,1);
    addToPanel(p,(toOneBox = new Checkbox("One",areaGroup,false)),gbc,7,0,6,1);
    addToPanel(p,new Checkbox("GRP1",areaGroup,false),gbc,13,0,6,1);
    addToPanel(p,new Checkbox("GRP2",areaGroup,false),gbc,19,0,6,1);

    return p;
  }

  public void playerNotify() {
    reconstructNamesPanel();
  }

  public BitSet getReceiver() {
    BitSet bs = new BitSet(GlobalDefines.maxPlayers);
        
    String s = areaGroup.getSelectedCheckbox().getLabel();
    
    if(s.equals("All")) {
      for (int i = 0; i < GlobalDefines.maxPlayers; i++){
	if(controller.playersInfo.isPlayerInGame(i))
	  bs.set(i);
      }
    } else if(s.equals("One")) {
      Checkbox cb = namesGroup.getSelectedCheckbox();
      for (int i = 0; i < GlobalDefines.maxPlayers; i++){
	if(cb == oneBox[i]) {
	  bs.set(i);
	}
      }
    } else if(s.equals("GRP1")) {
      for (int i = 0; i < GlobalDefines.maxPlayers; i++){
	if(nameBox1[i]!=null && nameBox1[i].getState()) {
	  bs.set(i);
	}
      }
    } else if(s.equals("GRP2")) {
      for (int i = 0; i < GlobalDefines.maxPlayers; i++){
	if(nameBox2[i]!=null && nameBox2[i].getState()) {
	  bs.set(i);
	}
      }
    }

    if(controller.playerNumber!=-1)
      bs.set(controller.playerNumber);
    return bs;
  }

  public ReceiverWindow(ClientController controller,
			ScreenLayout screenLayout) {
    this.controller=controller;
    setTitle(controller.getWorldProperties().getName("ReceiverWindow"));

    WindowInfo windowInfo = screenLayout.getWindowInfo("ClientChatter");

    if(windowInfo == null) {
	setSize(5*40,4*40);
    } else {
	setSize(windowInfo.getXSize(), windowInfo.getYSize());
	setLocation(windowInfo.getXCoord(), windowInfo.getYCoord());
    }

    Image i=GraphicsLoader.getImage("data/graph/still/icons/receiver.gif");
    setIconImage(i);

    closeButton = new Button("Close");
    helpButton = new Button("Help");
    closeButton.addActionListener(this);
    helpButton.addActionListener(this);

    namesPanel=createNamesPanel();
    topPanel=createTopPanel();

    scrollPane = new ScrollPane();
    scrollPane.add(namesPanel);
    scrollPane.setSize(5*40,3*40);

    //scrollPane.setSize(8*40,);
    
    gb = new GridBagLayout();
    GridBagConstraints gbc = new GridBagConstraints();
    setLayout(gb);

    gbc.fill=GridBagConstraints.BOTH;
    //gbc.anchor=GridBagConstraints.BOTH;
    gbc.weightx=100;
    gbc.weighty=0;
    
    add(helpButton,gbc,0,8,2,1);
    add(closeButton,gbc,2,8,2,1);
    add(topPanel,gbc,0,0,4,1);

    gbc.weightx=100;
    gbc.weighty=100;
    add(scrollPane,gbc,0,1,4,7);
    pack();
  }

  public void actionPerformed(ActionEvent evt) {
    String arg = evt.getActionCommand();
    
    if(arg.equals("Close")) {
      setVisible(false);
    }
    else if (arg.equals("Help")) {
      HelpWindow.callForHelp("data/help", "Window", "Receiver");
    }
  }

  public void itemStateChanged(ItemEvent evt) {
    if(evt.getStateChange()==ItemEvent.DESELECTED) return;
 
    areaGroup.setSelectedCheckbox(toOneBox);
    repaint();
  }

  public ChatClientMessage getChatMessage(String messageText) {
    BitSet receiverId=getReceiver();
    String s = areaGroup.getSelectedCheckbox().getLabel();
    String preChar="";
    
    if(s.equals("All")) {
      preChar=":";
    } else if(s.equals("One")) {
      preChar=">";
    } else if(s.equals("GRP1")) {
      preChar="1#";
    } else if(s.equals("GRP2")) {
      preChar="2#";
    }

    int gtIndex=messageText.indexOf(">");
    if(gtIndex!=-1) {
      String num = messageText.substring(0, gtIndex);
      try {
	int id = Integer.parseInt(num);
	if((gtIndex+1) == messageText.length()) {
	  // private mode on
	} else {
	  receiverId = new BitSet(receiverId.size());
	  receiverId.set(id);
	  if(controller.playerNumber!=-1)
	    receiverId.set(controller.playerNumber);
	  messageText=messageText.substring(gtIndex+1, messageText.length());
	  preChar=">";	  
	}
      } catch(NumberFormatException e) {}
    }

    gtIndex=messageText.indexOf("#");
    if(gtIndex!=-1) {
      String num = messageText.substring(0, gtIndex);
      try {
	int id = Integer.parseInt(num);
	if((gtIndex+1) == messageText.length()) {
	  // group mode on
	} else {
	  receiverId = new BitSet(receiverId.size());
	  Checkbox [] nameBox=null;
	  if(id==1)
	    nameBox=nameBox1;
	  if(id==2)
	    nameBox=nameBox2;

	  if(nameBox!=null) {
	    for (int i = 0; i < GlobalDefines.maxPlayers; i++){
	      if(nameBox[i]!=null && nameBox[i].getState()) {
		receiverId.set(i);
	      }
	    }

	    if(controller.playerNumber!=-1)
	      receiverId.set(controller.playerNumber);
	    messageText=messageText.substring(gtIndex+1, messageText.length());
	    preChar=id+"#";	  
	  }
	}
      } catch(NumberFormatException e) {}
    }
    
    ChatClientMessage ccm = 
      new ChatClientMessage(receiverId,messageText, preChar);

    return ccm;
  }
}
