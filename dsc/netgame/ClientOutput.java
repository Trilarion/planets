/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;

import java.io.*;
import dsc.util.Queue;

/**
 * ClientOutput is a thread for queuing messages from client. One
 * object should be created per connected client. This class is needed
 * because of lack of proper I/O multiplexing in Java.
 *
 * @version $Id$
 * @author $Author$
 */

class ClientOutput extends Thread {

  /**
   * The stream this input queue reads.
   */

  private ObjectInputStream inputStream;

  /**
   * The queue for to put the read objects to.
   */
  private Queue inputQueue;

  /**
   * Creates new client output reader.
   *
   * @param inputStream The stream to read for client output.
   * @param inputQueue The queue to put the object read from inputStream.
   */
  
  ClientOutput(ObjectInputStream inputStream, Queue inputQueue) {
    this.inputStream=inputStream;
    this.inputQueue=inputQueue;
  }

  public void run() {
    try {
      while(true) {
	  Object o = inputStream.readObject();
	  inputQueue.add(o);
	  try {
	    sleep(10);
	  } catch (Exception e) {}
      }
    } catch (Exception e) {inputQueue.add(new PlayerQuitMessage());}
  }
}
