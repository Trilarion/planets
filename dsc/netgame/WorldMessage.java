/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;

/** Message from clientside world to hostside world. Contains round
 * when this message was created. All messages that are from wrong round
 * are discarded by the host.
 *
 * @author Dodekaedron Software Creations, Inc. -- Wraith
 */

public class WorldMessage extends Message {

  private int round;

  /** Gets round when this message was created.
   *
   * @return Creation round of this message.
   */

  public int getRound() {
    return round;
  }

  /** Sets creation round of this message. Round must be set before
   * sending message to prevent it beeing discarded by the host.
   */

  public void setRound(int r) {
    round=r;
  }
}
