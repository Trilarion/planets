/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;


/**
 * Message that sets alliance information for sending player.
 *
 * @author Dodekaedron Software Creations, Inc. -- Wraith
 */

public class SetAllianceMessage extends WorldMessage {

  private Alliance alliance;

  /**
   * Creates alliance setting message with given alliance information.
   *
   * @param source Sender of this message
   * @param a      New alliance information for the sender
   */

  public SetAllianceMessage(int source, Alliance a) {
    setSource(source);
    alliance = (Alliance) a.clone();
  }

  /**
   * Gets alliance information for sender of this message.
   *
   * @return New alliance infromation for this player.
   */

  public Alliance getAlliance() {

    if(alliance == null)
      return new Alliance(getSource());

    return alliance;
  }
}
