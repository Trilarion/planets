/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;

import java.io.*;
import java.net.*;
import dsc.util.Queue;


class PlayerConnect extends Thread {

  Socket socket;
  private ClientOutput co = null;
  private Queue outputQueue;
  private Queue inputQueue;
  private ObjectInputStream inputStream;
  private ObjectOutputStream outputStream;
  private volatile boolean onLine=true;
  private boolean shouldResetStream=false;
  private HostController hostController;

  private String  playerName;
  private String  password;

  PlayerConnect (Socket socket_, HostController hc) {
    hostController=hc;
    socket=socket_;
    inputQueue = new Queue();
    outputQueue = new Queue();

    try {
      outputStream=new ObjectOutputStream(socket.getOutputStream());
      inputStream=new ObjectInputStream(socket.getInputStream());
    } catch (Exception e) {inputQueue.add(new PlayerQuitMessage()); onLine=false;}
  }

  public void run() {
    if(onLine) {
      co = new ClientOutput(inputStream,inputQueue);
      co.start();
    }

    try {
      HostHello hh = new HostHello();
      outputStream.writeObject(hh);

      while(inputQueue.isEmpty()) {
	sleep(250);
      }

      ClientHello ch = (ClientHello) inputQueue.remove();
      playerName=ch.getName();
      password=ch.getPassword();

      hostController.putJoiningPlayer(this);

      while(onLine) {
	if(!outputQueue.isEmpty()) {
	  Object o = outputQueue.remove();
	  outputStream.writeObject(o);
	  if(getShouldResetStream()) {
	    setShouldResetStream(false);
	    outputStream.reset();
	  }
	} 
	sleep(100);
      }
    } catch (Exception e) {inputQueue.add(new PlayerQuitMessage()); onLine=false;}

    if(co!=null)
      co.stop();
    try {
      socket.close();
    } catch (IOException e) {}
  }

  void quitConnection() {
    onLine=false;
  }

  public void putMessage(Message m) {
    outputQueue.add(m);
  }

  public Message getMessage() {
    Object o = inputQueue.remove();
    if (o instanceof Message) {
      return ((Message) o);
    } else {inputQueue.add(new PlayerQuitMessage()); onLine=false; return getMessage();}
  }

  public boolean isMoreMessages() { return (!inputQueue.isEmpty()); }

  public void resetObjectStreams() {
    setShouldResetStream(true);
  }

  private synchronized void setShouldResetStream(boolean b) {
    shouldResetStream=b;
  }

  private synchronized boolean getShouldResetStream() {
    return shouldResetStream;
  }

    public String getPassword() {
	return password;
    }

  public String getPlayerName() {
    return playerName;    
  }

  public void close() {
    onLine=false;
  }
}


