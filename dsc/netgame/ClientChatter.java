/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;

import java.awt.*;
import java.awt.event.*;
import dsc.awt.*;
import java.util.*;


/** Chatter window for player. Handel message sending and shows
 * incoming messages in scrollable TextArea. 
 *
 * By default uses  "data/graph/still/icons/chatter.gif" as icon. Property 
 * name of the window title is ClientChatter.
 *
 * @see WorldProperties
 * @see ReceiverWindow
 *
 * @author Dodekaedron Software Creations, Inc. -- Wraith
 */

class ClientChatter extends LayoutableFrame implements ActionListener{

  private TextField inputText;
  private TextArea  outputText;
  private Button helpButton, sendButton, settingsButton;
  private GridBagLayout gb;

  public static ClientController controller;

  private ReceiverWindow receiverWindow; // Message receiver selection window.
  private boolean shownReceiver = false; // Whether receiver window is active.

  /** Create new chatter window. 
   *
   * @param controller This client controller. Need this to send messages
   *        and to find out players' names.
   */

  public ClientChatter(ClientController controller, 
		       ScreenLayout screenLayout) {
    this.controller=controller;
    setTitle(controller.getWorldProperties().getName("ClientChatter"));
    
    WindowInfo windowInfo = screenLayout.getWindowInfo("ClientChatter");

    if(windowInfo == null) {
	setSize(8*40,12*40);
    } else {
	setSize(windowInfo.getXSize(), windowInfo.getYSize());
	setLocation(windowInfo.getXCoord(), windowInfo.getYCoord());
    }

    Image i=GraphicsLoader.getImage("data/graph/still/icons/chatter.gif");
    setIconImage(i);

    gb = new GridBagLayout();
    GridBagConstraints gbc = new GridBagConstraints();
    setLayout(gb);

    gbc.fill=GridBagConstraints.BOTH;
    gbc.weightx=100;
    gbc.weighty=100;

    /* inputText's actionlistener is provided as inner class. 
       This looks terrible, but works fine. */
    
    inputText=new TextField(50);
    inputText.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
	String arg = evt.getActionCommand();

	// Don't send empty messages.
	if(arg.equals("")) return;
	inputText.setText("");

	ChatClientMessage ccm = receiverWindow.getChatMessage(arg);
	if(ccm != null) 
	  ClientChatter.controller.sendToHost(ccm);
      }
    });

    /* This message should be changed */
    outputText = new TextArea("Connected to host\n\n",20,50,
			      TextArea.SCROLLBARS_VERTICAL_ONLY);
    outputText.setEditable(false);

    add(outputText,gbc,0,0,8,20);

    gbc.weightx=100;
    gbc.weighty=0;
    gbc.anchor=GridBagConstraints.SOUTH;

    gbc.fill=GridBagConstraints.HORIZONTAL;
    add(inputText,gbc,0,20,8,1);

    gbc.fill=GridBagConstraints.BOTH;

    sendButton=createButton("Send",this);
    helpButton=createButton("Help",this);
    settingsButton=createButton("Receiver",this);
    
    add(sendButton,gbc,6,21,2,1);
    add(helpButton,gbc,3,21,2,1);
    add(settingsButton,gbc,0,21,2,1);

    /* Create receiver window. Note that window contains BitSet that
       represents receiver's, so the window is needed wheter it is
       show or not. */
    receiverWindow = new ReceiverWindow(controller, screenLayout);
  }

  /** Show message in the TextArea. Messages sender is displayed in 
   * front of the message's text. This method is usually only called
   * in HostOutput-class.
   *
   * @param cm Message to show.
   * @see HostOutput
   */

  public void putMessage(ChatMessage cm) {
    String from;

    if(cm.whoFrom() == -1) {
      from="[Host]";
    } else {
      from=controller.playersInfo.getPlayerName(cm.whoFrom());
    }

    String text = from + " " +cm.getMessage();

    outputText.append(text + "\n");
    outputText.setCaretPosition(outputText.getText().length());

    GameWindow gw = controller.getGameWindow();

    if(gw != null && gw.chatToModeLine())
      gw.setModeLineText("<Chat> " + text);
  }

  /** Notifies chatter that player information has changed. This happens
   * when someone joins or quits the game, or when someone changes
   * name. This method must be called in these cases to keep the
   * chatter operating normally. Typically called in HostOutput when
   * needed.
   *
   * @see HostOutput
   */

  public void playerNotify () {
    if(receiverWindow!=null)
      receiverWindow.playerNotify();
  }

  public void actionPerformed(ActionEvent evt) {
    String arg = evt.getActionCommand();

    if(arg.equals("Quit")) {
      QuitQueryWindow w = new QuitQueryWindow(this);
      w.show();
    }
    else if (arg.equals("Receiver")) {
      if(shownReceiver)
	receiverWindow.setVisible(true);
      else {
	shownReceiver=true;
	receiverWindow.show();
      }
    }
    else if (arg.equals("Send")) {
      if(inputText.getText().equals(""))
	return;
	
      ChatClientMessage ccm 
	= receiverWindow.getChatMessage(inputText.getText());
      
      if(ccm != null)
	controller.sendToHost(ccm);      
      inputText.setText("");
    }
    else if (arg.equals("Help")) {
      HelpWindow.callForHelp("data/help", "Window", "Chatter");
    }
  }

    protected void updateWindowInfo(ScreenLayout sl) {
	WindowInfo wi = new WindowInfo(getSize().width,
				       getSize().height,
				       getLocation().x,
				       getLocation().y);

	sl.addWindowInfo("ClientChatter", wi);

	wi = new WindowInfo(receiverWindow.getSize().width,
			    receiverWindow.getSize().height,
			    receiverWindow.getLocation().x,
			    receiverWindow.getLocation().y);

	sl.addWindowInfo("ReceiverWindow", wi);
    }
}
