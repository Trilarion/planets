/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;

import java.awt.*;
import java.awt.event.*;
import dsc.awt.*;
import java.util.*;


/** GameWindow is the map window in the game. It contains game map and
 * panel for component specific information. GameWindow has also menubar
 * for game, world and component menus.
 *
 * @author Dodekaedron Software Creations, Inc. -- Wraith
 */

public class GameWindow extends LayoutableFrame implements ActionListener, MouseListener, QuitQueryCallback, ItemListener, KeyListener {

  /** Layout of the window. */
  private GridBagLayout gb;
  
  private MenuBar mb;
  private Menu allianceMenu;
  private Menu optionsMenu;
  private Menu componentMenu=null;

  private MenuItem endTurnMenuItem;
  private MenuItem timeoutMenuItem;

  private CheckboxMenuItem drawBackground;
  private CheckboxMenuItem chatToModeLineCheckbox;

  private ScrollPane gameMap;
  private GameMapCanvas mapCanvas;
  private int mapXSize;
  private int mapYSize;
  private PopupMenu popupMenu;
  private boolean requestPopActive;

  private Label modeLine;
  private Button cancelButton;

  private Panel componentPanel;

  private World world;
  private Hashtable images;

  private WorldComponent activeComponent;
  private boolean selectionMode;
  private boolean componentSelection;
  private boolean mouseInMap;

  private Panel emptyPanel;

  private Label turnLeftLabel;
  private TurnClock turnClock;

  private int scaleFactor=1;

  // Custom mutex to avoid conflicts with AWT synchronized methods
  private Integer mutex = new Integer(0);

  public GameWindow(StartGameMessage sgm, String winName, 
		    ScreenLayout screenLayout) {

    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) { System.exit(0); } 
    });

    WindowInfo windowInfo = screenLayout.getWindowInfo("GameWindow");

    selectionMode=false;
    componentSelection=false;
    mouseInMap=false;

    mapXSize=sgm.xSize;
    mapYSize=sgm.ySize;
    
    setTitle(winName);

    if(windowInfo == null) {
	setSize(800,640);
    } else {
	setSize(windowInfo.getXSize(), windowInfo.getYSize());
	setLocation(windowInfo.getXCoord(), windowInfo.getYCoord());
    }

    gb = new GridBagLayout();
    setLayout(gb);
    
    Image i=GraphicsLoader.getImage("data/graph/still/icons/game.gif");
    setIconImage(i);
    
    createMenus();

    gameMap = new ScrollPane();
    modeLine = new Label("The game has started.");
    cancelButton = createButton("Cancel",this);
    cancelButton.setEnabled(false);

    turnLeftLabel = new Label("XX:XX");
    
    componentPanel = new Panel();

    ImageComponent foo = new ImageComponent(GraphicsLoader.getImage(
			                    "data/graph/still/panel/ph.gif"));
    componentPanel.setLayout(new VerticalFlowLayout());
    componentPanel.add(foo);
    emptyPanel=componentPanel;

    images=new Hashtable();
    popupMenu = new PopupMenu();
    requestPopActive=false;

    mapCanvas = new GameMapCanvas(this, Color.black);
    mapCanvas.addMouseListener(this);
    addKeyListener(this);

    mapCanvas.setSize(sgm.xSize,sgm.ySize);
    gameMap.add(mapCanvas);
    Adjustable adj = gameMap.getVAdjustable();
    adj.setUnitIncrement(25);
    adj = gameMap.getHAdjustable();
    adj.setUnitIncrement(25); 

    add(popupMenu);
    gbAdd(gameMap, 100, 100, 0, 0, 10, 10);
    gbAdd(modeLine, 100, 0, 0, 10, 10, 1);
    gbAdd(componentPanel, 0, 100, 10, 0, 3, 10);
    gbAdd(cancelButton, 0, 0, 10, 10, 2, 1);
    gbAdd(turnLeftLabel, 0, 0, GridBagConstraints.CENTER, 
	  GridBagConstraints.BOTH ,12, 10, 1, 1);

    turnClock = new TurnClock(turnLeftLabel);
    turnClock.start();
  }

  private void createMenus() {
    mb = new MenuBar();
    createOptionsMenu();
    createWindowMenu();
    createHelpMenu();
    createTurnMenu();
    createAllianceMenu();

    setMenuBar(mb);
  }

  private void makeMenuItem(Menu m, 
			    String name, 
			    String actionCommand,
			    ActionListener al) {
    MenuItem mi = new MenuItem(name);
    mi.setActionCommand(actionCommand);
    mi.addActionListener(al);
    m.add(mi);
  }

  private void createOptionsMenu() {
    optionsMenu = new Menu("Options", true);

    makeMenuItem(optionsMenu,"Quit", "Command-Quit", this);

    optionsMenu.addSeparator();

    chatToModeLineCheckbox=new CheckboxMenuItem("Chat to modeline", false);
    optionsMenu.add(chatToModeLineCheckbox);

    drawBackground=new CheckboxMenuItem("Draw stars", true);
    optionsMenu.add(drawBackground);

    makeMenuItem(optionsMenu, "Save Layout" , "Command-Save-Layout" , this);

    mb.add(optionsMenu);
  }

  private void createWindowMenu() {
    Menu tm = new Menu("Map", true);
    
    MenuItem mi = new MenuItem("1:1", new MenuShortcut(KeyEvent.VK_1));
    mi.addActionListener(this);
    tm.add(mi);

    mi = new MenuItem("1:2", new MenuShortcut(KeyEvent.VK_2));
    mi.addActionListener(this);
    tm.add(mi);

    mi = new MenuItem("1:3", new MenuShortcut(KeyEvent.VK_3));
    mi.addActionListener(this);
    tm.add(mi);

    mi = new MenuItem("1:4", new MenuShortcut(KeyEvent.VK_4));
    mi.addActionListener(this);
    tm.add(mi);

    mi = new MenuItem("1:5", new MenuShortcut(KeyEvent.VK_5));
    mi.addActionListener(this);
    tm.add(mi);

    mi = new MenuItem("1:6", new MenuShortcut(KeyEvent.VK_6));
    mi.addActionListener(this);
    tm.add(mi);

    mi = new MenuItem("1:7", new MenuShortcut(KeyEvent.VK_7));
    mi.addActionListener(this);
    tm.add(mi);

    mi = new MenuItem("1:8", new MenuShortcut(KeyEvent.VK_8));
    mi.addActionListener(this);
    tm.add(mi);

    mi = new MenuItem("1:9", new MenuShortcut(KeyEvent.VK_9));
    mi.addActionListener(this);
    tm.add(mi);

    mb.add(tm);
  }

  private void createHelpMenu() {}

  private void createTurnMenu() {
    Menu tm = new Menu("Turn", true);
    
    MenuItem mi = new MenuItem("End", new MenuShortcut(KeyEvent.VK_E));
    mi.addActionListener(this);
    tm.add(mi);
    endTurnMenuItem=mi;

    mi = new MenuItem("Timeout", new MenuShortcut(KeyEvent.VK_T));
    mi.addActionListener(this);
    tm.add(mi);
    timeoutMenuItem=mi;

    mb.add(tm);

  }

  private void createAllianceMenu() {
    allianceMenu = new Menu("Alliance", true);
    mb.add(allianceMenu);
  }

  public void actionPerformed(ActionEvent evt) {
    String arg = evt.getActionCommand();

    if(arg == null) {
      MenuItem mi = (MenuItem) evt.getSource();
      arg=mi.getLabel();
    }

    if(arg.equals("Command-Quit")) {
      System.exit(0);
      /*
	QuitQueryWindow causes problems in many environments,
	removing it temporarily.

      QuitQueryWindow w = new QuitQueryWindow(this);
      w.setOKQuitCallback(this);
      w.show();
      */
    }
    else if (arg.equals("Help")) {
      HelpWindow.callForHelp("data/help", "Window", "Game");
    } 
    else if (arg.equals("End")) {
      endTurn();
    } else if(arg.equals("Timeout")) {
      timeout();
    }
    else if (arg.equals("Command-Save-Layout")) {
	world.getController().saveScreenLayout();
    }
    else if (arg.equals("Cancel")) {
      cancelSelectionMode();
    }
    else if (arg.startsWith("1:")) {
      String scale = arg.substring(2);
      Dimension dimension = gameMap.getViewportSize();
      Point scrollPosition = gameMap.getScrollPosition();
      int oldXMiddle, oldYMiddle;

      oldXMiddle = (scrollPosition.x + dimension.width/2)*scaleFactor; 
      oldYMiddle = (scrollPosition.y + dimension.height/2)*scaleFactor; 

      /*
      System.out.println("Old: " + oldXMiddle + "." + oldYMiddle);
      */

      try {
	scaleFactor = Integer.parseInt(scale);
      } catch(NumberFormatException e) {
	throw new IllegalStateException("Scale factor is not a number");
      }

      mapCanvas.setSize(mapXSize/scaleFactor,mapYSize/scaleFactor);

      int x = oldXMiddle/scaleFactor-dimension.width/2;
      int y = oldYMiddle/scaleFactor-dimension.height/2;

      x = x<0 ? 0 : x; y = y<0 ? 0 : y;
      
      /*
      System.out.println("New: " + x + "." + y);
      */

      gameMap.setScrollPosition(x,y);
      mapCanvas.repaint();
    }
    else {
      Enumeration e = world.getWorldComponents();
      while(e.hasMoreElements()) {
	WorldComponent wc = (WorldComponent) e.nextElement();
	if(wc.getIdString().equals(arg)) {
	  if(requestPopActive)
	    sendComponent(wc);
	  else
	    activateComponent(wc);
	  break;
	}
      }
    }
  }

  private void sendLocation(Location l) {
    activeComponent.locationSelected(l);
    cancelSelectionMode();
  }

  private void sendShiftLocation(Location l) {
    activeComponent.locationShiftSelected(l);
    cancelSelectionMode();
  }

  private void sendComponent(WorldComponent wc) {
    requestPopActive=false;
    activeComponent.componentSelected(wc);
    cancelSelectionMode();
  }

  public void requestLocation() {
    if(mouseInMap)
      setCursor(Cursor.getPredefinedCursor(CROSSHAIR_CURSOR));
    modeLine.setText("Select target.");
    selectionMode=true;
    componentSelection=false;
    cancelButton.setEnabled(true);
  }

  public void requestComponent() {
    if(mouseInMap)
      setCursor(Cursor.getPredefinedCursor(CROSSHAIR_CURSOR));
    modeLine.setText("Select target component.");
    selectionMode=true;
    componentSelection=true;
    cancelButton.setEnabled(true);
  }

  public void cancelSelectionMode() {
    selectionMode=false;
    cancelButton.setEnabled(false);
    modeLine.setText("");
    if(mouseInMap)
      setCursor(Cursor.getPredefinedCursor(HAND_CURSOR));
    else
      setCursor(Cursor.getDefaultCursor());
  }

  private void activateComponentsSelection(int x,int y) {
    Vector all = new Vector();
    Enumeration e = world.getWorldComponents();
    WorldComponent wc;
    Image i;
    Location l;
    int dx,dy;

    while(e.hasMoreElements()) {
      wc=(WorldComponent) e.nextElement();
      i=getImage(wc.getMapImage());
      dx=i.getWidth(this)/2; dy=i.getHeight(this)/2;
      l=wc.getLocation();

      if( (l.getX()-dx) <= x && (l.getX()+dx) >= x &&
	  (l.getY()-dy) <= y && (l.getY()+dy) >= y) {
	all.addElement(wc);
      }
    }    
    
    if(all.size()!=0) {
      if(all.size()==1) {
	if(requestPopActive) {
	  sendComponent((WorldComponent) all.elementAt(0));
	} else
	  activateComponent((WorldComponent) all.elementAt(0));
      }
      else {
	popupMenu.removeAll();
	e=all.elements();

	while(e.hasMoreElements()) {
	  wc=(WorldComponent) e.nextElement();
	  if(wc.isVisible() && !wc.paintToForeground())
	      makeMenuItem(popupMenu, wc.getName(), wc.getIdString(), this);
	}

	e=all.elements();
	popupMenu.addSeparator();

	while(e.hasMoreElements()) {
	  wc=(WorldComponent) e.nextElement();
	  if(wc.isVisible() && wc.paintToForeground())
	    makeMenuItem(popupMenu, wc.getName(), wc.getIdString(), this);
	}

	popupMenu.show(mapCanvas,x/scaleFactor,y/scaleFactor);
      }
    }
  }

    public void activateComponentByName(String name) {
	Enumeration e = world.getWorldComponents();
	
	while(e.hasMoreElements()) {
	    WorldComponent wc = (WorldComponent) e.nextElement();
	    if(wc.getName().equals(name) ||
	       wc.getTrueName().equals(name)) {
		if(requestPopActive) {
		    requestPopActive=false;
		    cancelSelectionMode();
		} else {
		    activateComponent(wc);
		}
		break;
	    }
	}
    }

  protected void activateComponent(WorldComponent wc) {
      synchronized (mutex) {
	  activeComponent=wc;
	  cancelSelectionMode();
	  remove(componentPanel);
	  componentPanel=wc.getControlPanel();
	  gbAdd(componentPanel, 0, 100, 10, 0, 3, 10);

	  if(componentMenu != null) {
	      mb.remove(componentMenu);
	  }
	  componentMenu = wc.getMenu();
	  if(componentMenu != null) {
	      mb.add(componentMenu);
	  }

	  validate();
	  mapCanvas.repaint();
      }
  }

  public void mousePressed(MouseEvent e) {
    int clickX = e.getX()*scaleFactor;
    int clickY = e.getY()*scaleFactor;

    if(selectionMode) {
      if(componentSelection) {
	requestPopActive=true;
	cancelSelectionMode();
	activateComponentsSelection(clickX, clickY);
      } else {
	if((e.getModifiers() & MouseEvent.SHIFT_MASK) != 0)
	  sendShiftLocation(new Location(clickX, clickY));
	else
	  sendLocation(new Location(clickX, clickY));
	cancelSelectionMode();
      }
    } else {
      if((e.getModifiers() & MouseEvent.BUTTON2_MASK) != 0) {
	if(activeComponent!= null) {
	  if((e.getModifiers() & MouseEvent.SHIFT_MASK) != 0)
	    activeComponent.pointShiftClicked(new Location(clickX, 
							   clickY));
	  else
	    activeComponent.pointClicked(new Location(clickX, 
						      clickY));
	}
      } else if((e.getModifiers() & MouseEvent.BUTTON3_MASK) != 0) {
	  centerMap(e.getX(), e.getY());
      } else {
	requestPopActive=false;
	activateComponentsSelection(clickX, clickY);
      }
    }
  }
 
    private void centerMap(int cx, int cy) {
	Dimension d = gameMap.getViewportSize();
	int x = cx-d.width/2;
        int y = cy-d.height/2;
	x = x<0 ? 0 : x; y = y<0 ? 0 : y;
	gameMap.setScrollPosition(x,y);
    }

  public void mouseEntered(MouseEvent e) {
    mouseInMap=true;
    if(selectionMode)
      setCursor(Cursor.getPredefinedCursor(CROSSHAIR_CURSOR));
    else
      setCursor(Cursor.getPredefinedCursor(HAND_CURSOR));
  }
 
  public void mouseExited(MouseEvent e) {
    mouseInMap=false;
    setCursor(Cursor.getDefaultCursor());
  }
 
  public void mouseClicked(MouseEvent e) {}
  public void mouseReleased(MouseEvent e) {}
 

  void setWorld(World w) {
    world=w;
  }

  Image getImage(String filename) {
    Image i;

    if(filename==null)
      return null;

    if(!images.containsKey(filename)) {
      i=dsc.awt.GraphicsLoader.getImage(filename);
      images.put(filename,i);
    } else {
      i=(Image) images.get(filename);
    }

    return i;
  }

  public void deactivateComponent() {
      synchronized (mutex) {
	  cancelSelectionMode();
	  remove(componentPanel);

	  componentPanel=emptyPanel;
	  gbAdd(componentPanel, 0, 100, 10, 0, 3, 10);

	  if(mb!=null && componentMenu != null)
	      mb.remove(componentMenu);

	  activeComponent=null;
	  validate();
      }
  }

  public void refreshMap() {
    mapCanvas.setComponents(world.getWorldComponents());
    mapCanvas.repaint();
  }

  public void addWorldMenu(Menu m) {
    mb.add(m);
    validate();
  }

  public void addAdditionalOption(MenuItem mi) {
    optionsMenu.add(mi);
    validate();
  }

  public void repaintMap() {
    mapCanvas.repaint();
  }

  public WorldComponent getActiveComponent() {
    return activeComponent;
  }

  public void drawBackground(ScaledGraphics g) {
    if(getDrawBackground())
      world.drawBackground(g);
  }

  public int getMapXSize() {
    return mapXSize;
  }

  public int getMapYSize() {
    return mapYSize;
  }

  public void restartClock(int timeleft) {
    turnClock.resetTime(timeleft);
  }

  public void incrementClock(int increment) {
      turnClock.incTime(increment*1000);
  }

  public int getPlayerNumber() {
    return world.getController().playerNumber;
  }

  public boolean getDrawBackground() {
    return drawBackground.getState();
  }

  public void setModeLineText(String s) {
    modeLine.setText(s);
  }

  public boolean chatToModeLine() {
    return chatToModeLineCheckbox.getState();
  }

  public void quitOkPressed() {
    world.playerQuits();
  }

  public void quitCancelPressed() {}

  public void setEmptyPanel(Panel p) {
    emptyPanel=p;
  }

  public void setAlliances(Alliance a) {
    allianceMenu.removeAll();

    PlayersInfo pi = world.getController().playersInfo;

    for(int i=0; i < GlobalDefines.maxPlayers; i++) {
      if(i == world.getController().playerNumber) 
	continue;

      if(i == GlobalDefines.maxPlayers-1) /* Ignore special host players */ 
	continue;

      if(world.playerHasVisibleObjects(i) || a.isAlly(i)) {
	CheckboxMenuItem cmi = new CheckboxMenuItem(pi.getPlayerName(i),
						    a.isAlly(i));
	cmi.addItemListener(this);
	allianceMenu.add(cmi);
      }
    }
  }


  public void itemStateChanged(ItemEvent evt) {
    if(evt.getSource() instanceof CheckboxMenuItem) {
      sendAllianceSettings();
    }
  }

  private void sendAllianceSettings() {
    Alliance a = new Alliance(world.getController().playerNumber);


    for(int i=0; i < allianceMenu.getItemCount(); i++) {
        CheckboxMenuItem cmi = (CheckboxMenuItem) allianceMenu.getItem(i);

	int nro = world.
	  getController().
	  playersInfo.getNumberByName(cmi.getLabel());

	a.setAlly(nro, cmi.getState());
    }

    SetAllianceMessage sam = 
      new SetAllianceMessage(world.getController().playerNumber, a);
    world.sendToHost(sam);
  }

  public int getScaleFactor() {
    return scaleFactor;
  }

    private boolean activateComponentAtBearing(int min, int max) {
	if(activeComponent == null)
	    return false;

	WorldComponent closestWC=null;
	int closestDistance=Integer.MAX_VALUE;
	Enumeration e = world.getWorldComponents();
	
	while(e.hasMoreElements()) {
	    WorldComponent wc = (WorldComponent) e.nextElement();
	    Location l        = wc.getLocation();
	    int bearing       = activeComponent.getLocation().getBearingTo(l);

	    if((max>min && bearing >= min && bearing <= max) ||
	       (max<min && (bearing >= min || bearing <= max))) {
		int distance = activeComponent.getLocation().distance(l);
		if(distance > 0 && distance < closestDistance) {
		    closestDistance = distance;
		    closestWC       = wc;
		}
	    }
	}

	if(closestWC != null) {
	    activateComponent(closestWC);    
	    return true;
	}

	return false;
    }

    void activateFirstComponent() {
	Enumeration e;
	WorldComponent wc;

	if(activeComponent == null)
	    return;

	e=world.getComponentsByLocation(activeComponent.getLocation());

	while(e.hasMoreElements()) {
	  wc=(WorldComponent) e.nextElement();
	  if(!wc.paintToForeground()) {
	      activateComponent(wc);
	      return;
	  }
	}

	e=world.getComponentsByLocation(activeComponent.getLocation());


	while(e.hasMoreElements()) {
	  wc=(WorldComponent) e.nextElement();
	  if(wc.paintToForeground())
	      activateComponent(wc);
	}
    }

    void activateLastComponent() {
	Enumeration e;
	WorldComponent wc;

	if(activeComponent == null)
	    return;

	WorldComponent last=null;

	e=world.getComponentsByLocation(activeComponent.getLocation());

	while(e.hasMoreElements()) {
	  wc=(WorldComponent) e.nextElement();
	  if(!wc.paintToForeground()) {
	      last = wc;
	  }
	}

	e=world.getComponentsByLocation(activeComponent.getLocation());

	while(e.hasMoreElements()) {
	  wc=(WorldComponent) e.nextElement();
	  if(wc.paintToForeground())
	      last = wc;
	}

	if(last != null)
	    activateComponent(last);
    }

    void activateNextComponent() {
	Enumeration e;
	WorldComponent wc;

	if(activeComponent == null)
	    return;

	boolean currentFound = false;

	e=world.getComponentsByLocation(activeComponent.getLocation());


	while(e.hasMoreElements()) {
	  wc=(WorldComponent) e.nextElement();
	  if(!wc.paintToForeground()) {
	      if(currentFound) {
		  activateComponent(wc);
		  return;
	      }
		  
	      if(wc == activeComponent)
		  currentFound = true;
	  }
	}

	e=world.getComponentsByLocation(activeComponent.getLocation());


	while(e.hasMoreElements()) {
	  wc=(WorldComponent) e.nextElement();
	  if(wc.paintToForeground())
	      if(currentFound) {
		  activateComponent(wc);
		  return;
	      }

	      if(wc == activeComponent)
		  currentFound = true;
	}
    }


    void activatePreviousComponent() {
	Enumeration e;
	WorldComponent wc;

	if(activeComponent == null)
	    return;

	WorldComponent last=null;

	e=world.getComponentsByLocation(activeComponent.getLocation());


	while(e.hasMoreElements()) {
	  wc=(WorldComponent) e.nextElement();
	  if(!wc.paintToForeground()) {
	      if(wc == activeComponent) {
		  if(last!=null) {
		      activateComponent(last);
		      return;
		  }
	      }
	      last = wc;
	  }
	}

	e=world.getComponentsByLocation(activeComponent.getLocation());

	while(e.hasMoreElements()) {
	  wc=(WorldComponent) e.nextElement();
	  if(wc.paintToForeground())
	      if(wc == activeComponent) {
		  if(last!=null) {
		      activateComponent(last);
		      return;
		  }
	      }
	      last = wc;
	}
    }

  public void keyTyped(KeyEvent e) {}
  public void keyReleased(KeyEvent e) {}

    public void keyPressed(KeyEvent e) {
	int key = e.getKeyCode();

	switch(key) {
	case KeyEvent.VK_PAGE_UP :
	    activatePreviousComponent();
	    break;

	case KeyEvent.VK_PAGE_DOWN :
	    activateNextComponent();
	    break;

	case KeyEvent.VK_INSERT :
	    break;

	case KeyEvent.VK_DELETE :
	    break;

	case KeyEvent.VK_HOME :
	    activateFirstComponent();
	    break;

	case KeyEvent.VK_END :
	    activateLastComponent();
	    break;

	case KeyEvent.VK_NUMPAD1 :
	    activateComponentAtBearing(204,247);
	    break;

	case KeyEvent.VK_NUMPAD2 :
	    activateComponentAtBearing(161,203);
	    break;

	case KeyEvent.VK_NUMPAD3 :
	    activateComponentAtBearing(115,160);
	   break;

	case KeyEvent.VK_NUMPAD4 :
	    activateComponentAtBearing(248,293);
	    break;

	case KeyEvent.VK_NUMPAD5 :
	    if(activeComponent != null) {
		Location l = activeComponent.getLocation();
		centerMap(l.getX(), l.getY());
	    }
	    break;

	case KeyEvent.VK_NUMPAD6 :
	    activateComponentAtBearing(69,114);
	    break;

	case KeyEvent.VK_NUMPAD7 :
	    activateComponentAtBearing(294,339);
	    break;

	case KeyEvent.VK_NUMPAD8 :
	    activateComponentAtBearing(340,22); 
	    break;

	case KeyEvent.VK_NUMPAD9 :
	    activateComponentAtBearing(23,68);
	    break;

        default:
	    break;
	}
    }

    public void update(Graphics g) {
	super.update(g);
	requestFocus();
    }

    public void endTurn() {
	turnClock.turnEndPressed();
	world.sendToHost(new EndTurnMessage());
	dsc.util.GarbageCollection.freeAll();      
    }

    public void timeout() {
	world.sendToHost(new TimeoutRequestMessage());
    }

    public void configTimeout(WorldState ws) {
	timeoutMenuItem.setEnabled(ws.getTimeoutPossibility());
    }

    protected void updateWindowInfo(ScreenLayout sl) {
	WindowInfo wi = new WindowInfo(getSize().width,
				       getSize().height,
				       getLocation().x,
				       getLocation().y);

	sl.addWindowInfo("GameWindow", wi);
    }
}
