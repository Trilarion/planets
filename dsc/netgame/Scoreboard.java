/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;
import java.util.*;

/**
 * Scoring system. This is
 * mostly left empty and can be implemented by the actual game.
 *
 * @author Dodekaedron Software Creations, Inc. -- Wraith 
 */

public abstract class Scoreboard implements java.io.Serializable {
    protected int turnLimit = 0;
    protected int pointLimit = 0;

    private Hashtable scores;

    protected Scoreboard(WorldOptions options, HostWorld world) {
	int numPlayers = world.getNumberOfPlayers();
	int maxPlayer  = 
	pointLimit = numPlayers+1;

	scores = new Hashtable();

	Enumeration players = world.getAllPlayers();

	while(players.hasMoreElements()) {
	    Integer i = (Integer) players.nextElement();
	    scores.put(i, getNewScoreEntry(world.getPlayerName(i.intValue()),
					   i.intValue()));
	}
    }

    protected abstract ScoreEntry getNewScoreEntry(String playerName,
						   int number);

    public boolean isGameOver(int newRoundNro) {
	if(turnLimit > 0 && 
	   newRoundNro >= turnLimit) {
	    return true;
	}

	if(pointLimit > 0 ) {
	    for(int i=0; i<GlobalDefines.maxPlayers; i++) {
		ScoreEntry se = getScore(i);

		if(se != null && se.getScore() >= pointLimit) {
		    return true;
		}
	    }
	}
	
	return false;
    }

    private void updateScore(ScoreEntry se, HostWorld world, int newRoundNro) {
	se.update(this, newRoundNro, world);
    }

    public void changeRound(HostWorld world, int newRoundNro) {
	Enumeration e = scores.elements();
	while(e.hasMoreElements()) {
	    ScoreEntry se = (ScoreEntry) e.nextElement();
	    updateScore(se, world, newRoundNro);
	}
    }

    public ScoreEntry getScore(int player) {
	if(player < 0 ||
	   player >= GlobalDefines.maxPlayers) {
	    throw new IllegalArgumentException("Player number out of range");
	}

	return ((ScoreEntry) scores.get(new Integer(player)));
    }

    public String getScores() {
	String s = "Scores:\n";

	Vector sortedScores = new Vector();

	Enumeration e = scores.elements();
	while(e.hasMoreElements()) {
	    sortedScores.addElement(e.nextElement());
	}

	dsc.util.Quicksort.quicksort(sortedScores);

	int position = 1;
	int true_position = 1;
	int prev_score = Integer.MAX_VALUE;

	e = sortedScores.elements();
	while(e.hasMoreElements()) {
	    ScoreEntry se = (ScoreEntry) e.nextElement();
	    if(se.getScore() < prev_score) {
		position = true_position;
		prev_score = se.getScore();
	    }

	    s += "" + position + ". ";
	    s += se.toString() + "\n";

	    true_position++;
	}

	return s;
    }

    public String toString() {
	return getScores();
    }
}
