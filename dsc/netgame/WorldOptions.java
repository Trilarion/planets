/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;

import java.awt.*;

/** WorldOptions contains typical user settable options for the game.
 * This class must be extended to provide options for the corresponding
 * world.
 *
 * @author Dodekaedron Software Creations, Inc. -- Wraith
 */

public abstract class WorldOptions implements java.io.Serializable {

  /** Number of players in the game. Host sets this number when the
   * game starts and it can be used in the initialization of the world.
   */

  protected int numberOfPlayers;

  /** Name of the game. Host sets the name before any connections are
   * accepted.
   */

  protected String gameName = "game";

  /** Sets number of players in the game. This method is called inside
   * HostController, don't call it from anywhere else.
   *
   * @param n Number of players in the game.
   */

  public void setNumberOfPlayers(int n) {
    numberOfPlayers=n;
  }

  /** Returns round time for given round. Host lets players deside their
   * actions for this amount of time during given round. 
   *
   * @param turnNumber Current game round.
   * @return Round time in milliseconds
   */

  public int getRoundTime(int turnNumber) {
    return 90*1000;
  }

  /** Sets game name. This method is called inside HostController,
   * don't call it from anywhere else.
   *
   * @param name name of the new game.
   */

  public void setGameName(String name) {
    gameName = name;
  }

  /** Returns save game suffix. Default is ".sav".
   *
   * @return Save game suffix
   */

  public String getSaveGameSuffix() {
    return ".sav";
  }

  /** Returns save game name.
   *
   * @return Save game name
   */

  public String getSaveGameName() {
      return gameName + getSaveGameSuffix();
  }

  /** Returns save game backup filename.
   *
   * @return Save game backup name
   */

  public String getSaveGameBackupName() {
      return gameName + "-BAK-" + getSaveGameSuffix();
  }

    /**
     * Defines whether the game should wait for missing players before
     * doing preliminary round run. 
     *
     * @return true if turn can end before it's time has passed
     * if all active players agree. Returns false if also currently missing
     * players must agree.
     *
     */

    public boolean endTurnWhenAway() {
	return true;
    }
}
