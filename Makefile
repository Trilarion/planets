# Compiler selection. Jikes is much faster than javac, but is not available
# to all platforms. If your platform does not have jikes compiler, comment
# the two first lines and uncomment the third. Note that you might
# need to adjust the classpath setting for jikes when using it.

#JCC = jikes
#JOPTS = -classpath /usr/local/JDK/lib/classes.zip:. +E
JCC   = javac 

NONDATADIRS="dsc/util dsc/awt dsc/netgame phworld"

compile : PlanetaryHoppers.java dsc/netgame/*.java dsc/awt/*.java \
          dsc/util/*.java phworld/*.java phworld/mapeditor/*.java \
          phworld/ai/*.java phworld/ai/defender/*.java
	misc/createversion
	$(JCC) $(JOPTS) dsc/netgame/*.java dsc/awt/*.java dsc/util/*.java \
                        phworld/*.java phworld/mapeditor/*.java \
                        phworld/ai/*.java phworld/ai/*/*.java \
                        PlanetaryHoppers.java 
	touch compile

check:
	@echo "Following namedatabase collisions detected in names:"
	-@cat data/text/names.ship.?.text data/text/themes/*/names.ship.?.text data/text/names.planet.text data/text/themes/*/names.planet.text | tr ':' ' '| sort | uniq -d

	@echo "Following names are invalid (space in end?):"
	-@grep '.*' data/text/names.ship.?.text data/text/themes/*/names.ship.?.text data/text/names.planet.text data/text/themes/*/names.planet.text | grep -v "^data.*text:[-.,'a-zA-Z0-9 ]\{1,16\}[0-9a-zA-Z.]$$"

clean :
	-rm *~
	-rm */*~
	-rm */*/*~
	-rm data/help/*/*~
	-rm *.class */*.class */*/*.class */*/*/*.class
	-rm compile
	-rm TEST.stats.*

dist : compile
	(cd .. ; zip -r planets-bin-`cat planets/misc/current.main.version`.`cat planets/misc/current.sub.version`.`cat planets/misc/current.patch.version`.zip `cat planets/misc/dist.files.misc` `cat planets/misc/dist.files.exec` `cat planets/misc/dist.files.data` )

srcdist :
	(cd .. ; zip -r planets-src-`cat planets/misc/current.main.version`.`cat planets/misc/current.sub.version`.`cat planets/misc/current.patch.version`.zip planets)

patchversion :
	echo "`cat misc/current.patch.version`+1" | bc > misc/current.patch.version

subversion :
	echo "`cat misc/current.sub.version`+1" | bc > misc/current.sub.version
	echo 0 > misc/current.patch.version

newgame : world
	java -classpath . PlanetaryHoppers -quick

world :
	cvs update -d
	make clean
	make

