# Make Gnuplot files to create statistics
# Usage: gawk -f make-stats.awk TEST.stats.players
#   creates TEST.stats.*.gpt

function plotcmd(ycol,     i) {
    gpt = gpt "plot \\\n";
    for (i = 0; i < players; i++) {
	if (i != 0)
	    gpt = gpt ", \\\n";
	gpt = gpt "  \"" basename ".player" playernum[i] "\" using 1:" ycol " ";
	gpt = gpt "title \"" playername[i] "\" with lines";
    }
    gpt = gpt "\n";
}

function writefile(outfile,     fn) {
    fn = basename "." outfile ".gpt";
    print gpt >fn;
    close(fn);
}

function docolstats(ycol, ylabel, outfile) {
    gpt = "";
    gpt = gpt "set grid\nset xlabel \"Round\"\nset ylabel \"" ylabel "\"\n";
    gpt = gpt "set key left reverse\n";
    plotcmd(ycol);
    writefile(outfile);
}

# array indices: left: 1-6, right: 7-12
function twelveplots(ltitle, rtitle, cols, labs,     i, y) {
    gpt = "";
    gpt = gpt "set grid\nset lmargin 8\nset rmargin 1\n";
    gpt = gpt "set tmargin 1\nset bmargin 1\n";
    gpt = gpt "set key left reverse\n";
    gpt = gpt "set origin 0.0,0.0\nset size 1,1\nset multiplot\n";

    gpt = gpt "set origin 0.0,0.0\nset size 0.5,0.17\n";
    gpt = gpt "set xlabel \"Rounds\"\nset ylabel \"" labs[6] "\"\n";
    plotcmd(cols[6]);
  
    gpt = gpt "set origin 0.5,0.0\nset size 0.5,0.17\n";
    gpt = gpt "set xlabel \"Rounds\"\nset ylabel \"" labs[12] "\"\n";
    plotcmd(cols[12]);

    i = 5; y = 0.17;
    while (i > 1) {
        gpt = gpt "set origin 0.0," y "\nset size 0.5,0.17\n";
        gpt = gpt "set xlabel\nset ylabel \"" labs[i] "\"\n";
        plotcmd(cols[i]);

	gpt = gpt "set origin 0.5," y "\nset size 0.5,0.17\n";
	gpt = gpt "set xlabel\nset ylabel \"" labs[i+6] "\"\n";
	plotcmd(cols[i+6]);
      
        i--; y += 0.17;
    }

    gpt = gpt "set origin 0.0," y "\nset size 0.5,0.17\n";
    gpt = gpt "set title \"" ltitle "\"\nset ylabel \"" labs[1] "\"\n";
    plotcmd(cols[1]);

    gpt = gpt "set origin 0.5," y "\nset size 0.5,0.17\n";
    gpt = gpt "set title \"" rtitle "\"\nset ylabel \"" labs[7] "\"\n";
    plotcmd(cols[7]);

    gpt = gpt "set nomultiplot\n";    
}

function dominimatstats(lscol, ltitle, rscol, rtitle, outfile, \
		            cols, labs) {
    cols[1] = lscol + 2; cols[7] = rscol + 2; labs[1] = labs[7] = "Metals";
    cols[2] = lscol + 3; cols[8] = rscol + 3; labs[2] = labs[8] = "Minerals";
    cols[3] = lscol + 4; cols[9] = rscol + 4; labs[3] = labs[9] = "Oil";
    cols[4] = lscol + 1; cols[10] = rscol + 1;
    labs[4] = labs[10] = "Dodechadrium";
    cols[5] = lscol + 0; cols[11] = rscol + 0; labs[5] = labs[11] = "Fuel";
    cols[6] = lscol + 5; cols[12] = rscol + 5; labs[6] = labs[12] = "Troops";

    twelveplots(ltitle, rtitle, cols, labs);
    writefile(outfile);
}

function doministats(outfile,     cols, labs) {
    cols[1] = 3; labs[1] = "Ships";
    cols[2] = 4; labs[2] = "Shield power";
    cols[3] = 5; labs[3] = "Hull weapon power";
    cols[4] = 6; labs[4] = "Shield weapon power";
    cols[5] = "($8+$9+$10+$11+$12)"; labs[5] = "Sum of max techs";
    cols[6] = 7; labs[6] = "Total techs";

    cols[7] = 2; labs[7] = "Planets";
    cols[8] = 8; labs[8] = "Max hull tech";
    cols[9] = 9; labs[9] = "Max engine tech";
    cols[10] = 10; labs[10] = "Max weapon tech";
    cols[11] = 11; labs[11] = "Max shield tech";
    cols[12] = 12; labs[12] = "Max misc tech";

    twelveplots("Ships", "Other", cols, labs);
    writefile(outfile);
}

function threeplots(title, col1, lab1, col2, lab2, col3, lab3) {
    gpt = "";
    gpt = gpt "set grid\nset lmargin 8\nset rmargin 1\n";
    gpt = gpt "set tmargin 1\nset bmargin 1\n";
    gpt = gpt "set key left reverse\n";
    gpt = gpt "set origin 0.0,0.0\nset size 1,1\nset multiplot\n";

    gpt = gpt "set origin 0.0,0.0\nset size 1,0.33\n";
    gpt = gpt "set xlabel \"Rounds\"\nset ylabel \"" lab3 "\"\n";
    plotcmd(col3);

    gpt = gpt "set origin 0.0,0.33\nset size 1,0.33\n";
    gpt = gpt "set xlabel\nset ylabel \"" lab2 "\"\n";
    plotcmd(col2);

    gpt = gpt "set origin 0.0,0.66\nset size 1,0.33\n";
    gpt = gpt "set title \"" title "\"\nset ylabel \"" lab1 "\"\n";
    plotcmd(col1);

    gpt = gpt "set nomultiplot\n";
}

function domatstats(startcol, title, outfile) {
    threeplots(title, startcol + 2, "Metals", startcol + 3, "Minerals", \
	       startcol + 4, "Oil");
    writefile(outfile);
}

function dodftstats(startcol, title, outfile) {
    threeplots(title, startcol + 1, "Dodechadrium", startcol + 0, "Fuel", \
	       startcol + 5, "Troops");
    writefile(outfile);
}

BEGIN { players = 0; basename = "TEST.stats"; } # XXX

NF > 1 { 
    playername[players] = $2; # XXX
    playernum[players]  = $1;
    players++;
}

END {
  doministats("mini");
  dominimatstats(19, "Total materials", 13, "Material production", "minimat");

  docolstats(2, "Planets", "planets");
  docolstats(3, "Ships", "ships");

  docolstats(4, "Total shield power", "shieldpwr");
  docolstats("($5+$6)", "Total weapon power", "weaponpwr");
  docolstats(5, "Total hull weapon power", "weaponhpwr");
  docolstats(6, "Total shield weapon power", "weaponspwr");
  docolstats(7, "Total tech levels", "tech");

  docolstats("($8+$9+$10+$11+$12)", "Sum of max tech levels", "maxtech");
  docolstats(8, "Max hull tech", "maxtech-h");
  docolstats(9, "Max engine tech", "maxtech-e");
  docolstats(10, "Max weapon tech", "maxtech-w");
  docolstats(11, "Max shield tech", "maxtech-s");
  docolstats(12, "Max misc tech", "maxtech-m");
  
  domatstats(13, "Material production", "matprod");
  dodftstats(13, "Dodechadrium/fuel/troop production", "dftprod");
  domatstats(19, "Total materials", "mattotal");
  dodftstats(19, "Total dodechadrium/fuel/troops", "dfttotal");

  docolstats(25, "Score", "score");
}
