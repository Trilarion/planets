/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import dsc.netgame.*;
import java.util.*;
import java.io.*;
import dsc.util.*;

public class PHNameDatabase implements Serializable {

    private static final int PLANET_NAMES = 8;

    private NameDatabase [] defaultNameDatabase = 
    { new NameDatabase("data/text/names.ship.1.text"),
      new NameDatabase("data/text/names.ship.2.text"),
      new NameDatabase("data/text/names.ship.3.text"),
      new NameDatabase("data/text/names.ship.4.text"),
      new NameDatabase("data/text/names.ship.5.text"),
      new NameDatabase("data/text/names.ship.6.text"),
      new NameDatabase("data/text/names.ship.7.text"),
      new NameDatabase("data/text/names.ship.8.text"),
      new NameDatabase("data/text/names.planet.text") };

    Hashtable databases;

    public PHNameDatabase() {
	databases = new Hashtable();

	Enumeration e = getThemeDirs();
	int player=0;

	while(e.hasMoreElements()) {
	    String s = (String) e.nextElement();

	    for(int i=1; i<=8 ; i++) {
		databases.put(player + ":" + i, 
			      new NameDatabase(s+"/names.ship."+i+".text"));
	    }

	    databases.put("planet:" + player,
			  new NameDatabase(s+"/names.planet.text"));

	    player++;
	}
    }

    private Enumeration getThemeDirs() {
	Vector dirs = new Vector();
	File d = new File("data/text/themes");

	String s [] =d.list();
    
	for (int j = 0; j < s.length-1; j++) {
	    int max = s.length-1;
	    int r = ((int) (Math.random()*((double)(max-j+1))))+j;

	    if(j != r) {
		String tmp = s[r];
		s[r] = s[j];
		s[j] = tmp;
	    }
	}

	for (int i = 0; i < s.length; i++){
	    if(!s[i].equals("CVS"))
		dirs.addElement("data/text/themes/" + s[i]);
	}

	return dirs.elements();
    }


    private NameDatabase getShipDatabaseForPlayer(int player, int tech) {
	NameDatabase db = (NameDatabase) databases.get(player + ":" + tech);

	if(db == null) {
	    return defaultNameDatabase[tech-1];
	} else {
	    return db;
	}
    }

    public String getNewShipName(int player, int tech) {
	return getShipDatabaseForPlayer(player, tech).getNewName();
    }

    private NameDatabase getPlanetDatabaseForPlayer(int player) {
	NameDatabase db = (NameDatabase) databases.get("planet:" + player);

	if(db == null) {
	    return defaultNameDatabase[PLANET_NAMES];
	} else {
	    return db;
	}
    }

    public String getNewPlanetName(int player) {
	return getPlanetDatabaseForPlayer(player).getNewName();
    }
}

