/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import dsc.netgame.*;
import java.util.*;
import dsc.util.*;


public class HostShip extends HostWorldComponent 
                      implements Sortable, MovingObject {


  private Location waypoint;
  private ShipHull hull;
  private Vector engines;
  private Vector weapons;
  private Vector shields;
  private Vector misc;
  private MaterialBundle cargoSpace;
  private int trackTarget;
  private int lastOwner;
  private int currentShieldPower;
  private TargetValuation targetValuation;
  private int surrenderTo;
  private int experience;

  boolean isDestroyed = false;
  boolean runOutOfFuel = false;
  private int shieldDamageThisRound;
  private int hullDamageThisRound;
  private int componentDamageThisRound;
  private ShipReport battleReportThisRound;

  private static final int SHIP_START_EXPERIENCE = 10;
    
  public void putMessage(WorldComponentMessage m) {
    if(m.getSource()!=getOwner()) {
      return;
    }

    if(m instanceof SetWaypointWCM) {
	resetWaypoint();
	resetSurrender();

	Location l;
	l=((SetWaypointWCM) m).getLocation();
	if(l!=null) 
	    waypoint=l;
    }

    if(m instanceof MaterialTransferWCM) {
      MaterialTransferWCM mtw = (MaterialTransferWCM) m;
      mtw.setInitiator(this);
      transferMaterial(mtw);
    }

    if(m instanceof SetTrackingWCM) {
	resetSurrender();
	trackTarget=((SetTrackingWCM) m).getTarget();
	if(trackTarget<-1)
	    trackTarget=-1;
    }

    if(m instanceof ShipComponentStateWCM) {
      setSubcomponentStates((ShipComponentStateWCM) m);
    }

    if(m instanceof FormFleetWCM) {
      formFleet();
    }

    if(m instanceof SetTargetValuationWCM) {
	SetTargetValuationWCM stvw = (SetTargetValuationWCM) m;
	// null as value is OK, it is checked at get*
	this.targetValuation = stvw.getTargetValuation();
    }

    if(m instanceof SurrenderShipWCM) {
	SurrenderShipWCM ssw = (SurrenderShipWCM) m;
	this.surrenderTo =  ssw.getSurrenderTarget();
	resetWaypoint();
    }
  }

    public void resetSurrender() {
	this.surrenderTo = -1;
    }

    public void resetWaypoint() {
	setTrackTarget(-1);
	setWaypoint(getLocation());
    }

    public void setTrackTarget(int targetId) {
	if(targetId<-1)
	    this.trackTarget = -1;
	else
	    this.trackTarget = targetId;
    }

    public void setWaypoint(Location wp) {
	waypoint = (Location) wp.clone();
    }

  private void setSubcomponentStates(ShipComponentStateWCM m) {
    Vector newStates = m.getNewStates();

    if(newStates==null) return;

    Enumeration e = newStates.elements();

    while (e.hasMoreElements()) {
      Object o = e.nextElement();
      
      if(o instanceof ShipComponentState) {
	ShipComponentState scs = (ShipComponentState) o;
	int cid = scs.getShipComponentId();
	boolean nstate = scs.getNewState();

	setSubcomponentState(cid, nstate);
      }
    }
  }

  private void setSubcomponentState(int componentId, boolean newState) {
    findAndSetState(engines, componentId, newState);
    findAndSetState(shields, componentId, newState);
    findAndSetState(weapons, componentId, newState);
    findAndSetState(misc, componentId, newState);
  }

  private void findAndSetState(Vector v, int componentId, boolean newState) {
    Enumeration e = v.elements();

    while (e.hasMoreElements()) {
      ShipComponent sc = (ShipComponent) e.nextElement();
      
      if(sc.getId() == componentId) {
	sc.setActive(newState);
      }
    }
  }

  protected void transferMaterial(MaterialTransferWCM mtm) {
    int target = mtm.getTransferTarget();
    HostWorldComponent hwc=hostWorld.getComponentById(mtm.getTransferTarget());
    
    if(hwc.getOwner() != getOwner()) {
      mtm.forceOneWayTransfer();
      if(hwc instanceof HostShip)
	return;
    }

    if(!hwc.getLocation().equals(location))
      return;


    MaterialBundle targetBundle = new MaterialBundle(MaterialBundle.UNLIMITED);

    if(hwc instanceof HostPlanet) {
      HostPlanet hp = (HostPlanet) hwc;
      targetBundle=hp.getMaterials();
    }

    if(hwc instanceof HostShip) {
      HostShip hs = (HostShip) hwc;
      targetBundle=hs.getMaterials();
    }

    Enumeration validToEnumeration =
      cargoSpace.takeComponents(mtm.getToComponents());

    Enumeration validFromEnumeration =
      targetBundle.takeComponents(mtm.getFromComponents());

    // Do tricks to ensure that all things are first taken out 
    // before anything is put in.
    boolean fromCargoSpaceOK = false;

    if(cargoSpace.subtractFullBundle(mtm.getToMaterials()))
      fromCargoSpaceOK = true;
    
    if(targetBundle.subtractFullBundle(mtm.getFromMaterials()))
      cargoSpace.addFullBundle(mtm.getFromMaterials());

    if(fromCargoSpaceOK) {
      if(hwc.getOwner() != getOwner() &&
	 hwc instanceof HostPlanet) {
	  ((PHHostWorld) hostWorld).addToMaterialUsageStatistics(getOwner(), 
						       mtm.getToMaterials());

	  if(!hostWorld.isAlly(getOwner(), hwc.getOwner())) {
	      int troops = mtm.getToMaterials().getTroops();
	      mtm.getToMaterials().takeTroops(troops);

	      ((HostPlanet) hwc).troopAttack(getOwner(), troops);
	  }

	  /*
	  if(mtm.getToMaterials().getMass() > 0) {
	      chatMessageQueue.put(hwc.getOwner() + "",
					hostWorld.getPlayerName(getOwner()) + 
				     " dropped following materials on your " +
				     "planet " + hwc.getName() + ":" +
				     mtm.getToMaterials().toString()); 
	  }
	  */
      }

      targetBundle.addFullBundle(mtm.getToMaterials());
    }

    while (validToEnumeration.hasMoreElements()) {
      TransferableComponent tc = 
	(TransferableComponent) validToEnumeration.nextElement();

      targetBundle.addComponent(tc);
    }

    while (validFromEnumeration.hasMoreElements()) {
      TransferableComponent tc = 
	(TransferableComponent) validFromEnumeration.nextElement();

      cargoSpace.addComponent(tc);
    }
  }

    private void removeMiscShipComponent(ShipMisc sm) {
		misc.removeElement(sm);
		MaterialBundle newCargoSpace = 
		    new MaterialBundle(cargoSpace.getCapacity() +
				       sm.getMass());
		newCargoSpace.addFullBundle(cargoSpace);
		cargoSpace = newCargoSpace;
    }

  private void handleUpkeepCost() {
      int upkeep = hull.getUpkeepCost();
      int max = ((PHHostWorld) hostWorld).getCredits(getOwner());
      
      if(upkeep > max) {
	  upkeep = max;
      }

      ((PHHostWorld) hostWorld).takeCredits(getOwner(), upkeep);
  }

  public void doRound() {
      if(getOwner() == -1) {
	  takeMeteorDamage();
	  return;
      } else {
	  handleUpkeepCost();
      }

    Vector removedItems = new Vector();

    Enumeration miscItems = misc.elements();
    while(miscItems.hasMoreElements()) {
	ShipMisc sm = (ShipMisc) miscItems.nextElement();

	/* Deploy mine */
	if(sm.getTech() == 5) {
	    if(sm.isActive()) {
		removedItems.addElement(sm);

		HostMine oldField = null;
		Enumeration e=hostWorld.getComponentsByLocation(getLocation());

		while(e.hasMoreElements()) {
		    HostWorldComponent hwc = (HostWorldComponent) 
			e.nextElement();

		    if(hwc.getOwner() == getOwner() &&
		       hwc instanceof HostMine) {
			oldField=(HostMine) hwc;
			break;
		    }
		}

		if(oldField == null) {
		    HostMine hm = new HostMine(hostWorld,
					   getOwner(),
					   (Location) location.clone(),
					   sm);
		    hostWorld.putComponent(hm);
		    hm.setName("Minefield-"+hm.getId());
		} else {
		    oldField.addMines(sm);
		}
	    }
	}
	/* Deploy defense pod */
	else if(sm.getTech() == 2) {
	    HostPlanet hp = overOwnPlanet();
	    if(sm.isActive() && hp!=null) {
		removedItems.addElement(sm);
		hp.addDefensePod(sm.getPowerOutput());
		hostWorld.sendChatMessage(owner, name + " deployed " + 
					  "defense pod on planet " +
					  hp.getName() + ".");
	    }
	}
    }

    Enumeration removed = removedItems.elements();
    while(removed.hasMoreElements()) {
	removeMiscShipComponent((ShipMisc) removed.nextElement());
    }

    Enumeration allComponents = hostWorld.getComponentsByLocation(location);
    boolean consumesIdleFuel  = true;
    
    while(allComponents.hasMoreElements()) {
      HostWorldComponent hwc = 
	(HostWorldComponent) allComponents.nextElement();
      if(hwc instanceof HostPlanet)
	consumesIdleFuel=false;
    }

    if(consumesIdleFuel) {
	addExperience(2);
    } else {
	addExperience(1);
    }

    runOutOfFuel=false;
    if(consumesIdleFuel) {
      int fuelConsumption=hull.getPowerConsumption()+
	ShipMath.calcFuelConsumption(misc);
      int reactorPower = ShipMath.calcReactorPower(misc);

      fuelConsumption-=reactorPower;
      if(fuelConsumption<0)
	  fuelConsumption=0;

      if(!cargoSpace.takeFuel(fuelConsumption)) {
	HostPlanet nearestPlanet = ((PHHostWorld) hostWorld).
				  findNearestPlanet(location);
	String nearestPlanetName = "NONE";
	if(nearestPlanet != null)
	  nearestPlanetName=nearestPlanet.getName();
	
	hostWorld.broadcastChatMessage(name + 
				  ": MAYDAY! We are out of fuel near planet "
				  + nearestPlanetName
				  + ". Using emergency life support." 
				  + " Disabling all non-vital systems.");
	lastOwner=getOwner();
	owner=-1;
	runOutOfFuel=true;
	disableAllComponents();
	resetWaypoint();
	//hostWorld.removeComponent(getId());
	return;
      } else {
	  MaterialBundle fuelConsMat = 
	      new MaterialBundle(MaterialBundle.UNLIMITED);
	  fuelConsMat.putFuel(fuelConsumption);
	  ((PHHostWorld) hostWorld).addToMaterialUsageStatistics(getOwner(), 
								 fuelConsMat);
      }
    }
  }

    private void takeMeteorDamage() {
	int max = hull.getMaxDamage();
	hull.addDamage((max*2)/100);
	if(hull.isDestroyed()) {
	    hostWorld.removeComponent(getId());
	}
    }

  public void runOrbitalBombard() {
      Enumeration e;

      if(getTargetValuation().isBombardingEnabled()) {
	  e = hostWorld.getComponentsByLocation(location);

	  while(e.hasMoreElements()) {
	      HostWorldComponent hwc = (HostWorldComponent) e.nextElement();
	      if(hwc instanceof HostPlanet) {
		  if(!hostWorld.isAlly(getOwner(), hwc.getOwner()) &&
		     hwc.getOwner() != -1)
		      bombardPlanet((HostPlanet) hwc);
	      }
	  }
      }

    e = hostWorld.getComponents();

    while(e.hasMoreElements()) {
	HostWorldComponent hwc = (HostWorldComponent) e.nextElement();
	if(hwc instanceof HostMine) {
	    if(!hostWorld.isAlly(getOwner(), hwc.getOwner()) &&
	       hwc.getOwner() != -1) {
		if(getLocation().distance(hwc.getLocation()) <
		   (((HostMine) hwc).getRadius()+35)) {
		    bombardMine((HostMine) hwc);
		}
	    }
	}
    }
  }

  private void bombardMine(HostMine mine) {
    int damage=getRandomWeaponHullDamage() + getRandomWeaponShieldDamage();
    if(damage==0)
      return;

    damage*=4;
    int distance = mine.getLocation().distance(getLocation());
    
    if(distance > 90)
	distance = 90;

    if(distance >= 30)
	damage/=(distance/30);

    mine.takeDamage(damage, this);
  }


  private void bombardPlanet(HostPlanet planet) {
    MaterialBundle planetMaterials = planet.getMaterials();
    if(planetMaterials.getTroops()==0)
      return;

    int damage=getRandomWeaponTroopDamage();
    if(damage==0)
      return;

    int retaliationDamage = planet.getDefenseDamage();
    if(retaliationDamage > 0 && (hull.getTech() != 8)) {
	retaliationDamage = retaliationDamage * hull.getVolume() / 
	    HostPlanet.DEFENSE_DIVISOR;

	takeDamageFromPlanet(planet, retaliationDamage);
	if(hull.isDestroyed()) {
	    return;
	}
    }

    if(planetMaterials.getTroops() < damage)
      damage=planetMaterials.getTroops();

    planetMaterials.takeTroops(damage);

    hostWorld.sendChatMessage(owner, name + " bombarded " + 
			      hostWorld.getPlayerName(planet.getOwner()) + "\'s" +
			      " planet " + planet.getName() + " killing " +
			      damage + " troops (" + 
			      planetMaterials.getTroops() +
			      " remaining)."); 

    hostWorld.sendChatMessage(planet.getOwner(), 
			      hostWorld.getPlayerName(getOwner()) + "\'s" +
			      " ship " + name + " bombarded " + 
			      "your" +
			      " planet " + planet.getName() + " killing " +
			      damage + " troops (" + 
			      planetMaterials.getTroops() +
			      " remaining)."); 

  }

    private void takeDamageFromPlanet(HostPlanet planet, int amount) {
	hull.addDamage(amount);

	hostWorld.sendChatMessage(planet.getOwner(), 
				  "Your planet " + planet.getName() + 
				  " fired at " + 
				  hostWorld.getPlayerName(getOwner()) + "\'s" +
				  " ship " + name + " for " + 
				  amount + " hull points.");

	hostWorld.sendChatMessage(owner, 
				  hostWorld.getPlayerName(
				      planet.getOwner()) + "\'s" +
				  " planet " + planet.getName() + 
				  " fired at" + 
				  " your " + 
				  " ship " + name + " for " + 
				  amount + " hull points (" +
				  (100-hull.getDamagePercentage()) + "%" +
				  " remaining).");


	if(hull.isDestroyed()) {
	    hostWorld.removeComponent(this.getId());
	    hostWorld.sendChatMessage(owner, "Ship " + name +
				      " has been destroyed by defense fire" +
				      " over " + planet.getName() + ".");
	    hostWorld.sendChatMessage(planet.getOwner(),
				      "Your planet " + planet.getName() +
				      " destroyed " + 
				      hostWorld.getPlayerName(getOwner()) +
				      "\'s ship " + name + ".");
	} 
    }


  private void disableAllComponents() {
    disableAllComponents(engines);
    disableAllComponents(weapons);
    disableAllComponents(shields);
    disableAllComponents(misc);
  }

  private void disableAllComponents(Vector v) {
    Enumeration e = v.elements();

    while (e.hasMoreElements()) {
      ShipComponent sc = (ShipComponent) e.nextElement();
      sc.setActive(false);
    }
  }

  public void doMovement() {
    if(trackTarget != -1) {
	HostWorldComponent hwc = hostWorld.getComponentById(trackTarget);

      if(hostWorld.isVisibleByPlayer(getOwner(), hwc) ||
	 hwc instanceof HostPlanet) {
	waypoint=hwc.getLocation();
      }
    }

    repairDamage();

    if(!waypoint.equals(location)) {
      int speed       = ShipMath.speed(getMass(), getPower());
      int consumption = ShipMath.calcFuelConsumption(engines);
      int idleConsumption = hull.getPowerConsumption() +
	  ShipMath.calcFuelConsumption(misc);
      int reactorPower    = ShipMath.calcReactorPower(misc);
      int distance    = location.distance(waypoint);

      reactorPower-=idleConsumption;
      if(reactorPower<0)
	  reactorPower=0;
      consumption-=reactorPower;
      if(consumption<0)
	  consumption=0;

      if(speed!=0) {
	if(distance<speed) {
	  consumption=consumption*distance/speed;
	  speed=distance;
	}	
	if(consumption>cargoSpace.getFuel()) {
	  speed=speed*cargoSpace.getFuel()/consumption;
	  consumption=cargoSpace.getFuel();
	}

	cargoSpace.takeFuel(consumption);
	MaterialBundle consMat = new MaterialBundle(MaterialBundle.UNLIMITED);
	consMat.putFuel(consumption);
	((PHHostWorld) hostWorld).addToMaterialUsageStatistics(getOwner(), 
							       consMat);

	location=location.moveTowards(waypoint, speed); 
	takeMineDamage();
      }
    }
  }

    public int getNextMovementAmount() {
      int consumption = ShipMath.calcFuelConsumption(engines);
      int idleConsumption = hull.getPowerConsumption() +
	  ShipMath.calcFuelConsumption(misc);
      int reactorPower    = ShipMath.calcReactorPower(misc);

      if(reactorPower <= idleConsumption) {
	  idleConsumption -= reactorPower;
      } else {
	  reactorPower -= idleConsumption;
	  idleConsumption = 0;
	  if(reactorPower <= consumption) {
	      consumption -= reactorPower;
	  } else {
	      consumption = 0;
	  }
      }

      if(idleConsumption>cargoSpace.getFuel())
	  idleConsumption = cargoSpace.getFuel();

      int fuel = cargoSpace.getFuel();
      int speed       = ShipMath.speed(getMass(), 
				       getPower());

      if(speed!=0) {
	if(consumption>fuel) {
	    if(consumption != 0)
		speed=(speed*fuel)/consumption;
	}

	return speed-1;
      }

      return 0;
    }

    private void takeMineDamage() {
	Enumeration e = hostWorld.getComponents();
	boolean inMinefield = false;

	while(e.hasMoreElements()) {
	    HostWorldComponent hwc = (HostWorldComponent) e.nextElement();
	    if(hwc instanceof HostMine) {
		if(!hostWorld.isAlly(hwc.getOwner(), getOwner()) &&
		   hwc.getOwner() != -1) {
		    if(getLocation().distance(hwc.getLocation()) <
		       ((HostMine) hwc).getRadius() &&
		       ((HostMine) hwc).isEffective()) {
			inMinefield=true;
		    }
		}
	    }
	}
	
	if(inMinefield) {
	    takeDamageFromMine(hull.getVolume()*4);
	}
    }

    private void takeDamageFromMine(int amount) {
	int shieldAmount = amount;
	int hullAmount   = 0;
	int shieldPower  = getCurrentShieldPower() -
	    getCurrentPersistentShieldPower();

	if(shieldPower < 0)
	    shieldPower = 0;

	if(shieldAmount > shieldPower) {
	    shieldAmount = shieldPower;
	    hullAmount   = (amount - shieldAmount)/15;
	}

	takeShieldDamage(shieldAmount);
	hull.addDamage(hullAmount);

	amount=shieldAmount + hullAmount;

	if(hull.isDestroyed()) {
	    hostWorld.removeComponent(this.getId());
	    hostWorld.sendChatMessage(owner, "Ship " + name +
				      " took " + amount + 
				      " damage from minefield (S" +
				      shieldAmount + "/H" + hullAmount +
				      ") and exploded.");
	} else {
	    hostWorld.sendChatMessage(owner, "Ship " + name +
				      " took " + amount + 
				      " damage from minefield (S" +
				      shieldAmount + "/H" + hullAmount +
				      ").");
	}


	
    }

    void checkSurrender() {
	/* Check voluntary surrender */
	if(!runOutOfFuel && surrenderTo != -1) {
	    Enumeration e = hostWorld.getComponentsByLocation(location);

	    while(e.hasMoreElements()) {
		HostWorldComponent hwc = (HostWorldComponent) e.nextElement();
		if(hwc.getOwner() == surrenderTo && hwc instanceof HostShip) {
		    HostShip otherShip = (HostShip) hwc;
		    hostWorld.sendChatMessage(owner, 
					      "Ship " + getName() + 
					      " surrendered to " +
					      hostWorld.getPlayerName(surrenderTo) +
					      "'s ship " + otherShip.getName() +
					      ".");

		    hostWorld.sendChatMessage(surrenderTo,
					      hostWorld.getPlayerName(getOwner()) +
					      "'s ship " + getName() +
					      " surrendered to your ship " +
					      otherShip.getName() + "."); 
		    
		    lastOwner = owner = surrenderTo;
		    resetComponentActivities();
		    resetWaypoint();
		    targetValuation = null;
		    experience = SHIP_START_EXPERIENCE;
		    break;
		}
	    }
	}

	surrenderTo = -1; /* Reset voluntary surrender order */

	/* Check involuntary surrender */
	if(!runOutOfFuel)
	    return;

	Enumeration e = hostWorld.getComponentsByLocation(location);

	boolean ownShip = false;
	boolean enemyShip = false;
	int enemyShipOwner = -1;

	while(e.hasMoreElements()) {
	    HostWorldComponent hwc = (HostWorldComponent) e.nextElement();
	    if((hwc instanceof HostShip) &&
	       !((HostShip) hwc).isHelpless()) {
		if((hwc.getOwner() == lastOwner))
		    ownShip=true;
		else {
		    if(!hostWorld.isAlly(hwc.getOwner(), getOwner())) {
			enemyShip=true;
			enemyShipOwner=hwc.getOwner();
		    }
		}
	    }
	}

	int newOwner=enemyShipOwner;
	if(ownShip)
	    newOwner=lastOwner;

	if(ownShip || enemyShip) {
	    owner=newOwner;
	    resetComponentActivities();

	    if(ownShip) {
		hostWorld.sendChatMessage(owner, 
					  "Ship " + 
					  name + " is again under your control.");
		experience = SHIP_START_EXPERIENCE;
	    }	else {
		hostWorld.sendChatMessage(owner, "Ship " + name + " was boarded by your ship.");
		experience = SHIP_START_EXPERIENCE;
	    }
	} else {
	    // Take damage in deep space

	    hull.addDamage(hull.getMaxDamage()/50);
	    if(hull.isDestroyed()) {
		hostWorld.removeComponent(getId());
	    }
	}
    }

  public int getSpeed() {
    int speed       = ShipMath.speed(getMass(), getPower());
    int reactorPower = ShipMath.calcReactorPower(misc);

    if(cargoSpace.getFuel() > 0 || reactorPower > 0)
	return speed;
    
    return 0;
  }

  public WorldComponent getWorldComponent(int player) {
    
    if(hostWorld.isVisibleByPlayer(player, this)) {
      Ship p=new Ship(id, name, owner, location);
      
      if(runOutOfFuel)
	p.setScanRange(-1);
      else
	p.setScanRange(150);
      p.setHullTech(hull.getTech());
      p.setShieldPower(getCurrentShieldPower());

      if(isInFleet())
	  p.setBearing(getFleet().getBearing());
      else
	  p.setBearing(location.getBearingTo(waypoint));

      if(ShipMath.calcCloakPower(misc) > 0)
	  p.setCloak(true);

      if(player == owner) {
	p.setHull((ShipHull) hull.clone());
	p.setEngines(cloneVector(engines));
	p.setWeapons(cloneVector(weapons));
	p.setShields(cloneVector(shields));
	p.setMisc(cloneVector(misc));
	p.setCargo((MaterialBundle) cargoSpace.clone());
	p.setTrackTarget(trackTarget);
	p.setWaypoint(waypoint);
	p.setTargetValuation(targetValuation);
	p.setScanRange(getScanRange());
      } else {
	  int sp, msp;

	  sp  = getCurrentShieldPower();
	  msp = ShipMath.calcMaxShieldPower(shields);

	  p.setEnemyInfoText("Owned by " + 
			     hostWorld.getPlayerName(getOwner()) + "\n\n" +
			     "Hull: " + 
			     (hull.getMaxDamage()-hull.getDamageReceived()) +
			     "/" +
			     hull.getMaxDamage() +
			     " (" +
			     (100-hull.getDamagePercentage()) +
			     "%)\n" +
			     "Shields: " +
			     sp + 
			     "\n");
      }

      return p;
    } else
      return null;
  }


  public HostShip(HostWorld hw, int owner, Location location) {
    super(hw,owner,location);
    
    waypoint = location;
    name="Unset"; 
    trackTarget=-1;
    currentShieldPower=0;
    targetValuation = null;
    experience = SHIP_START_EXPERIENCE;

    engines=new Vector();
    weapons=new Vector();
    shields=new Vector();
    misc   = new Vector();

    surrenderTo = -1;
  }

    private void activateAll(Enumeration e) {
	while(e.hasMoreElements()) {
	    ShipComponent sc = (ShipComponent) e.nextElement();
	    sc.setActive(true);
	}
    }

    private void activateReactors(Enumeration e) {
	while(e.hasMoreElements()) {
	    ShipMisc sc = (ShipMisc) e.nextElement();
	    if(sc.getTech() == 3 || sc.getTech() == 6)
		sc.setActive(true);
	}
    }

    private void resetComponentActivities() {
	activateAll(engines.elements());
	activateAll(weapons.elements());
	activateAll(shields.elements());
	activateReactors(misc.elements());
    }

  public void setComponents(ShipHull hull, Enumeration components) {
    this.hull=hull;

    name=hull.getName();

    while(components.hasMoreElements()) {
      ShipComponent sc = (ShipComponent) components.nextElement();

      if(sc instanceof ShipEngine) {
	engines.addElement(sc);
      }
      if(sc instanceof ShipWeapon) {
	weapons.addElement(sc);
      }
      if(sc instanceof ShipShield) {
	shields.addElement(sc);
      }
      if(sc instanceof ShipMisc) {
	  misc.addElement(sc);
	  sc.setActive(false);
      }
    }

    activateReactors(misc.elements());

    cargoSpace=new MaterialBundle(hull.getVolume() -
				  ShipMath.calculateMass(engines.elements()) -
				  ShipMath.calculateMass(weapons.elements()) -
				  ShipMath.calculateMass(shields.elements()) -
				  ShipMath.calculateMass(misc.elements()));

    currentShieldPower = ShipMath.calcMinShieldPower(shields);
  }

  public boolean isVisible() {
    return true;
  }

  public static int getBuildTimeByComponents(int n) {
    return (n/5)+1;
  }

  private static Vector cloneVector(Vector v) {
    Enumeration e = v.elements();
    Vector n = new Vector();

    while(e.hasMoreElements()) {
      TransferableComponent tc = (TransferableComponent) e.nextElement();
      n.addElement(tc.clone());
    }

    return n;
  }

  public int getMass() {
    int mass=hull.getMass();

    mass+=ShipMath.calculateMass(engines.elements());
    mass+=ShipMath.calculateMass(weapons.elements());
    mass+=ShipMath.calculateMass(shields.elements());
    mass+=ShipMath.calculateMass(misc.elements());
    mass+=cargoSpace.getMass();

    return mass;
  }

  public int getPower() {
    return ShipMath.calcEnginePower(engines);
  }

  public int getKey() {
    int key=location.getX();
    key+=location.getY()<<16;
    return key;
  }

  public int greaterThan(Sortable s) {
    HostShip hs = (HostShip) s;

    if(getKey() > hs.getKey())
      return 1;
    if(hs.getKey() > getKey())
      return -1;

    return 0;
  }

  public void initBattle() {
    shieldDamageThisRound=0;
    hullDamageThisRound=0;
    componentDamageThisRound=0;
    battleReportThisRound = new ShipReport(getName(), 
					   getOwner(),
					   hull.getTech());
  }

  private int getCurrentShieldPower() {
    int max = ShipMath.calcShieldPower(shields);

    if(currentShieldPower > max)
      currentShieldPower = max;

    return currentShieldPower;
  }

    private int getCurrentPersistentShieldPower() {
	int min = ShipMath.calcMinShieldPower(shields);
	return min;
    }

    private void addCurrentShieldPower(int amount) {
	int max = ShipMath.calcShieldPower(shields);
	currentShieldPower += amount;

	if(currentShieldPower > max)
	    currentShieldPower = max;
    } 

    private void takeShieldDamage(int amount) {
	int min = ShipMath.calcMinShieldPower(shields);
	currentShieldPower -= amount;

	if(currentShieldPower < min)
	    currentShieldPower = min;
    } 


  private int getAbsorbedDamage(int damage, int capasity) {
    if(damage==0)
      return 0;

    /*
    if(capasity/damage >= 15) {
      return damage;
    }
    */

    int ab = (damage*72)/100+1;
    if(ab>capasity)
      ab=capasity;
    if(ab>damage)
      ab=damage;

    return ab;
  }

  private void addToVector(Vector v, Enumeration e) {
    while (e.hasMoreElements()) {
      Object o=e.nextElement();
      v.addElement(o);
    }
  }

  private Vector getComponents() {
    Vector v = new Vector();
    
    addToVector(v, shields.elements());
    addToVector(v, engines.elements());
    addToVector(v, weapons.elements());
    addToVector(v, misc.elements());

    return v;
  }

  public Enumeration getRecycleComponents() {
    Vector v = getComponents();
    Vector r = new Vector();
    Enumeration e = v.elements();

    while (e.hasMoreElements()) {
      ShipComponent sc = (ShipComponent) e.nextElement();
      sc.setActive(true);

      if(sc.getDamagePercentage() != 100)
	r.addElement(sc);
    }

    r.addElement(hull);
    
    return r.elements();
  }

  public boolean takeDamage(int hullAmount, 
			    int shieldAmount, 
			    int directComponentAmount,
			    HostShip whoShot) {


      int shield_damage = 0;

      if(getCurrentShieldPower() > 0) {
        shield_damage = (shieldAmount*getCurrentShieldPower())/
	    (hull.getVolume() + getCurrentShieldPower());

	if(shieldAmount > 0 && shield_damage <= 0)
	    shield_damage = 1;
      }

      int hull_basic_damage = 0;
      int hullpoints_left   = hull.getMaxDamage() -
	  hull.getDamageReceived();

      if(hullpoints_left > 0) {
	  hull_basic_damage = (hullAmount*hull.getVolume())/
	      (hull.getVolume() + getCurrentShieldPower());

	if(hullAmount > 0 && hull_basic_damage <= 0)
	    hull_basic_damage = 1;
      }

    int hull_damage   = getAbsorbedDamage(hull_basic_damage,
					  hullpoints_left);
    int component_damage = hull_basic_damage - hull_damage +
	directComponentAmount;

    shieldDamageThisRound+=shield_damage;
    takeShieldDamage(shield_damage);

    hull.addDamage(hull_damage);
    hullDamageThisRound+=hull_damage;

    Vector allComponents = getComponents();

    if(allComponents.size() == 0) {
      hull.addDamage(component_damage);
      hullDamageThisRound+=component_damage;
    } else {
      componentDamageThisRound+=component_damage;
      double temp = Math.random() * ((double) allComponents.size());
      int nro = ((int) temp);
      if(nro == allComponents.size()) nro--;
      
      ShipComponent sc = (ShipComponent) allComponents.elementAt(nro);
      sc.addDamage(component_damage);
    }
  
    if(hull.getDamagePercentage() >= 100 && !isDestroyed) {
      isDestroyed=true;
      hostWorld.removeComponent(getId());
      return false;
    }

    return true;
  }

  private HostShip getRandomTarget(Vector ships) {

    if(ships.size()==0) {
      return null;
    } 

    double temp = Math.random() * ((double) ships.size());
    int nro = ((int) temp);
    if(nro == ships.size()) nro--;
    
    return (HostShip) ships.elementAt(nro);
  }

  private HostShip getGoodTarget(Vector ships) {

    if(ships.size()==0) {
      return null;
    } 

    if(ships.size()==1) {
      return (HostShip) ships.elementAt(0);
    }

    Enumeration e = ships.elements();
    HostShip bestHS = (HostShip) e.nextElement();

    while (e.hasMoreElements()) {
      HostShip hs = (HostShip) e.nextElement();
      if(hs.getValueAsTarget(getTargetValuation()) > 
	 bestHS.getValueAsTarget(getTargetValuation()))
	bestHS=hs;
    }

    return bestHS;
  }

  public HostShip selectShootingTarget(Vector possibleTargets,
				       Vector shootedSomeone) {

    if(possibleTargets.size() == 0)
      return null;

    if(shootedSomeone.size() == 0)
      return getGoodTarget(possibleTargets);

    return getGoodTarget(shootedSomeone);
  }

  public void doPreBattleRoundActions() {
  }

  private TargetValuation getTargetValuation() {
      if(targetValuation != null)
	  return targetValuation;

      if(isInFleet()) {
	  return getFleet().getTargetValuation();
      } else {
	  return new TargetValuation();
      }
  }

  public int getValueAsTarget(TargetValuation tv) {
      return tv.getTargetValue(ShipHull.getMaxDamage(hull.getTech()) - hull.getDamageReceived(),
			       getCurrentShieldPower(),
			       getCurrentPersistentShieldPower(),
			       battleReportThisRound.getHullAttack(),
			       battleReportThisRound.getShieldAttack(),
			       getMass(),
			       hull.getVolume());
  }

  public int getWeaponHullDamage() {
    int total=getRandomWeaponHullDamage();
    battleReportThisRound.addHullAttack(total);
    return total;
  }

  public int getWeaponShieldDamage() {
    int total=getRandomWeaponShieldDamage();
    battleReportThisRound.addShieldAttack(total);
    return total;
  }

  public int getRandomWeaponHullDamage() {
    Enumeration e = weapons.elements();
    int total=0;
    
    while (e.hasMoreElements()) {
      ShipWeapon sw = (ShipWeapon) e.nextElement();
      
      total+=sw.getInflictedHullDamage();
    }

    return total;
  }

  public int getRandomWeaponShieldDamage() {
    Enumeration e = weapons.elements();
    int total=0;
    
    while (e.hasMoreElements()) {
      ShipWeapon sw = (ShipWeapon) e.nextElement();
      
      total+=sw.getInflictedShieldDamage();
    }

    return total;
  }

  public int getRandomWeaponTroopDamage() {
    Enumeration e = weapons.elements();
    int total=0;
    
    while (e.hasMoreElements()) {
      ShipWeapon sw = (ShipWeapon) e.nextElement();
      
      total+=sw.getInflictedTroopDamage();
    }

    return total;
  }


  public int getDamagePercentage() {
    return hull.getDamagePercentage();
  }

  public String getBattleSummary() {
    String s= "Ship \"" + name + "\": ";
    s+="Shields absorbed " + shieldDamageThisRound + ", ";
    s+="Hull took " + hullDamageThisRound + ", ";
    s+="Components took " + componentDamageThisRound + " pts of damage. ";

    if(isDestroyed)
      s+="Ship was destroyed in the battle!";
	    
    return s;
  }

  public boolean orbitingAnyPlanet() {
    Enumeration e = hostWorld.getComponentsByLocation(location);

    while (e.hasMoreElements()) {
      HostWorldComponent hwc = (HostWorldComponent) e.nextElement();

      if(hwc instanceof HostPlanet) {
	  return true;
      }
      else
	  return false;
    }

    return false;
  }

    public boolean isAtAlliedSpaceStation() {
	Enumeration e = hostWorld.getComponentsByLocation(location);

	while (e.hasMoreElements()) {
	    HostWorldComponent hwc = (HostWorldComponent) e.nextElement();

	    if(hwc instanceof HostShip &&	     
	       ((HostShip) hwc).getHullTech() == 5) {
		if(hostWorld.isAlly(hwc.getOwner(), owner) &&
		   hostWorld.isAlly(owner, hwc.getOwner()))
		    return true;
		else
		    return false;
	    }
	}

	return false;
    }

  public void repairDamage() {
    int amount=0;
    boolean atSpaceStation = isAtAlliedSpaceStation();
    boolean atPlanet       = orbitingAnyPlanet();

    if(atSpaceStation && waypoint.equals(location)) {
	hull.repairDamage(hull.getMaxDamage()/50+1);
	amount=4;
    } else if(atPlanet && waypoint.equals(location)) {
	hull.repairDamage(hull.getMaxDamage()/200+1);
	amount=2;
    } else {
	hull.repairDamage(hull.getMaxDamage()/400+1);
	amount=1;
    }

    Vector components = getComponents();
    
    for(int i=0; i<components.size(); i++) {
      ShipComponent sc = (ShipComponent) components.elementAt(i);
      sc.repairDamage(((sc.getMaxDamage()*amount)/100)+1);
    }

    int diff = 
	getCurrentPersistentShieldPower() -
	getCurrentShieldPower();

    if(diff > 0) {
	addCurrentShieldPower(diff);
    }

    if(waypoint.equals(location))
	addCurrentShieldPower(ShipMath.calcShieldPower(shields)/62 +
			      ShipMath.calcEnginePower(engines)/12);
  }

  public MaterialBundle getMaterials() {
    return cargoSpace;
  }

  public String toString() {
    return getName();
  }

  public int getScanRange() {
    if(runOutOfFuel)
      return -1;
    else {
	int maxScanrange = hull.getScanRange();
	
	Enumeration e = misc.elements();

	while(e.hasMoreElements()) {
	    ShipMisc sm = (ShipMisc) e.nextElement();
	    
	    if(sm.getTech() == 1) {
		if(sm.getPowerOutput() > maxScanrange)
		    maxScanrange = sm.getPowerOutput();
	    }
	}

	return maxScanrange;
    }
  }

  public boolean isHelpless() {
    return runOutOfFuel;
  }

  ShipReport getBattleReportMessage() {
    battleReportThisRound.setAliveness(!isDestroyed);
    battleReportThisRound.setShieldDamage(shieldDamageThisRound);
    battleReportThisRound.setHullDamage(hullDamageThisRound);
    battleReportThisRound.setComponentDamage(componentDamageThisRound);
    battleReportThisRound.setShieldsLeft(getCurrentShieldPower());
    battleReportThisRound.setHullDamageTotal(hull.getDamageReceived());

    return battleReportThisRound;
  }

  public boolean canAttack() {
    Enumeration e = weapons.elements();
    int total=0;
    
    while (e.hasMoreElements()) {
      ShipWeapon sw = (ShipWeapon) e.nextElement();
      
      total+=sw.getInflictableHullDamage() + sw.getInflictableShieldDamage();
    }

    return (total>0);
  }

  public boolean isTrackingSomeone() {
      if(trackTarget < 0)
	  return false;
      else
	  return true;
  }

    public boolean nearWormhole() {
	Enumeration e = hostWorld.getComponents();

	while(e.hasMoreElements()) {
	    HostWorldComponent hwc = (HostWorldComponent) e.nextElement();
	    if(hwc instanceof HostWormhole) {
		if(hwc.getLocation().distance(getLocation()) <
		   HostWormhole.CLOAK_PREVENT_RADIUS)
		    return true;
	    }
	}

	return false;
    }

  public double getScanRangeDivisor() {
      int cloakPower = ShipMath.calcCloakPower(misc);
      if(nearWormhole()) {
	  cloakPower = 0;
      }

      if(cloakPower == 0)
	  return 1.0;

      if(cloakPower >= hull.getVolume()) {
	  return 10.0;
      }

      if(cloakPower*10 <= hull.getVolume()) {
	  return 1.052;
      }

      return 10.0/((double) hull.getVolume() / (double) cloakPower);
  }

  public void checkSuperBombs() {
    Enumeration e = misc.elements();
    
    while(e.hasMoreElements()) {
      ShipMisc sm = (ShipMisc) e.nextElement();
	    
      if(sm.getTech() == 8 && sm.isActive() && !sm.isDestroyed()) {
	/* Superbomb explosion */
	
	Enumeration en = hostWorld.getComponentsByLocation(location);

	while (en.hasMoreElements()) {
	  HostWorldComponent hwc = (HostWorldComponent) en.nextElement();

	  if(hwc instanceof HostPlanet) {
	    ((HostPlanet) hwc).takeSuperBombDamage();
	  } else {
	    hostWorld.removeComponent(hwc.getId());
	  }
	}

	HostPlanet nearestPlanet = ((PHHostWorld) hostWorld).
				  findNearestPlanet(location);
	String nearestPlanetName = "NONE";
	if(nearestPlanet != null)
	  nearestPlanetName=nearestPlanet.getName();
	
	hostWorld.broadcastChatMessage( 
				  "WARNING! Superbomb explosion detected " +
				  "near planet " +
				  nearestPlanetName
				  + "!");
      }
    }
  }

    private void formFleet() {
	/* Do not form fleet if we already are in one. */
	if(isInFleet())
	    return;
	
	HostFleet hf = new HostFleet(hostWorld,
				     getOwner(),
				     (Location) location.clone(),
				     (Location) waypoint.clone(),
				     trackTarget);

	hostWorld.putComponent(hf);
	hf.setName("Fleet-"+hf.getId());

	hostWorld.sendChatMessage(getOwner(), 
				  getName() + " formed " + hf.getName() + ".");

	// Join the new fleet
	trackTarget = hf.getId();
	setWaypoint(location);

	NewFleetMessage nfm = new NewFleetMessage(hf, this);
	hostWorld.sendMessage(nfm);
    }

    HostFleet getFleet() {
	HostWorldComponent hwc = hostWorld.getComponentById(trackTarget);
	if((hwc != null) &&
	   (hwc instanceof HostFleet) && 
	   (hwc.getLocation().equals(getLocation())) &&
	   (hwc.getOwner() == getOwner()) &&
	   (trackTarget == hwc.getId())) {
	    return (HostFleet) hwc;
	}

	return null;
    }

    boolean isInFleet() {
	if(getFleet() == null)
	    return false;
	else
	    return true;
    }

    public int getTrackTarget() {
	return trackTarget;
    }

    private HostPlanet overOwnPlanet() {
	Enumeration e = hostWorld.getComponentsByLocation(getLocation());
	
	while(e.hasMoreElements()) {
	    HostWorldComponent hwc = (HostWorldComponent) e.nextElement();
	    if((hwc instanceof HostPlanet) &&
	       (hwc.getOwner() == getOwner()))
		return (HostPlanet) hwc;
	}

	return null;
    }

    public int putMaxFuel(int amount) {
	int max = getMaxLoadableFuelAmount();

	if(amount>max)
	    amount=max;
	
	cargoSpace.putFuel(amount);
	return amount;
    }

    public int forcePutFuel(int amount) {
	int max = cargoSpace.getFreeSpace();

	if(amount>max)
	    amount=max;

	cargoSpace.putFuel(amount);
	return amount;
    }

    public int getMaxLoadableFuelAmount() {
	int amount=cargoSpace.getFreeSpace();
	int minAmount = getIdleConsumption();
	minAmount -= cargoSpace.getFuel();
	if(amount < minAmount)
	    return amount;

	if((amount+hull.getMass()+hull.getVolume()-cargoSpace.getFreeSpace())
	   > ShipMath.calcEnginePower(engines))
	    amount = ShipMath.calcEnginePower(engines) -
		hull.getMass() - hull.getVolume() + cargoSpace.getFreeSpace();

	if(amount < minAmount)
	    amount = minAmount;

	if(amount>0)
	    return amount;

	return 0;
    }

    public int getIdleConsumption() {
	int idleConsumption = hull.getPowerConsumption() +
	    ShipMath.calcFuelConsumption(misc);
	int reactorPower = ShipMath.calcReactorPower(misc);

	idleConsumption-=reactorPower;
	if(idleConsumption<0)
	    idleConsumption = 0;

	return idleConsumption;
    }

    public int getFuelConsumptionForOneRound() {
	int consumption = ShipMath.calcFuelConsumption(engines);
	int idleConsumption = hull.getPowerConsumption() +
	    ShipMath.calcFuelConsumption(misc);
	int reactorPower    = ShipMath.calcReactorPower(misc);
	int totalConsumption = consumption + idleConsumption;

	totalConsumption-=reactorPower;
	if(totalConsumption<0)
	    return 0;
	else
	    return totalConsumption;
    }

    public int getHullTech() {
	return hull.getTech();
    }

    public HostWorldComponent getEffectiveTrackTarget() {
	if(getSpeed() == 0)
	    return null;

	HostWorldComponent target = 
	    hostWorld.getComponentById(trackTarget);

	if(target == null)
	    return null;

	if(target instanceof MovingObject) {
	    if(target instanceof HostFleet &&
	       getFleet() == target) {
		return ((HostFleet) target).getEffectiveTrackTarget();
	    }

	    return target;   
	} 

	return null;
    }

    private Location getEffectiveWaypoint() {
	HostWorldComponent hwc = getEffectiveTrackTarget();

	if(hwc == null)
	    return waypoint;

	return hwc.getLocation();
    }

    public int getETA() {
	int speed = getSpeed();

	if(speed == 0)
	    return Integer.MAX_VALUE;

	return (100*getEffectiveWaypoint().distance(getLocation()))/speed;
    }

    protected void moveToLocation(Location newLocation) {
	if(waypoint.equals(getLocation())) {
	    setLocation(newLocation);
	    setWaypoint(newLocation);
	}
    }

    public int getExperience() {
	return experience;
    }

    public void addExperience(int newExp) {
	if(newExp<0)
	    return;

	experience += newExp;
    }

    public int getExpValue() {
	return hull.getMaxDamage();
    }
}
