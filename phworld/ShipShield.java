/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;


public class ShipShield extends ShipComponent {

  private static int mass[] = {0,25,30,38,44,50,55,55,55};

  private static int fuel[] = {0,0,0,0,0,0,0,0,0};
  private static int dodechadrium[] = {0,0,0,0,0,0,0,0,2};
  private static int metals[] =   {0, 8,15,27,41,55, 75, 85,110};
  private static int minerals[] = {0,11,20,31,50,70,100,120,170};
  private static int oil[] =      {0, 8,20,27,34,45, 65, 85,110};
  private static int troops[] = {0,0,0,0,0,0,0,0,0}; 

  private static int techFuel[] = {0,0,0,0,0,0,0,0,0};
  private static int techDodechadrium[] = {0,0,0,0,0,0,0,125,350};
  private static int techMetals[]   = smallTechRequirement;
  private static int techMinerals[] = mediumTechRequirement;
  private static int techOil[]      = highTechRequirement;
  private static int techTroops[] = {0,0,0,0,0,0,0,0,0};


  private static int powerConsumption[] = {0,0,0,0,0,0,0,0,0}; 
  private static int shieldPower[] =  {0,130,1200,380,2400,650,4200,4500,8500};
  private static int minShieldPower[]={0,110,   0,330,   0,550,   0, 575, 800};
  private static int damageResistance[] = {0,55,45,115,95,
					   225,175,275,450}; 

    private static String[] gadgetName = {
	"0",
	"SubMag fd",
	"Screen",
	"Graviton fd",
	"Barrier",
	"Harmonic fd",
	"Phoenix",
	"Polymorphic",
	"Stasis fd"
    };

//CONSTRUCTOR
  public ShipShield(int tech) {
    super(tech);
  }

  /** Return maxinum shield power. The damage is NOT taken into account.
   *
   * @return Maxinum shield power.
   */

  public int getMaxShieldPower() {
    return (shieldPower[techLevel]);
  }

  /** Returns the current shield power.
   *
   * @return Shield Power
   */
  public int getShieldPower() {
    if (this.isActive()) 
      return (getMaxShieldPower() * (100-this.getDamagePercentage()) / 100);
    else return 0;
  } 

  /** Returns the current minumum shield power.
   *
   * @return Shield Power
   */
  public int getMinShieldPower() {
    if (this.isActive()) 
      return (minShieldPower[techLevel] * (100-this.getDamagePercentage()) / 100);
    else return 0;
  } 

  public int getPowerOutput() {
    return getShieldPower();
  }

  /** Returns materials required to build the ShipShield. 
   * getBuildMaterials() returns a MaterialBundle that has all 
   * the materials required to build the ShipShield.
   *
   * @param tech Tech level
   * @return Materials required to build a shield of the spec. tech level
   */
  public static MaterialBundle getBuildMaterials(int tech) {
    MaterialBundle mb = new MaterialBundle(MaterialBundle.UNLIMITED);
    if (tech<0 || tech>8) throw new IllegalArgumentException
			    ("Non-existent tech level not between (0-8)");
    mb.putFuel(fuel[tech]);
    mb.putDodechadrium(dodechadrium[tech]);
    mb.putMetals(metals[tech]);
    mb.putMinerals(minerals[tech]);
    mb.putOil(oil[tech]);
    mb.putTroops(troops[tech]);

    return mb;
  }

  /** Returns the time taken to build the ShipShield.
   * Shield of different tech level have varying building times. Presently,
   * the building time is directly proportional to the tech level of the
   * shield.
   *
   * @param tech Tech level
   * @return Time taken in building the ShipShield of the spec. tech level
   */
  public static int getBuildTime(int tech) {
      return 1;
  }

  /** Returns the materials required in upgrading the ShipShield tech level 
   * of a planet.
   *
   * @param tech Tech level
   * @return Materials required in upgrading one tech to given techlevel
   */
  public static MaterialBundle getTechMaterials(int tech) {
    MaterialBundle mb = new MaterialBundle(MaterialBundle.UNLIMITED);
    if (tech<0 || tech>8) throw new IllegalArgumentException
			    ("Non-existent tech level not between (0-8)");
    mb.putFuel(techFuel[tech]);
    mb.putDodechadrium(techDodechadrium[tech]);
    mb.putMetals(techMetals[tech]);
    mb.putMinerals(techMinerals[tech]);
    mb.putOil(techOil[tech]);
    mb.putTroops(techTroops[tech]);

    return mb;
  }

  /** Returns the power consumption of the Shield.
   * ShipShields consume presently no at all...
   *
   * @return Power consumption of the ShipShield
   */ 
  public int getPowerConsumption() {
    if (this.isActive()) return powerConsumption[techLevel];
    else return 0;
  }


  /** Returns the maximum damgage that the ShipShield can receive without 
   * being totally destroyed.
   * 
   * @return Maximum obtainable damage
   */
  public int getMaxDamage() {
    return damageResistance[techLevel];
  }

  /** Returns the mass of the ShipShield.
   *
   * @return Mass of the ShipShield
   */
  public int getMass() {
    return mass[techLevel];
  }

  public String toString() {
    return "S"+techLevel+" " + gadgetName[techLevel];
  }

    public int getTechType() {
	return ShipComponent.SHIELD;
    }
}
