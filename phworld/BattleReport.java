/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import java.util.*;
import java.io.Serializable;


public class BattleReport implements Serializable {
  
  private String name;
  private Vector ships;
  private Hashtable attacks;
  private Vector participants;

  public BattleReport(String name) {
    this.name=name;
    ships   = new Vector();
    attacks = new Hashtable();
    participants = new Vector();
  }

  public String getName() {
    return name;
  }

  public Vector getParticipants() {
    return participants;
  }

  public Vector getShips() {
    return ships;
  }

  public Vector getAttacks(int byPlayer) {
    return (Vector) attacks.get(new Integer(byPlayer));
  }

  protected void addShip(ShipReport sr) {
    ships.addElement(sr);
  }

  protected void addAttack(int byPlayer, int toPlayer) {
    Vector myAttacks = (Vector) attacks.get(new Integer(byPlayer));
    if(myAttacks == null) {
      throw new IllegalStateException("Added attack for nonexisting battle participant");
    }

    Integer newAttack = new Integer(toPlayer);

    for(int i=0; i<myAttacks.size(); i++) {
      if(myAttacks.elementAt(i).equals(newAttack))
	return;
    }

    myAttacks.addElement(newAttack);
  } 

  protected void addParticipant(int player) {
    participants.addElement(new Integer(player));
    attacks.put(new Integer(player), new Vector());
  }
}
