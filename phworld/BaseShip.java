/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import dsc.netgame.*;
import dsc.awt.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.awt.image.*;

public abstract class BaseShip extends 
			       WorldComponent implements ActionListener {


    protected Location waypoint;
    protected int trackTarget=-1;
    protected int bearing;

    protected int hullTechLevel;
    protected Color warningCircleColor = null;
    protected TargetValuation targetValuation;


    public BaseShip (int id, String name, int owner, Location location) {
	super(id,name,owner,location);
    }

    public String getWaypointText() {
	if(waypoint.equals(location))
	    return "[Here]";

	Enumeration e = world.getComponentsByLocation(waypoint);
	WorldComponent wc = null;

	while(e.hasMoreElements()) {
	    wc = (WorldComponent) e.nextElement();

	    if(wc instanceof Planet)
		break;
	}

	if(wc==null)
	    return waypoint.toString();
	else
	    return wc.getName();
    }

    public boolean isVisible() {
	return true;
    }

    protected WorldComponent calculateWaypointTarget(Location l) {
	Location actualWaypoint = l;
	Enumeration e = world.getWorldComponents();
	WorldComponent wc;
	Location p;

	while(e.hasMoreElements()) {
	    wc = (WorldComponent) e.nextElement();
	    p=wc.getLocation();
	    if(Math.abs(p.getX()-l.getX()) <= 20 &&
	       Math.abs(p.getY()-l.getY()) <= 20) {
		return wc;
	    }
	}

	return null;
    }
    

    protected Location calculateWaypointLocation(Location l) {
	Location actualWaypoint = l;
	Enumeration e = world.getWorldComponents();
	WorldComponent wc;
	Location p;

	while(e.hasMoreElements()) {
	    wc = (WorldComponent) e.nextElement();
	    p=wc.getLocation();
	    if(Math.abs(p.getX()-l.getX()) <= 20 &&
	       Math.abs(p.getY()-l.getY()) <= 20) {
		actualWaypoint=(Location) p.clone();
		break;
	    }
	}

	return actualWaypoint;
    }

    public void resetSurrender() {}

    public void setNewWaypoint(Location l) {
	waypoint=l;

	WorldComponentMessage wcm = new SetWaypointWCM(getId(), waypoint);
	world.putComponentMessage(wcm);
	setInfoText();
	world.repaintMap();
	resetSurrender();
    }

    public void setNewTargetValuation(TargetValuation tv) {
	this.targetValuation = tv;
	
	WorldComponentMessage wcm = new SetTargetValuationWCM(getId(),
							      tv);
	world.putComponentMessage(wcm);
    }

    public void componentSelected(WorldComponent wc) {
	startTracking(wc);
    }

    public void startTracking(WorldComponent target) {
	if(target == null) {
	    resetTrack();
	    return;
	}
	    
	trackTarget=target.getId();
	setNewWaypoint(target.getLocation());
	WorldComponentMessage wcm = new SetTrackingWCM(getId(), 
						       target.getId());
	world.putComponentMessage(wcm);
	resetSurrender();
    }

    public void resetTrack() {
	WorldComponentMessage wcm = new SetTrackingWCM(getId(), -1);
	world.putComponentMessage(wcm);
	trackTarget = -1;
	setNewWaypoint(getLocation());
	resetSurrender();
    }

    public void pointClicked(Location l) {
	if(world.getController().playerNumber==getOwner()) {
	    locationSelected(l);
	}
    }

    public void pointShiftClicked(Location l) {
	if(world.getController().playerNumber==getOwner()) {
	    locationShiftSelected(l);
	}
    }

    public void locationSelected(Location l) {
	trackTarget = -1;
	setNewWaypoint(calculateWaypointLocation(l));
    }

    public void locationShiftSelected(Location l) {
	trackTarget=-1;
	setNewWaypoint(l);
    }

    public int getBearing() {
	if(waypoint!=null)
	    return location.getBearingTo(waypoint);
	else
	    return bearing;
    }

    public void setBearing(int b) {
	bearing = b;
    }

    public Location getWaypoint() {
	return waypoint;
    }

    public void setWaypoint(Location l) {
	waypoint=l;
    }

    public void setHullTech(int tech) {
	hullTechLevel=tech;
    }

    public void setTrackTarget(int target) {
	trackTarget=target;
    }

    public void setTargetValuation(TargetValuation tv) {
	targetValuation = tv;
    }

    public TargetValuation getTargetValuation() {
	return targetValuation;
    }

    public Planet overOwnPlanet() {
	Enumeration e=world.getComponentsByLocation(location);
	Planet planet=null;

	while (e.hasMoreElements()) {
	    WorldComponent wc = (WorldComponent) e.nextElement();

	    if(wc instanceof Planet)
		if(wc.getOwner()==getOwner()) {
		    planet=(Planet) wc;
		    break;
		}
	}

	return planet;
    }

    public Planet overAnyPlanet() {
	Enumeration e=world.getComponentsByLocation(location);
	Planet planet=null;

	while (e.hasMoreElements()) {
	    WorldComponent wc = (WorldComponent) e.nextElement();

	    if(wc instanceof Planet) {
		planet=(Planet) wc;
		break;
	    }
	}

	return planet;
    }

    public void drawAdditionalGraphs(ScaledGraphics g) {
	int x     = getLocation().getX()+26;
	int y     = getLocation().getY()-26;
	int width = 10;

	Color pri = GlobalDefines.getPrimaryColor(owner);
	Color sec = GlobalDefines.getSecondaryColor(owner);

	g.setColor(pri);
	g.fillArc(x, y, width, width, 90, 180);
	g.setColor(sec);
	g.fillArc(x, y, width, width, 0, 90);
	g.fillArc(x, y, width, width, 270, 90);

	drawWarningMarker(g);
	drawWaypointMarker(g);
    }

    public void drawAdditionalPreGraphs(ScaledGraphics g) {
	if(world.getActiveComponent() == this)
	    drawEDTCircle(g);

	if(waypoint == null || waypoint.equals(location))
	    return;

	if(world.getActiveComponent() == this) { 
	    if(!((PHWorld)world).isWaypointDrawingEnabled())
		return;
	} else
	    if(!((PHWorld)world).isWaypointDrawingForAllEnabled())
		return;

	Color pri = GlobalDefines.getPrimaryColor(owner);
	g.setColor(pri);
	g.drawLine(location.getX(), location.getY(),
		   waypoint.getX(), waypoint.getY());
    }

    public void clientsideInitialization() {
	updateWarningCircle();
    }

    protected void drawWarningMarker(ScaledGraphics g) {
	if(world.getController().playerNumber!=getOwner())
	    return;

	if(warningCircleColor == null)
	    return;

	Image i = world.getImage(getMapImage());
	int dxsize = i.getWidth(getImageObserver())/2;

	g.setColor(warningCircleColor);
	g.drawArc(getLocation().getX()-dxsize-1,
		  getLocation().getY()-dxsize-1,
		  i.getWidth(getImageObserver())+2,
		  i.getHeight(getImageObserver())+2,
		  0,
		  360);
    }

    protected void drawWaypointMarker(ScaledGraphics g) {
	if(waypoint == null)
	    return;

	if(!((PHWorld)world).isWaypointMarkerDrawingEnabled())
	    return;

	if(!(world.getActiveComponent() == this))
	    return;

	if(waypoint.equals(location))
	    return;

	String color = "green";
    
	if(warningCircleColor == Color.red)
	    color = "red";

	if(warningCircleColor == Color.yellow)
	    color = "yellow";

	Image i = 
	    world.getImage("data/graph/still/components/ships/waypoint."+color+".gif");
        
	int width = i.getWidth(getImageObserver())/2;
	g.drawImage(i, 
		    waypoint.getX()-width, 
		    waypoint.getY()-width, 
		    getImageObserver());
    }

    public int getTrackTarget() {
	return trackTarget;
    }

    void initiateBattleOrders(TargetValuation tv) {
	targetValuation = tv;
	
	WorldComponentMessage wcm = new SetTargetValuationWCM(getId(), tv);
	world.putComponentMessage(wcm);
    }

    public abstract void setInfoText();

    public abstract void drawEDTCircle(ScaledGraphics g);

    public abstract void updateWarningCircle();

    protected abstract ImageObserver getImageObserver();
}
