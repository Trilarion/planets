/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import java.awt.*;
import java.awt.event.*;
import dsc.awt.*;
import java.util.*;


class ShipSelectionPanel extends ShipSidePanel implements ActionListener {

  private Vector ships;

  ShipSelectionPanel() {
    super("data/graph/still/panel/ship.transfer.gif");
    contentsPanel.setLayout(new VerticalFlowLayout());
  }

  void initializeContents(BaseShip bs) {
      Ship s = (Ship) bs;
    contentsPanel.removeAll();
    contentsPanel.add(new Label("Select a ship:"));
    ships   = s.ownShipsAtSameLocation();

    for (int i = 0; i < ships.size() ; i++) {
      Ship   ship = (Ship) ships.elementAt(i);

      Button b = new Button(ship.getName());
      b.addActionListener(this);
      contentsPanel.add(b);
    }
  }

  public void actionPerformed(ActionEvent evt) {
    String arg = evt.getActionCommand();

    for (int i = 0; i < ships.size() ; i++) {
      Ship   s = (Ship) ships.elementAt(i);

      if(s.getName().equals(arg)) {
	((Ship) getShip()).startTransferWithShip(s);
	return;
      }
    }
  }

  boolean OKAction() {    
    return true;
  }

  void initiateHelp() {
  }

  public void initiateTechGallery() {
      TechGallery.showTechGallery();      
      return;
  }
}
