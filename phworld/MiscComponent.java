/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import dsc.netgame.*;
import dsc.awt.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;


public abstract class MiscComponent 
    extends WorldComponent implements ActionListener {

  private static Panel controlPanel = null;  
  private static GridBagConstraints controlPanelConstraints = null;

  /* Primary Card components */
  private static Panel generalPanel = null;
  private static TextArea infoText = null;
  private static Panel buttonPanel = null;
  private static Button selfDestructButton = null;
  private static MiscComponent buttonListener = null;
  private static ImageComponent imageComponent;

  protected String message;

  public MiscComponent (int id, String name, int owner, Location location) {
    super(id,name,owner,location);
  }

  private void changeControlPanel(Panel newPanel) {
    if(controlPanelConstraints == null) {
      controlPanelConstraints = new GridBagConstraints();

      controlPanelConstraints.fill=GridBagConstraints.BOTH;
      controlPanelConstraints.anchor=GridBagConstraints.NORTH;
      controlPanelConstraints.weightx=100;
      controlPanelConstraints.weighty=1;
      controlPanelConstraints.gridwidth=1; 
      controlPanelConstraints.gridheight=1;
      controlPanelConstraints.gridx=0;
      controlPanelConstraints.gridy=0;
    }

    controlPanel.removeAll();
    controlPanel.add(newPanel, controlPanelConstraints);
    controlPanel.validate();
  }

  private void createControlPanel() {
    controlPanel=new Panel();
    controlPanel.setLayout(new GridBagLayout());

    generalPanel=new Panel();
    changeControlPanel(generalPanel);

    imageComponent = new ImageComponent(
		 world.getImage(getPanelImageName()));
    generalPanel.add(imageComponent);

    infoText=new TextArea("",15,22,TextArea.SCROLLBARS_NONE);
    infoText.setEditable(false);
    
    generalPanel.setLayout(new VerticalFlowLayout());
    generalPanel.add(infoText);

    buttonPanel=new Panel();
    buttonPanel.setLayout(new VerticalFlowLayout());

    selfDestructButton=new Button("Self Destruct");
    buttonPanel.add(selfDestructButton);

    generalPanel.add(buttonPanel);
  }

  protected void resetInfoText() {
    infoText.setText(getName() + "\n\n");

    infoText.append(message + "\n\n");
  }

  protected void resetControlPanel() {

    imageComponent.setImage(world.getImage(getPanelImageName()));
    resetInfoText();
    
    if(buttonListener!=null) {
      selfDestructButton.removeActionListener(buttonListener);
      buttonListener=null;
    }

    if(world.getController().playerNumber==getOwner()) {
      selfDestructButton.addActionListener(this);
      buttonListener=this;
      buttonPanel.setEnabled(true);
    } else {
      buttonPanel.setEnabled(false);
    }

    changeControlPanel(generalPanel);
  }

  public boolean isVisible() {
    return true;
  }

  public Panel getControlPanel() {
    if(controlPanel==null) {
      createControlPanel();
    }
    
    resetControlPanel();

    return controlPanel;
  }

  public boolean paintToForeground() {
    return false;
  }

  public void actionPerformed(ActionEvent evt) {
    String arg = evt.getActionCommand();
    Object src = evt.getSource();

    if(arg.equals("Self Destruct")) {
	selfDestruct();
    } 
  }

  void selfDestruct() {
    WorldComponentMessage wcm = new SelfDestructWCM(getId());
    world.putComponentMessage(wcm);
    message="Self destructing...";
    resetInfoText();
  }

    void setMessage(String m) {
	message = m;
    }

  public void drawAdditionalPreGraphs(ScaledGraphics g) {
      /*
    Color pri = Color.red;
    pri=pri.darker().darker().darker().darker();

    g.fillOval(location.getX(), location.getY(),
	       radius*2, radius*2);
      */
  }


  public void drawAdditionalGraphs(ScaledGraphics g) {}

    public String getPanelImageName() {
	return "data/graph/still/panel/planet.component.gif";
    }
}
