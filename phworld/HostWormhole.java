/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import dsc.netgame.*;
import java.util.*;


public class HostWormhole extends HostWorldComponent {

    private HostWormhole pairWormhole;
    private int bearing;
    private double tharganProbMultiplier=1.0;

    private static int WORMHOLE_SPEED = 30;
    private static int BEARING_VARIANCE = 10;
    private static int MASS_VARIANCE    = 1000;
    private static int SHIP_DISTANCE_VARIANCE = 90;
    public static int CLOAK_PREVENT_RADIUS = 295;

    public static int THARGAN_BASE_MASS = 150;

    /* probabilities are one out of X */
    private static int SHIP_LOST_PROBABILITY = 50;
    private static int SHIP_RELOCATE_PROBABILITY = 50;
    private static int THARGON_PROBABILITY = 15;
    
    /* reverse probability, larger numbers mean less thargons */
    private static double RANDOM_THARGAN_PROB = 65000.0;

    public void putMessage(WorldComponentMessage m) {

    }

    public void doRound() { 
	Enumeration e = hostWorld.getComponentsByLocation(location);
	Location target = pairWormhole.getNewShipLocation();
	
	while(e.hasMoreElements()) {
	    HostWorldComponent hwc = (HostWorldComponent) e.nextElement();
	    if(hwc instanceof HostShip) {
		if(!((HostShip) hwc).isInFleet()) {
		    sendShip((MovingObject) hwc, target);
		}
	    } else if(hwc instanceof HostFleet) {
		sendShip((MovingObject) hwc, target);
	    }
	}

	randomThargans();
	moveWormhole();
    }

    private void moveWormhole() {
	int distance = (int) (Math.random()*((double) WORMHOLE_SPEED));
	bearing = bearing + 
	    ((int) (Math.random() * (double) BEARING_VARIANCE)) -
	    BEARING_VARIANCE/2;

	Location newLocation = location.moveTowards(bearing, distance);

	if(newLocation.getX() >= (hostWorld.getXSize()-100) ||
	   newLocation.getX() <= 100 ||
	   newLocation.getY() >= (hostWorld.getYSize()-100) ||
	   newLocation.getY() <= 100) {
	    moveToRandomLocation();
	    return;
	}

	setLocation(newLocation);
    }

    private void moveToRandomLocation() {
	int x = (int) (Math.random() * (double) (hostWorld.getXSize()));
	int y = (int) (Math.random() * (double) (hostWorld.getYSize()));

	bearing = (int) (Math.random() * 360.0);
	setLocation(new Location(x, y));
    }

    private void sendShip(MovingObject movingObject, Location target) {
	if( ((int) (Math.random() * THARGON_PROBABILITY)) == 1) {
	    thargan(movingObject);
	}

	if( ((int) (Math.random() * SHIP_LOST_PROBABILITY)) == 1) {
	    shipLost(movingObject);
	    return; 
	} 

	if(movingObject instanceof HostShip) {
	    HostShip ship = (HostShip) movingObject;

	    /* Ignore ships moved by fleets */
	    if(!ship.getLocation().equals(getLocation())) {
		return;
	    }

	    ship.setLocation(target);

	    hostWorld.sendChatMessage(ship.getOwner(), 
				      "Wormhole moved your ship "
				      + ship.getName() + 
				      " to another location.");
	} else if(movingObject instanceof HostFleet){
	    HostFleet fleet = (HostFleet) movingObject;

	    fleet.moveToLocation(target);

	    hostWorld.sendChatMessage(fleet.getOwner(), 
				      "Wormhole moved your fleet "
				      + fleet.getName() + 
				      " to another location.");
	}

    }

    private Location getNewShipLocation() {
	int x,y;

	if( ((int) (Math.random() * SHIP_RELOCATE_PROBABILITY)) == 1) {
	    x = (int) (Math.random() * (double) (hostWorld.getXSize()-100))+50;
	    y = (int) (Math.random() * (double) (hostWorld.getYSize()-100))+50;
	} else {
	    x = getLocation().getX() +
		((int) (Math.random() * (double) SHIP_DISTANCE_VARIANCE));
	    y = getLocation().getY() +
		((int) (Math.random() * (double) SHIP_DISTANCE_VARIANCE));
	}

	return new Location(x, y);
    }

    private void randomThargans() {
	if( (Math.random() * RANDOM_THARGAN_PROB) <=
	    (((double)hostWorld.getCurrentRound()) * tharganProbMultiplier) ) {
	    
	    double mass = Math.pow(1.0075,
				   (double) hostWorld.getCurrentRound()) *
		THARGAN_BASE_MASS;

	    thargan((int) mass);
	}
    }

    private void thargan(MovingObject movingShip) {
	int mass = (int) (Math.random()*(double) MASS_VARIANCE);

	if(movingShip instanceof HostFleet) {
	    mass += ((HostFleet) movingShip).getMass();
	} else if (movingShip instanceof HostShip){
	    mass += ((HostShip) movingShip).getMass();
	}

	thargan(mass);
    }

    private void thargan(int mass) {
	while(mass>=200) {
	    if(mass>=1720) {
		hostWorld.putComponent(getTharganBattleship());
		mass-=1720;
	    } else if(mass>=900) {
		hostWorld.putComponent(getTharganDestroyer());
		mass-=900;
	    } else {
		hostWorld.putComponent(getTharganScout());
		mass-=500;
	    }
	}

	sendTharganMessage();
    }

    private void sendTharganMessage() {
	hostWorld.broadcastChatMessageAs(GlobalDefines.maxPlayers-1,
					 "All your base are belong to us!");
    }

    private HostShip getTharganScout() {
	Vector shipComponents = new Vector();
	ShipHull shipHull;
	HostShip ship = new HostShip(hostWorld, 
				     GlobalDefines.maxPlayers-1, 
				     location);

	shipHull = new ShipHull(GlobalDefines.maxPlayers-1, 1);
	shipComponents.addElement(new ShipEngine(1));
	shipComponents.addElement(new ShipWeapon(1));
	shipComponents.addElement(new ShipShield(1));
	shipComponents.addElement(new ShipMisc(3));

	ship.setComponents(shipHull, shipComponents.elements());
	return ship;
    }

    private HostShip getTharganDestroyer() {
	Vector shipComponents = new Vector();
	ShipHull shipHull;
	HostShip ship = new HostShip(hostWorld, 
				     GlobalDefines.maxPlayers-1, 
				     location);

	shipHull = new ShipHull(GlobalDefines.maxPlayers-1, 2);
	shipComponents.addElement(new ShipEngine(3));
	shipComponents.addElement(new ShipWeapon(4));
	shipComponents.addElement(new ShipWeapon(3));
	shipComponents.addElement(new ShipShield(4));
	shipComponents.addElement(new ShipShield(3));
	shipComponents.addElement(new ShipShield(2));
	shipComponents.addElement(new ShipMisc(3));
	shipComponents.addElement(new ShipMisc(3));

	ship.setComponents(shipHull, shipComponents.elements());
	return ship;
    }

    private HostShip getTharganBattleship() {
	Vector shipComponents = new Vector();
	ShipHull shipHull;
	HostShip ship = new HostShip(hostWorld, 
				     GlobalDefines.maxPlayers-1, 
				     location);

	shipHull = new ShipHull(GlobalDefines.maxPlayers-1, 4);
	shipComponents.addElement(new ShipEngine(6));
	shipComponents.addElement(new ShipWeapon(6));
	shipComponents.addElement(new ShipWeapon(6));
	shipComponents.addElement(new ShipWeapon(5));
	shipComponents.addElement(new ShipWeapon(1));
	shipComponents.addElement(new ShipShield(5));
	shipComponents.addElement(new ShipShield(5));
	shipComponents.addElement(new ShipShield(5));
	shipComponents.addElement(new ShipShield(6));
	shipComponents.addElement(new ShipMisc(3));
	shipComponents.addElement(new ShipMisc(3));
	shipComponents.addElement(new ShipMisc(3));

	ship.setComponents(shipHull, shipComponents.elements());
	return ship;
    }


    private void shipLost(MovingObject movingObject) {
	if(movingObject instanceof HostShip) {
	    HostShip ship = (HostShip) movingObject;

	    hostWorld.sendChatMessage(ship.getOwner(), 
				      "Wormhole moved your ship "
				      + ship.getName() + 
				      " out of this universe.");

	    hostWorld.removeComponent(ship.getId());
	} else if(movingObject instanceof HostFleet) {
	    HostFleet fleet = (HostFleet) movingObject;

	    hostWorld.sendChatMessage(fleet.getOwner(), 
				      "Wormhole moved your fleet "
				      + fleet.getName() + 
				      " out of this universe.");

	    Enumeration e = fleet.getShips();
	    
	    while(e.hasMoreElements()) {
		HostShip hs = (HostShip) e.nextElement();
		hostWorld.removeComponent(hs.getId());
	    }

	    hostWorld.removeComponent(fleet.getId());
	}
    }

    public WorldComponent getWorldComponent(int player) {
	Wormhole m;

	if(hostWorld.isVisibleByPlayer(player, this)) {
	    m=new Wormhole(id, name, location);
	    m.setBearing(getLocation().
			 getBearingTo(pairWormhole.getLocation()));
	}
	else {
	    return null;
	}

	return m;
    }

    public void setName(String n) {
	name = n;
    }

    public void setPairWormhole(HostWormhole hw) {
	pairWormhole=hw;
    }

    public void setTharganActivity(double activity) {
	tharganProbMultiplier = activity;
    }

    public HostWormhole(HostWorld hw) {
	super(hw,-1,new Location(0,0));
	name = "WORMHOLE";
	moveToRandomLocation();
    }

    public boolean isVisible() {
	return true;
    }
}

