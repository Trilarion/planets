/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import java.awt.*;
import java.awt.event.*;
import dsc.awt.*;


class ImproveTechPanel extends PlanetSidePanel implements ItemListener {
  private GridBagConstraints gbc;
  private Checkbox hullBox, engineBox, weaponBox, shieldBox, miscBox;
  private Label hullLabel, engineLabel, weaponLabel, shieldLabel, miscLabel;
  private Label ETALabel;
  private TextArea materialText;
  private CheckboxGroup checkboxGroup;
  private PHWorld world;
  private int player;

  ImproveTechPanel() {
    super("data/graph/still/panel/planet.tech.gif");

    checkboxGroup=new CheckboxGroup();
    contentsPanel.setLayout(new GridBagLayout());
    gbc=new GridBagConstraints();

    gbc.fill=GridBagConstraints.BOTH;
    gbc.anchor=GridBagConstraints.NORTH;
    gbc.weightx=100;
    gbc.weighty=0;

    hullBox=addCheckbox(gbc,2,7,6,1,"Hull",checkboxGroup,true);
    engineBox=addCheckbox(gbc,2,8,6,1,"Engine",checkboxGroup,false);
    weaponBox=addCheckbox(gbc,2,9,6,1,"Weapon",checkboxGroup,false);
    shieldBox=addCheckbox(gbc,2,10,6,1,"Shield",checkboxGroup,false);
    miscBox=addCheckbox(gbc,2,11,6,1,"Misc",checkboxGroup,false);

    hullLabel=new Label("0");
    engineLabel=new Label("0");
    weaponLabel=new Label("0");
    shieldLabel=new Label("0");
    miscLabel=new Label("0");
    ETALabel=new Label("[Turns]");

    materialText=new TextArea("",13,22,TextArea.SCROLLBARS_NONE);
    materialText.setEditable(false);

    gbc.gridwidth=2;
    gbc.gridheight=1;
    gbc.gridx=0;
    
    gbc.gridy=7;
    contentsPanel.add(hullLabel, gbc);
    gbc.gridy=8;
    contentsPanel.add(engineLabel, gbc);
    gbc.gridy=9;
    contentsPanel.add(weaponLabel, gbc);
    gbc.gridy=10;
    contentsPanel.add(shieldLabel, gbc);
    gbc.gridy=11;
    contentsPanel.add(miscLabel, gbc);

    gbc.gridy=13;
    gbc.gridwidth=4;
    gbc.gridx=2;
    contentsPanel.add(ETALabel, gbc);

    gbc.gridy=14;
    gbc.gridwidth=8;
    gbc.gridx=0;
    gbc.weighty=100;
    contentsPanel.add(materialText, gbc);
  }

  void initializeContents(Planet p) {
    TechLevel tl = p.getTechLevels();
    world = (PHWorld) p.getWorld();
    player = p.getOwner();
    
    hullLabel.setText("  " + tl.getHullTech());
    engineLabel.setText("  " + tl.getEngineTech());
    weaponLabel.setText("  " + tl.getWeaponTech());
    shieldLabel.setText("  " + tl.getShieldTech());
    miscLabel.setText(  "  " + tl.getMiscTech());

    checkboxGroup.setSelectedCheckbox(hullBox);
    
    setETAandCostLabels(ShipComponent.HULL, tl.getHullTech()+1);
  }

    private void setETAandCostLabels(int techType, int level) {
	ETALabel.setText("[" + 
	       TechLevel.getTechTime(techType, level) +
			 " turns]");     

	int n = PlanetMath.calcNumberOfTechPlanets(player,
						   world.getWorldComponents(),
						   techType,
						   level);

	String s;

	s=TechLevel.getName(techType, level) + ":\n\n";

	s+=TechLevel.getShortDescription(techType, level) + "\n";
	
	s+="Requirements / On planet:\n";
	s=s+(TechLevel.getTechMaterials(techType, level, n).
	     mergeToString(getPlanet().getMaterials()));
				
	materialText.setText(s);
    }

  private Checkbox addCheckbox(GridBagConstraints gbc,int x,int y, 
			       int w, int h, String name,
			       CheckboxGroup g,boolean v) {
    Checkbox c=new Checkbox(name,g,v);
    c.addItemListener(this);
    gbc.gridx=x;
    gbc.gridy=y;
    gbc.gridwidth=w;
    gbc.gridheight=h;
    contentsPanel.add(c,gbc);
    return c;
  }

  boolean OKAction() {
    int type = -1;

    if(hullBox.getState()) {
      type = ShipComponent.HULL;
    }

    if(engineBox.getState()) {
      type = ShipComponent.ENGINE;
    }

    if(weaponBox.getState()) {
      type = ShipComponent.WEAPON;
    }

    if(shieldBox.getState()) {
      type = ShipComponent.SHIELD;
    }

    if(miscBox.getState()) {
      type = ShipComponent.MISC;
    }

    if(type == -1) {
      throw(new IllegalStateException("None of the checkboxes selected."));
    }

    getPlanet().startImproveTech(type);

    return true;
  }

  public void itemStateChanged(ItemEvent evt) {
    if(evt.getStateChange()==ItemEvent.DESELECTED) return;

    TechLevel tl = getPlanet().getTechLevels();
    int techType=-1;
 
    if(evt.getItem().equals("Hull")){
      techType=ShipComponent.HULL;
    }
    else if (evt.getItem().equals("Engine")) {
      techType=ShipComponent.ENGINE;
    }
    else if (evt.getItem().equals("Weapon")) {
      techType=ShipComponent.WEAPON;
    }
    else if (evt.getItem().equals("Shield")) {
      techType=ShipComponent.SHIELD;
    }
    else if (evt.getItem().equals("Misc")) {
      techType=ShipComponent.MISC;
    }

    if(techType!=-1) {
	setETAandCostLabels(techType, tl.getTech(techType)+1);
    }
  }

  void initiateHelp() {
    HelpWindow.callForHelp("data" + java.io.File.separator + "help",
			   "Playing",
			   "Improve Tech");
  }

  public void initiateTechGallery() {
    int type = -1;

    if(hullBox.getState()) {
      type = ShipComponent.HULL;
    }

    if(engineBox.getState()) {
      type = ShipComponent.ENGINE;
    }

    if(weaponBox.getState()) {
      type = ShipComponent.WEAPON;
    }

    if(shieldBox.getState()) {
      type = ShipComponent.SHIELD;
    }

    if(miscBox.getState()) {
      type = ShipComponent.MISC;
    }

    if(type == -1) {
      throw(new IllegalStateException("None of the checkboxes selected."));
    }

    int level = getPlanet().getTechLevels().getTech(type) + 1;
    if(level>8)
      level=8;

    TechGallery.showTechGallery(TechLevel.getTechName(type), level);
  }

}
