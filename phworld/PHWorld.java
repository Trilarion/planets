/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import dsc.netgame.*;
import java.awt.*;
import java.awt.event.*;
import dsc.awt.*;
import java.util.*;


public class PHWorld extends World implements ActionListener {

  private int [] starX;
  private int [] starY;
  private int [] starBrightness;
  private Color [] starColor = {Color.white, Color.gray};
  private int numberOfStarColors = 2;
  private Panel worldPanel;
  private TextArea infoText;

  private CheckboxMenuItem drawAllWaypointLines;
  private CheckboxMenuItem drawActiveWaypointLine;
  private CheckboxMenuItem drawWaypointMarker;
  private CheckboxMenuItem drawEDTCircle;
  private CheckboxMenuItem drawActivityText;

  private BattleReportWindow battleReportWindow;

  private void makeMenuItem(Menu m, String name, ActionListener al) {
    MenuItem mi = new MenuItem(name);
    mi.addActionListener(al);
    m.add(mi);
  }

  public PHWorld(GameWindow gw, ClientController c, WorldProperties wp) {
    super(gw,c,wp);
     
    Menu m = new Menu("World", true);
    makeMenuItem(m,"Technology Gallery",this);    
    makeMenuItem(m,"Battle Reports",this);
    makeMenuItem(m,"Statistics", this);

    addWorldMenu(m);
    createOptionsMenu();

    battleReportWindow=new BattleReportWindow(new BattleOutcomes(),
					      this);

    worldPanel = new Panel();

    ImageComponent im = new ImageComponent(GraphicsLoader.getImage(
					   "data/graph/still/panel/ph.gif"));
    worldPanel.setLayout(new VerticalFlowLayout());
    worldPanel.add(im);

    infoText = new TextArea("",34,22,TextArea.SCROLLBARS_VERTICAL_ONLY);
    infoText.setEditable(false);
    worldPanel.add(infoText);

    gw.setEmptyPanel(worldPanel);
  } 

  private void createOptionsMenu() {
    drawActiveWaypointLine = new CheckboxMenuItem("Draw waypoint line", true);
    drawAllWaypointLines   = new CheckboxMenuItem("Draw all waypoints", true);
    drawWaypointMarker     = new CheckboxMenuItem("Draw waypoint marker",true);
    drawEDTCircle          = new CheckboxMenuItem("Draw EDT circle", true);
    drawActivityText       = new CheckboxMenuItem("Planet activity texts", true);

    addOptionForMenu(drawActiveWaypointLine);
    addOptionForMenu(drawAllWaypointLines);
    addOptionForMenu(drawWaypointMarker);
    addOptionForMenu(drawEDTCircle);
    addOptionForMenu(drawActivityText);
  }

  public void actionPerformed(ActionEvent evt) {
    String arg = evt.getActionCommand();
    
    if(arg.equals("Technology Gallery")) {
      TechGallery.showTechGallery();
    } else if(arg.equals("Battle Reports")) {
      battleReportWindow.show();
    } else if(arg.equals("Statistics")) {
      deactivateComponent();
    }
  }

  private int getNumberOfStars() {
    return 1000;
  }

  private void placeStars() {
    starX = new int [getNumberOfStars()];
    starY = new int [getNumberOfStars()];
    starBrightness = new int [getNumberOfStars()];
  
    int n = getNumberOfStars();
    int ys= getYSize();
    int xs= getXSize();
    for(int i=0; i<n; i++) {
      starX[i]=(int) (Math.random()*((double) xs));
      starY[i]=(int) (Math.random()*((double) xs));
      starBrightness[i]=i%2;
    }
  }

  public void drawBackground(ScaledGraphics g) {
    if(starX==null)
      placeStars();
    
    int numberOfStars = getNumberOfStars();
    numberOfStars/=g.getScaleFactor();
    numberOfStars/=g.getScaleFactor();

    for(int i=0; i<numberOfStars; i++) {
      g.setColor(starColor[starBrightness[i]]);
      g.drawLine(starX[i],starY[i],starX[i],starY[i]);
    }
  }

  public int getNumberOfPlanets() {
    Enumeration e = getWorldComponents();
    int n=0;

    while (e.hasMoreElements()) {
      WorldComponent wc = (WorldComponent) e.nextElement();

      if((wc instanceof Planet) && 
	 (wc.getOwner()==getController().playerNumber))
	n++;
    }

    return n;
  }

  public int getNumberOfShips() {
    Enumeration e = getWorldComponents();
    int n=0;

    while (e.hasMoreElements()) {
      WorldComponent wc = (WorldComponent) e.nextElement();

      if((wc instanceof Ship) && (wc.getOwner()==getController().playerNumber))
	n++;
    }

    return n;
  }

  public MaterialBundle getTotalMaterials() {
    Enumeration e = getWorldComponents();
    MaterialBundle mb = new MaterialBundle(MaterialBundle.UNLIMITED);

    while (e.hasMoreElements()) {
      WorldComponent wc = (WorldComponent) e.nextElement();

      if((wc instanceof Planet) && 
	 (wc.getOwner()==getController().playerNumber)) {

	mb.addFullBundle(((Planet) wc).getMaterials());
      }

      if((wc instanceof Ship) && 
	 (wc.getOwner()==getController().playerNumber)) {

	mb.addFullBundle(((Ship) wc).getMaterials());
      }
    }

    return mb;
  }

  public MaterialBundle getTotalProduction() {
    Enumeration e = getWorldComponents();
    MaterialBundle mb = new MaterialBundle(MaterialBundle.UNLIMITED);

    while (e.hasMoreElements()) {
      WorldComponent wc = (WorldComponent) e.nextElement();

      if((wc instanceof Planet) && 
	 (wc.getOwner()==getController().playerNumber)) {

	mb.addFullBundle(((Planet) wc).getProduction());
      }
    }

    return mb;
  }


  public void newRoundStarted() {
    PHWorldState worldState = (PHWorldState) getWorldState();

    infoText.setText("Round " + getRound() + "\n\n");

    infoText.append("Score: " + worldState.getScore() + "\n");
    infoText.append(getNumberOfPlanets() + " planet(s)\n");
    infoText.append(getNumberOfShips()   + " ship(s)\n");
    infoText.append("\nTotal resources:\n");
    infoText.append("Credits: " + worldState.getCredits() + "\n");
    infoText.append(getTotalMaterials().toString());

    infoText.append("\nTotal production:\n");
    infoText.append("Credits: " + worldState.getCreditProduction() + "\n");
    infoText.append(getTotalProduction().toString());

    infoText.append("\nTotal use last round:\n");
    infoText.append("Credits: " + worldState.getCreditUsage() + "\n");
    infoText.append(worldState.getMaterialUsage().toString());

    battleReportWindow.initialize(worldState.getBattleOutcomes());
  }

  public boolean isWaypointDrawingEnabled() {
    return drawActiveWaypointLine.getState();
  }

  public boolean isWaypointDrawingForAllEnabled() {
    return drawAllWaypointLines.getState();
  }

  public boolean isWaypointMarkerDrawingEnabled() {
    return drawWaypointMarker.getState();
  }

  public boolean isEDTCircleDrawingEnabled() {
    return drawEDTCircle.getState();
  }

  public boolean isActivityTextDrawingEnabled() {
    return drawActivityText.getState();
  }

  public void putControlMessage(HostWorldMessage m) {
      super.putControlMessage(m);

      if(m instanceof NewFleetMessage) {
	  NewFleetMessage nfm = (NewFleetMessage) m;

	  Ship s = (Ship) getComponentById(nfm.getShipId());
	  Fleet f = nfm.getFleet();

	  if(f != null && s != null) {
	      addWorldComponent(f);
	      refreshMap();
	      s.startTracking(f);
	      activateComponent(f);
	  }
      }
  }
}
