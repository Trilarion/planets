/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import java.io.*;
import java.util.*;


/** This is a holdingplace for materials and components in the game.
 * Bundle can be used as ship's cargo room, planet's storage or
 * to describe how much materials are required to build something.
 *
 * @author Dodekaedron Software Creations, Inc. -- Wraith
 */

public class MaterialBundle implements Serializable, Cloneable {

  /** Represents unlimented storage capasity. Use this as a parameter
   * in the constructor.
   */
  public static final int UNLIMITED = Integer.MAX_VALUE/2;

  public static final int FUEL = 0;
  public static final int DODECHADRIUM = 1;
  public static final int METALS = 2;
  public static final int MINERALS = 3;
  public static final int OIL = 4;
  public static final int TROOPS = 5;

  private int capacity;
  
  private int fuel;
  private int dodechadrium;
  private int metals;
  private int minerals;
  private int oil;
  private int troops;

  private Hashtable components;

  /** Create empty MaterialBundle with given capasity.
   * If UNLIMITED is given as a parameter, bundle does not have
   * storage limits.
   *
   * @param capacity Storage capacity.
   */

  public MaterialBundle(int capacity) {
    this.capacity = capacity;
    components = new Hashtable();
  }

    /**
     * Prints two MaterialBundles side to side into one string. Values
     * are separated by slash. This bunlde is printed to the left and
     * the parameter bundle to the right side.
     *
     * @param right Bundle to print alogside this bundle
     *
     * @return The combined string like in toString().
     * 
     */

    public String mergeToString(MaterialBundle right) {
	String s;
	
	s="Fuel: " + fuel + " / " + right.getFuel() + "\n";
	s=s+"Dodechadrium: "+dodechadrium+" / " + right.getDodechadrium()+"\n";
	s=s + "Metals: " + metals + " / " + right.getMetals() + "\n";
	s=s + "Minerals: " + minerals + " / " + right.getMinerals() + "\n";
	s=s + "Oil: " + oil + " / " + right.getOil() + "\n";
	s=s + "Troops: " + troops + " / " + right.getTroops() + "\n";

	return s;
    }


  public String toString() {
    String s;
    
    s="Fuel: " + fuel + "\n";
    s=s + "Dodechadrium: " + dodechadrium + "\n";
    s=s + "Metals: " + metals + "\n";
    s=s + "Minerals: " + minerals + "\n";
    s=s + "Oil: " + oil + "\n";
    s=s + "Troops: " + troops + "\n";

    return s;
  }

  public Object clone() {
    MaterialBundle mb = new MaterialBundle(capacity);

    mb.fuel=fuel;
    mb.dodechadrium=dodechadrium;
    mb.metals=metals;
    mb.minerals=minerals;
    mb.oil=oil;
    mb.troops=troops;
    
    Enumeration e = components.elements();
    mb.components = new Hashtable();

    while(e.hasMoreElements()) {
      TransferableComponent tc = (TransferableComponent) e.nextElement();
      mb.components.put(new Integer(tc.getId()),tc.clone());
    }

    return mb;
  }

  /** Adds contents of given bundle into this bundle.
   *
   * @param b Bundle to add
   * @return True if there was enough space and the transfer was done.
   */

  public boolean addFullBundle(MaterialBundle b) {
    if(b.getMass() > this.getFreeSpace())
      return false;

    putFuel(b.getFuel());
    putDodechadrium(b.getDodechadrium());
    putMetals(b.getMetals());
    putMinerals(b.getMinerals());
    putOil(b.getOil());
    putTroops(b.getTroops());

    return true;
  }

  /** Subtracts contents of given bundle from this bundle.
   *
   * @param b Bundle to add
   * @return True if there was enough materials and the subraction was done.
   */

  public boolean subtractFullBundle(MaterialBundle b) {
    if(getFuel() < b.getFuel())
      return false;

    if(getDodechadrium() < b.getDodechadrium())
      return false;

    if(getMetals() < b.getMetals())
      return false;

    if(getMinerals() < b.getMinerals())
      return false;

    if(getOil() < b.getOil())
      return false;

    if(getTroops() < b.getTroops())
      return false;

    takeFuel(b.getFuel());
    takeDodechadrium(b.getDodechadrium());
    takeMetals(b.getMetals());
    takeMinerals(b.getMinerals());
    takeOil(b.getOil());
    takeTroops(b.getTroops());

    return true;
  }


  /**
   * Scales the materials (not components) in this bundle by given
   * amount. The scale is given in percents, ie. 100 means no change.
   * Note that this can be done only if the capacity of the bundle is
   * unlimited.
   *
   * @param scale The amount scaled (in percentage)
   */

  public void scaleBundle(int scale) {
    if(capacity != UNLIMITED)
      throw new IllegalStateException("Bundle scaling can only be committed on unlimited space bundles");

    fuel         = (fuel * scale) / 100;
    dodechadrium = (dodechadrium * scale) / 100;
    metals       = (metals * scale) /100;
    minerals     = (minerals * scale) /100;
    oil          = (oil * scale) /100;
    troops       = (troops * scale) /100;
  }

  /** Gets enumeration of all components stored in this bundle.
   *
   * @return List of components.
   */

  public Enumeration components() {
    return components.elements();
  }

  /** Adds new component in the bundle.
   *
   * @param tc Component to add.
   * @return True if there was enough space in the bundle. 
   * False if not. If false is returned, nothing is done.
   */

  public boolean addComponent(TransferableComponent tc) {
    if(tc.getMass()<=getFreeSpace()) {
      components.put(new Integer(tc.getId()), tc);
      return true;
    } else
      return false;
  }

  /** Get component out of the bundle.
   * 
   * @param tc Component to remove
   * @return True if component was in the bundle and was removed. False
   * if not.
   */

  public boolean takeComponent(TransferableComponent tc) {
    if(components.containsKey(new Integer(tc.getId()))) {
      components.remove(new Integer(tc.getId()));
      return true;
    } else {
      return false;
    }
  }

  /** Get component out of the bundle.
   *
   * @param id Subcomponent id of the component to remove
   * @return Compoment with given id, null if not found.
   */

  public TransferableComponent takeComponent(int id) {
    if(components.containsKey(new Integer(id))) {
      TransferableComponent tc = 
	(TransferableComponent) components.get(new Integer(id));

      components.remove(new Integer(id));
      return tc;
    }

    return null;
  }

  /**
   * Get list of components out of the bundle.
   *
   * @param e Enumeration of java.lang.Integer objects that represent the
   * id-numbers of the requested components.
   * @return Enumeration of TransferableComponent objects that were
   * found and removed from this bundle.
   */

  public Enumeration takeComponents(Enumeration e) {

    Vector      takenComponents = new Vector();

    while (e.hasMoreElements()) {
      Integer i = (Integer) e.nextElement();

      TransferableComponent tc =
	takeComponent(i.intValue());

      if(tc != null)
	takenComponents.addElement(tc);
    }

    return takenComponents.elements();
  }
   
  /** Get storage capacity of the bundle
   *
   * @return Storage capacity */

  public int getCapacity() {
    return capacity;
  }

  /** Get free storage capacity in the bundle. Returns UNLIMITED if
   * capacity is unlimited.
   *
   * @return Free storage.
   */

  public int getFreeSpace() {
    if(capacity==UNLIMITED)
      return UNLIMITED;
    else
      return (capacity-getMass());
  }

  /** Gets total mass of all materials and components in the bundle.
   *
   * @return Total mass.
   */

  public int getMass() {
    int m=0;
    Enumeration e = components();
    TransferableComponent tc;

    while(e.hasMoreElements()) {
      tc = (TransferableComponent) e.nextElement();
      m += tc.getMass();
    }

    return (m+fuel+dodechadrium+metals+minerals+oil+troops);
  }

  /** Gets amount of fuel in the bundle.
   *
   * @return Amount of fuel in the bundle.
   */
  
  public int getFuel() {
    return fuel;
  }

  /** Gets amount of dodechadrium in the bundle.
   *
   * @return Amount of dodechadrium in the bundle.
   */


  public int getDodechadrium() {
    return dodechadrium;
  }

  /** Gets amount of metals in the bundle.
   *
   * @return Amount of metals in the bundle.
   */

  public int getMetals() {
    return metals;
  }

  /** Gets amount of minerals in the bundle.
   *
   * @return Amount of minerals in the bundle.
   */

  public int getMinerals() {
    return minerals;
  }

  /** Gets amount of oil in the bundle.
   *
   * @return Amount of oil in the bundle.
   */

  public int getOil() {
    return oil;
  }

  /** Gets amount of troops in the bundle.
   *
   * @return Amount of troops in the bundle.
   */

  public int getTroops() {
    return troops;
  }

  /** Take given amount of fuel out of the bundle.
   *
   * @param a Amount of fuel to take.
   * @return True if there were enough and amount was removed. False if not.
   */

  public boolean takeFuel(int a) {
    if(fuel>=a) {
      fuel-=a;
      return true;
    } else
      return false;
  }

  /** Take given amount of dodechadrium out of the bundle.
   *
   * @param a Amount of dodechadrium to take.
   * @return True if there were enough and amount was removed. False if not.
   */

  public boolean takeDodechadrium(int a) {
    if(dodechadrium>=a) {
      dodechadrium-=a;
      return true;
    } else
      return false;
  }

  /** Take given amount of metals out of the bundle.
   *
   * @param a Amount of metals to take.
   * @return True if there were enough and amount was removed. False if not.
   */

  public boolean takeMetals(int a) {
    if(metals>=a) {
      metals-=a;
      return true;
    } else
      return false;
  }

  /** Take given amount of minerals out of the bundle.
   *
   * @param a Amount of minerals to take.
   * @return True if there were enough and amount was removed. False if not.
   */

  public boolean takeMinerals(int a) {
    if(minerals>=a) {
      minerals-=a;
      return true;
    } else
      return false;
  }

  /** Take given amount of oil out of the bundle.
   *
   * @param a Amount of oil to take.
   * @return True if there were enough and amount was removed. False if not.
   */

  public boolean takeOil(int a) {
    if(oil>=a) {
      oil-=a;
      return true;
    } else
      return false;
  }

  /** Take given amount of troops out of the bundle.
   *
   * @param a Amount of troops to take.
   * @return True if there were enough and amount was removed. False if not.
   */

  public boolean takeTroops(int a) {
    if(troops>=a) {
      troops-=a;
      return true;
    } else
      return false;
  }

  /** Add given amount of fuel in the bundle.
   *
   * @param a Amount of fuel to add.
   * @return True if there were enough space and the amount was added. 
   * False if not.
   */

  public boolean putFuel(int a) {
    if(a>getFreeSpace()) {
      return false;
    } else {
      fuel+=a;
      return true;
    }
  }

  /** Add given amount of dodechadrium in the bundle.
   *
   * @param a Amount of dodechadrium to add.
   * @return True if there were enough space and the amount was added. 
   * False if not.
   */

  public boolean putDodechadrium(int a) {
    if(a>getFreeSpace()) {
      return false;
    } else {
      dodechadrium+=a;
      return true;
    }
  }

  /** Add given amount of metals in the bundle.
   *
   * @param a Amount of metals to add.
   * @return True if there were enough space and the amount was added. 
   * False if not.
   */

  public boolean putMetals(int a) {
    if(a>getFreeSpace()) {
      return false;
    } else {
      metals+=a;
      return true;
    }
  }

  /** Add given amount of minerals in the bundle.
   *
   * @param a Amount of minerals to add.
   * @return True if there were enough space and the amount was added. 
   * False if not.
   */

  public boolean putMinerals(int a) {
    if(a>getFreeSpace()) {
      return false;
    } else {
      minerals+=a;
      return true;
    }
  }

  /** Add given amount of oil in the bundle.
   *
   * @param a Amount of oil to add.
   * @return True if there were enough space and the amount was added. 
   * False if not.
   */

  public boolean putOil(int a) {
    if(a>getFreeSpace()) {
      return false;
    } else {
      oil+=a;
      return true;
    }
  }

  /** Add given amount of troops in the bundle.
   *
   * @param a Amount of troops to add.
   * @return True if there were enough space and the amount was added. 
   * False if not.
   */

  public boolean putTroops(int a) {
    if(a>getFreeSpace()) {
      return false;
    } else {
      troops+=a;
      return true;
    }
  }

}
