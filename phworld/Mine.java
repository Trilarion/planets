/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import dsc.netgame.*;
import dsc.awt.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;


public class Mine
    extends MiscComponent {

    private int power;
    private int radius;

  public Mine (int id, 
	       String name, 
	       int owner, 
	       Location location,
	       int power) {
    super(id,name,owner,location);
    this.power=power;

    radius = calcRadius(power);
  }
  
    public static int calcRadius(int power) {
	if(power < 4)
	    power = 4;

	return (int) Math.sqrt(((double)power)/Math.PI);
    }

  public void drawAdditionalPreGraphs(ScaledGraphics g) {
    Color pri = Color.red;
    pri=pri.darker().darker().darker().darker();
    g.setColor(pri);

    g.fillOval(getLocation().getX()-getRadius(),
	       getLocation().getY()-getRadius(),
	       getRadius()*2,
	       getRadius()*2);
  }

    private int getRadius() {
	return radius;
    }

  public void drawAdditionalGraphs(ScaledGraphics g) {}

    public String getMapImage() {
	return "data/graph/still/components/misc/mine.gif";
    }

    public String getPanelImageName() {
	return "data/graph/still/panel/mine.gif";
    }
}
