/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import java.awt.*;
import dsc.awt.*;
import java.util.*;
import java.awt.event.*;
import dsc.util.*;


class BuildShipPanel extends PlanetSidePanel 
    implements ItemListener, ActionListener {

  private Choice hullChoice;
  private ScrollPane componentPickPane;
  private Panel componentPickPanel;
  private Label cargoLabel, massLabel, powerLabel, consumptionAndBuildtimeLabel, estimatedSpeedAndTravelDistanceLabel;
  private Button engineDivider, weaponDivider, shieldDivider, miscDivider;

  private Vector hulls, engines, weapons, shields, misc, allComponents;
  private int numberOfLabels=0;
  private int numberOfComponentPicks=0;
  private Vector checkboxes, nameLabels, conditionLabels, massLabels;
  private ShipHull currentHull;

    private boolean listenStateChanges = true;

  BuildShipPanel() {
    super("data/graph/still/panel/planet.build.gif");

    hulls=new Vector();
    engines=new Vector();
    weapons=new Vector();
    shields=new Vector();
    misc   =new Vector();

    checkboxes=new Vector();
    nameLabels=new Vector();
    conditionLabels=new Vector();
    massLabels=new Vector();

    hullChoice=new Choice();
    hullChoice.addItemListener(this);

    engineDivider=new Button("== ENGINES ==");
    weaponDivider=new Button("== WEAPONS ==");
    shieldDivider=new Button("== SHIELDS ==");
    miscDivider  =new Button("==  MISC   ==");
    engineDivider.setBackground(Color.blue);
    weaponDivider.setBackground(Color.green);
    shieldDivider.setBackground(Color.red);
    miscDivider.setBackground(Color.black);
    engineDivider.setForeground(Color.white);
    weaponDivider.setForeground(Color.white);
    shieldDivider.setForeground(Color.white);
    miscDivider.setForeground(Color.white);

    engineDivider.addActionListener(this);
    weaponDivider.addActionListener(this);
    shieldDivider.addActionListener(this);
    miscDivider.addActionListener(this);

    contentsPanel.setLayout(new GridBagLayout());
    GridBagConstraints gbc = new GridBagConstraints();

    gbc.fill=GridBagConstraints.BOTH;
    gbc.anchor=GridBagConstraints.NORTH;
    gbc.weightx=100;
    gbc.weighty=1;
    gbc.gridwidth=8; 
    gbc.gridheight=1;

    gbc.gridx=0; gbc.gridy=0;
    contentsPanel.add(hullChoice, gbc);

    componentPickPane = new ScrollPane();

    componentPickPanel = new Panel();
    componentPickPanel.setLayout(new GridBagLayout());
    componentPickPane.add(componentPickPanel);
    Adjustable adj = componentPickPane.getVAdjustable();
    adj.setUnitIncrement(engineDivider.getBounds().height);
    adj = componentPickPane.getHAdjustable();
    adj.setUnitIncrement(25); 

    gbc.gridx=0;
    gbc.gridy=1;
    gbc.gridwidth=8; gbc.gridheight=6;
    gbc.weighty=100;
    contentsPanel.add(componentPickPane, gbc);

    Panel textPanel = createLabels();
    gbc.gridx=0; 
    gbc.gridy=7;
    gbc.gridwidth=8; gbc.gridheight=4;    
    gbc.weighty=1;
    contentsPanel.add(textPanel, gbc);
  }

  private Panel createLabels() {
    Panel textPanel = new Panel();
    textPanel.setLayout(new VerticalFlowLayout());

    cargoLabel = new Label("Cargo: x/x");
    massLabel = new Label("Mass: x/x");
    powerLabel = new Label("E/W/S: x/x/x");
    estimatedSpeedAndTravelDistanceLabel = new Label("ES: x + EDT: #"); 
    consumptionAndBuildtimeLabel = new Label("EC: x(+x) + # rounds");


    textPanel.add(cargoLabel);
    textPanel.add(massLabel);
    textPanel.add(powerLabel);
    textPanel.add(estimatedSpeedAndTravelDistanceLabel);
    textPanel.add(consumptionAndBuildtimeLabel);


    return textPanel;
  }

  private void initializeListVectors(int max) {
    int i;

    numberOfComponentPicks=max;

    for(i=numberOfLabels; i<max; i++) {
      Checkbox c = new Checkbox();
      c.addItemListener(this);
      checkboxes.addElement(c);
      nameLabels.addElement(new Label());
      conditionLabels.addElement(new Label());
      massLabels.addElement(new Label());
    }
  }

  private void initializeVectors(MaterialBundle mb) {
    hulls=new Vector();
    engines=new Vector();
    weapons=new Vector();
    shields=new Vector();
    misc   =new Vector();
    allComponents=new Vector();

    Enumeration e = mb.components();
    
    while(e.hasMoreElements()) {
      Object o = e.nextElement();
      if(o instanceof ShipComponent) {
	if(o instanceof ShipHull)
	  hulls.addElement(o);
	if(o instanceof ShipEngine) {
	  engines.addElement(o);
	}
	if(o instanceof ShipWeapon) {
	  weapons.addElement(o);
	}
	if(o instanceof ShipShield) {
	  shields.addElement(o);
	}
	if(o instanceof ShipMisc) {
	  misc.addElement(o);
	}
      }
    }


    Quicksort.quicksort(hulls);
    Quicksort.quicksort(engines);
    Quicksort.quicksort(weapons);
    Quicksort.quicksort(shields);
    Quicksort.quicksort(misc);

    for(int i=0; i<engines.size(); i++)
      allComponents.addElement(engines.elementAt(i));
    for(int i=0; i<weapons.size(); i++)
      allComponents.addElement(weapons.elementAt(i));
    for(int i=0; i<shields.size(); i++)
      allComponents.addElement(shields.elementAt(i));
    for(int i=0; i<misc.size(); i++)
      allComponents.addElement(misc.elementAt(i));
  
    initializeListVectors(hulls.size() + engines.size() +
			  weapons.size() + shields.size() + misc.size());
  }
  
  /** Calculates total mass of all components currently selected.
   *  Does not include mass of the hull.
   *
   * @return Total mass.
   */

  private int calculateMass() {
    int mass=0;

    for(int i=0 ; i<allComponents.size(); i++) {
      Checkbox cb = (Checkbox) checkboxes.elementAt(i);
      TransferableComponent tc = 
	(TransferableComponent) allComponents.elementAt(i);
      
      if(cb.getState())
	mass+=tc.getMass();
    }

    return mass;
  }

  private void setCheckboxes(int maxMass) {
    for(int i=0 ; i<allComponents.size(); i++) {
      Checkbox cb = (Checkbox) checkboxes.elementAt(i);
      TransferableComponent tc = 
	(TransferableComponent) allComponents.elementAt(i);
      
      if(tc.getMass()<=maxMass)
	cb.setEnabled(true);
      else {
	if(!cb.getState())
	  cb.setEnabled(false);
      }
    }
  }

  private void setHullList(int minMass) {
    String current = hullChoice.getSelectedItem();
   
    hullChoice.removeAll(); 
    Enumeration e = hulls.elements();
    
    while(e.hasMoreElements()) {
      ShipHull sh = (ShipHull) e.nextElement();
      
      if(sh.getVolume()>=minMass)
	hullChoice.add(sh.toString());
    }

    hullChoice.select(current);
  }

  private int calculateEnergyConsumption() {
    int power=0;

    for(int i=0 ; i<allComponents.size(); i++) {
      Checkbox cb = (Checkbox) checkboxes.elementAt(i);
      ShipComponent sc = (ShipComponent) allComponents.elementAt(i);
      
      if(cb.getState()) {
	power+=sc.getPowerConsumption();
      }
    }

    return power;
  }

  private int calculateEnginePower() {
    int power=0;

    for(int i=0 ; i<engines.size(); i++) {
      Checkbox cb = (Checkbox) checkboxes.elementAt(i);
      ShipEngine tc = 
	(ShipEngine) engines.elementAt(i);
      
      if(cb.getState()) {
	power+=tc.getPowerProduction();
      }
    }

    return power;
  }

  private int calculateEngineConsumption() {
    int consumption=0;

    for(int i=0 ; i<engines.size(); i++) {
      Checkbox cb = (Checkbox) checkboxes.elementAt(i);
      ShipEngine tc = 
	(ShipEngine) engines.elementAt(i);
      
      if(cb.getState()) {
	consumption+=tc.getPowerConsumption();
      }
    }

    return consumption;
  }

  private int calculateWeaponHullPower() {
    int power=0;

    for(int i=0 ; i<weapons.size(); i++) {
      Checkbox cb = (Checkbox) checkboxes.elementAt(i+engines.size());
      ShipWeapon tc = 
	(ShipWeapon) weapons.elementAt(i);
      
      if(cb.getState()) {
	power+=tc.getMaxInflictableHullDamage();
      }
    }

    return power;
  }

  private int calculateWeaponShieldPower() {
    int power=0;

    for(int i=0 ; i<weapons.size(); i++) {
      Checkbox cb = (Checkbox) checkboxes.elementAt(i+engines.size());
      ShipWeapon tc = 
	(ShipWeapon) weapons.elementAt(i);
      
      if(cb.getState()) {
	power+=tc.getMaxInflictableShieldDamage();
      }
    }

    return power;
  }

  private int calculateShieldPower() {
    int power=0;

    for(int i=0 ; i<shields.size(); i++) {
      Checkbox cb = (Checkbox) checkboxes.elementAt(i+engines.size() +
						    weapons.size());
      ShipShield tc = 
	(ShipShield) shields.elementAt(i);
      
      if(cb.getState()) {
	power+=tc.getShieldPower();
      }
    }

    return power;
  }

  private int calculateMinShieldPower() {
    int power=0;

    for(int i=0 ; i<shields.size(); i++) {
      Checkbox cb = (Checkbox) checkboxes.elementAt(i+engines.size() +
						    weapons.size());
      ShipShield tc = 
	(ShipShield) shields.elementAt(i);
      
      if(cb.getState()) {
	power+=tc.getMinShieldPower();
      }
    }

    return power;
  }


  private ShipHull mapStringToHull(String s) {
    Enumeration e = hulls.elements();

    while(e.hasMoreElements()) {
      TransferableComponent tc = (TransferableComponent) e.nextElement();
      
      if(tc.toString().equals(s))
	return ((ShipHull) tc);
    }
    
    return null;
  }

  private int calculateBuildTime() {
    int n=0;

    for(int i=0 ; i<allComponents.size(); i++) {
      Checkbox cb = (Checkbox) checkboxes.elementAt(i);
      
      if(cb.getState())
	n++;
    }    

    return HostShip.getBuildTimeByComponents(n);
  }

  private void updateContents() {
    if(hullChoice.getSelectedItem()==null) {
      cargoLabel.setEnabled(false);
      massLabel.setEnabled(false);
      powerLabel.setEnabled(false);
      estimatedSpeedAndTravelDistanceLabel.setEnabled(false);
      consumptionAndBuildtimeLabel.setEnabled(false);
      setImage("data/graph/still/panel/planet.build.gif");
      return;
    }
      
    int mass = calculateMass();
    ShipHull hull = mapStringToHull(hullChoice.getSelectedItem());
    int maxMass = hull.getVolume()-mass;

    setCheckboxes(maxMass);

    if(currentHull != hull) {
        setHullList(mass);
	currentHull = hull;
    }

    cargoLabel.setText("Cargo: " +
		       maxMass +
		       "/" +
		       (hull.getVolume()));
    massLabel.setText("Mass: " +
		      (mass+hull.getMass()) +
		      "/"+
		      (hull.getMass()+hull.getVolume()));
    powerLabel.setText("W/S: " +
		       calculateWeaponHullPower() + "+" +
		       calculateWeaponShieldPower() + "/" +
		       calculateShieldPower() + "+" +
		       calculateMinShieldPower());

    int    enginePower = calculateEnginePower();
    int    fuelAmount  = hull.getVolume() - mass;

    if(enginePower>(hull.getMass()+mass) &&
       enginePower<(hull.getMass()+hull.getVolume()))
      fuelAmount=enginePower-hull.getMass()-mass;

    int edtInt = ShipMath.calculateEDT(true,
			     hull.getMass()+mass+fuelAmount, 
			     fuelAmount, 
			     enginePower,
			     calculateEngineConsumption(), 
			     hull.getPowerConsumption());


    String edtString = "" + (((double) edtInt) / ShipMath.SPEED_CONST);

    if(edtString.length() > 4)
      edtString=edtString.substring(0, 4);

    estimatedSpeedAndTravelDistanceLabel.setText(
     "E: " + calculateEnginePower() + 
     " ES: "+ ShipMath.warpSpeedString((hull.getVolume()+hull.getMass()),
				      calculateEnginePower())+
     " EDT: "+ edtString);
    consumptionAndBuildtimeLabel.setText("EC: " +
			     calculateEnergyConsumption() + "(+" +
			     hull.getPowerConsumption() +")  [" + 
			     calculateBuildTime()+ " turns]");

    cargoLabel.setEnabled(true);
    massLabel.setEnabled(true);
    powerLabel.setEnabled(true);
    estimatedSpeedAndTravelDistanceLabel.setEnabled(true);
    consumptionAndBuildtimeLabel.setEnabled(true);
   
    if(getPlanet().getEDTCircleRadius() != edtInt) { 
        getPlanet().setEDTCircleRadius(edtInt);
        getPlanet().redrawEDT();
    }

    setImage("data/graph/still/panel/ship" + hull.getTech() +
	     ".build.gif");
  }

  private int placeComponents(Button divider, 
			      GridBagConstraints gbc, 
			      int resource, 
			      Vector v) {
    gbc.gridwidth=16; 
    gbc.gridx=0;
    componentPickPanel.add(divider, gbc);
    gbc.gridy++;

    for (int i = 0; i < v.size(); i++) {
      Checkbox c = ((Checkbox) checkboxes.elementAt(resource));
      c.setState(false);
      gbc.gridx=0; gbc.gridwidth=2;
      componentPickPanel.add(c, gbc);

      Label l = ((Label) nameLabels.elementAt(resource));
      ShipComponent sc=(ShipComponent) v.elementAt(i);
      l.setText(sc + "");
      gbc.gridx=2; gbc.gridwidth=8;
      componentPickPanel.add(l, gbc);

      l = ((Label) conditionLabels.elementAt(resource));
      l.setText((100 - sc.getDamagePercentage()) + "%");
      gbc.gridx=10; gbc.gridwidth=3;
      componentPickPanel.add(l, gbc);

      l = ((Label) massLabels.elementAt(resource));
      l.setText(sc.getMass() + "");
      gbc.gridx=13; gbc.gridwidth=3;
      componentPickPanel.add(l, gbc);

      gbc.gridy++;
      resource++;
    }
    
    return resource;
  }

  private void createPickPanel() {
    componentPickPanel.removeAll();

    GridBagConstraints gbc = new GridBagConstraints();
    gbc.fill=GridBagConstraints.HORIZONTAL;
    gbc.anchor=GridBagConstraints.NORTH;
    gbc.weightx=100;
    gbc.weighty=0;
    gbc.gridheight=1;

    gbc.gridy=0;

    int resource=0;

    if(engines.size()>0) {
      resource=placeComponents(engineDivider, gbc, resource, engines);
    }
    if(weapons.size()>0) {
      resource=placeComponents(weaponDivider, gbc, resource, weapons);
    }
    if(shields.size()>0) {
      resource=placeComponents(shieldDivider, gbc, resource, shields);
    }
    if(misc.size()>0) {
      resource=placeComponents(miscDivider, gbc, resource, misc);
    }
  }

  void initializeContents(Planet p) {
    initializeVectors(p.getMaterials());

    hullChoice.removeAll();
    for (int i = 0; i < hulls.size() ; i++) {
      if(i==0) {
          currentHull = (ShipHull) hulls.elementAt(i);
      }
      hullChoice.addItem(hulls.elementAt(i)+"");
    }

    createPickPanel();
    updateContents();
  }

  private Vector createComponentVector () {
    Vector v = new Vector();

    for(int i=0 ; i<allComponents.size(); i++) {
      Checkbox cb = (Checkbox) checkboxes.elementAt(i);
      TransferableComponent tc = 
	(TransferableComponent) allComponents.elementAt(i);
      
      if(cb.getState())
	v.addElement(new Integer(tc.getId()));
    }

    return v;
  }

  boolean OKAction() {
    if(hulls.isEmpty())
      return false;

    ShipHull hull = mapStringToHull(hullChoice.getSelectedItem());
    int hullID=hull.getId();
    
    getPlanet().startBuildShip(hullID, createComponentVector());

    getPlanet().setEDTCircleRadius(0);
    getPlanet().redrawEDT();
    return true;
  }

  void cancelAction() {
    getPlanet().setEDTCircleRadius(0);
    getPlanet().redrawEDT();
    super.cancelAction();
  }

  void initiateHelp() {
    HelpWindow.callForHelp("data" + java.io.File.separator + "help",
			   "Playing",
			   "Build Ship");
  }

  public void itemStateChanged(ItemEvent evt) {
      if(listenStateChanges) 
	  updateContents();
  }

    private void toggleComponents(int minIndex, int maxIndex) {
	if(hullChoice.getSelectedItem()==null) {
	    return;
	}

	int mass = calculateMass();
	ShipHull hull = mapStringToHull(hullChoice.getSelectedItem());
	int remaining_space = hull.getVolume() - mass;

	for(int i=minIndex ; i<=maxIndex; i++) {
	    Checkbox cb = (Checkbox) checkboxes.elementAt(i);
	    ShipComponent sc = 
		(ShipComponent) allComponents.elementAt(i);

	    if(cb.getState()) {
		remaining_space += sc.getMass();
	    } else {
		remaining_space -= sc.getMass();
	    }
	}

	if(remaining_space <= 0) {
	    /* cannot togle */
	    return;
	}

	listenStateChanges = false;

	for(int i=minIndex ; i<=maxIndex; i++) {
	    Checkbox cb = (Checkbox) checkboxes.elementAt(i);
	    cb.setState(!cb.getState());
	}

	listenStateChanges = true;

	updateContents();
    }

    private void toggleEngines() {
	toggleComponents(0, engines.size()-1);
    }

    private void toggleWeapons() {
	toggleComponents(engines.size(), 
			 engines.size() + weapons.size()-1);
    }

    private void toggleShields() {
	toggleComponents(engines.size() + weapons.size(),
			 engines.size() + weapons.size() + shields.size()-1);
    }

    private void toggleMisc() {
	toggleComponents(engines.size() + weapons.size() + shields.size(),
			 engines.size() + weapons.size() + shields.size() +
			 misc.size()-1);
    }


    public void actionPerformed(ActionEvent evt) {
	super.actionPerformed(evt);

	Object src = evt.getSource();

	if(src == engineDivider)
	    toggleEngines();
	else if(src == weaponDivider)
	    toggleWeapons();
	else if(src == shieldDivider)
	    toggleShields();
	else if(src == miscDivider)
	    toggleMisc();
    }
}


