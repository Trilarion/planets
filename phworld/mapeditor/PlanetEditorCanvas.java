package phworld.mapeditor;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import dsc.awt.*;
import dsc.netgame.*;
import phworld.PlanetType;

public class PlanetEditorCanvas extends Canvas implements MouseListener, ActionListener, MouseMotionListener {

    private int       xSize,ySize;
    private int       gameLength;
    private String    winCondition;


    private int       startingPointX,startingPointY,endPointX,endPointY;
    private boolean   grid;

    private Vector    planetVector;
    private ImageLoadBuffer images;

    private PopupMenu planetMenu;

    private PlanetEditorPlanet planetToChange;
    private PlanetEditorPlanet selectedPlanet;
    private int startX;
    private int startY;

    private static Font textFont = null;
    private static FontMetrics textFontMetrics = null;
    private static final int TEXT_FONT_SIZE = 12; 

    private File file;
    private Hashtable planetProperties = new Hashtable();

    public PlanetEditorCanvas(int x,int y,Color c) {

	setSize(x,y);
	gameLength=200; // defaults to 200
	winCondition = new String("MOST"); // default to MOST (not CRITICAL)

	createPopupMenu();

	setBackground(c);
    	addMouseListener(this);
	addMouseMotionListener(this);

	planetVector = new Vector(5);
	images = new ImageLoadBuffer();

	grid=true;
    }

    private void createPopupMenu() {
	planetMenu = new PopupMenu();
	MenuItem mi;

	planetMenu.add(mi = new MenuItem("HOME"));
	mi.addActionListener(this);

	planetMenu.add(mi = new MenuItem("DODECHAEDRON"));
	mi.addActionListener(this);

	for(int i = PlanetType.MIN_NUMBER+1 ; 
	    i <= PlanetType.MAX_NUMBER ; 
	    i++) {
	    planetMenu.add(mi = new MenuItem(PlanetType.getPlanetTypeName(i)));
	    mi.addActionListener(this);
	}

	planetMenu.add(mi = new MenuItem("RANDOM"));
	mi.addActionListener(this);

	planetMenu.addSeparator();

	planetMenu.add(mi = new MenuItem("DELETE PLANET"));
	mi.addActionListener(this);

	add(planetMenu);
    }


    public void paint(Graphics g) {
    
   	Enumeration e = planetVector.elements();

	PlanetEditorPlanet p;

	if (grid==true) {
	    g.setColor(Color.gray);

	    for(int height=0;height <= ySize;height=height+100)
		g.drawLine(0,height,xSize,height);
	    for(int width=0;width <= xSize;width=width+100)
		g.drawLine(width,0,width,ySize);
	}
	    


	while (e.hasMoreElements()) {
	    p = (PlanetEditorPlanet) e.nextElement();
	    Image planetImage = images.getImage(p.getImageFileName());

	    int x = p.getX();
	    int y = p.getY();
	    String s  = p.getName();
	    int owner = p.getPlanetOwner();
	    
	    g.drawImage(planetImage,
			x-(planetImage.getWidth(this)/2),
			y-(planetImage.getHeight(this)/2),this);
	    
	    // Pretty much copied straight out from phworld/Planet.java
	    
	    if (s.equals("")) continue;

	    if(textFont==null) {
		textFont = new Font("SansSerif", Font.ITALIC, TEXT_FONT_SIZE);
		textFontMetrics = g.getFontMetrics(textFont);
	    }
	    
	    int width = textFontMetrics.stringWidth(s);
	    int height= textFontMetrics.getHeight();
	    
	    Color pri = GlobalDefines.getPrimaryColor(owner);
	    Color sec = GlobalDefines.getSecondaryColor(owner);
	    
	    g.setColor(pri);
	    g.setFont(textFont);
	    g.drawString(s, x-(width)/2, y+25+height);
	    
	    if(!sec.equals(pri)) {
		g.setColor(sec);
		g.drawLine( x-width/2, y+27+height,
			    x+width/2, y+27+height);
	    }
	    
	}

	if (selectedPlanet != null) {
	    //draw a rectangle around the last selected planet

	    g.setColor(Color.white);

	    int h = getImageHeight(selectedPlanet);
	    int w = getImageWidth(selectedPlanet);
	    int x = selectedPlanet.getX();
	    int y = selectedPlanet.getY();

	    g.drawRect(x-w/2,y-h/2,w,h);
	}
	
    }


    public void mouseClicked(MouseEvent e) {
	PlanetEditorPlanet plan;

	if ((e.getModifiers() & MouseEvent.BUTTON1_MASK) != 0) {
	    //Button 1 clicked

	    if(clickedOnPlanet(e.getX(),e.getY(),2.0) == null) { 
		//and not overlapping another planet -> New Planet
		planetVector.addElement(new PlanetEditorPlanet(e.getX(),
							       e.getY()));
		
		repaint();
	    } else if((plan = clickedOnPlanet(e.getX(),e.getY(),1.0)) != null) {
		//Clicked the left mouse button on a planet -> Let's select it!
		selectedPlanet = plan;
		
		repaint();
	    }

	}
	else if ((e.getModifiers() & MouseEvent.BUTTON3_MASK) != 0) {

	    if( (plan = clickedOnPlanet(e.getX(),e.getY(),1.0)) != null) {
		//Clicked the right mouse button on a planet
		planetToChange = plan;
		
		planetMenu.show(e.getComponent(),e.getX(),e.getY());
	    }
	    repaint();
	}
    }
    public void mousePressed(MouseEvent e) {
	PlanetEditorPlanet plan;

	if ((e.getModifiers() & MouseEvent.BUTTON1_MASK) != 0) {
	    //Button 1 clicked

	    plan = clickedOnPlanet(e.getX(),e.getY(),1.0);

	    if(plan == null) { 
		//not clicked on a planet -> lose focus
		selectedPlanet = null;

		repaint();
	    } else if (plan != selectedPlanet) {
		//clicked on another planet -> change focus
		selectedPlanet = plan;
		repaint();
	    }

	    startX = e.getX();
	    startY = e.getY();

	    //	    System.out.println("startX:"+startX+"startY:"+startY);
	}

    }

    private int distanceBetween(int x1,int y1,int x2,int y2) {
	double distance = 
	    Math.sqrt( (double) ((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1)) );
	return ((int) Math.round(distance));
    }

    private PlanetEditorPlanet clickedOnPlanet(int x, int y,double multiplier) {
	
	Enumeration e = planetVector.elements();
	
	while (e.hasMoreElements()) {
	    PlanetEditorPlanet p = (PlanetEditorPlanet) e.nextElement();
	    Image planetImage = images.getImage(p.getImageFileName());	    
	    
	    if ( (distanceBetween(x,y,p.getX(),p.getY())) 
		 < (int) (planetImage.getWidth(this)/2*multiplier) ) {
		return p;
	    }
	}
	return null;
    }

    public void mouseReleased(MouseEvent e) {
    }
    public void mouseEntered(MouseEvent e) {}
    public void mouseExited(MouseEvent e) {}

    public void mouseDragged(MouseEvent e) {

	if ((e.getModifiers() & MouseEvent.BUTTON1_MASK) != 0) {
	    //Button 1 pressed
	    
	    if (selectedPlanet != null) {
		int h = getImageHeight(selectedPlanet);
		int w = getImageWidth(selectedPlanet);
		int xold = selectedPlanet.getX();
		int yold = selectedPlanet.getY();
		int x = e.getX();
		int y = e.getY();
		
		selectedPlanet.setX(x);
		selectedPlanet.setY(y);
	    
		repaint();
	    }
	}
    }


    public void mouseMoved(MouseEvent e) {}

    public void actionPerformed(ActionEvent e) {
	if(e.getActionCommand().equals("DELETE PLANET")) {
	    deletePlanet();
	} else {
	    changePlanetType(e.getActionCommand());
	}
    }  

    
    public void clearMap() {
	planetVector.removeAllElements();
	selectedPlanet=null;
	planetToChange=null;
    }

    public void setGrid(boolean b) {
	grid = b;

	repaint();
    }

    public void setSize(int x,int y) {
	xSize=x;
	ySize=y;
	super.setSize(x,y);
    }

    public int getXSize() {
	return xSize;
    }

    public int getYSize() {
	return ySize;
    }

    public boolean getGrid() {
	return grid;
    }

    private void deletePlanet() {
	planetVector.removeElement(planetToChange);

	if (selectedPlanet==planetToChange) {
	    selectedPlanet=null;
	}
	planetToChange=null;

	
	repaint();
    }
    
    private void changePlanetType(String s) {

	planetToChange.setPlanetType(s);

	repaint();
    }

    private int getImageWidth(PlanetEditorPlanet p) {
	Image i = images.getImage(p.getImageFileName());
	return i.getWidth(this);
    }

    private int getImageHeight(PlanetEditorPlanet p) {
	Image i = images.getImage(p.getImageFileName());
	return i.getHeight(this);
    }

    public void loadMap(String s) {
	file = new File(s);
	
	if (file.exists()) {
	    if (file.canRead() && file.isFile()) {
		EditorMapLoader eml = new EditorMapLoader(s);

		planetVector = eml.getPlanetVector();
		setSize(eml.getXSize(),eml.getYSize());
		
		planetProperties = eml.getPlanetResources();

		selectedPlanet=null;
		planetToChange=null;

		repaint();
		
	    } else {
		System.out.println("File can't be read!");
		System.exit(2);
	    }

	}
	else System.out.println("Creating new file "+s);

    }

    public void saveMap(String s) {

	FileWriter out;
	try {
	    out = new FileWriter(s);
	} catch (Exception e) {
	    System.err.println("Can't write to file " + s);
	    return;
	}

	try {
	    out.write("#This is a map created by the PH Map Editor\n");
	    out.write("\n");
	    out.write("BEGIN MAPINFO\n");

	    planetProperties.remove("XSIZE");
	    planetProperties.remove("YSIZE");
	    planetProperties.put("YSIZE", Integer.toString(ySize));
	    planetProperties.put("XSIZE", Integer.toString(xSize));

	    Enumeration e = planetProperties.keys();

	    while(e.hasMoreElements()) {
		String key = (String) e.nextElement();
		out.write(key + "=" + planetProperties.get(key) + "\n");
	    }

	    out.write("END MAPINFO\n");
	    out.write("\n");
	    out.write("BEGIN PLANETS\n");
	    out.write("#X\tY\tTYPE\t\tOWNER\tMAT/PROD/TECH\tNAME\n");
	    
	    e = planetVector.elements();
	    
	    while (e.hasMoreElements()) {
		PlanetEditorPlanet p = (PlanetEditorPlanet) e.nextElement();
		out.write(p.toString()+"\n");
	    }
	    
	    out.write("END PLANETS\n");
	} catch (IOException e) {
	    System.err.println("Can't write to file " + s);
	    return;
	}   
	
	try {	
	    out.close();
	} catch (IOException e) {
	    System.err.println("Can't close file " + s);
	    return;
	}

    }

    public void trimMap() {
	int minX,minY,maxX,maxY;
	int planWidth = 100; //gives some space for planet names etc.
	
	int tempX,tempY;
	PlanetEditorPlanet p;
   	Enumeration e;


	e = planetVector.elements();

	if (e.hasMoreElements()) {
	    p = (PlanetEditorPlanet) e.nextElement();
	    minX = maxX = p.getX();
	    minY = maxY = p.getY();
	} else return; //tried to trim an empty map

	while (e.hasMoreElements()) {
	    p = (PlanetEditorPlanet) e.nextElement();
	    tempX=p.getX(); tempY=p.getY();

	    if(tempX > maxX) maxX=tempX;
	    else if (tempX < minX) minX=tempX;
	    
	    if(tempY > maxY) maxY=tempY;
	    else if (tempY < minY) minY=tempY;
	}

	int xShift = planWidth/2 - minX;
	int yShift = planWidth/2 - minY;

   	e = planetVector.elements();
	while (e.hasMoreElements()) {
	    p = (PlanetEditorPlanet) e.nextElement();
	    tempX=p.getX(); tempY=p.getY();
	    
	    p.setX(tempX+xShift); p.setY(tempY+yShift);

	}

	setSize(maxX-minX+planWidth,maxY-minY+planWidth);

	return;

    }

    public void scaleMap(double scalingFactor) {

	PlanetEditorPlanet p;
	int oldX,oldY;

	Enumeration e = planetVector.elements();

	setSize((int) (getXSize() * scalingFactor),
		(int) (getYSize() * scalingFactor));
    
	while (e.hasMoreElements()) {
	    p = (PlanetEditorPlanet) e.nextElement();
	    oldX = p.getX(); oldY = p.getY();

	    p.setX( (int) (oldX * scalingFactor) );
	    p.setY( (int) (oldY * scalingFactor) );

	}
	return;
    }

    public Hashtable getPlanetProperties() {
	return planetProperties;
    }

    public void setPlanetProperties(Hashtable pp) {
	planetProperties = pp;
    }

    public PlanetEditorPlanet getSelectedPlanet() {
	return selectedPlanet;
    }
}
