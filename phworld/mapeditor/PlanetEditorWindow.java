/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2001 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net
  leksa@users.sourceforge.net  
*/

package phworld.mapeditor;

import java.awt.*;
import java.awt.event.*;
import dsc.awt.*;
import java.util.*;
import java.io.*;

/** The sole purpose for this editor is to create and edit map files
 * to be used in the game Planetary Hoppers
 *
 * @author Dodekaedron Software Creations, Inc.
 */

public class PlanetEditorWindow extends CloseableLayoutableFrame implements ActionListener, MouseListener, ItemListener {

    private ScrollPane mapPane; 
    private PlanetEditorCanvas  mapCanvas;
    private int        mapXSize,mapYSize;
    private MenuBar    mb;

    private String     mapFile;
    private String     mapDirectory="";
    private MapSizeDialog sizeDialog;
    private FileDialog loadDialog,saveDialog;

    private MatProdTechDialog mptDialog;
    private MaterialEditDialog matEditDialog;
    private ProductionEditDialog prodEditDialog;
    private TechnologyEditDialog techEditDialog;
    private ComponentsEditDialog compEditDialog;

    private NameDialog nameDialog;

    private MenuItem newMI,openMI,saveMI,saveasMI;

    class MyMenuItem extends MenuItem {
	public MyMenuItem(String s, ActionListener al) {
	    super(s);
	    addActionListener(al);
	}
    }

    public PlanetEditorWindow(String s) {

	mapFile = s;
	if (!s.equals("")) {
	    setTitle("PH Map Editor - " + mapFile);
	} else {
	    setTitle("PH Map Editor - new map");
	}

	setSize(600,480);
	
	Image i=GraphicsLoader.getImage("data/graph/still/icons/game.gif");
	setIconImage(i);
	
	mapPane = new ScrollPane();
	
	add(mapPane);
	
	mapCanvas = new PlanetEditorCanvas(mapXSize=1000,mapYSize=1000,
					   Color.black);
	mapPane.add(mapCanvas);
	Adjustable adj = mapPane.getVAdjustable();
	adj.setUnitIncrement(25);
	adj = mapPane.getHAdjustable();
	adj.setUnitIncrement(25); 

	if (!mapFile.equals("")) {
	    mapCanvas.loadMap(mapFile);
	}

	mapXSize = mapCanvas.getXSize();
	mapYSize = mapCanvas.getYSize();

	sizeDialog = new MapSizeDialog(this,mapXSize,mapYSize);

	mptDialog = new MatProdTechDialog(this);
	matEditDialog = new MaterialEditDialog(this);
	prodEditDialog = new ProductionEditDialog(this);
	techEditDialog = new TechnologyEditDialog(this);
	compEditDialog = new ComponentsEditDialog(this);

	nameDialog = new NameDialog(this);

	loadDialog = new FileDialog(this,"Load Map");
	loadDialog.setDirectory("data"+File.separator+"maps");

	saveDialog = new FileDialog(this,"Save Map as",FileDialog.SAVE);
	saveDialog.setDirectory("data"+File.separator+"maps");

	Menu file = new Menu ("File",true);
	file.add(newMI    = new MyMenuItem ("New",this));
	file.add(openMI   = new MyMenuItem ("Open",this));
	file.add(saveMI   = new MyMenuItem ("Save",this));
	file.add(saveasMI = new MyMenuItem ("Save as",this));
	file.addSeparator();
	file.add(new MyMenuItem ("Quit",this));

	if (mapFile.equals("")) {
	    saveMI.setEnabled(false);
	}

	Menu settings = new Menu ("Settings",true);
	settings.add(new MyMenuItem ("Map Size",this));
	settings.add(new MyMenuItem ("Grid On/Off",this));
	settings.add(new MyMenuItem ("Trim Map",this));

	Menu scale = new Menu("Scaling",false);
	scale.add(new MyMenuItem ("+10%",this));
	scale.add(new MyMenuItem ("-10%",this));
	settings.add(scale);

	Menu edit = new Menu ("Edit",true);
	edit.add(new MyMenuItem ("name",this));
	edit.add(new MyMenuItem ("mat/prod/tech",this));

	mb = new MenuBar();
	mb.add(file);
	mb.add(settings);
	mb.add(edit);
	setMenuBar(mb);
    }

    public void refreshMap() {
	mapCanvas.repaint();
    }

    public void repaintMap() {
	mapCanvas.repaint();
    }

    public int getMapXSize() {
	return mapXSize;
    }
    
    public int getMapYSize() {
	return mapYSize;
    }

    public void setupMaterialEditDialog(PlanetEditorPlanet planet, Hashtable pp,String matString) {
	matEditDialog.setupMaterialEditDialog(planet, pp, matString);
    }

    public void setupProductionEditDialog(PlanetEditorPlanet planet, Hashtable pp,String prodString) {
	prodEditDialog.setupProductionEditDialog(planet, pp, prodString);
    }


    public void setupTechnologyEditDialog(PlanetEditorPlanet planet, Hashtable pp,String techString) {
	techEditDialog.setupTechnologyEditDialog(planet, pp, techString);
    }

    public void setupComponentsEditDialog(PlanetEditorPlanet planet, Hashtable pp, String compString) {
	compEditDialog.setupComponentsEditDialog(planet, pp, compString);
    }


    public Hashtable getPlanetProperties() {
	return mapCanvas.getPlanetProperties();
    }
    
    public void setPlanetProperties(Hashtable pp) {
	mapCanvas.setPlanetProperties(pp);
    }
    
    public PlanetEditorPlanet getSelectedPlanet() {
	return mapCanvas.getSelectedPlanet();
    }

    public void resetMatProdTechDialog(int i,String s) {
	mptDialog.reset(i,s);
    }

    public void resetMaterialEditDialog(String s) {
	matEditDialog.reset(s);
    }

    public void mouseClicked(MouseEvent e) {
    }
    public void mousePressed(MouseEvent e) {
    }
    public void mouseReleased(MouseEvent e) {
    }
    public void mouseEntered(MouseEvent e) {}
    public void mouseExited(MouseEvent e) {}

    public void itemStateChanged(ItemEvent e) {}

    public void actionPerformed(ActionEvent e) {

	if(e.getActionCommand().equals("Quit")) {
	    dispose();
	    System.exit(1);
	} else if(e.getActionCommand().equals("Open")) { 

	    loadDialog.show();
	    String f = loadDialog.getFile();
	    if (f != null) {
		mapDirectory = loadDialog.getDirectory();
		mapFile = mapDirectory+f;

		mapCanvas.loadMap(mapFile);
		saveMI.setEnabled(true);		
		setTitle("PH Map Editor - " + mapFile);
		mptDialog.flush();
	    }

	} else if(e.getActionCommand().equals("Save")) {
	    mapCanvas.saveMap(mapFile);
	} else if(e.getActionCommand().equals("Save as")) {
	    saveDialog.show();

	    String f = saveDialog.getFile();
	    if (f != null) {
		mapFile = saveDialog.getDirectory()+f;
		mapCanvas.saveMap(mapFile);
		setTitle("PH Map Editor - " + mapFile);		
		saveMI.setEnabled(true);
	    }


	} else if(e.getActionCommand().equals("New")) {
	    mapCanvas.clearMap();
	    mapFile = "";
	    mapCanvas.repaint();
	    setTitle("PH Map Editor - new map");		
	    saveMI.setEnabled(false);
	    mapCanvas.setPlanetProperties(new Hashtable());
	    
  	} else if(e.getActionCommand().equals("Map Size")) {

	    sizeDialog.setPanelSize(mapCanvas.getXSize(),mapCanvas.getYSize());
	    sizeDialog.show();
	    mapXSize=sizeDialog.getXSize();
	    mapYSize=sizeDialog.getYSize();

	    mapCanvas.setSize(mapXSize,mapYSize);
	    mapCanvas.repaint();

  	} else if(e.getActionCommand().equals("Grid On/Off")) {
	    if (mapCanvas.getGrid()) mapCanvas.setGrid(false);
	    else mapCanvas.setGrid(true);
  	} else if(e.getActionCommand().equals("Trim Map")) {
	    mapCanvas.trimMap();
	    mapCanvas.repaint();
  	} else if(e.getActionCommand().equals("+10%")) {
	    mapCanvas.scaleMap(1.10);
	    mapCanvas.repaint();
  	} else if(e.getActionCommand().equals("-10%")) {
	    mapCanvas.scaleMap(0.90);
	    mapCanvas.repaint();
	} else if(e.getActionCommand().equals("mat/prod/tech")) {
	    mptDialog.showYourself();       
	} else if(e.getActionCommand().equals("name")) {
	    nameDialog.showYourself();       
	    mapCanvas.repaint();
	}

    }



    public static void main(String[] args) {
	
	Frame f;

	if(args.length > 0) f=new PlanetEditorWindow(args[0]);
	else f=new PlanetEditorWindow("");

	f.show();
    }
}
