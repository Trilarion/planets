package phworld.mapeditor;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class ProductionEditDialog extends Dialog implements ActionListener{

    Button    okButton;
    Button    cancelButton;
    
    TextField prodStringName,fueTF,dodTF,metTF,minTF,oilTF,troTF;

    PlanetEditorWindow parentFrame;

    PlanetEditorPlanet selPlanet;
    Hashtable planetProperties;

    GridBagLayout gb;
    GridBagConstraints gbc;

    static int BOTH = GridBagConstraints.BOTH;
    static int WEST = GridBagConstraints.WEST;
    static int CENTER = GridBagConstraints.CENTER;
    static int EAST = GridBagConstraints.EAST;


    public ProductionEditDialog (PlanetEditorWindow parent) {
	super(parent, "Production Edit Dialog", true);

	parentFrame = parent;

	okButton =  new Button("OK");
	cancelButton =  new Button("Cancel");

	okButton.addActionListener(this);
	cancelButton.addActionListener(this);


	gb = new GridBagLayout();
	gbc = new GridBagConstraints();
	setLayout(gb);

	try {

	    //LABELS
	    addComponent(this,new Label("Production set name"),0,0,1,1,BOTH,WEST);
	    addComponent(this,new Label("Dodechadrium"),0,2,1,1,BOTH,WEST);
	    addComponent(this,new Label("Fuel"),0,3,1,1,BOTH,WEST);
	    addComponent(this,new Label("Metals"),0,4,1,1,BOTH,WEST);
	    addComponent(this,new Label("Minerals"),0,5,1,1,BOTH,WEST);
	    addComponent(this,new Label("Oil"),0,6,1,1,BOTH,WEST);
	    addComponent(this,new Label("Troops"),0,7,1,1,BOTH,WEST);

	    //TEXTFIELDS
	    addComponent(this,prodStringName = new TextField(10),1,0,1,1,BOTH,CENTER);
	    addComponent(this,dodTF = new TextField(5),1,2,1,1,BOTH,CENTER);
	    addComponent(this,fueTF = new TextField(5),1,3,1,1,BOTH,CENTER);
	    addComponent(this,metTF = new TextField(5),1,4,1,1,BOTH,CENTER);
	    addComponent(this,minTF = new TextField(5),1,5,1,1,BOTH,CENTER);
	    addComponent(this,oilTF = new TextField(5),1,6,1,1,BOTH,CENTER);
	    addComponent(this,troTF = new TextField(5),1,7,1,1,BOTH,CENTER);


	    //BUTTONS
	    addComponent(this,okButton,0,10,1,1,BOTH,CENTER);
	    addComponent(this,cancelButton,1,10,2,1,BOTH,CENTER);

	} catch (AWTException e) {}

	setSize(300,300);
    }

    private void addComponent(Container cont,Component comp, int gridx, int gridy,
			      int gridwidth,int gridheight,int fill,int anchor) 
	throws AWTException {
	gbc.gridx = gridx; gbc.gridy=gridy;
	gbc.gridwidth = gridwidth; gbc.gridheight = gridheight;
	gbc.fill = fill;
	gbc.anchor=anchor;
	cont.add(comp,gbc);
    }


    public void actionPerformed(ActionEvent e) {
	validate();

	if (e.getActionCommand().equals("OK")) {
	    String prodString = new String();

	    prodString += dodTF.getText().trim() + "/";
	    prodString += fueTF.getText().trim() + "/";
	    prodString += metTF.getText().trim() + "/";
	    prodString += minTF.getText().trim() + "/";
	    prodString += oilTF.getText().trim() + "/";
	    prodString += troTF.getText().trim();

	    String newName = "P" + prodStringName.getText().trim();

	    planetProperties.remove(newName);

	    planetProperties.put(newName,prodString);


	    parentFrame.resetMatProdTechDialog(1,newName); // 0 - material, 1 - prod, 2 - technology 

	} else if (e.getActionCommand().equals("Cancel")) {
	} else {
	    System.err.println("Undefined Button Pressed!");
	    System.exit(3);
	}

	setVisible(false);
    }

    public void setupProductionEditDialog(PlanetEditorPlanet planet, Hashtable pp, String pString) {

	selPlanet = planet;
	planetProperties = pp;

	if (pString.equals("-"))
	    prodStringName.setText(selPlanet.getName());
	else prodStringName.setText(pString.substring(1));

	StringTokenizer st;

	if (pString.equals("-")) {
	    st = new StringTokenizer("0/0/0/0/0/0","/");
	}	
	else {
	    st = new StringTokenizer((String) planetProperties.get(
				  pString),"/");
	}

	dodTF.setText(st.nextToken());
	fueTF.setText(st.nextToken());
	metTF.setText(st.nextToken());
	minTF.setText(st.nextToken());
	oilTF.setText(st.nextToken());
	troTF.setText(st.nextToken());

	show();

    }

}
