package phworld.mapeditor;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class ComponentsEditDialog extends Dialog implements ActionListener{

    Button    okButton;
    Button    cancelButton;
    
    TextField componentsStringName;
    
    TextField[][] componentNumber;

    String    originalComponentsString;

    PlanetEditorWindow parentFrame;

    PlanetEditorPlanet selPlanet;
    Hashtable planetProperties;

    GridBagLayout gb;
    GridBagConstraints gbc;

    static int BOTH = GridBagConstraints.BOTH;
    static int WEST = GridBagConstraints.WEST;
    static int CENTER = GridBagConstraints.CENTER;
    static int EAST = GridBagConstraints.EAST;


    public ComponentsEditDialog (PlanetEditorWindow parent) {
	super(parent, "Components Edit Dialog", true);

	parentFrame = parent;

	okButton =  new Button("OK");
	cancelButton =  new Button("Cancel");

	okButton.addActionListener(this);
	cancelButton.addActionListener(this);

	componentNumber = new TextField[phworld.TechLevel.N_TECH_TYPES]
	                               [phworld.TechLevel.MAX_TECH];


	gb = new GridBagLayout();
	gbc = new GridBagConstraints();
	setLayout(gb);

	try {

	    //LABELS
	    addComponent(this,new Label("Component Set Name"),0,0,2,1,BOTH,CENTER);
	    addComponent(this,new Label("Tech"),0,1,1,1,BOTH,CENTER);
	    for(int i=1;i<=phworld.TechLevel.N_TECH_TYPES;i++)
		addComponent(this,new Label(phworld.TechLevel.getTechName(i)),i,1,1,1,BOTH,CENTER);
	    for(int j=1;j<=phworld.TechLevel.MAX_TECH;j++)
		addComponent(this,new Label(j+""),0,1+j,1,1,BOTH,CENTER);
		

	    //TEXTFIELDS
	    addComponent(this,componentsStringName = new TextField(10),2,0,3,1,BOTH,CENTER);
	    for(int i=0; i<phworld.TechLevel.N_TECH_TYPES; i++) 
		for(int j=0; j<phworld.TechLevel.MAX_TECH; j++) 
		    addComponent(this,componentNumber[i][j] = 
				 new TextField(4),1+i,2+j,1,1,BOTH,CENTER);


	    //BUTTONS
	    addComponent(this,okButton,0,2+phworld.TechLevel.MAX_TECH,1,1,BOTH,CENTER);
	    addComponent(this,cancelButton,phworld.TechLevel.N_TECH_TYPES,2+phworld.TechLevel.MAX_TECH,2,1,BOTH,CENTER);
	    
	} catch (AWTException e) {}

	setSize(500,400);
    }

    private void addComponent(Container cont,Component comp, int gridx, int gridy,
			      int gridwidth,int gridheight,int fill,int anchor) 
	throws AWTException {
	gbc.gridx = gridx; gbc.gridy=gridy;
	gbc.gridwidth = gridwidth; gbc.gridheight = gridheight;
	gbc.fill = fill;
	gbc.anchor=anchor;
	cont.add(comp,gbc);
    }


    public void actionPerformed(ActionEvent e) {
	validate();

	if (e.getActionCommand().equals("OK")) {
	    String techKey,techString;
	

	    for(int i=0; i<phworld.TechLevel.N_TECH_TYPES; i++) {
	
		techKey = "C" + phworld.TechLevel.getTechName(i+1).charAt(0)
		    + componentsStringName.getText().trim();
		techString="";

		for(int j=0; j<phworld.TechLevel.MAX_TECH; j++) {
		    techString += componentNumber[i][j].getText().trim();
		    if (j<phworld.TechLevel.MAX_TECH-1) 
			techString += "/";
		}

		planetProperties.remove(techKey);
		planetProperties.put(techKey,techString);

	    }	

	    techKey = "C_"+componentsStringName.getText().trim();
	    techString = "";

	    for(int i=0; i<phworld.TechLevel.N_TECH_TYPES; i++) {
		techString += "C" + phworld.TechLevel.getTechName(i+1).charAt(0)
		    + componentsStringName.getText().trim();
		if (i<phworld.TechLevel.N_TECH_TYPES-1) 
		    techString += "/";
	    }

	    planetProperties.remove(techKey);
	    planetProperties.put(techKey,techString);

	    parentFrame.resetMaterialEditDialog(componentsStringName.getText().trim()); 

	} else if (e.getActionCommand().equals("Cancel")) {
	} else {
	    System.err.println("Undefined Button Pressed!");
	    System.exit(3);
	}

	setVisible(false);
    }

    public void setupComponentsEditDialog(PlanetEditorPlanet planet, Hashtable pp, String cString) {

	String[] techStrings;
	
	selPlanet = planet;
	planetProperties = pp;

	techStrings = new String[phworld.TechLevel.N_TECH_TYPES];

	if (cString.equals("-"))
	    componentsStringName.setText(selPlanet.getName());
	else componentsStringName.setText(cString.substring(2));

	StringTokenizer st;

	if (cString.equals("-")) {
	    for(int i=0;i<phworld.TechLevel.N_TECH_TYPES; i++)
		for(int j=0;j<phworld.TechLevel.MAX_TECH;j++)
		    componentNumber[i][j].setText("0");
	} else {
	    st = new StringTokenizer((String) planetProperties.get(cString),"/");
	    for(int i=0;i<phworld.TechLevel.N_TECH_TYPES; i++) {
		techStrings[i] = st.nextToken();
	    }   
	    for(int i=0;i<phworld.TechLevel.N_TECH_TYPES; i++) {
		st = new StringTokenizer((String) planetProperties.get(techStrings[i]),"/");
		for(int j=0;j<phworld.TechLevel.MAX_TECH;j++)
		    componentNumber[i][j].setText(st.nextToken());
	
		
	    }
	}
	
	show();
	
    }
}
