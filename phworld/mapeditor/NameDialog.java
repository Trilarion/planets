package phworld.mapeditor;

import java.awt.*;
import java.awt.event.*;

public class NameDialog extends Dialog implements ActionListener,KeyListener {

    TextField name;
    
    Button    okButton;
    Button    cancelButton;

    GridBagLayout gb;
    GridBagConstraints gbc;

    static int BOTH = GridBagConstraints.BOTH;
    static int WEST = GridBagConstraints.WEST;
    static int CENTER = GridBagConstraints.CENTER;
    static int EAST = GridBagConstraints.EAST;

    PlanetEditorWindow parentFrame;
    PlanetEditorPlanet selPlanet;

    public NameDialog (PlanetEditorWindow parent) {
	super(parent, "Name of the Planet", true);

	parentFrame = parent;
	gb = new GridBagLayout();
	gbc = new GridBagConstraints();
	setLayout(gb);

	name = new TextField();
	name.addKeyListener(this);

	okButton =  new Button("OK");
	cancelButton =  new Button("Cancel");
	okButton.addActionListener(this);
	cancelButton.addActionListener(this);

	try {
	    addComponent(this,new Label("Name"),0,0,2,1,BOTH,WEST);

	    addComponent(this,name,2,0,4,1,BOTH,EAST);
	    
	    addComponent(this,okButton,0,2,3,1,BOTH,CENTER);
	    addComponent(this,cancelButton,3,2,3,1,BOTH,CENTER);
	} catch (AWTException e) {}

	setSize(160,120);
    }

    private void addComponent(Container cont,Component comp, int gridx, int gridy,
			      int gridwidth,int gridheight,int fill,int anchor) 
	throws AWTException {
	gbc.gridx = gridx; gbc.gridy=gridy;
	gbc.gridwidth = gridwidth; gbc.gridheight = gridheight;
	gbc.fill = fill;
	gbc.anchor=anchor;
	cont.add(comp,gbc);
    }


    public void actionPerformed(ActionEvent e) {
	validate();

	if (e.getActionCommand().equals("OK")) {
	    selPlanet.setName(name.getText());
		
	} else if (e.getActionCommand().equals("Cancel")) {
	} else {
	    System.err.println("Undefined Button Pressed!");
	    System.exit(3);
	}

	setVisible(false);
    }

    public boolean showYourself() {
	selPlanet = parentFrame.getSelectedPlanet();
	if (selPlanet != null) {
	    name.setText(selPlanet.getName());
	    show();
	    return true;
	}
	else return false;
    }
    
    public void keyTyped(KeyEvent e) {
    }
    public void keyPressed(KeyEvent e) {
	if (e.getKeyCode() == KeyEvent.VK_ENTER) {
	    selPlanet.setName(name.getText());
	    setVisible(false);
	}
	if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
	    setVisible(false);
	}
    }
    public void keyReleased(KeyEvent e) {
    }
}









