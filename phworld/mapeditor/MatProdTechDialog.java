package phworld.mapeditor;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class MatProdTechDialog extends Dialog implements ActionListener{

    TextField mdo,mfu,mme,mmi,moi,mtr;
    TextField pdo,pfu,pme,pmi,poi,ptr;
    TextField thull,teng,twea,tshi,tmisc;

    Button    okButton;
    Button    cancelButton;
    Button    editMatButton,editProdButton,editTechButton;
   
    Choice    matChoice;
    Choice    prodChoice;
    Choice    techChoice;

    GridBagLayout gb;
    GridBagConstraints gbc;

    static int BOTH = GridBagConstraints.BOTH;
    static int WEST = GridBagConstraints.WEST;
    static int CENTER = GridBagConstraints.CENTER;
    static int EAST = GridBagConstraints.EAST;

    PlanetEditorWindow parentFrame;
    Hashtable planetProperties;
    Vector matKeys,prodKeys,techKeys;

    PlanetEditorPlanet selPlanet;

    public MatProdTechDialog (PlanetEditorWindow parent) {
	super(parent, "Material-Production-Technology Stats", true);

	parentFrame = parent;

	matKeys = new Vector();
	prodKeys = new Vector();
	techKeys = new Vector();
	    

	gb = new GridBagLayout();
	gbc = new GridBagConstraints();
	setLayout(gb);

	okButton =  new Button("OK");
	cancelButton =  new Button("Cancel");

	okButton.addActionListener(this);
	cancelButton.addActionListener(this);

	editMatButton = new Button("edit");
	editMatButton.setActionCommand("edit1");
	editMatButton.addActionListener(this);

	editProdButton = new Button("edit");
	editProdButton.setActionCommand("edit2");
	editProdButton.addActionListener(this);

	editTechButton = new Button("edit");
	editTechButton.setActionCommand("edit3");
	editTechButton.addActionListener(this);

	matChoice = new Choice();
	setupChoice(matChoice,matKeys);
	
	prodChoice = new Choice();
	setupChoice(prodChoice,prodKeys);

	techChoice = new Choice();
	setupChoice(techChoice,techKeys);
	
	try {

	    //LABELS
	    addComponent(this,new Label("Material"),0,0,1,1,BOTH,WEST);
	    addComponent(this,new Label("Production"),0,1,1,1,BOTH,WEST);
	    addComponent(this,new Label("Technology"),0,2,1,1,BOTH,WEST);

	    //CHOICES
	    addComponent(this,matChoice,1,0,1,1,BOTH,CENTER);
	    addComponent(this,prodChoice,1,1,1,1,BOTH,CENTER);
	    addComponent(this,techChoice,1,2,1,1,BOTH,CENTER);

	    //BUTTONS
	    addComponent(this,okButton,0,3,1,1,BOTH,CENTER);
	    addComponent(this,cancelButton,1,3,2,1,BOTH,CENTER);

	    addComponent(this,editMatButton,2,0,1,1,BOTH,CENTER);
	    addComponent(this,editProdButton,2,1,1,1,BOTH,CENTER);
	    addComponent(this,editTechButton,2,2,1,1,BOTH,CENTER);
	    
	} catch (AWTException e) {}

	setSize(240,180);
    }

    private void addComponent(Container cont,Component comp, int gridx, int gridy,
			      int gridwidth,int gridheight,int fill,int anchor) 
	throws AWTException {
	gbc.gridx = gridx; gbc.gridy=gridy;
	gbc.gridwidth = gridwidth; gbc.gridheight = gridheight;
	gbc.fill = fill;
	gbc.anchor=anchor;
	cont.add(comp,gbc);
    }


    public void actionPerformed(ActionEvent e) {
	validate();

	if (e.getActionCommand().equals("OK")) {
	    selPlanet.setMaterialString(matChoice.getSelectedItem());
	    selPlanet.setProductionString(prodChoice.getSelectedItem());
	    selPlanet.setTechnologyString(techChoice.getSelectedItem());

	} else if (e.getActionCommand().equals("Cancel")) {
	} else if (e.getActionCommand().equals("edit1")) {
	    parentFrame.setupMaterialEditDialog(selPlanet,planetProperties,matChoice.getSelectedItem());
	    return;
	} else if (e.getActionCommand().equals("edit2")) {
	    parentFrame.setupProductionEditDialog(selPlanet,planetProperties,prodChoice.getSelectedItem());
	    return;
	} else if (e.getActionCommand().equals("edit3")) {
	    parentFrame.setupTechnologyEditDialog(selPlanet,planetProperties,techChoice.getSelectedItem());
	    return;
	} else {
	    System.err.println("Undefined Button Pressed!");
	    System.exit(3);
	}

	setVisible(false);
    }

    public void flush() {
	String key;

	planetProperties = parentFrame.getPlanetProperties();
	
	matKeys.removeAllElements();
	prodKeys.removeAllElements();
	techKeys.removeAllElements();
	
	Enumeration e = planetProperties.keys();

	while (e.hasMoreElements()) {
	    key = (String) e.nextElement();
	    if (key.startsWith("M")) matKeys.addElement(key);
	    if (key.startsWith("P")) prodKeys.addElement(key);
	    if (key.startsWith("T")) techKeys.addElement(key);
	}

	setupChoice(matChoice,matKeys);
	setupChoice(prodChoice,prodKeys);
	setupChoice(techChoice,techKeys);
    }

    public void reset(int matProdOrTech,String s) {
	String key;

	String matSelect = matChoice.getSelectedItem();
	String prodSelect = prodChoice.getSelectedItem();
	String techSelect = techChoice.getSelectedItem();
	
	matKeys.removeAllElements();
	prodKeys.removeAllElements();
	techKeys.removeAllElements();
	
	Enumeration e = planetProperties.keys();

	while (e.hasMoreElements()) {
	    key = (String) e.nextElement();
	    if (key.startsWith("M")) matKeys.addElement(key);
	    if (key.startsWith("P")) prodKeys.addElement(key);
	    if (key.startsWith("T")) techKeys.addElement(key);
	}

	setupChoice(matChoice,matKeys);
	setupChoice(prodChoice,prodKeys);
	setupChoice(techChoice,techKeys);

	switch (matProdOrTech) {

	    case 0: 
		matChoice.select(s);
		prodChoice.select(prodSelect);
		techChoice.select(techSelect);
		break;
	    case 1:
		matChoice.select(matSelect);
		prodChoice.select(s);
		techChoice.select(techSelect);
		break;
	    case 2:
		matChoice.select(matSelect);
		prodChoice.select(prodSelect);
		techChoice.select(s);
		break;
	}
	validate();
    }

    private void setupChoice(Choice choice,Vector vec) {

	choice.removeAll();

	choice.add("-");
	Enumeration e = vec.elements();

	while (e.hasMoreElements()) choice.add((String) e.nextElement());
    }

    public boolean showYourself() {
	selPlanet = parentFrame.getSelectedPlanet();
	planetProperties = parentFrame.getPlanetProperties();

	if (selPlanet != null) {
	    matChoice.select(selPlanet.getMaterialString());
	    prodChoice.select(selPlanet.getProductionString());
	    techChoice.select(selPlanet.getTechnologyString());
	    
	    show();
	    return true;
	}
	else return false;
    }

}









