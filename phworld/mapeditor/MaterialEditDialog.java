package phworld.mapeditor;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class MaterialEditDialog extends Dialog implements ActionListener{

    Button    okButton;
    Button    cancelButton;
    Button    editComponentsButton;
    
    TextField matStringName,fueTF,dodTF,metTF,minTF,oilTF,troTF;

    Choice    componentsChoice;

    Vector    componentsKeys;

    String    compString;
    String    originalMaterialString;

    PlanetEditorWindow parentFrame;

    PlanetEditorPlanet selPlanet;
    Hashtable planetProperties;


    GridBagLayout gb;
    GridBagConstraints gbc;

    static int BOTH = GridBagConstraints.BOTH;
    static int WEST = GridBagConstraints.WEST;
    static int CENTER = GridBagConstraints.CENTER;
    static int EAST = GridBagConstraints.EAST;


    public MaterialEditDialog (PlanetEditorWindow parent) {
	super(parent, "Materials Edit Dialog", true);

	parentFrame = parent;

	componentsKeys = new Vector();

	okButton =  new Button("OK");
	cancelButton =  new Button("Cancel");

	okButton.addActionListener(this);
	cancelButton.addActionListener(this);

	editComponentsButton = new Button("edit");
	editComponentsButton.setActionCommand("editComponents");
	editComponentsButton.addActionListener(this);

	componentsChoice = new Choice();
	setupChoice(componentsChoice,componentsKeys);
	

	gb = new GridBagLayout();
	gbc = new GridBagConstraints();
	setLayout(gb);

	try {

	    //LABELS
	    addComponent(this,new Label("Material set name"),0,0,1,1,BOTH,WEST);
	    addComponent(this,new Label("Dodechadrium"),0,2,1,1,BOTH,WEST);
	    addComponent(this,new Label("Fuel"),0,3,1,1,BOTH,WEST);
	    addComponent(this,new Label("Metals"),0,4,1,1,BOTH,WEST);
	    addComponent(this,new Label("Minerals"),0,5,1,1,BOTH,WEST);
	    addComponent(this,new Label("Oil"),0,6,1,1,BOTH,WEST);
	    addComponent(this,new Label("Troops"),0,7,1,1,BOTH,WEST);
	    addComponent(this,new Label("Components"),0,8,1,1,BOTH,WEST);

	    //TEXTFIELDS
	    addComponent(this,matStringName = new TextField(10),1,0,1,1,BOTH,CENTER);
	    addComponent(this,dodTF = new TextField(5),1,2,1,1,BOTH,CENTER);
	    addComponent(this,fueTF = new TextField(5),1,3,1,1,BOTH,CENTER);
	    addComponent(this,metTF = new TextField(5),1,4,1,1,BOTH,CENTER);
	    addComponent(this,minTF = new TextField(5),1,5,1,1,BOTH,CENTER);
	    addComponent(this,oilTF = new TextField(5),1,6,1,1,BOTH,CENTER);
	    addComponent(this,troTF = new TextField(5),1,7,1,1,BOTH,CENTER);


	    //CHOICES
	    addComponent(this,componentsChoice,1,8,1,1,BOTH,CENTER);

	    //BUTTONS
	    addComponent(this,okButton,0,10,1,1,BOTH,CENTER);
	    addComponent(this,cancelButton,1,10,2,1,BOTH,CENTER);

	    addComponent(this,editComponentsButton,2,8,1,1,BOTH,CENTER);
	    
	} catch (AWTException e) {}

	setSize(300,300);
    }

    private void addComponent(Container cont,Component comp, int gridx, int gridy,
			      int gridwidth,int gridheight,int fill,int anchor) 
	throws AWTException {
	gbc.gridx = gridx; gbc.gridy=gridy;
	gbc.gridwidth = gridwidth; gbc.gridheight = gridheight;
	gbc.fill = fill;
	gbc.anchor=anchor;
	cont.add(comp,gbc);
    }


    public void actionPerformed(ActionEvent e) {
	validate();

	if (e.getActionCommand().equals("OK")) {
	    String matString = new String();

	    matString += dodTF.getText().trim() + "/";
	    matString += fueTF.getText().trim() + "/";
	    matString += metTF.getText().trim() + "/";
	    matString += minTF.getText().trim() + "/";
	    matString += oilTF.getText().trim() + "/";
	    matString += troTF.getText().trim() + "/";
	    matString += componentsChoice.getSelectedItem();

	    String newName = "M" + matStringName.getText().trim();

	    planetProperties.remove(newName);

	    planetProperties.put(newName,matString);


	    parentFrame.resetMatProdTechDialog(0,newName); // 0 - material, 1 - prod ...

	} else if (e.getActionCommand().equals("Cancel")) {
	} else if (e.getActionCommand().equals("editComponents")) {
	    parentFrame.setupComponentsEditDialog(selPlanet,planetProperties,componentsChoice.getSelectedItem()); 
	    return;
	} else {
	    System.err.println("Undefined Button Pressed!");
	    System.exit(3);
	}

	setVisible(false);
    }


    public void reset(String s) {
	Enumeration e = planetProperties.keys();
	String key;

	componentsKeys.removeAllElements();

	while (e.hasMoreElements()) {
	    key = (String) e.nextElement();
	    if (key.startsWith("C_")) componentsKeys.addElement(key);
	}

	setupChoice(componentsChoice,componentsKeys);

	componentsChoice.select("C_"+s);

	validate();
    }

    public void setupMaterialEditDialog(PlanetEditorPlanet planet, Hashtable pp, String mString) {

	selPlanet = planet;
	planetProperties = pp;
	originalMaterialString = mString;

	if (originalMaterialString.equals("-"))
	    matStringName.setText(selPlanet.getName());
	else matStringName.setText(originalMaterialString.substring(1));

	componentsKeys.removeAllElements();
	
	Enumeration e = planetProperties.keys();
	String key;

	while (e.hasMoreElements()) {
	    key = (String) e.nextElement();
	    if (key.startsWith("C_")) componentsKeys.addElement(key);
	}

	setupChoice(componentsChoice,componentsKeys);

	StringTokenizer st;

	if (originalMaterialString.equals("-")) {
	    st = new StringTokenizer("0/0/0/0/0/0/-","/");
	}	
	else {
	    st = new StringTokenizer((String) planetProperties.get(
				  originalMaterialString),"/");
	}

	dodTF.setText(st.nextToken());
	fueTF.setText(st.nextToken());
	metTF.setText(st.nextToken());
	minTF.setText(st.nextToken());
	oilTF.setText(st.nextToken());
	troTF.setText(st.nextToken());
	compString = st.nextToken();

	if (compString.startsWith("C_")) {
	    componentsChoice.select(compString);
	}
	else componentsChoice.select("-");

	show();

    }

    private void setupChoice(Choice choice,Vector vec) {

	choice.removeAll();

	choice.add("-");
	Enumeration e = vec.elements();

	while (e.hasMoreElements()) choice.add((String) e.nextElement());
    }

}
