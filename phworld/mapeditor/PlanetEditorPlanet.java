package phworld.mapeditor;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import dsc.awt.*;
import phworld.PlanetType;

/*
  valid names for planettypes :
  HOME, DODECHAEDRON, GAIA, MARS,
  SEA, VENUS, ASTEROID, SATURN, (RANDOM).
*/


public class PlanetEditorPlanet {


    private int     locationX,locationY;
    private String  planetType;
    private int     owner;
    private String  matProdTech;
    private String  name; //empty string indicates a random name
    private String  material,production,technology;

    public PlanetEditorPlanet(int x,int y,String type,int own,String mpt,String n) {
	locationX=x;
	locationY=y;
	planetType=type; 
	owner=own;
	matProdTech = mpt;
	name = n;

	StringTokenizer st = new StringTokenizer(mpt,"/");
	material = st.nextToken();
	production = st.nextToken();
	technology = st.nextToken();
    }

    public PlanetEditorPlanet(int x,int y,String type,int own,String mat,String prod,String tech,String n) {
	this(x,y,type,own,mat + "/" + prod + "/" + tech, n);
    }


    public PlanetEditorPlanet(int x,int y,String type) {
	// owner set to -1 -> no owner
	// Mat/Prod/Tech is -/-/- -> defaults
	// name selected randomly

	this(x,y,type,-1,"-","-","-","");
    }

    public PlanetEditorPlanet(int x,int y) {
	// defaults planet type to RANDOM

	this(x,y,"RANDOM");
    }



    public Point getPlanetLocation() {
	return new Point(locationX,locationY);
    }

    public int getX() {
	return locationX;
    }

    public int getY() {
	return locationY;
    }

    public void setX(int x) {
	locationX = x;
    }

    public void setY(int y) {
	locationY = y;
    }

    public String getMaterialString() {
	return material;
    }
    public String getProductionString() {
	return production;
    }
    public String getTechnologyString() {
	return technology;
    }

    public void setMaterialString(String s) {
	material = s;
	matProdTech = material + "/" + production + "/" + technology;
    }
    public void setProductionString(String s) {
	production = s; 
	matProdTech = material + "/" + production + "/" + technology;
    }
    public void setTechnologyString(String s) {
	technology = s;
	matProdTech = material + "/" + production + "/" + technology;
    }

    public String getPlanetTypeString() {
	return planetType;
    }

    public int getPlanetTypeNumber() {

	if (planetType.equals("RANDOM")) return PlanetType.RANDOM;

	return PlanetType.getPlanetTypeNumber(planetType);
    }

    public String getImageFileName() {

	return PlanetType.getImageFileName(getPlanetTypeNumber());

    }

    public void setPlanetType(String s) {
	planetType=s;
    }

    public int getPlanetOwner() {
	return owner;
    }

    public void setPlanetOwner(int o) {
	owner = o;
    }

    public String getName() {
	return name;
    }
    public void setName(String s) {
	name = s;
    }


    public String toString() {
	String s = new String();
	s=Integer.toString(locationX) + "\t" + Integer.toString(locationY) + 
	    "\t" + planetType + "\t";
	if ((!planetType.equals("DODECHAEDRON")) && 
	    (!planetType.equals("ASTEROID"))) s = s +"\t";

	if (owner == -1) s=s+"no";
	else s=s+Integer.toString(owner);

	if (!planetType.equals("DODECHAEDRON"))
	    s= s + "\t" + matProdTech + "\t\t" + "\"" +name+ "\"";
	else
	    s= s + "\t" + matProdTech + "\t\t" + "\"\"";

	return s;
    }

}

