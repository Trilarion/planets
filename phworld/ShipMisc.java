/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;


public class ShipMisc extends ShipComponent {

    private static int mass[] = {0, 10, 150, 50, 6, 150, 165, 40, 1500};

    private static int fuel[] =         {0,  0,  0,700,500, 500,999,500,10000};
    private static int dodechadrium[] = {0,  1,  2,  3,  4,   5, 10, 30,  300};
    private static int metals[] =       {0,200, 40, 50,600, 100,600,950, 3000};
    private static int minerals[] =     {0, 50,240,500,350, 750,600,950, 3000};
    private static int oil[] =          {0, 50,480,200, 60,1750,600,950, 3000};
    private static int troops[] =       {0,  0,125,  0,  0,   0,  0,  0,    0};

    private static int techFuel[] =        {0,  0,  0,  0,  0,   0,   0,   0,   0};
    private static int techDodechadrium[] ={0,  3, 20, 25, 10,  40, 100, 150, 750};
    private static int techMetals[]   =    {0,150,250,500,250,1250,2000,2000,2500};
    private static int techMinerals[] =    {0,150,250,500,250,1250,2000,2000,2500};
    private static int techOil[]      =    {0,150,250,500,250,1250,2000,2000,2500};
    private static int techTroops[] =      {0,  0,  0,  0,  0,   0,   0,   0,   0};

    private static int powerConsumption[] = {0,12,0,0,14,0,0,60,0}; 
    private static int powerProduction[] = {0,300,1500,26,150,135,150,4000,0};
    private static int damageResistance[] = {0,50,250,75,50,
					     90,200,160,350}; 

    private static int buildTime[] = {0, 1, 1, 1, 2, 3, 2, 2, 10};
    private static int techTime[] = {0, 1, 2, 3, 4, 5, 6, 7, 8};
    private static String[] gadgetName = { 
	"0",
	"Scanner",
	"Defense Pod",
	"Reactor",
	"Cloak",
	"Mine",
	"Reactor",
	"Cloak",
	"Superbomb"
    };

	//CONSTRUCTOR
	public ShipMisc(int tech) {
	super(tech);
    }
 
    /** Returns the power production of the gadget
     *
     * @return Power production of the gadget. Type of energy depends
     * on techlevel. */

    public int getPowerProduction() {
	if (this.isActive()) 
	    return ((getMaxPowerProduction() * 
		     (100-this.getDamagePercentage())) / 100);
	else return 0;
    } 

    public int getPowerOutput() {
	return getPowerProduction();
    }

    /** Returns the maxinum power production of this piece of equipment
     *
     * @return Maxinum power production of the component
     */

    public int getMaxPowerProduction() {
	return powerProduction[techLevel];
    }

    /** Returns materials required to build the ShipMisc. 
     * getBuildMaterials() returns a MaterialBundle that has all 
     * the materials required to build the ShipMisc.
     *
     * @param tech Tech level
     * @return Materials required to build an misc tech of spec. tech level
     */
    public static MaterialBundle getBuildMaterials(int tech) {
	MaterialBundle mb = new MaterialBundle(MaterialBundle.UNLIMITED);
	if (tech<0 || tech>8) throw new IllegalArgumentException
	    ("Non-existent tech level not between (0-8)");
	mb.putFuel(fuel[tech]);
	mb.putDodechadrium(dodechadrium[tech]);
	mb.putMetals(metals[tech]);
	mb.putMinerals(minerals[tech]);
	mb.putOil(oil[tech]);
	mb.putTroops(troops[tech]);

	return mb;
    }

    /** Returns the time taken to build the ShipMisc.
     * Gadgets of different tech level have varying building times. Presently,
     * the building time is directly proportional to the tech level of the
     * gadget.
     *
     * @param tech Tech level
     * @return Time taken in building the ShipMisc of the spec. tech level
     */
    public static int getBuildTime(int tech) {
	if (tech<0 || tech>8) throw new IllegalArgumentException
	    ("Non-existent tech level not between (0-8)");

	return buildTime[tech];
    }


    /** Returns the materials required in upgrading the ShipMisc tech level 
     * of a planet.
     *
     * @param tech Tech level
     * @return Materials required in upgrading one tech to given techlevel
     */
    public static MaterialBundle getTechMaterials(int tech) {
	MaterialBundle mb = new MaterialBundle(MaterialBundle.UNLIMITED);
	if (tech<0 || tech>8) throw new IllegalArgumentException
	    ("Non-existent tech level not between (0-8)");
	mb.putFuel(techFuel[tech]);
	mb.putDodechadrium(techDodechadrium[tech]);
	mb.putMetals(techMetals[tech]);
	mb.putMinerals(techMinerals[tech]);
	mb.putOil(techOil[tech]);
	mb.putTroops(techTroops[tech]);

	return mb;
    }

    /** Returns the power consumption of the Gadged.
     * ShipMisc consume power only if the ship is not over friendly planet 
     * and the gadeg is active.
     *
     * @return Power consumption of the ShipMisc
     */ 
    public int getPowerConsumption() {
	if (this.isActive()) return powerConsumption[techLevel];
	else return 0;
    }


    /** Returns the maximum damgage that the ShipMisc can receive without 
     * being totally destroyed.
     * 
     * @return Maximum obtainable damage
     */
    public int getMaxDamage() {
	return damageResistance[techLevel];
    }

    /** Returns the mass of the ShipMisc.
     *
     * @return Mass of the ShipMisc gadget
     */
    public int getMass() {
	return mass[techLevel];
    }

    public String toString() {
	return "M"+techLevel+" " + gadgetName[techLevel];
    }

    public String getPowerDescription() {
	int range = (int) (((double) getPowerOutput())/ShipMath.SPEED_CONST);

	switch(techLevel) {
	case 1:
	    return "Scan range: " + range;
	case 2:
	    return "Defense value: " + getPowerOutput();
	case 3:
	case 6:
	    return "Power production: " +getPowerOutput();
	case 5:
	    return "Minefield radius: " + range;
	case 4:
	case 7:
	    return "Cloaking power: " +getPowerOutput();
	case 8:
	    return "Explosion damage: unlimited";
	default :
	    return "##ERROR##";
	}
    }

    public int getTechType() {
	return ShipComponent.MISC;
    }
}

