/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import java.util.*;
import dsc.netgame.*;


/** ShipMath contains various functions that are used in calculations
 * regarding ships in the game. Functions are intended to be same in both
 * HostShip and Ship classes which both already inherit class from
 * dsc.netgame. All methods are static.
 * 
 * @author Dodekaedron Software Creations, Inc. -- Wraith
 */

public final class ShipMath {

   public static final double SPEED_CONST = 50.0;

  /** Calculates speed of a ship.
   *
   * @param mass Total mass of the ship.
   * @param power Total engine power of the ship.
   * @return Movement speed of the ship.
   */

  public static int speed(int mass, int power) {
    if(mass>power) 
      return 0;

    double N  = warpSpeed(mass, power) * SPEED_CONST;
    int n     = (int ) N;
    return n;
  } 

  public static double warpSpeed(int mass, int power) {
    if(mass <= 0)
      return -1.0;

    if(mass>power)
      return 0.0;

    double mp = ((double) power) / ((double) mass);
    double p  = Math.pow(mp, 0.65);

    return p;
  }

  public static String warpSpeedString(int mass, int power) {
    String warp_speed = "" + warpSpeed(mass, power);

    if(warp_speed.length() > 4)
      warp_speed=warp_speed.substring(0, 4);

    return warp_speed;
  }

  /** Calculates mass of components in given enumeration.
   *
   * @param e Enumeration of components which mass is wanted.
   * @retrun Total mass of given components.
   */

  public static int calculateMass(Enumeration e) {
    int n=0;

    while(e.hasMoreElements()) {
      TransferableComponent tc = (TransferableComponent) e.nextElement();
      
      n+=tc.getMass();
    }

    return n;
  }

  public static int calcShieldPower(Vector shields) {
    Enumeration e = shields.elements();
    int total=0;

    while (e.hasMoreElements()) {
      ShipShield ss = (ShipShield) e.nextElement();
      
      total+=ss.getShieldPower();
    }

    return total;
  }

  public static int calcMinShieldPower(Vector shields) {
    Enumeration e = shields.elements();
    int total=0;

    while (e.hasMoreElements()) {
      ShipShield ss = (ShipShield) e.nextElement();
      
      total+=ss.getMinShieldPower();
    }

    return total;
  }

  public static int calcMaxShieldPower(Vector shields) {
    Enumeration e = shields.elements();
    int total=0;

    while (e.hasMoreElements()) {
      ShipShield ss = (ShipShield) e.nextElement();
      
      total+=ss.getMaxShieldPower();
    }

    return total;
  }

  public static int calcWeaponHullPower(Vector weapons) {
    Enumeration e = weapons.elements();
    int total=0;

    while (e.hasMoreElements()) {
      ShipWeapon sw = (ShipWeapon) e.nextElement();
      
      total+=sw.getInflictableHullDamage();
    }

    return total;
  }

  public static int calcWeaponShieldPower(Vector weapons) {
    Enumeration e = weapons.elements();
    int total=0;

    while (e.hasMoreElements()) {
      ShipWeapon sw = (ShipWeapon) e.nextElement();
      
      total+=sw.getInflictableShieldDamage();
    }

    return total;
  }


  public static int calcMaxWeaponHullPower(Vector weapons) {
    Enumeration e = weapons.elements();
    int total=0;

    while (e.hasMoreElements()) {
      ShipWeapon sw = (ShipWeapon) e.nextElement();
      
      total+=sw.getMaxInflictableHullDamage();
    }

    return total;
  }

  public static int calcMaxWeaponShieldPower(Vector weapons) {
    Enumeration e = weapons.elements();
    int total=0;

    while (e.hasMoreElements()) {
      ShipWeapon sw = (ShipWeapon) e.nextElement();
      
      total+=sw.getMaxInflictableShieldDamage();
    }

    return total;
  }

  public static int calcEnginePower(Vector engines) {
    Enumeration e = engines.elements();
    int total=0;

    while (e.hasMoreElements()) {
      ShipEngine se = (ShipEngine) e.nextElement();
      
      total+=se.getPowerProduction();
    }

    return total;
  }

  public static int calcMaxEnginePower(Vector engines) {
    Enumeration e = engines.elements();
    int total=0;

    while (e.hasMoreElements()) {
      ShipEngine se = (ShipEngine) e.nextElement();
      
      total+=se.getMaxPowerProduction();
    }

    return total;
  }

  public static int calcFuelConsumption(Vector gadgets) {
    Enumeration e = gadgets.elements();
    int total=0;

    while (e.hasMoreElements()) {
      ShipComponent se = (ShipComponent) e.nextElement();
      
      total+=se.getPowerConsumption();
    }

    return total;
  }

  public static int calcReactorPower(Vector gadgets) {
    Enumeration e = gadgets.elements();
    int total=0;

    while (e.hasMoreElements()) {
      ShipMisc se = (ShipMisc) e.nextElement();
      
      if(se.getTech() == 3 || se.getTech()== 6)
	  total+=se.getPowerOutput();
    }

    return total;
  }

  public static int calcCloakPower(Vector gadgets) {
    Enumeration e = gadgets.elements();
    int total=0;

    while (e.hasMoreElements()) {
      ShipMisc se = (ShipMisc) e.nextElement();
      
      if(se.getTech() == 4 || se.getTech()== 7)
	  if(total < se.getPowerOutput())
	      total = se.getPowerOutput();
    }

    return total;
  }

  public static TravelInfo calcEFCandETA(Location startPoint,
					 Location endPoint,
					 boolean startsAtPlanet,
					 int fuelMass,
					 int totalMass,
					 int enginePower,
					 int consumption,
					 int idleConsumption) {

    TravelInfo ti = new TravelInfo();

    if(startPoint.equals(endPoint)) {
      ti.setEta(0);
      ti.setEfc(0);
      return ti;
    }

    if(startsAtPlanet) {
      if(totalMass>enginePower) {
	ti.setEta(-1);
	ti.setEfc(-1);
	return ti;
      }
    } else
        if((totalMass-idleConsumption)>enginePower) {
	  ti.setEta(-1);
	  ti.setEfc(-1);
	  return ti;
	}

    int efc=0; int eta=0;
    int dryMass=totalMass-fuelMass;

    Location currentPoint=(Location) startPoint.clone();

    while(!currentPoint.equals(endPoint)) {
      eta++;

      if(! (currentPoint.equals(startPoint) && startsAtPlanet) ) {
	totalMass-=idleConsumption;
	efc+=idleConsumption;
      }

      if(totalMass<dryMass)
	totalMass=dryMass;

      int movement = speed(totalMass, enginePower);
      int distance = currentPoint.distance(endPoint); 

      //System.out.println(movement + ":" + distance + ":" +dryMass + ":" + totalMass);
      
      if(movement>distance) {
	consumption=consumption*distance/movement;
      }

      currentPoint=currentPoint.moveTowards(endPoint, movement);
      totalMass-=consumption;
      efc+=consumption;

    }

    ti.setEfc(efc);
    if(efc<=fuelMass)
      ti.setEta(eta);
    else
      ti.setEta(-1);
    return ti;
  }


  public static int calculateEDT(boolean startsOverOwnPlanet, int totalMass, 
				 int fuel, int enginePower, int consumption, 
				 int idleConsumption) {
    int dist=0;
    int fuel_temp=fuel;
    int totalMassWOFuel = totalMass - fuel;
    int movement = speed(totalMass, enginePower);    

    if(movement==0)
      return 0;

    if(idleConsumption+consumption <= 0)
	return 100000;

    while (fuel_temp > 0) {
      if (startsOverOwnPlanet && (consumption >= fuel_temp)) {
	startsOverOwnPlanet=false;
	dist+=(movement*fuel_temp)/consumption;
	return dist;
      } else if (!startsOverOwnPlanet && 
		 ((consumption+idleConsumption) >= fuel_temp)) {
	dist+=(movement*fuel_temp)/(consumption+idleConsumption);
	return dist;
      }

      dist+=movement;

      if (startsOverOwnPlanet) {
	startsOverOwnPlanet=false;
	fuel_temp-=consumption;
      } else 
	fuel_temp-=consumption+idleConsumption;

      movement = speed(totalMassWOFuel+fuel_temp, enginePower);
    }
    return 0;
  }

  public static int getPercentage(int a, int b) {
      if(b==0)
	  return 0;

      return (a*100)/b;
  }

    public static String speedToWarpString(int speed) {
	String warp_speed = "" + ((double) speed / SPEED_CONST);

	if(warp_speed.length() > 4)
	    warp_speed=warp_speed.substring(0, 4);

	return warp_speed;
    }
}
