/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import dsc.netgame.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import dsc.awt.*;


public class BattleOrderPanel extends ShipSidePanel implements ItemListener,
                                                    AdjustmentListener {

    private static final int SCROLLBAR_VISIBLE_AREA = 0;
    private BaseShip   ship;

    private Label nameLabel;

    private Label hullLabel, shieldLabel, persistentLabel, hweaponLabel,
	sweaponLabel, massLabel, volumeLabel;

    private Label vhullLabel, vshieldLabel, vpersistentLabel, vhweaponLabel,
	vsweaponLabel, vmassLabel, vvolumeLabel;

    private Scrollbar hullBar, shieldBar, persistentBar, hweaponBar,
	sweaponBar, massBar, volumeBar;

    private CheckboxGroup checkboxGroup;
    private Checkbox defaultBox, customBox, bombardBox; 
    private Panel scrollbarPanel;


    private Checkbox addCheckbox(GridBagConstraints gbc,int x,int y, 
				 int w, int h, String name,
				 CheckboxGroup g,boolean v) {
	Checkbox c=new Checkbox(name,g,v);
	c.addItemListener(this);
	gbc.gridx=x;
	gbc.gridy=y;
	gbc.gridwidth=w;
	gbc.gridheight=h;
	contentsPanel.add(c,gbc);
	return c;
    }

    public BattleOrderPanel() {
	super(null);

	nameLabel=new Label("HEADER");

	checkboxGroup = new CheckboxGroup();

	hullBar = new Scrollbar(Scrollbar.HORIZONTAL, 0, SCROLLBAR_VISIBLE_AREA, 0, 255);
	shieldBar = new Scrollbar(Scrollbar.HORIZONTAL, 0, SCROLLBAR_VISIBLE_AREA, 0, 255);
	persistentBar = new Scrollbar(Scrollbar.HORIZONTAL, 0, SCROLLBAR_VISIBLE_AREA, 0, 255);
	hweaponBar = new Scrollbar(Scrollbar.HORIZONTAL, 0, SCROLLBAR_VISIBLE_AREA, 0, 255);
	sweaponBar = new Scrollbar(Scrollbar.HORIZONTAL, 0, SCROLLBAR_VISIBLE_AREA, 0, 255);
	massBar = new Scrollbar(Scrollbar.HORIZONTAL, 0, SCROLLBAR_VISIBLE_AREA, 0, 255);
	volumeBar = new Scrollbar(Scrollbar.HORIZONTAL, 0, SCROLLBAR_VISIBLE_AREA, 0, 255);
	bombardBox = new Checkbox("Bombard", true);

	hullBar.addAdjustmentListener(this);
	shieldBar.addAdjustmentListener(this);
	persistentBar.addAdjustmentListener(this);
	hweaponBar.addAdjustmentListener(this);
	sweaponBar.addAdjustmentListener(this);
	massBar.addAdjustmentListener(this);
	volumeBar.addAdjustmentListener(this);

	hullLabel       = new Label("Hull hitpoints");
	shieldLabel     = new Label("Shield power");
	persistentLabel = new Label("Persistent shields");
	hweaponLabel    = new Label("Hull weapons");
	sweaponLabel    = new Label("Shield weapons");
	massLabel       = new Label("Mass");
	volumeLabel     = new Label("Hull volume");

	vhullLabel       = new Label("0");
	vshieldLabel     = new Label("0");
	vpersistentLabel = new Label("0");
	vhweaponLabel    = new Label("0");
	vsweaponLabel    = new Label("0");
	vmassLabel       = new Label("0");
	vvolumeLabel     = new Label("0");


	contentsPanel.setLayout(new GridBagLayout());
	GridBagConstraints gbc = new GridBagConstraints();

	gbc.anchor=GridBagConstraints.NORTH;
	gbc.fill=GridBagConstraints.NONE;
	gbc.gridx=0;
	gbc.gridy=0;
	gbc.gridwidth=8;
	gbc.gridheight=1;
	gbc.weightx=0;
	gbc.weighty=1;


	gbc.gridy=0; contentsPanel.add(nameLabel, gbc);

	gbc.gridy=1; contentsPanel.add(new Label(""), gbc);

        defaultBox=addCheckbox(gbc, 0, 2, 8, 1, "Default", checkboxGroup,true);
	customBox=addCheckbox(gbc, 0, 3, 8, 1, "Custom", checkboxGroup, false);

	gbc.gridy=4; contentsPanel.add(new Label(""), gbc);

	scrollbarPanel = new Panel();
	scrollbarPanel.setLayout(new GridBagLayout());

	gbc.anchor=GridBagConstraints.CENTER;
	gbc.gridy=1; scrollbarPanel.add(hullLabel, gbc);
	gbc.gridy=3; scrollbarPanel.add(shieldLabel, gbc);
	gbc.gridy=5; scrollbarPanel.add(persistentLabel, gbc);
	gbc.gridy=7; scrollbarPanel.add(hweaponLabel, gbc);
	gbc.gridy=9; scrollbarPanel.add(sweaponLabel, gbc);
	gbc.gridy=11; scrollbarPanel.add(massLabel, gbc);
	gbc.gridy=13; scrollbarPanel.add(volumeLabel, gbc);

	gbc.anchor=GridBagConstraints.NORTH;
	gbc.fill=GridBagConstraints.HORIZONTAL;
	gbc.weightx=10;
	gbc.gridwidth=6;
	gbc.gridx=2;
	gbc.gridy=2; scrollbarPanel.add(hullBar, gbc);
	gbc.gridy=4; scrollbarPanel.add(shieldBar, gbc);
	gbc.gridy=6; scrollbarPanel.add(persistentBar, gbc);
	gbc.gridy=8; scrollbarPanel.add(hweaponBar, gbc);
	gbc.gridy=10; scrollbarPanel.add(sweaponBar, gbc);
	gbc.gridy=12; scrollbarPanel.add(massBar, gbc);
	gbc.gridy=14; scrollbarPanel.add(volumeBar, gbc);

	gbc.gridx=0;
	gbc.gridwidth=2;
	gbc.fill=GridBagConstraints.NONE;
	gbc.weightx=1;
	gbc.gridy=2; scrollbarPanel.add(vhullLabel, gbc);
	gbc.gridy=4; scrollbarPanel.add(vshieldLabel, gbc);
	gbc.gridy=6; scrollbarPanel.add(vpersistentLabel, gbc);
	gbc.gridy=8; scrollbarPanel.add(vhweaponLabel, gbc);
	gbc.gridy=10; scrollbarPanel.add(vsweaponLabel, gbc);
	gbc.gridy=12; scrollbarPanel.add(vmassLabel, gbc);
	gbc.gridy=14; scrollbarPanel.add(vvolumeLabel, gbc);

	gbc.gridx=0;
	gbc.gridwidth=8;
	gbc.gridy=16; scrollbarPanel.add(bombardBox, gbc);
	
	gbc.gridx=0; gbc.gridy=5;
	gbc.gridwidth=8;
	gbc.gridheight=14;
	gbc.weightx=10;
	gbc.fill=GridBagConstraints.HORIZONTAL;
	gbc.anchor=GridBagConstraints.NORTH;
	contentsPanel.add(scrollbarPanel, gbc);

	gbc.gridy=19;
	gbc.gridwidth=8;
	gbc.gridheight=10;
	gbc.fill=GridBagConstraints.BOTH;
	gbc.weighty=100;
	
	Panel fillPanel = new Panel();
	fillPanel.add(new Label(""));

	// For vertical fill
	contentsPanel.add(fillPanel, gbc);
    }

    void initiateHelp() {
	HelpWindow.callForHelp("data" + java.io.File.separator + "help",
			       "Playing",
			       "Battle Orders");

    }

    void initializeContents(BaseShip s) {
	ship=s;

	TargetValuation targetValuation = s.getTargetValuation();
	if(targetValuation == null) {
	    targetValuation = new TargetValuation();
	    defaultBox.setState(true);
	    customBox.setState(false);
	    disableScrollbars();
	} else {
	    targetValuation = (TargetValuation) targetValuation.clone();
	    defaultBox.setState(false);
	    customBox.setState(true);
	    enableScrollbars();
	}
	
	nameLabel.setText(s.getName() + "'s orders");

	setScrollbars(targetValuation);
	setLabels();
	validate();
    }

    private void enableScrollbars() {
	scrollbarPanel.setEnabled(true);
    }

    private void disableScrollbars() {
	scrollbarPanel.setEnabled(false);
    }

    private void setScrollbars(TargetValuation tv) {
	hullBar.setValues(tv.getHullValue(), 
			  SCROLLBAR_VISIBLE_AREA, 
			  TargetValuation.MIN_VALUE, 
			  TargetValuation.MAX_VALUE+1);

	shieldBar.setValues(tv.getShieldValue(), 
			  SCROLLBAR_VISIBLE_AREA, 
			  TargetValuation.MIN_VALUE, 
			  TargetValuation.MAX_VALUE+1);

	persistentBar.setValues(tv.getPersistentshieldValue(), 
			  SCROLLBAR_VISIBLE_AREA, 
			  TargetValuation.MIN_VALUE, 
			  TargetValuation.MAX_VALUE+1);

	hweaponBar.setValues(tv.getHullweaponValue(), 
			  SCROLLBAR_VISIBLE_AREA, 
			  TargetValuation.MIN_VALUE, 
			  TargetValuation.MAX_VALUE+1);

	sweaponBar.setValues(tv.getShieldweaponValue(), 
			  SCROLLBAR_VISIBLE_AREA, 
			  TargetValuation.MIN_VALUE, 
			  TargetValuation.MAX_VALUE+1);

	massBar.setValues(tv.getMassValue(), 
			  SCROLLBAR_VISIBLE_AREA, 
			  TargetValuation.MIN_VALUE, 
			  TargetValuation.MAX_VALUE+1);

	volumeBar.setValues(tv.getVolumeValue(), 
			  SCROLLBAR_VISIBLE_AREA, 
			  TargetValuation.MIN_VALUE, 
			  TargetValuation.MAX_VALUE+1);

	bombardBox.setState(tv.isBombardingEnabled());
    }

    private TargetValuation getTargetValuation() {
	if(defaultBox.getState()) {
	    return null;
	} else {
	    TargetValuation tv = new TargetValuation();

	    tv.setHullValue(hullBar.getValue());
	    tv.setShieldValue(shieldBar.getValue());
	    tv.setPersistentShieldValue(persistentBar.getValue());
	    tv.setHullWeaponValue(hweaponBar.getValue());
	    tv.setShieldWeaponValue(sweaponBar.getValue());
	    tv.setMassValue(massBar.getValue());
	    tv.setVolumeValue(volumeBar.getValue());
	    tv.setBombarding(bombardBox.getState());

	    return tv;
	}
    }

    private void setLabels() {
	vhullLabel.setText("" + hullBar.getValue());
	vshieldLabel.setText("" + shieldBar.getValue());
	vpersistentLabel.setText("" + persistentBar.getValue());
	vhweaponLabel.setText("" + hweaponBar.getValue());
	vsweaponLabel.setText("" + sweaponBar.getValue());
	vmassLabel.setText("" + massBar.getValue());
	vvolumeLabel.setText("" + volumeBar.getValue());
    }

    boolean OKAction() {
	TargetValuation tv = getTargetValuation();

	ship.initiateBattleOrders(tv);
	return true;
    }

    public void adjustmentValueChanged(AdjustmentEvent evt) {
	setLabels();
    }

    public void itemStateChanged(ItemEvent evt) {
	if(evt.getStateChange()==ItemEvent.DESELECTED) return;
	Checkbox c = checkboxGroup.getSelectedCheckbox();
 
	if(evt.getItem().equals("Default")){
	    disableScrollbars();
	} 

	if(evt.getItem().equals("Custom")){
	    enableScrollbars();
	} 
    }
}
