/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import dsc.netgame.*;
import dsc.awt.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.awt.image.*;


public class Ship extends BaseShip implements SidePanelOwner {

    private static Panel controlPanel = null;
    private static GridBagConstraints controlPanelConstraints = null;

    private static ImageComponent shipImageComponent;
    private static Panel generalPanel = null;
    private static Label nameLabel = null;
    private static Button waypointButton = null;
    private static Button trackShipButton = null;
    private static Button loadFuelButton = null;
    private static Button transferMaterialsButton = null;
    private static Button shipTransferButton      = null;
    private static Button componentControlsButton = null;
    private static Menu shipMenu = null;
    private static Menu surrenderMenu = null;
    private static MenuItem recycleMenuItem = null;
    private static MenuItem formFleetMenuItem = null;
    private static MenuItem ordersMenuItem    = null;
    private static MenuItem loadFuelMemuItem  = null;
    private static MenuItem optimizeFuelMemuItem = null;
    private static MenuItem optimizeFullFuelMenuItem = null;
    private static Ship buttonListener = null;


    private static TextArea infoText = null;
    private static String etaText="";			    

    private static MaterialTransferPanel materialTransferPanel  = null;
    private static ShipSidePanel componentControlsPanel = null;
    private static ShipSidePanel shipSelectionPanel     = null;
    private static BattleOrderPanel ordersPanel         = null;
    
    private ShipHull hull;
    private Vector engines;
    private Vector weapons;
    private Vector shields;
    private Vector misc;
    private MaterialBundle cargoSpace;
    private int currentShieldPower;

    private String enemyInfoText     = null;
    private boolean cloaking = false;
    private String surrenderTo;

    public Ship (int id, String name, int owner, Location location) {
	super(id,name,owner,location);
    }


    protected void changeControlPanel(Panel newPanel) {
	if(controlPanelConstraints == null) {
	    controlPanelConstraints = new GridBagConstraints();

	    controlPanelConstraints.fill=GridBagConstraints.BOTH;
	    controlPanelConstraints.anchor=GridBagConstraints.NORTH;
	    controlPanelConstraints.weightx=100;
	    controlPanelConstraints.weighty=1;
	    controlPanelConstraints.gridwidth=1; 
	    controlPanelConstraints.gridheight=1;
	    controlPanelConstraints.gridx=0;
	    controlPanelConstraints.gridy=0;
	}

	controlPanel.removeAll();
	controlPanel.add(newPanel, controlPanelConstraints);
	controlPanel.validate();
    }

    void createControlPanel() {
	controlPanel=new Panel();
	controlPanel.setLayout(new GridBagLayout());

	generalPanel=new Panel();
	changeControlPanel(generalPanel);

	generalPanel.setLayout(new VerticalFlowLayout());

	shipImageComponent = new ImageComponent(
						world.getImage("data/graph/still/panel/ship1.gif"));
	generalPanel.add(shipImageComponent);
    
	nameLabel=new Label("Name");
	generalPanel.add(nameLabel);

	infoText=new TextArea("",15,22,TextArea.SCROLLBARS_NONE);
	infoText.setEditable(false);

	generalPanel.add(infoText);

	waypointButton=new Button("Set Waypoint");
	trackShipButton=new Button("Track");
	loadFuelButton=new Button("Load fuel");
	transferMaterialsButton=new Button("Transfer with Planet");
	shipTransferButton     =new Button("Transfer with Ship");
	componentControlsButton=new Button("Engineering");

	shipMenu = new Menu("Ship");

	surrenderMenu = new Menu("Surrender to");
	shipMenu.add(surrenderMenu);

	recycleMenuItem = new MenuItem("Recycle",
				       new MenuShortcut(KeyEvent.VK_Q));
	shipMenu.add(recycleMenuItem);
	formFleetMenuItem = new MenuItem("Form Fleet",
					 new MenuShortcut(KeyEvent.VK_F));
	shipMenu.add(formFleetMenuItem);
	ordersMenuItem = new MenuItem("Battle Orders",
				      new MenuShortcut(KeyEvent.VK_O));
	shipMenu.add(ordersMenuItem);
	
	shipMenu.addSeparator();

	loadFuelMemuItem = new MenuItem("Load Fuel",
					new MenuShortcut(KeyEvent.VK_A));
	shipMenu.add(loadFuelMemuItem);
	optimizeFuelMemuItem = new MenuItem("Load Min Fuel",
					    new MenuShortcut(KeyEvent.VK_M));
	shipMenu.add(optimizeFuelMemuItem);
	optimizeFullFuelMenuItem = new MenuItem("Load Optimum Fuel",
					      new MenuShortcut(KeyEvent.VK_Y));
	shipMenu.add(optimizeFullFuelMenuItem);

 	generalPanel.add(waypointButton);
	generalPanel.add(trackShipButton);
	generalPanel.add(loadFuelButton);
	generalPanel.add(transferMaterialsButton);
	generalPanel.add(shipTransferButton);
	generalPanel.add(componentControlsButton);

	materialTransferPanel  = new MaterialTransferPanel();
	componentControlsPanel = new ComponentControlsPanel();
	shipSelectionPanel     = new ShipSelectionPanel();
	ordersPanel            = new BattleOrderPanel();
    }


    private void setInfoTextEnemy() {
	if(enemyInfoText != null)
	    infoText.setText(enemyInfoText);
	else { 
	    if(getOwner() == -1) {
		infoText.setText("Abandoned");
	    } else {
		infoText.setText("Owned by " + 
				 world.getController().
				 playersInfo.
				 getPlayerName(getOwner()));
	    }
	}
    }

    public int getEnginePower() {
	return ShipMath.calcEnginePower(engines);
    }

    public int getMaxEnginePower() {
	return ShipMath.calcMaxEnginePower(engines);
    }

    public int getWeaponHullPower() {
	return ShipMath.calcWeaponHullPower(weapons);
    }

    public int getMaxWeaponHullPower() {
	return ShipMath.calcMaxWeaponHullPower(weapons);
    }

    public int getWeaponShieldPower() {
	return ShipMath.calcWeaponShieldPower(weapons);
    }

    public int getMaxWeaponShieldPower() {
	return ShipMath.calcMaxWeaponShieldPower(weapons);
    }

    public int getMaxShieldPower() {
	return ShipMath.calcMaxShieldPower(shields);
    }

    public int getHullHPLeft() {
	return (hull.getMaxDamage() - hull.getDamageReceived());
    }

    public int getMaxHullHP() {
	return hull.getMaxDamage();
    }

    private void setInfoTextOwn() {
	int total_mass=(hull.getMass() + 
			hull.getVolume() - 
			cargoSpace.getFreeSpace()); 
	int min_mass=hull.getMass() + 
	    hull.getVolume() - 
	    cargoSpace.getCapacity();
	int engine_power=getEnginePower();
	int max_engine_power=getMaxEnginePower();
	int weapon_hull_power=getWeaponHullPower();
	int max_weapon_hull_power=getMaxWeaponHullPower();
	int weapon_shield_power=getWeaponShieldPower();
	int max_weapon_shield_power=getMaxWeaponShieldPower();
	int shield_power=getShieldPower();
	int max_shield_power=getMaxShieldPower();

	int consumption=ShipMath.calcFuelConsumption(engines);
	int idle_consumption=hull.getPowerConsumption() + 
	    ShipMath.calcFuelConsumption(misc);
	int reactor_power = ShipMath.calcReactorPower(misc);
	idle_consumption -= reactor_power;
	if(idle_consumption < 0) {
	    consumption += idle_consumption;
	    idle_consumption = 0;
	    if(consumption < 0)
		consumption = 0;
	}

	boolean starts_at_planet=false;

	Enumeration e = world.getComponentsByLocation(location);
	while(e.hasMoreElements()) {
	    WorldComponent wc = (WorldComponent) e.nextElement();
	    if(wc instanceof Planet)
		starts_at_planet=true;
	}

	infoText.setText("Hull: " + getHullHPLeft()
			  + "/" +
			 getMaxHullHP() + " (" +
			 (100 - hull.getDamagePercentage()) + "%)\n");
	infoText.append("Engines: " + engine_power +
			"/" + max_engine_power + " (" +
			ShipMath.getPercentage(engine_power,
					       max_engine_power) + "%)\n");
	infoText.append("Weapons H: " + 
			weapon_hull_power + 
			"/" + max_weapon_hull_power + 
			" (" +
			ShipMath.getPercentage(weapon_hull_power,
				      max_weapon_hull_power) + "%)\n");
	infoText.append("Weapons S: " + 
			weapon_shield_power + 
			"/" + max_weapon_shield_power + 
			" (" +
			ShipMath.getPercentage(weapon_shield_power,
				      max_weapon_shield_power) + "%)\n");

	infoText.append("Shields: " + shield_power + 
			"/" + max_shield_power + " (" +
			ShipMath.getPercentage(shield_power, 
					       max_shield_power) + "%)\n");
	infoText.append("\nCargo: " + cargoSpace.getMass() + "/" +
			cargoSpace.getCapacity() + "\n");
	infoText.append("Mass: " + total_mass +
			"/" + (hull.getMass() + hull.getVolume()) + "\n");

	String warp_speed = ShipMath.warpSpeedString(total_mass, engine_power);
	String max_warp   = ShipMath.warpSpeedString(min_mass, max_engine_power);

	infoText.append("Fuel: " +
			cargoSpace.getFuel() + "\n");

	infoText.append("Warp Speed: " + 
			warp_speed +
			"/" + 
			max_warp + "\n\n");

	if(surrenderTo != null) {
	    infoText.append("Surrender to " + surrenderTo + "\n");
	} else if(trackTarget==-1)
	    infoText.append("Waypoint: " + getWaypointText() + "\n");
	else {
	    WorldComponent ttarget = world.getComponentById(trackTarget);
	    if(ttarget==null)
		infoText.append("Lost track\n");
	    else
		infoText.append("Tracking " + ttarget.getName() + "\n");
	}

	TravelInfo travelInfo = ShipMath.calcEFCandETA(
						       location,
						       waypoint,
						       starts_at_planet,
						       cargoSpace.getFuel(),
						       total_mass,
						       engine_power,
						       consumption,
						       idle_consumption);

	etaText= "D: " + 
	    ShipMath.speedToWarpString(location.distance(waypoint)) + 
	    " " + travelInfo;

	infoText.append(etaText);
	updateWarningCircle();
    }
    
    public int getSpeed() {
	return ShipMath.speed(getMass() + getMaterials().getMass(), 
			      ShipMath.calcEnginePower(engines));
    }

    public int getTotalConsumption() {
	int c = ShipMath.calcFuelConsumption(engines) +
	    hull.getPowerConsumption() + 
	    ShipMath.calcFuelConsumption(misc);

	c-=ShipMath.calcReactorPower(misc);
	if(c<0)
	    return 0;
	else
	    return c;
    }

    public void setInfoText() {
	if(infoText == null)
	    return;
	
	if(world.getController().playerNumber==getOwner()) {
	    setInfoTextOwn();
	} else {
	    setInfoTextEnemy();
	}
    }

    protected void resetControlPanel() {
	nameLabel.setText(getName());

	if(isCloaking()) {
	    shipImageComponent.setImage(
					world.getImage("data/graph/still/panel/ship"+hullTechLevel+"cloak.gif"));
	} else {
	    shipImageComponent.setImage(
					world.getImage("data/graph/still/panel/ship"+hullTechLevel+".gif"));
	}

	if(buttonListener!=null) {
	    waypointButton.removeActionListener(buttonListener);
	    trackShipButton.removeActionListener(buttonListener);
	    loadFuelButton.removeActionListener(buttonListener);
	    transferMaterialsButton.removeActionListener(buttonListener);
	    shipTransferButton.removeActionListener(buttonListener);
	    componentControlsButton.removeActionListener(buttonListener);
      
	    recycleMenuItem.removeActionListener(buttonListener);
	    formFleetMenuItem.removeActionListener(buttonListener);
	    ordersMenuItem.removeActionListener(buttonListener);

	    loadFuelMemuItem.removeActionListener(buttonListener);
	    optimizeFuelMemuItem.removeActionListener(buttonListener);
	    optimizeFullFuelMenuItem.removeActionListener(buttonListener);


	    buttonListener=null;
	}

	if(world.getController().playerNumber==getOwner()) {
	    waypointButton.addActionListener(this);
	    trackShipButton.addActionListener(this);
	    loadFuelButton.addActionListener(this);
	    transferMaterialsButton.addActionListener(this);
	    shipTransferButton.addActionListener(this);
	    componentControlsButton.addActionListener(this);
      
	    recycleMenuItem.addActionListener(this);
	    formFleetMenuItem.addActionListener(this);
	    ordersMenuItem.addActionListener(this);
	    loadFuelMemuItem.addActionListener(this);
	    optimizeFuelMemuItem.addActionListener(this);
	    optimizeFullFuelMenuItem.addActionListener(this);

	    buttonListener=this;

	    waypointButton.setEnabled(true);
	    trackShipButton.setEnabled(true);
	    componentControlsButton.setEnabled(true);

	    if(ownShipsAtSameLocation().size()>0)
		shipTransferButton.setEnabled(true);
	    else
		shipTransferButton.setEnabled(false);

	    if(overOwnPlanet() != null) {
		loadFuelButton.setEnabled(true);
		recycleMenuItem.setEnabled(true);
	    }
	    else {
		loadFuelButton.setEnabled(false);
		recycleMenuItem.setEnabled(false);
	    }

	    if(overAnyPlanet() != null) {
		transferMaterialsButton.setEnabled(true);
	    } else {
		transferMaterialsButton.setEnabled(false);
	    }
	} else {
	    waypointButton.setEnabled(false);
	    trackShipButton.setEnabled(false);
	    loadFuelButton.setEnabled(false);
	    recycleMenuItem.setEnabled(false);
	    transferMaterialsButton.setEnabled(false);
	    shipTransferButton.setEnabled(false);
	    componentControlsButton.setEnabled(false);
	}

	setInfoText();
	changeControlPanel(generalPanel);
    }

    public boolean isCloaking() {
	if(world.getController().playerNumber==getOwner() &&
	   ShipMath.calcCloakPower(misc) > 0) {
	    return true;
	} else {
	    if(cloaking)
		return true;
	}

	return false;
    }

    public String getMapImage() {
	if(isCloaking())
	    return ShipHull.getCloakMapImageName(hullTechLevel);

	return ShipHull.getMapImageName(hullTechLevel);
    }

    public int getHullTech() {
	return hullTechLevel;
    }

    public boolean isVisible() {
	if(isInFleet())
	    return false;
	else
	    return true;
    }

    public Panel getControlPanel() {
    
	if(controlPanel==null) {
	    createControlPanel();
	}
    
	resetControlPanel();

	return controlPanel;
    }


    public void actionPerformed(ActionEvent evt) {
	String arg = evt.getActionCommand();

	if(arg == null) {
	    MenuItem mi = (MenuItem) evt.getSource();
	    arg=mi.getLabel();
	}

	if(arg.equals("Set Waypoint")) 
	    world.requestLocation();
	else if(arg.equals("Track"))
	    world.requestComponent();
	else if(arg.equals("Load fuel"))
	    initiateLoadFuel();
	else if(arg.equals("Recycle"))
	    initiateRecycle();
	else if(arg.equals("Battle Orders")) {
	    ordersPanel.initialize(this);
	    changeControlPanel(ordersPanel);
	}
	else if(arg.equals("Load Fuel"))
	    initiateLoadFuel();
	else if(arg.equals("Load Min Fuel"))
	    initiateLoadMinFuel();
	else if(arg.equals("Load Optimum Fuel"))
	    initiateLoadOptimumFuel();
	else if(arg.equals("Transfer with Planet")) {
	    materialTransferPanel.setTransferTarget(overAnyPlanet());
	    materialTransferPanel.initialize(this);
	    changeControlPanel(materialTransferPanel);
	}
	else if(arg.equals("Transfer with Ship")) {
	    Vector possibleTargets=ownShipsAtSameLocation();

	    if(possibleTargets.size()==0)
		return;

	    if(possibleTargets.size()==1) {
		startTransferWithShip((Ship) possibleTargets.elementAt(0));
		return;
	    }

	    shipSelectionPanel.initialize(this);
	    changeControlPanel(shipSelectionPanel);
	}
	else if(arg.equals("Engineering")) {
	    componentControlsPanel.initialize(this);
	    changeControlPanel(componentControlsPanel);
	}
	else if(arg.equals("Form Fleet")) {
	    initiateFormFleet();
	} else if(arg.startsWith("Action: Surrender to #")) {
	    initiateSurrender(arg);
	}
    }

    public void initiateSurrender(String msg) {
	for(int i=0; i < GlobalDefines.maxPlayers; i++) {
	    if(i == world.getController().playerNumber ||
	       i == GlobalDefines.maxPlayers-1) 
		continue;
	    
	    if(msg.equals("Action: Surrender to #" + i)) {
		initiateSurrender(i);
		break;
	    }
	}
    }

    public void initiateSurrender(int surrenderToPlayer) {
	WorldComponentMessage wcm = new SurrenderShipWCM(getId(), 
							 surrenderToPlayer);
	world.putComponentMessage(wcm);
	surrenderTo = 
	    world.getController().playersInfo.getPlayerName(surrenderToPlayer);
	setInfoText();
    }

    public void startTransferWithShip(Ship s) {
	materialTransferPanel.setTransferTarget(s);
	materialTransferPanel.initialize(this);
	changeControlPanel(materialTransferPanel);
    }

    public void setHull(ShipHull h) {
	hull = h;
    }

    public void setEngines(Vector v) {
	engines = v;
    }

    public void setMisc(Vector v) {
	misc = v;
    }

    public void setCloak(boolean b) {
	cloaking = b;
    }

    public Vector getEngines() {
	return engines;
    }

    public Vector getMisc() {
	return misc;
    }

    public int getShieldPower() {
	if(shields == null)
	    return currentShieldPower;

	int max = ShipMath.calcShieldPower(shields);

	if(max < currentShieldPower)
	    return max;
	else
	    return currentShieldPower;
    }


    public void setWeapons(Vector v) {
	weapons = v;
    }
  
    public Vector getWeapons() {
	return weapons;
    }

    public void setShields(Vector v) {
	shields = v;
    }

    public void setShieldPower(int s) {
	currentShieldPower = s;
    }


    public Vector getShields() {
	return shields;
    }

    public void setCargo(MaterialBundle m) {
	cargoSpace=m;
    }

    public void setEnemyInfoText(String s) {
	enemyInfoText = s;
    }

    public int getMass() {
	int mass=hull.getMass();

	mass+=ShipMath.calculateMass(engines.elements());
	mass+=ShipMath.calculateMass(weapons.elements());
	mass+=ShipMath.calculateMass(shields.elements());
	mass+=ShipMath.calculateMass(misc.elements());

	return mass;
    }

    public int getTrackTarget() {
	return trackTarget;
    }

    public void initiateRecycle() {
	Planet planet=overOwnPlanet();

	planet.startRecycleShip(getId());
    }

    public void initiateLoadFuel() {
	initiateLoadFuel(Integer.MAX_VALUE);
    }

    public void initiateLoadMinFuel() {
	int fuelMass = cargoSpace.getFreeSpace() + cargoSpace.getFuel();
	int totalMass= hull.getMass() + hull.getVolume();
	int consumption=ShipMath.calcFuelConsumption(engines);
	int engine_power=ShipMath.calcEnginePower(engines);
	int idle_consumption=hull.getPowerConsumption() + 
	    ShipMath.calcFuelConsumption(misc);
	int reactor_power = ShipMath.calcReactorPower(misc);
	idle_consumption -= reactor_power;
	if(idle_consumption < 0) {
	    consumption += idle_consumption;
	    idle_consumption = 0;
	    if(consumption < 0)
		consumption = 0;
	}

	TravelInfo ti = new TravelInfo();
	ti.setEfc(fuelMass);

	do {
	    fuelMass = ti.getEfc();
	    totalMass = hull.getMass() + hull.getVolume() -
		cargoSpace.getFreeSpace() - cargoSpace.getFuel() +
		fuelMass;

	    ti = ShipMath.calcEFCandETA(getLocation(),
					getWaypoint(),
					overAnyPlanet() != null,
					fuelMass,
					totalMass,
					engine_power,
					consumption,
					idle_consumption);
	} while(ti.getEfc() < fuelMass);

	initiateFuelTransfer(fuelMass);
    }

    public void initiateLoadOptimumFuel() {
	int fuelMass = cargoSpace.getCapacity();
	int fullMass= hull.getMass() + hull.getVolume();
	int consumption=ShipMath.calcFuelConsumption(engines);
	int engine_power=ShipMath.calcEnginePower(engines);
	int idle_consumption=hull.getPowerConsumption() + 
	    ShipMath.calcFuelConsumption(misc);
	int reactor_power = ShipMath.calcReactorPower(misc);
	idle_consumption -= reactor_power;
	if(idle_consumption < 0) {
	    consumption += idle_consumption;
	    idle_consumption = 0;
	    if(consumption < 0)
		consumption = 0;
	}

	TravelInfo ti = ShipMath.calcEFCandETA(getLocation(),
					       getWaypoint(),
					       overAnyPlanet() != null,
					       fuelMass,
					       fullMass,
					       engine_power,
					       consumption,
					       idle_consumption);

	initiateFuelTransfer(ti.getEfc());
    }

    public void initiateFuelTransfer(int preferedFuel) {
	Planet planet=overOwnPlanet();

	if(planet!=null && preferedFuel >= 0) {
	    int amount=preferedFuel - cargoSpace.getFuel();

	    if(amount > cargoSpace.getFreeSpace())
		amount = cargoSpace.getFreeSpace();

	    if(amount>planet.getMaterials().getFuel())
		amount=planet.getMaterials().getFuel();

	    if(amount!=0) {
		MaterialBundle toMaterials=new 
		    MaterialBundle(MaterialBundle.UNLIMITED);
		MaterialBundle fromMaterials=new 
		    MaterialBundle(MaterialBundle.UNLIMITED);

		if(amount > 0)
		    fromMaterials.putFuel(amount);
		else
		    toMaterials.putFuel(-amount);

		WorldComponentMessage wcm = 
		    new MaterialTransferWCM(getId(), 
					    planet.getId(),
					    toMaterials,
					    fromMaterials,
					    null,
					    null);

		world.putComponentMessage(wcm);

		if(amount > 0) {
		    cargoSpace.addFullBundle(fromMaterials);
		    planet.getMaterials().subtractFullBundle(fromMaterials);
		} else {
		    cargoSpace.subtractFullBundle(toMaterials);
		    planet.getMaterials().addFullBundle(toMaterials);
		}
		setInfoText();
		world.repaintMap();
	    }
	}
    }

    public void initiateLoadFuel(int max) {
	Planet planet=overOwnPlanet();

	if(planet!=null) {
	    int amount=cargoSpace.getFreeSpace();

	    if(amount>planet.getMaterials().getFuel())
		amount=planet.getMaterials().getFuel();

	    if((amount+hull.getMass()+hull.getVolume()-cargoSpace.getFreeSpace())
	       > ShipMath.calcEnginePower(engines))
		amount = ShipMath.calcEnginePower(engines) -
		    hull.getMass() - hull.getVolume() + cargoSpace.getFreeSpace();

	    if(amount>max)
		amount = max;

	    if(amount>0) {
		MaterialBundle toMaterials=new 
		    MaterialBundle(MaterialBundle.UNLIMITED);
		MaterialBundle fromMaterials=new 
		    MaterialBundle(MaterialBundle.UNLIMITED);

		fromMaterials.putFuel(amount);

		WorldComponentMessage wcm = 
		    new MaterialTransferWCM(getId(), 
					    planet.getId(),
					    toMaterials,
					    fromMaterials,
					    null,
					    null);

		world.putComponentMessage(wcm);
		cargoSpace.addFullBundle(fromMaterials);
		planet.getMaterials().subtractFullBundle(fromMaterials);
		setInfoText();
		world.repaintMap();
	    }
	}
    }

    public void initiateFormFleet() {
	WorldComponentMessage wcm = new FormFleetWCM(getId());
	world.putComponentMessage(wcm);
    }

    public Vector ownShipsAtSameLocation() {
	Enumeration e=world.getComponentsByLocation(location);
	Vector ownShips=new Vector();

	while (e.hasMoreElements()) {
	    WorldComponent wc = (WorldComponent) e.nextElement();

	    if(wc instanceof Ship) {
		Ship ship=(Ship) wc;
		if(ship.getOwner() == getOwner() &&
		   ship != this)
		    ownShips.addElement(ship);
	    }
	}

	return ownShips;
    }

    public int getEDT() {
	if(getOwner() != world.getController().playerNumber)
	    return 0;

	int total_mass=(hull.getMass() + 
			hull.getVolume() - 
			cargoSpace.getFreeSpace()); 
	int engine_power=ShipMath.calcEnginePower(engines);
	int consumption=ShipMath.calcFuelConsumption(engines);
	int idle_consumption=hull.getPowerConsumption() + 
	    ShipMath.calcFuelConsumption(misc);
	boolean starts_at_planet=false;
	int reactor_power = ShipMath.calcReactorPower(misc);
	idle_consumption -= reactor_power;
	if(idle_consumption < 0) {
	    consumption += idle_consumption;
	    idle_consumption = 0;
	    if(consumption < 0)
		consumption = 0;
	}

	if(overAnyPlanet()!=null)
	    starts_at_planet=true;

	int edt = ShipMath.calculateEDT(starts_at_planet,
					total_mass,
					cargoSpace.getFuel(),
					engine_power,
					consumption,
					idle_consumption);

	return edt;
    }

    public void drawEDTCircle(ScaledGraphics g) {
	if(getOwner() != world.getController().playerNumber)
	    return;

	if(!((PHWorld)world).isEDTCircleDrawingEnabled())
	    return;

	int edt = getEDT();

	if(edt <= 0)
	    return;

	g.setColor(Color.green);
	g.drawArc(getLocation().getX()-edt,
		  getLocation().getY()-edt,
		  edt*2,
		  edt*2,
		  0,
		  360);
    }

    public void sidePanelOut() {
	changeControlPanel(generalPanel);
    }

    public MaterialBundle getMaterials() {
	return cargoSpace;
    }			   

    void startMaterialTransfer(int toId, 
				      MaterialBundle toMaterials,
				      MaterialBundle fromMaterials,
				      Vector         toComponents,
				      Vector         fromComponents) {
    
	WorldComponentMessage wcm = new MaterialTransferWCM(getId(), toId,
							    toMaterials,
							    fromMaterials,
							    toComponents,
							    fromComponents);
	world.putComponentMessage(wcm);
	setInfoText();
	world.repaintMap();
    }

    public void initiateMaterialTransfer(int toId, 
					 MaterialBundle toMaterials,
					 MaterialBundle fromMaterials,
					 Vector         toComponents,
					 Vector         fromComponents) {

	WorldComponent target = world.getComponentById(toId);
	MaterialBundle targetBundle = null;

	if(target == null) 
	    return;

	if(target instanceof Planet) {
	    targetBundle = ((Planet) target).getMaterials();
	} else if(target instanceof Ship) {
	    targetBundle = ((Ship) target).getMaterials();
	} 

	if(targetBundle == null)
	    return;
	
	cargoSpace.subtractFullBundle(toMaterials);
	Enumeration targetComponents = 
	    cargoSpace.takeComponents(toComponents.elements());

	targetBundle.subtractFullBundle(fromMaterials);
	Enumeration sourceComponents =
	    targetBundle.takeComponents(fromComponents.elements());

	while (targetComponents.hasMoreElements()) {
	    TransferableComponent tc = 
		(TransferableComponent) targetComponents.nextElement();

	    targetBundle.addComponent(tc);
	}

	while (sourceComponents.hasMoreElements()) {
	    TransferableComponent tc = 
		(TransferableComponent) sourceComponents.nextElement();

	    cargoSpace.addComponent(tc);
	}

	cargoSpace.addFullBundle(fromMaterials);
	targetBundle.addFullBundle(toMaterials);

	startMaterialTransfer(toId,
			      (MaterialBundle) toMaterials.clone(),
			      (MaterialBundle) fromMaterials.clone(),
			      toComponents,
			      fromComponents);
    }

    public void setSubcomponentStates(Vector newStates) {
	WorldComponentMessage wcm = 
	    new ShipComponentStateWCM(getId(), newStates);
	world.putComponentMessage(wcm);
	setInfoText();
	world.repaintMap();
    }
				    

    String getEtaText() {
	return etaText;
    }

    public void updateWarningCircle() {
	if(getOwner() != world.getController().playerNumber)
	    return;
    

	int total_mass=(hull.getMass() + 
			hull.getVolume() - 
			cargoSpace.getFreeSpace()); 
	int engine_power=ShipMath.calcEnginePower(engines);
	int consumption=ShipMath.calcFuelConsumption(engines);
	int idle_consumption=hull.getPowerConsumption() + 
	    ShipMath.calcFuelConsumption(misc);
	boolean starts_at_planet=false;

	int reactor_power = ShipMath.calcReactorPower(misc);
	idle_consumption -= reactor_power;
	if(idle_consumption < 0) {
	    consumption += idle_consumption;
	    idle_consumption = 0;
	    if(consumption < 0)
		consumption = 0;
	}

	Enumeration e = world.getComponentsByLocation(location);
	while(e.hasMoreElements()) {
	    WorldComponent wc = (WorldComponent) e.nextElement();
	    if(wc instanceof Planet)
		starts_at_planet=true;
	}

	TravelInfo travelInfo = ShipMath.calcEFCandETA(
						       location,
						       waypoint,
						       starts_at_planet,
						       cargoSpace.getFuel(),
						       total_mass,
						       engine_power,
						       consumption,
						       idle_consumption);

	warningCircleColor=null;
    
	if(!world.getComponentsByLocation(waypoint).hasMoreElements()) {
	    warningCircleColor=Color.yellow;
	}

	if(!travelInfo.reachesDestination()) {
	    warningCircleColor=Color.red;
	}
    }

    public String getName() {
	String name;
	String prefix="";
	String postfix="";

	prefix  = "["+ hullTechLevel + "]";
	postfix = "(" + 
	    world.getController().playersInfo.getPlayerName(getOwner()) + 
	    ")";

	if(world.getController().playerNumber==getOwner()) {
	    postfix = "";	

	    if(engines.size()==0)
		prefix = "[S" + hullTechLevel +"]";
	}

	name = prefix + " " + super.getName() + " " + postfix;

	return name;
    }

    public String getTrueName() {
	return super.getName();
    }

    public int getScanRange() {
	if(super.getScanRange() == 0)
	    return 0;

	if(misc == null)
	    return super.getScanRange();

	Enumeration e = misc.elements();

	int maxScan = super.getScanRange();

	while(e.hasMoreElements()) {
	    ShipMisc sm = (ShipMisc) e.nextElement();

	    if(sm.getTech() == 1) {
		if(sm.getPowerOutput() > maxScan)
		    maxScan=sm.getPowerOutput();
	    }
	}

	return maxScan;
    }

    private void updateSurrenderMenu() {
	surrenderMenu.removeAll();

	if(getOwner() != world.getController().playerNumber)
	    return;

	surrenderMenu.setEnabled(false);
	
	boolean shownPlayers[] = new boolean[GlobalDefines.maxPlayers];
	for(int i=0; i < GlobalDefines.maxPlayers; i++) {
	    shownPlayers[i] = false;
	}

	/* Ignore self and thargons */
	shownPlayers[world.getController().playerNumber] = true;
	shownPlayers[GlobalDefines.maxPlayers-1] = true;

	Enumeration e = world.getComponentsByLocation(getLocation());
	while(e.hasMoreElements()) {
	    WorldComponent wc = (WorldComponent) e.nextElement();
	    if(wc instanceof Ship && !shownPlayers[wc.getOwner()]) {
		shownPlayers[wc.getOwner()] = true;
		MenuItem mi = 
		    new MenuItem(world.getController().playersInfo.
				 getPlayerName(wc.getOwner()));
		mi.setActionCommand("Action: Surrender to #" + wc.getOwner());
		mi.addActionListener(this);
		surrenderMenu.add(mi);
		surrenderMenu.setEnabled(true);
	    }
	}
    }

    public Menu getMenu() {
	if(world.getController().playerNumber==getOwner()) {
	    Fleet fleet = getFleet();
	    if(fleet==null) {
		updateSurrenderMenu();
		return shipMenu;
	    }
	    else
		return fleet.getMenu();
	}
	else
	    return null;
    }

    public Fleet getFleet() {
	WorldComponent hwc = world.getComponentById(trackTarget);
	if((hwc != null) &&
	   (hwc instanceof Fleet) && 
	   (hwc.getLocation().equals(getLocation())) &&
	   (hwc.getOwner() == getOwner())) {
	    return (Fleet) hwc;
	}

	return null;
    }

    public boolean isInFleet() {
	if(getFleet() == null)
	    return false;
	else
	    return true;
    }

    protected ImageObserver getImageObserver() {
	return generalPanel;
    }

    public void resetSurrender() {
	surrenderTo = null;
    }
}
