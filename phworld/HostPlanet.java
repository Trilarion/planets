/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import dsc.netgame.*;
import java.util.*;


public class HostPlanet extends HostWorldComponent {

  private MaterialBundle baseMaterialProduction;
  private MaterialBundle efectiveMaterialProduction;
  private MaterialBundle materials;
  private int baseCreditProduction;
  private int autotransferTarget;
  private TechLevel techLevels;
  private int planetType;
  private boolean ownershipVisible=false;
  private int defenseValue;
  private Location originalLocation;
  private double thargonDistanceMultiplier = 1.0;  

  public static final int NOTHING            = 0; 
  public static final int BUILD_SHIP         = 1; 
  public static final int BUILD_COMPONENT    = 2; 
  public static final int IMPROVE_TECH       = 3; 
  public static final int RECYCLE_SHIP       = 4;
  public static final int IMPROVE_PRODUCTION = 5;
  public static final int WASTE_TIME         = 6;
  public static final int CLONE_SHIP         = 7;

  public static final int PRODUCTION_IMPROVEMENT_START = -25;
  public static final int MAX_PRODUCTION_IMPROVEMENT   =  50;
  public static final int AUTOTRANSFER_RATE            =  50;
  public static final double TRANSFER_CREDIT_COST         =  0.00444;
  public static final int MINIMUM_DEFENSE_VALUE        = 150;

    /* ships hull volome is divided by this number and planet defense
       fire damage is multiplied by the result */
  public static final int DEFENSE_DIVISOR              = 500;
  
  private int currentAction = NOTHING;
  private int actionTech = 0;
  private int actionLevel = 0;
  private int actionLasted = 0;

  private int holdedByCurrentOwner = 0;
  private String actionText = "Doing nothing.";

  private Hashtable groundAttackQueue;

  // Variables used in ship construction
  private int buildTime = 0;
  private ShipHull shipHull = null;
  private Vector buildComponents = new Vector();
  private CloneShipInfo cloneShipInfo = null;

  // Variables used in ship recycling
  private int shipToRecycle = -1;

    // Misc junk
  private int orbitalVelocity = 0;
    
  private void resetAction() {
    currentAction = NOTHING;
    actionLasted = 0;

    Enumeration e = buildComponents.elements();

    while (e.hasMoreElements()) {
      TransferableComponent tc = (TransferableComponent) e.nextElement();

      materials.addComponent(tc);
    }

    buildComponents=new Vector();

    if(shipHull!=null)
      materials.addComponent(shipHull);

    shipHull=null;
  }

  public void putMessage(WorldComponentMessage m) {
    if(m.getSource()!=getOwner()) {
      return;
    }
    
    if(m instanceof PlanetActivityWCM) {

      if(m instanceof BuildShipWCM) {
	BuildShipWCM bs = (BuildShipWCM) m;
	resetAction();

	Vector v = bs.getComponents();

	if(v != null) {
	  int hullID = bs.getHullID();
	  TransferableComponent tc = materials.takeComponent(hullID);
	  if(tc instanceof ShipHull) {
	    shipHull=(ShipHull) tc;

	    buildComponents = makeBuildComponentVector(shipHull, v.elements());
	    buildTime = HostShip.getBuildTimeByComponents(v.size());
	    currentAction = BUILD_SHIP;
	  }
	}
      }

      if(m instanceof CloneShipWCM) {
	  resetAction();

	  if(cloneShipInfo != null) {
	      buildTime = cloneShipInfo.getBuildTime();
	      currentAction = CLONE_SHIP;
	  }
      }

      if(m instanceof BuildComponentWCM) {
	resetAction();
	currentAction = BUILD_COMPONENT;
	BuildComponentWCM bcw = (BuildComponentWCM) m;
	actionTech = bcw.getType();
	actionLevel = bcw.getLevel();
	if(actionLevel > techLevels.getTech(actionTech)) {
	  resetAction();
	  return;
	}
      }

      if(m instanceof ImproveTechWCM) {
	resetAction();
	currentAction = IMPROVE_TECH;
	actionTech = ((ImproveTechWCM) m).getTechType();
      }

      if(m instanceof RecycleShipWCM) {
	resetAction();
	currentAction = RECYCLE_SHIP;
	shipToRecycle=((RecycleShipWCM) m).getShipId();
      }

      if(m instanceof StartIdlingWCM) {
	resetAction();
	currentAction = WASTE_TIME;
      }

      if(m instanceof ImproveProductionWCM) {
	if(currentAction != IMPROVE_PRODUCTION) {
	  resetAction();
	  currentAction = IMPROVE_PRODUCTION;
	}
      }

      if(m instanceof AutotransferWCM) {
	  int target = ((AutotransferWCM) m).getTransferTarget();

	  if(target == -1)
	      autotransferTarget = -1;
	  else {
	      HostWorldComponent hwc =
		  hostWorld.getComponentById(target);

	      if(hwc != null && hwc instanceof HostPlanet) {
		  if(hwc.getOwner() == getOwner() 
		     && !hwc.getName().equals("Dodechaedron")) {
		      autotransferTarget = target;
		  }
	      }
	  }
      }
    }
  }

  public int getHoldCount() {
    return holdedByCurrentOwner;
  }

    void troopAttack(int attacker, int troops) {
	Integer previousTroops = (Integer) 
	    groundAttackQueue.get(attacker + "");
	
	if(previousTroops == null) {
	    groundAttackQueue.put(attacker + "", new Integer(troops));
	} else {
	    int t = previousTroops.intValue() + troops;
	    groundAttackQueue.put(attacker + "", new Integer(t));
	}
    }

    private void handleGroundCombat() {
	Enumeration e = groundAttackQueue.keys();

	while(e.hasMoreElements()) {
	    String attacker = (String) e.nextElement();
	    int a = -1;

	    try {
		a = Integer.parseInt(attacker);
	    } catch (NumberFormatException ex) {}
	    
	    handleTroopAttack(a, 
			      ((Integer) groundAttackQueue.get(attacker)).
			      intValue());

	    groundAttackQueue.remove(attacker);
	}
    }

  private void handleTroopAttack(int attacker, int troops) {
      int killedtroops = materials.getTroops();
      if(killedtroops > troops)
	  killedtroops = troops;

      if(killedtroops <= 0)
	  return;

      materials.takeTroops(killedtroops);
      MaterialBundle killedMat = new MaterialBundle(MaterialBundle.UNLIMITED);
      killedMat.putTroops(killedtroops);
      ((PHHostWorld) hostWorld).addToMaterialUsageStatistics(getOwner(), 
							     killedMat);

      hostWorld.sendChatMessage(getOwner(), 
				hostWorld.getPlayerName(attacker) + 
				" dropped " + troops + " troops on your " +
				"planet " + getName() + " and killed " +
				killedtroops + " your troops (" +
				materials.getTroops() + " remaining).");

      hostWorld.sendChatMessage(attacker,
				"You " +
				"dropped " + troops + " troops on " +
				hostWorld.getPlayerName(getOwner()) + 
				"'s planet " + getName() + " and killed " +
				killedtroops + " troops (" +
				materials.getTroops() + " remaining).");
  }

  public void ownershipCheck() {

    if(materials.getTroops() == 0 || getOwner() == -1) {
      Enumeration e = hostWorld.getComponentsByLocation(location);

      boolean ownShip = false;
      boolean enemyShip = false;
      int enemyShipOwner = -1;

      while(e.hasMoreElements()) {
	HostWorldComponent hwc = (HostWorldComponent) e.nextElement();
	if(hwc instanceof HostShip) {
	    if(hostWorld.isAlly(hwc.getOwner(), getOwner()))
		ownShip=true;
	    else {
		if(!hostWorld.isAlly(hwc.getOwner(), getOwner())) {
		    Enumeration es=hostWorld.getComponentsByLocation(location);
		    boolean allAllied = true;
		    while(es.hasMoreElements()) {
			HostWorldComponent h = 
			    (HostWorldComponent) es.nextElement();
			if(h instanceof HostShip) {
			    if(!hostWorld.isAlly(h.getOwner(),
						hwc.getOwner())) {
				allAllied = false;
				break;
			    }
			}
		    }

		    if(allAllied) {
			enemyShip=true;
			enemyShipOwner=hwc.getOwner();
		    } 
		}
	    }
	}
      }

      if(!ownShip && enemyShip) {
	if(name.equals("Dodechaedron"))
	  hostWorld.broadcastChatMessage(name + " has been taken over by " +
					 hostWorld.getPlayerName(enemyShipOwner));
	else
	  hostWorld.sendChatMessage(owner, name + " has been taken over by " +
				    hostWorld.getPlayerName(enemyShipOwner));
	owner=enemyShipOwner;
	hostWorld.sendChatMessage(owner, name + " is now under your control");

	holdedByCurrentOwner = 0;
	autotransferTarget   = -1;
	defenseValue         = 0;
	resetAction();
	actionText   = "Doing nothing.";
	thargonDistanceMultiplier = Math.random()*0.8+0.2; 
      }
    }

    if(owner != -1)
      holdedByCurrentOwner++;

    if(owner != -1 && 
       name.equals("Dodechaedron") && 
       holdedByCurrentOwner%10==0) {
      hostWorld.broadcastChatMessage("Planet Dodechaedron held " 
                                     + holdedByCurrentOwner
				     + " round(s) by " + 
				     hostWorld.getPlayerName(owner));
    }
  }

    private int getCreditProduction() {
	if(getPlanetType() == PlanetType.DODECHAEDRON) {
	    return 0;
	}

	int prod = baseCreditProduction;
	int add  = (int) 
	    Math.pow((((double)getMaterials().getTroops())/10.0), 0.5);

	return prod + add;
    }

  private void doProduction() {

    actionText="Doing nothing";
    actionLasted++;

    int production_percentage = 100;
    if(currentAction == IMPROVE_PRODUCTION) {
      production_percentage = actionLasted + PRODUCTION_IMPROVEMENT_START;
      if(production_percentage > MAX_PRODUCTION_IMPROVEMENT &&
	 !getName().equals("Dodechaedron"))
	production_percentage = MAX_PRODUCTION_IMPROVEMENT;

      if(production_percentage > 0)
	actionText="Improve Production (+" +production_percentage+ "%)";
      else
	actionText="Improve Production (" +production_percentage+ "%)";

      production_percentage+=100;
    }

    efectiveMaterialProduction = 
      (MaterialBundle) baseMaterialProduction.clone();
    efectiveMaterialProduction.scaleBundle(production_percentage);

    if(owner!=-1) {
      materials.addFullBundle(efectiveMaterialProduction);
      ((PHHostWorld) hostWorld).addCredits(getOwner(), getCreditProduction());
    }

    if(currentAction == WASTE_TIME) {
      actionText="In idle mode";
    }


    if(currentAction == BUILD_SHIP) {
      actionText="Building ship ("+actionLasted+"/"+buildTime+")";
      if(buildTime <= actionLasted) { 

	if( (shipHull!=null) ) { 
	  HostShip ship = new HostShip(hostWorld, owner, location);
	  ship.setComponents(shipHull, buildComponents.elements());
	  hostWorld.putComponent(ship);
	  buildComponents=new Vector();
	  shipHull=null;
	  hostWorld.sendChatMessage(owner, name + 
				    " built new ship");
	}

	resetAction();
	actionText="Just built a new ship";
      }
    }

    if(currentAction == CLONE_SHIP) {
      actionText="";
      if(!decreaseCloneMaterials()) {
	actionText="[MAT] ";
	actionLasted--;
      }

      actionText+="Cloning ship ("+actionLasted+"/"+buildTime+")";
      if(buildTime <= actionLasted) { 
	if( (cloneShipInfo != null) ) { 
	    HostShip ship = cloneShipInfo.getNewCloneShip(hostWorld,
							  owner,
							  location);
	    hostWorld.putComponent(ship);
	    cloneShipInfo.incBuildCount();

	    hostWorld.sendChatMessage(owner, name + 
				      " cloned a ship");
	}

	actionLasted=0;
	buildTime   = cloneShipInfo.getBuildTime();
	actionText="Just cloned a new ship";
      }
    }

    if(currentAction == IMPROVE_TECH) {
      actionText="";
      if(!decreaseTechMaterials()) {
	actionText="[MAT] ";
	actionLasted--;
      }

      actionText+="Improving " + TechLevel.getTechName(actionTech) +
	          " ("+actionLasted+"/"+(techLevels.getTech(actionTech)+1)+")";
      if(techLevels.getTech(actionTech) < actionLasted) {
	techLevels.setTech(actionTech, techLevels.getTech(actionTech)+1);
	hostWorld.sendChatMessage(owner, name + 
				  " improved techlevel of " + 
				  TechLevel.getTechName(actionTech) +
				  " to " + techLevels.getTech(actionTech) +
				  ".");
	if(actionTech == ShipComponent.MISC && 
	   techLevels.getTech(actionTech) == 8) {
		hostWorld.broadcastChatMessage(
			   "Security warning: Some has tested superbomb!");
	}

	actionLasted = 0;
	actionText="Improved " + TechLevel.getTechName(actionTech) +
	  " to tech " + techLevels.getTech(actionTech);
	
	actionLevel=techLevels.getTech(actionTech);
	TransferableComponent tc = null;
	switch(actionTech) {
	case ShipComponent.ENGINE:
	  tc = new ShipEngine(actionLevel);
	  break;
	case ShipComponent.WEAPON:
	  tc = new ShipWeapon(actionLevel);
	  break;
	case ShipComponent.SHIELD:
	  tc = new ShipShield(actionLevel);
	  break;
	}
	if(tc!=null)
	  materials.addComponent(tc);

	if(techLevels.getTech(actionTech) >= techLevels.MAX_TECH) {
	  actionText="Just reached max " + TechLevel.getTechName(actionTech) +
		     " tech.";
	  resetAction();

	}
      }
    }

    if(currentAction == BUILD_COMPONENT) {
      actionText="";
      if(!decreaseComponentMaterials()) {
	actionText="[MAT] ";
	actionLasted--;
      }

      actionText+="Building " + TechLevel.getNounTechName(actionTech) +
	" (" + actionLevel + ".)("+actionLasted+"/"+
	techLevels.getBuildTime(actionTech, actionLevel)+")";

      if(techLevels.getBuildTime(actionTech, actionLevel) <= actionLasted) {
	actionLasted = 0;
	TransferableComponent tc = null;
	switch(actionTech) {
	case ShipComponent.HULL:
	  tc = new ShipHull(getOwner(), actionLevel);
	  break;
	case ShipComponent.ENGINE:
	  tc = new ShipEngine(actionLevel);
	  break;
	case ShipComponent.WEAPON:
	  tc = new ShipWeapon(actionLevel);
	  break;
	case ShipComponent.SHIELD:
	  tc = new ShipShield(actionLevel);
	  break;
	case ShipComponent.MISC:
	  tc = new ShipMisc(actionLevel);
	  break;
	}
	
	if(tc!=null) {
	  if(tc instanceof ShipHull)
	    hostWorld.sendChatMessage(owner, name + 
				      " built new hull");
	  else
	    hostWorld.sendChatMessage(owner, name + 
				      " built " + tc);
	  materials.addComponent(tc);
	  actionText="Just built new " +TechLevel.getNounTechName(actionTech) +
	    " (" + actionLevel + ")"; 
	}
      }
    }

    if(currentAction == RECYCLE_SHIP) {
      String shipName="";
      HostShip ship=null;
      HostWorldComponent hwc = hostWorld.getComponentById(shipToRecycle);
      if(hwc instanceof HostShip)
	ship=(HostShip) hwc;

      if(ship!=null && 
	 ship.getLocation().equals(getLocation()) &&
	 ship.getOwner()==getOwner()) 
	{
	  shipName=ship.getName();
	  Enumeration e = ship.getRecycleComponents();
	  
	  while (e.hasMoreElements()) {
	    TransferableComponent tc = (TransferableComponent) e.nextElement();
	    materials.addComponent(tc);
	  }

	  materials.addFullBundle(ship.getMaterials());

	  e = ship.getMaterials().components();

	  while (e.hasMoreElements()) {
	    TransferableComponent tc = (TransferableComponent) e.nextElement();
	    materials.addComponent(tc);
	  }

	  hostWorld.removeComponent(ship.getId());
	  hostWorld.sendChatMessage(owner, name + 
				    " recycled ship " + shipName);
	  actionText="Just recycled " + shipName;

	} 
      
      resetAction();
    }
  }

  private void repairComponents() {
    int amount=6;

    Enumeration e = materials.components();

    while (e.hasMoreElements()) {
      TransferableComponent tc = (TransferableComponent) e.nextElement();

      if(tc instanceof ShipComponent) {
	ShipComponent sc = (ShipComponent) tc;

	if(sc instanceof ShipHull &&
	   sc.getTech() <= techLevels.getTech(ShipComponent.HULL))
	  sc.repairDamage(amount*2);

	if(sc instanceof ShipEngine &&
	   sc.getTech() <= techLevels.getTech(ShipComponent.ENGINE))
	  sc.repairDamage(amount);

	if(sc instanceof ShipShield &&
	   sc.getTech() <= techLevels.getTech(ShipComponent.SHIELD))
	  sc.repairDamage(amount);

	if(sc instanceof ShipWeapon &&
	   sc.getTech() <= techLevels.getTech(ShipComponent.WEAPON))
	  sc.repairDamage(amount);

	if(sc instanceof ShipMisc &&
	   sc.getTech() <= techLevels.getTech(ShipComponent.MISC))
	  sc.repairDamage(amount);
      }
    }
  }

  private void doAutotransfers() {
      if(autotransferTarget == -1 ||
	 autotransferTarget == getId())
	  return;

      MaterialBundle transferBundle = 
	  new MaterialBundle(MaterialBundle.UNLIMITED);

      int a;

      transferBundle.putMetals(materials.
			  getMetals()>AUTOTRANSFER_RATE ? AUTOTRANSFER_RATE : 0);

      transferBundle.putMinerals(materials.
			  getMinerals()>=AUTOTRANSFER_RATE ? AUTOTRANSFER_RATE : 0);

      transferBundle.putOil(materials.
			  getOil()>=AUTOTRANSFER_RATE ? AUTOTRANSFER_RATE : 0);


      if(transferBundle.getMass() > 0 &&
         materials.subtractFullBundle(transferBundle)) {
	  Location source, target;
	  source = (Location) getLocation().clone();
	  target = (Location) hostWorld.
	      getComponentById(autotransferTarget).
	      getLocation().clone();
	  int creditCost;

	  creditCost = (int) ((((double) transferBundle.getMass())*
	      ((double) source.distance(target)))*TRANSFER_CREDIT_COST);
	  ((PHHostWorld) hostWorld).takeCredits(getOwner(), creditCost);

	  MaterialBundle fullTransfer = (MaterialBundle)transferBundle.clone();
	  transferBundle.scaleBundle(
	      HostTransferer.getEfectivityPercentage(source.distance(target)));

	  fullTransfer.subtractFullBundle(transferBundle);
	  ((PHHostWorld) hostWorld).addToMaterialUsageStatistics(getOwner(), 
								 fullTransfer);

	  HostTransferer ht =
	      new HostTransferer(hostWorld,
				 source,
				 target,
				 autotransferTarget,
				 transferBundle);

	  hostWorld.putComponent(ht);
      }
  }

  public void doRound() {
    doProduction();
    repairComponents();
    doAutotransfers();
    handleGroundCombat();
  }

    void addDefensePod(int dp) {
	defenseValue += dp;
    }

    public int getDefensePower() {
	int max = defenseValue + MINIMUM_DEFENSE_VALUE;

	if(materials.getTroops() < max)
	    max = materials.getTroops();

	max=max*2;
	double p  = Math.pow((double) max, 0.5);

	return ((int) p)*2;
    }

    public int getDefenseDamage() {
	int power = getDefensePower();
	int damage;

	if(power <= 0) {
	    damage=0;
	} else {
	    damage=((int) (Math.random() * power))+1;
	}

	return damage;
    }

  public WorldComponent getWorldComponent(int player) {
    Planet p;

    if(hostWorld.isVisibleByPlayer(player, this) ||
       ownershipVisible) {
      p=new Planet(id, name, owner, location);
    } else
      p=new Planet(id, name, -1, location);

    p.setScanRange(scanRange);
    p.setPlanetType(planetType);
    p.setTroops(-1);

    if(player == owner) {
      p.setTechLevels((TechLevel) techLevels.clone());
      p.setMaterials((MaterialBundle) materials.clone());
      p.setProduction((MaterialBundle) efectiveMaterialProduction.clone());
      p.setActivityDuration(actionLasted);
      p.setActivityType(currentAction);
      p.setActivityText(actionText);
      p.setAutotransferTarget(autotransferTarget);
      p.setDefensePower(getDefensePower()/2);
      p.setCloneShipInfo(cloneShipInfo);
    } else {
	if(isShipOnOrbit(player))
	    p.setTroops(materials.getTroops());
    }
    return p;
  }

  private void initializePlanet(MaterialBundle production, 
				MaterialBundle materials,
				int creditProduction) {
    scanRange=300;
    name="Name Not Set";
    autotransferTarget=-1;

    this.baseMaterialProduction     = production;
    this.efectiveMaterialProduction = production;
    this.materials = materials;

    this.baseCreditProduction       = creditProduction;

    techLevels = new TechLevel();

    if(planetType==PlanetType.DODECHAEDRON) {
	setName("Dodechaedron");
	setOwnershipVisiblity(true);
    }

    groundAttackQueue = new Hashtable();
    defenseValue = 0;

    if(planetType == PlanetType.HOME) {
	orbitalVelocity = 15;
	thargonDistanceMultiplier = 1.0;
    } else {
	orbitalVelocity = 14 + ((int) (Math.random() * 5.0));
	thargonDistanceMultiplier = Math.random()*0.8+0.2; 
    }
  }
  
  protected void scaleRichness(int percentage) {
    this.baseMaterialProduction.scaleBundle(percentage);
    this.efectiveMaterialProduction.scaleBundle(percentage);
    this.materials.scaleBundle(percentage);
  }

  public HostPlanet(HostWorld hw, int owner, Location location, int type) {
    super(hw,owner,location);
    planetType=type;

    originalLocation = (Location) location.clone();

    MaterialBundle prod = PlanetType.getProductionMaterials(type);
    MaterialBundle initial = PlanetType.getInitialMaterials(type);
    int credProd   = PlanetType.getCreditProduction(type);

    initializePlanet(prod, initial, credProd);
  }

  public HostPlanet(HostWorld hw, 
		    int owner, 
		    Location location,
		    MaterialBundle prod,
		    MaterialBundle materials,
		    int type) {
    super(hw,owner,location);
    planetType=type;

    originalLocation = (Location) location.clone();
    int credProd     = PlanetType.getCreditProduction(type);

    initializePlanet(prod, materials, credProd);
  }

  public boolean isVisible() {
    return true;
  }

  public void setTech(TechLevel tl) {
    techLevels=tl;
  } 

  public void setOwnershipVisiblity(boolean b) {
    ownershipVisible=b;
  }

  public boolean getOwnershipVisibility() {
    return ownershipVisible;
  }

  private Vector makeBuildComponentVector(ShipHull shipHull, Enumeration e) {
    Vector v = new Vector(); 
    CloneShipInfo csi = new CloneShipInfo(shipHull.getTech(), 
					  shipHull.getName());

    while (e.hasMoreElements()) {
      Integer i=null;

      try {
	i = (Integer) e.nextElement();
      } catch (ClassCastException ex) {return v;}
      
      TransferableComponent tc = materials.takeComponent(i.intValue());

      if(tc != null && (tc instanceof ShipComponent)) {
	  ShipComponent sc = (ShipComponent) tc;
	  v.addElement(tc);
	  csi.addComponent(sc.getTechType(), sc.getTech());
      }
    }
    
    if(csi.canBeBuilt(techLevels))
	cloneShipInfo = csi;

    return v;
  }

  private boolean decreaseCloneMaterials() {
    MaterialBundle mb;

    if(actionLasted == 1) 
	mb = cloneShipInfo.getFirstRoundBuildMaterials();
    else
	mb = cloneShipInfo.getRestRoundsBuildMaterials();

    if(materials.subtractFullBundle(mb)) {
	((PHHostWorld) hostWorld).addToMaterialUsageStatistics(getOwner(), mb);
	return true;
    } 
    
    return false;
  }

  private boolean decreaseComponentMaterials() {
    MaterialBundle mb;

    if(actionLasted == 1) 
      mb = TechLevel.getFirstRoundBuildMaterials(actionTech, actionLevel);
    else
      mb = TechLevel.getRestRoundsBuildMaterials(actionTech, actionLevel);

    if(materials.subtractFullBundle(mb)) {
	((PHHostWorld) hostWorld).addToMaterialUsageStatistics(getOwner(), mb);
	return true;
    }

    return false;
  }

  private boolean decreaseTechMaterials() {
    MaterialBundle mb;
    int level = techLevels.getTech(actionTech)+1;

    int existingNumberOfTechs = 
	PlanetMath.calcNumberOfTechPlanets(getOwner(),
					   getAllPlanets(),
					   actionTech,
					   level);

    if(level<=TechLevel.MAX_TECH) {
      if(actionLasted == 1) 
	mb = TechLevel.getFirstRoundTechMaterials(actionTech, 
						  level, 
						  existingNumberOfTechs);
      else
	mb = TechLevel.getRestRoundsTechMaterials(actionTech, 
						  level,
						  existingNumberOfTechs);

      if(materials.subtractFullBundle(mb)) {
	  ((PHHostWorld) hostWorld).addToMaterialUsageStatistics(getOwner(), 
								 mb);
	  return true;
      } else {
	  return false;
      }
    }

    return true;
  }


  public MaterialBundle getMaterials() {
    return materials;
  }

  public void takeSuperBombDamage() {
    planetType = PlanetType.ASTEROID;
    materials = new MaterialBundle(MaterialBundle.UNLIMITED);
    baseMaterialProduction = new MaterialBundle(MaterialBundle.UNLIMITED);
    techLevels = new TechLevel();

    hostWorld.sendChatMessage(owner, 
			      name + " has been destroyed by superbomb!");

  }

    public int getPlanetType() {
	return planetType;
    }

    public TechLevel getTechLevels() {
	return techLevels;
    }

    private Enumeration getAllPlanets() {
	Enumeration e = hostWorld.getComponents();
	Vector v = new Vector();

	while(e.hasMoreElements()) {
	    HostWorldComponent hwc = (HostWorldComponent) e.nextElement();

	    if(hwc instanceof HostPlanet)
		v.addElement(hwc);
	}

	return v.elements();
    }

    private boolean isShipOnOrbit(int player) {
      Enumeration e = hostWorld.getComponentsByLocation(location);

      while(e.hasMoreElements()) {
	HostWorldComponent hwc = (HostWorldComponent) e.nextElement();
	if(hwc instanceof HostShip) {
	  if(hwc.getOwner() == player)
	      return true;
	}
      }

      return false;
    }

    protected int getBaseOrbitalVelocity() {
	return orbitalVelocity;
    }

    protected Location getOriginalLocation() {
	if(originalLocation == null)
	    return getLocation();

	return originalLocation;
    }

    public void setLocation(Location l) {
	if(getOriginalLocation() == null)
	    originalLocation = (Location) l.clone();

	super.setLocation(l);
    }

    public double getThargonDistanceMultiplier() {
	return thargonDistanceMultiplier;
    }
}
