/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;


public class PHWorldState extends dsc.netgame.WorldState {

    BattleOutcomes battleOutcomes = null;
    Race           race           = null;
    MaterialBundle materialUsage  = null;
    
    int            credits          = 0;
    int            creditUsage      = 0;
    int            creditProduction = 0;

    public PHWorldState() {
	resetResourceUsage();
    }

    protected void setBattleOutcomes(BattleOutcomes bo) {
	battleOutcomes=bo;
    }

    public BattleOutcomes getBattleOutcomes() {
	if(battleOutcomes == null)
	    return new BattleOutcomes();

	return battleOutcomes;
    }

    protected void setRace(Race r) {
	race = r;
    }

    public Race getRace() {
	return race;
    }

    public MaterialBundle getMaterialUsage() {
	return materialUsage;
    }

    public int getCreditUsage() {
	return creditUsage;
    }

    public int getCreditProduction() {
	return creditProduction;
    }

    public void resetResourceUsage() {
	materialUsage    = new MaterialBundle(MaterialBundle.UNLIMITED);
	creditUsage      = 0;
	creditProduction = 0;
    }

    public void addMaterialUsage(MaterialBundle m) {
	materialUsage.addFullBundle(m);
    }

    public boolean takeCredits(int amount) {
	if(amount < 0 || amount>credits) {
	    return false;
	}

	credits     -= amount;
	creditUsage += amount;
	return true;
    }

    public int getCredits() {
	return credits;
    }

    public void putCredits(int amount) {
	if(amount < 0) {
	    throw(new IllegalArgumentException("Amount must be positive"));
	}

	credits          += amount;
	creditProduction += amount;
    }
}
