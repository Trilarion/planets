/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import dsc.netgame.*;
import java.util.*;


public abstract class HostMiscComponent extends HostWorldComponent {

  private boolean isSelfDestructing = false;

  public void putMessage(WorldComponentMessage m) {
    if(m.getSource()!=getOwner()) {
      return;
    }
    
    if(m instanceof MiscActivityWCM) {
      if(m instanceof SelfDestructWCM) {
	  isSelfDestructing = true;
      }
    }
  }

  public void ownershipCheck() {
      Enumeration e = hostWorld.getComponentsByLocation(location);

      boolean ownShip = false;
      boolean enemyShip = false;
      int enemyShipOwner = -1;

      while(e.hasMoreElements()) {
	HostWorldComponent hwc = (HostWorldComponent) e.nextElement();
	if(hwc instanceof HostShip) {
	  if(hwc.getOwner() == owner)
	    ownShip=true;
	  else {
	    if(!hostWorld.isAlly(hwc.getOwner(), getOwner())) {
	      enemyShip=true;
	      enemyShipOwner=hwc.getOwner();
	    }
	  }
	}
      }

      if(!ownShip && enemyShip) {
	hostWorld.sendChatMessage(owner, name + " has been taken over by " +
				  hostWorld.getPlayerName(enemyShipOwner));
	owner=enemyShipOwner;
	hostWorld.sendChatMessage(owner, name + " is now under your control");
      }
  }

    public void doRound() {
	if(isSelfDestructing) {
	    hostWorld.removeComponent(this.getId());

	    hostWorld.sendChatMessage(owner, name +
				      " has self-destructed.");
	}
    }

  public HostMiscComponent(HostWorld hw, 
			   int owner, 
			   Location location) {
    super(hw,owner,location);
  }

  public boolean isVisible() {
    return true;
  }
}
