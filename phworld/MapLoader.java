/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import java.util.*;
import java.io.*;
import dsc.netgame.*;

/**
   MapLoader loads given mapfile and parses it.
 */

public class MapLoader {
  private PHHostWorld world;
  private String mapFile;
  private int players;

  private boolean mapOK;
  private int xSize, ySize;
  private Vector components;

    private Hashtable planetResources;
  private BufferedReader in;
  private int lineNumber=0;

  public MapLoader(PHHostWorld world, String mapFile, int players) {
    this.world=world;
    this.mapFile=mapFile;
    this.players=players;

    components=new Vector();
    planetResources=new Hashtable();
    mapOK=true;

    loadMap(mapFile);
  }

  public boolean isMapValid() {
    return mapOK;
  }

  public int getMapXSize() {
    return xSize;
  }

  public int getMapYSize() {
    return ySize;
  }

  public Enumeration getComponents() {
    return components.elements();
  }

  public static Hashtable getLoadableMaps() {
    Hashtable maps = new Hashtable();
    File d = new File("data/maps");

    String s [] =d.list();
    
    for (int i = 0; i < s.length; i++){
       maps.put(s[i] , "data/maps/" + s[i]);
    }

    return maps;
  }

  private void loadMap(String mapFile) {
    File f   = new File(mapFile);
    try {
      in=new BufferedReader(new FileReader(f));
    } catch (Exception e) {
      System.err.println("Can't open file " + mapFile); System.exit(1);
    }

    if(loadHeader()) loadPlanets();

    try {
      in.close();
    } catch (IOException e) {warning("Can't close file");}
  }

  private StringTokenizer getNewLine() {
    while(true) {
      String line;

      try {
	lineNumber++; line=in.readLine();
      } catch(IOException e) {
	error(e+"");
	return null;
      }
      
      if(line==null) return null;
      if(line.startsWith("#") || line.trim().equals("")) continue;
      return new StringTokenizer(line);
    }
  }
  
  private boolean expect(StringTokenizer st, String s) {
    if(!st.hasMoreTokens() || !st.nextToken().equals(s)) {
      error("Expecting token: " +s);
      return false;
    }
    
    return true;
  }

  private boolean loadHeader() {
    StringTokenizer st=null;

    st=getNewLine();
    if(st==null) {
      error("No header.");
      return false;
    }
    
    if(!expect(st,"BEGIN")) return false;
    if(!expect(st,"MAPINFO")) return false;

    while(true) {
      st=getNewLine();
      if(st==null) {
	error("No header end tag found");
	return false;
      }
      
      String s;

      if(st.countTokens()<1) {
	error("Invalid entry");
	return false;
      }

      s=st.nextToken();
      
      if(s.equals("END")) {
	if(!expect(st,"MAPINFO")) return false;
	return true;
      }

      StringTokenizer stLine = new StringTokenizer(s, "=");
      if(stLine.countTokens()!=2) {
	error("Invalid entry");
	return false;
      }

      s=stLine.nextToken();

      if(s.startsWith("XSIZE")) {
	try {
	  xSize=Integer.parseInt(stLine.nextToken());
	} catch (Exception e) {
	  error("Invalid XSIZE: " + e);
	  return false;
	}
      } else if(s.startsWith("YSIZE")) {
	try {
	  ySize=Integer.parseInt(stLine.nextToken());
	} catch (Exception e) {
	  error("Invalid YSIZE: " + e);
	  return false;
	}
      } else {
	  String value = stLine.nextToken();
	  planetResources.put(s, value);	  
      }
    }
  }

  private boolean loadPlanets() {
    StringTokenizer st=null;

    st=getNewLine();
    if(st==null) {
      error("No planets.");
      return false;
    }
    
    if(!expect(st,"BEGIN")) return false;
    if(!expect(st,"PLANETS")) return false;

    while(true) {
      st=getNewLine();
      if(st==null) {
	error("No planets end tag found");
	return false;
      }
      
      String s;

      if(st.countTokens()<1) {
	error("Invalid entry");
	return false;
      }

      s=st.nextToken();
      if(s.equals("END")) {
	if(!expect(st,"PLANETS")) return false;
	return true;
      }

      if(st.countTokens()<5) {
	error("Invalid planet entry");
	return false;
      }

      int xCoord, yCoord, owner;
      String type, resources, name;

      try {
	xCoord=Integer.parseInt(s);
      } catch (NumberFormatException e){
	error("Invalid X-coordinate");
	return false;
      }

      s=st.nextToken();
      try {
	yCoord=Integer.parseInt(s);
      } catch (NumberFormatException e){
	error("Invalid Y-coordinate");
	return false;
      }
      
      type=st.nextToken();

      s=st.nextToken();
      owner=-1;
      try {
	owner=Integer.parseInt(s);
      } catch (NumberFormatException e){}
      
      resources=st.nextToken();

      name="";
      while(st.countTokens()>0) {
	name+=st.nextToken() + " ";
      }
      name=name.trim();
      if(name.equals("\"\""))
	  name="";
      
      placePlanet(new Location(xCoord, yCoord),
		  name, type, owner, resources);
    }    
  }

  private String verifyName(String name, int owner) {
      if(name.equals("")) {
	  return verifyName(world.getNameDatabase().getNewPlanetName(owner), owner);
      } else if(name.startsWith("\"") && name.lastIndexOf('\"')==name.length()-1) {
	name=name.substring(1,name.length()-1);
      }

      Enumeration e = components.elements();
      while(e.hasMoreElements()) {
	String s = ((HostWorldComponent) e.nextElement()).getName();
	if(s.equals(name)) {
	  warning("Duplicate name: " + name);
	  return verifyName(world.getNameDatabase().getNewPlanetName(owner), owner);
	}
      }

      return name;
  }

  protected void placePlanet(Location location, 
			   String name, 
			   String type, 
			   int owner,
			   String resources) {

    Enumeration e;
    Location temp;

    name=verifyName(name, owner);

    e=components.elements();
    while(e.hasMoreElements()) {
      temp=((HostWorldComponent) (e.nextElement())).getLocation();
      if(location.distance(temp)<70) {
	warning("Planet " + name + " at " +location +
		" too close to other planet at " + temp);
      }
    }

    if(location.getX()>(xSize-50) || location.getX()<25
       || location.getY()>(ySize-50) || location.getY()<25) {
	warning("Planet " + name + " outside the map area");
    }

    HostPlanet hp;
    int planetType = PlanetType.getPlanetTypeNumber(type);

    if(planetType==PlanetType.HOME && owner >=0) {
      Enumeration ep = world.getAllPlayers();

      while(ep.hasMoreElements()) {
	int n=((Integer) ep.nextElement()).intValue();
	if(n==owner) {
	    hp=new HostPlanet(world, 
			      owner, 
			      location,
			      getInitialProduction(resources, planetType),
			      getInitialMaterials(resources, planetType, owner),
			      PlanetType.HOME);
	    hp.setTech(getInitialTech(resources, planetType));
	    hp.setOwnershipVisiblity(true);
	    hp.setName(name);
	    components.addElement(hp);
	    return;
	}
      }

      planetType=PlanetType.GAIA;
    } 

    if(planetType==-1) {
      error("Invalid planet type: " + type);
      return;
    }

    if(resources.equals("-")) {
	hp=new HostPlanet(world, owner, location, planetType);
    } else {
	hp=new HostPlanet(world, 
			  owner, 
			  location,
			  getInitialProduction(resources, planetType),
			  getInitialMaterials(resources, planetType, owner),
			  planetType);
	hp.setTech(getInitialTech(resources, planetType));
    }
    
    if(planetType != PlanetType.DODECHAEDRON)
	hp.setName(name);

    components.addElement(hp);
  }

    private String getNthSubstring(String s, int n) {
      StringTokenizer stLine = new StringTokenizer(s, "/ ");

      if(stLine.countTokens()<=n) {
	error("Invalid MAT/PROD/TECH declaration \"" + s + "\"");
	return "-";
      }

      for(int i=0; i<n; i++)
	  stLine.nextToken();

      return stLine.nextToken();
    }

    private int getNthSubInt(String s, int n) {
	String ns = getNthSubstring(s, n);
	
	if(s==null) {
	    warning("Null pointer in MAT/PROD/TECH declaration");
	    return 0;
	}

	try {
	    return Integer.parseInt(ns);
	} catch (NumberFormatException e) {
	    warning("Invalid number in MAT/PROD/TECH declaration");
	    return 0;
	}
    }

    private int getSubnumber(String s, int n) {
	return getNthSubInt((String) planetResources.get(s), n);
    }

    private String getSubString(String s, int n) {
	return getNthSubstring((String) planetResources.get(s), n);
    }


    private MaterialBundle getInitialProduction(String res, int planetType) {

	if(res.equals("-") || getNthSubstring(res, 1).equals("-")) {
	    if(planetType == PlanetType.HOME) {
		return PlanetType.getHomePlanetProduction();
	    } else {
		return PlanetType.getProductionMaterials(planetType);
	    }
	}
	
	MaterialBundle prod = new MaterialBundle(MaterialBundle.UNLIMITED);
	String pointer = getNthSubstring(res, 1);

	prod.putDodechadrium(getSubnumber(pointer, 0));
	prod.putFuel(getSubnumber(pointer, 1));
	prod.putMetals(getSubnumber(pointer, 2));
	prod.putMinerals(getSubnumber(pointer, 3));
	prod.putOil(getSubnumber(pointer, 4));
	prod.putTroops(getSubnumber(pointer, 5));

	return prod;
    }

    private MaterialBundle getInitialMaterials(String res, 
					       int planetType,
					       int player) {

	if(res.equals("-") || getNthSubstring(res, 0).equals("-")) {
	    if(planetType == PlanetType.HOME) {
		return PlanetType.getHomePlanetMaterials(player);
	    } else {
		return PlanetType.getInitialMaterials(planetType);
	    }
	}
	MaterialBundle prod = new MaterialBundle(MaterialBundle.UNLIMITED);
	String pointer = getNthSubstring(res, 0);

	prod.putDodechadrium(getSubnumber(pointer, 0));
	prod.putFuel(getSubnumber(pointer, 1));
	prod.putMetals(getSubnumber(pointer, 2));
	prod.putMinerals(getSubnumber(pointer, 3));
	prod.putOil(getSubnumber(pointer, 4));
	prod.putTroops(getSubnumber(pointer, 5));

	String components = getSubString(pointer, 6);

	if(!components.equals("-")) {
	    String hulls = getSubString(components, 0);
	    for(int i=0; i<8; i++) {
		int nro = getSubnumber(hulls, i);
		for(int j=0; j<nro; j++)
		    prod.addComponent(new ShipHull(player, i+1));
	    }
	    String engines = getSubString(components, 1);
	    for(int i=0; i<8; i++) {
		int nro = getSubnumber(engines, i);
		for(int j=0; j<nro; j++)
		    prod.addComponent(new ShipEngine(i+1));
	    }
	    String weapons = getSubString(components, 2);
	    for(int i=0; i<8; i++) {
		int nro = getSubnumber(weapons, i);
		for(int j=0; j<nro; j++)
		    prod.addComponent(new ShipWeapon(i+1));
	    }
	    String shields = getSubString(components, 3);
	    for(int i=0; i<8; i++) {
		int nro = getSubnumber(shields, i);
		for(int j=0; j<nro; j++)
		    prod.addComponent(new ShipShield(i+1));
	    }
	    String misc = getSubString(components, 4);
	    for(int i=0; i<8; i++) {
		int nro = getSubnumber(misc, i);
		for(int j=0; j<nro; j++)
		    prod.addComponent(new ShipMisc(i+1));
	    }
	}

	return prod;
	
    }

    private TechLevel getInitialTech(String res, int planetType) {

	if(res.equals("-") || getNthSubstring(res, 2).equals("-")) {
	    if(planetType == PlanetType.HOME) {
		return PlanetType.getHomePlanetTechLevel();
	    } else {
		return new TechLevel();
	    }
	}

	TechLevel tl = new TechLevel();
	String pointer = getNthSubstring(res, 2);

	tl.setHullTech(getSubnumber(pointer, 0));	
	tl.setEngineTech(getSubnumber(pointer, 1));
	tl.setWeaponTech(getSubnumber(pointer, 2));
	tl.setShieldTech(getSubnumber(pointer, 3));
	tl.setMiscTech(getSubnumber(pointer, 4));

	return tl;
    }

  private void error(String s) {
    mapOK=false;
    System.out.println("ERROR: While loading " + mapFile + 
		       " (" + lineNumber+"): " + s + "\n");
  }

  private void warning(String s) {
    System.out.println("WARNING: While loading " + mapFile + 
		       " (" + lineNumber+"): " + s + "\n");
  }

    public Hashtable getPlanetResources() {
	return planetResources;
    }
}
