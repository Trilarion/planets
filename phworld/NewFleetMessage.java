/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2008 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import dsc.netgame.HostWorldMessage;


/** Message from host informing the client about
 *  new craeted fleet. Needed to make new fleets
 *  appear on the round they are created instead
 *  of the next round update.
 *
 * @author Dodekaedron Software Creations, Inc. -- tjt
 */

public class NewFleetMessage extends HostWorldMessage {

    private Fleet newFleet;
    private int creatorShip;

    /** Create message with the new fleet and the
     *  ship which created the fleet. The
     *  message receiver is set to the 
     * Remember to set round information.
     *
     * @param fleet The new fleet object.
     * @param ship  The creator ship
     * @param text Modeline text for the client.
     * @see HostWorldMessage#setRound
     */

    public NewFleetMessage(HostFleet fleet, HostShip ship) {
	super(fleet.getOwner());
	
	newFleet    = (Fleet) fleet.getWorldComponent(fleet.getOwner());
	creatorShip = ship.getId();
    }

    /** Gets the newly created client side fleet
     *
     * @return The new fleet
     */

    public Fleet getFleet() {
	return newFleet;
    }

    /**
     * Gets the ship id of the ship which created the fleet
     */
    
    public int getShipId() {
	return creatorShip;
    }
}
