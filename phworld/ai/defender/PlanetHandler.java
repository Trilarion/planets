/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2001 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld.ai.defender;

import dsc.netgame.*;
import phworld.*;
import java.util.*;

class PlanetHandler {

    private AI parentAI;
    private PHWorld world;

    public PlanetHandler(AI parentAI) {
	this.parentAI = parentAI;
    }

    void debugPrint(String s) {
	parentAI.debugPrint("(PH)" + s);
    }

    protected MaterialBundle getTotalProduction() {
	Enumeration e = parentAI.allOwnPlanets();
	MaterialBundle sum = new MaterialBundle(MaterialBundle.UNLIMITED);

	while(e.hasMoreElements()) {
	    Planet p = (Planet) e.nextElement();
	    
	    sum.addFullBundle(p.getProduction());
	}
	
	return sum;
    }

    private int getMinimumProductionSum() {
	MaterialBundle sum = getTotalProduction();

	int min = sum.getMetals();
	int minerals = sum.getMinerals();
	int oil      = sum.getOil();

	if(minerals < min) min = minerals;
	if(oil      < min) min = oil;
	
	return min;
    }

    private int getTargetNumberOfBuilders() {
	int min = getMinimumProductionSum();
	int target;

	target = (min+30)/130;
	if(target == 0)
	    target = 1;

	return target;
    }

    protected boolean isBuildingPlanet(Planet p) {
	if(p.getTechLevels().getHullTech() >= 1 ||
	   p.getActivityType() == HostPlanet.IMPROVE_TECH)	
	    return true;

	return false;
    }

    private int getNumberOfBuilders() {
	Enumeration e = parentAI.allOwnPlanets();
	int n=0;

	while(e.hasMoreElements()) {
	    Planet p = (Planet) e.nextElement();

	    if(isBuildingPlanet(p))
		n++;
	}

	return n;
    }
    
    protected Enumeration getBuildingPlanets() {
	Enumeration e = parentAI.allOwnPlanets();
	Vector v = new Vector();

	while(e.hasMoreElements()) {
	    Planet p = (Planet) e.nextElement();

	    if(isBuildingPlanet(p))
		v.addElement(p);
	}

	return v.elements();
    }

    protected Planet getPreferredTransferTarget(MaterialBundle cargo,
						Location cargoLocation,
						int maxDistance) {
	Planet target = null;
	int    best   = -1;
	Enumeration e = getBuildingPlanets();

	while(e.hasMoreElements()) {
	    Planet p = (Planet) e.nextElement();
	    
	    int goodness = 0;

	    goodness += (cargo.getMinerals()*5000)/
		(p.getMaterials().getMinerals()+150);
	    goodness += (cargo.getMetals()*5000)/
		(p.getMaterials().getMetals()+150);
	    goodness += (cargo.getOil()*5000)/
		(p.getMaterials().getOil()+150);


	    TechLevel tl = p.getTechLevels();
	    int techSum      =
		tl.getHullTech()*3 + tl.getShieldTech()*2 +
		tl.getWeaponTech()*2 + tl.getEngineTech() + tl.getMiscTech()*5;

	    goodness += cargo.getDodechadrium()*techSum*5;

	    goodness += techSum*150;
	    goodness -= cargoLocation.distance(p.getLocation())*15;

	    if(cargoLocation.equals(p.getLocation()))
		goodness += 500000;

	    if(goodness < 0)
		goodness = 0;

	    if(goodness > best && 
	       maxDistance >= p.getLocation().distance(cargoLocation)) {
		target = p;
		best   = goodness;
	    }
	}

	return target;
    }

    private int getDistanceToNearestBuilder(Location l) {
	int minDistance = Integer.MAX_VALUE/10;

	Enumeration e = getBuildingPlanets();

	while(e.hasMoreElements()) {
	    Planet p = (Planet) e.nextElement();

	    int distance = l.distance(p.getLocation());

	    if(distance < minDistance)
		minDistance = distance;
	}

	return minDistance;
    }

    private int calcBuildingGoodness(Planet p) {
	if(isBuildingPlanet(p))
	    return -1;

	int goodness = 0;

	if(p.getName().equals("Dodechaedron"))
	    return -1;

	goodness += p.getMaterials().getTroops()*2;
	goodness += getDistanceToNearestBuilder(p.getLocation()) * 6;
	goodness += p.getProduction().getMass()*5;
	goodness += p.getProduction().getTroops()*25;
	
	return goodness;
    }

    private void tryAddOneBuilder() {
	int best = -10;
	Planet bestPlanet = null;

	Enumeration e = parentAI.allOwnPlanets();

	while(e.hasMoreElements()) {
	    Planet p = (Planet) e.nextElement();

	    int goodness = calcBuildingGoodness(p);
	    if(goodness > best) {
		best = goodness;
		bestPlanet = p;
	    }
	}

	if(bestPlanet != null && best >0) {
	    debugPrint("Changing " + bestPlanet.getName() + 
		       " to be a building planet");
	    bestPlanet.startImproveTech(TechLevel.getTechNumber("Hull"));
	}
    }

    protected void doPlanetActions(PHWorld world) {
	this.world = world;
	debugPrint("Handling planets");

	int targetNumberOfBuilders =
	    getTargetNumberOfBuilders();

	if(targetNumberOfBuilders > getNumberOfBuilders()) {
	    debugPrint("Trying to add one builder");
	    tryAddOneBuilder();
	}
	
	Enumeration e = world.getWorldComponents();

	while(e.hasMoreElements()) {
	    WorldComponent wc = (WorldComponent) e.nextElement();
	    if(world.getController().playerNumber==wc.getOwner() &&
	       wc instanceof Planet) {
		handlePlanet((Planet) wc);
	    }
	}
    }

    protected void handlePlanet(Planet p) {
	debugPrint("+ Handling planet " + p.getName());

	if(!p.isDoingSomething()) {
	    debugPrint("++ Planet is free to do action");

	    if(p.getTechLevels().getHullTech() == 0 &&
	       p.getActivityType() != HostPlanet.IMPROVE_TECH) {
		if(p.getActivityType() != HostPlanet.IMPROVE_PRODUCTION) {
		    p.startProductionImproving();
		    debugPrint("++ Improving production");
		}
	    }

	    if(!tryBuildShip(p)) {
		if(!tryImproveTech(p)) {
		    tryBuildComponents(p);
		}
	    }
	}

	if(p.getActivityType() == HostPlanet.IMPROVE_PRODUCTION &&
	   (p.getMaterials().getMass() - p.getMaterials().getTroops() -
	    p.getMaterials().getFuel()) > 
	   parentAI.getShipHandler().getPreferredMaterialStock(p)) {
	    Planet target = findGoodAutotransferTarget(p);

	    if(target != null) {
		if(!(target == p ||
		     target.getId() == p.getAutotransferTarget())) {
		    p.setAndSendAutotransferTarget(target.getId());
		    debugPrint("++ Autotrasfering to planet " 
			       + target.getName());
		}
	    }
	} else {
	    if(p.getAutotransferTarget() != -1) {
		p.setAndSendAutotransferTarget(-1);
		debugPrint("++ Canceled autotransfer");
	    }
	}
    }


    protected Planet findGoodAutotransferTarget(Planet p) {
	
	Planet target = null;
	MaterialBundle production = p.getProduction();
	int best      = -1;
	Enumeration e = parentAI.allOwnPlanets();

	while(e.hasMoreElements()) {
	    Planet candidate = (Planet) e.nextElement();
	    TechLevel tl     = candidate.getTechLevels();

	    int efficiency   = HostTransferer.
		getEfectivityPercentage(p.getLocation().
					distance(candidate.getLocation()));

	    MaterialBundle materials =
		candidate.getMaterials();

	    MaterialBundle netProduction =
		((MaterialBundle) production.clone());
	    netProduction.scaleBundle(efficiency);

	    int oil = materials.getOil()+150;
	    int metals = materials.getMetals()+150;
	    int minerals = materials.getMinerals()+150;

	    int netEfficiency =
		netProduction.getMinerals()*5000/minerals +
		netProduction.getMetals()  *5000/metals +
		netProduction.getOil()     *5000/oil;

	    int techSum      =
		tl.getHullTech()*3 + tl.getShieldTech()*2 +
		tl.getWeaponTech()*2 + tl.getEngineTech() + tl.getMiscTech();

	    if(candidate.getActivityType() == HostPlanet.IMPROVE_TECH)
		techSum++;


	    if(techSum > 0 && netEfficiency > best) {
		best   = netEfficiency;
		target = candidate;
	    }
	}

	return target;
    }


    protected boolean tryBuildShip(Planet p) {
	int requested_type = parentAI.getShipHandler().
	    getPreferredNewShipType(p);

	ShipHull hull = phworld.ai.Toolkit.getHullInStock(p);

	if(hull == null)
	    return false;

	if(hull.getTech() == 1)
	    return tryBuildScoutShip(p);

	if(hull.getTech() == 3 ||
	   hull.getTech() == 6)
	    return tryBuildCargoShip(p);

	if(requested_type == ShipHandler.DEFENDER)
	    return tryBuildDefenderShip(p);

	return tryBuildAttackShip(p);
    }

    protected boolean tryBuildDefenderShip(Planet p) {
	debugPrint("++ Checking for ship components defender");

	MaterialBundle materials = p.getMaterials();
	Enumeration shipParts;

	boolean hasWeapon = false;
	boolean hasShield = false;
	boolean hasEngine = false;

	ShipHull hull         = null;
	int     compoMassLeft = 0;
	Vector  compos        = new Vector();

	hull = phworld.ai.Toolkit.getHullInStock(p);
	if(hull == null)
	    return false;

	compoMassLeft = hull.getVolume();
	debugPrint("+++ Found hull \"" + hull.toString() + "\".");

	shipParts = materials.components();
	while(shipParts.hasMoreElements()) {
	    TransferableComponent tc = 
		(TransferableComponent) shipParts.nextElement();

	    if(tc instanceof ShipWeapon) {
		int weaponId      = tc.getId();
		if(tc.getMass() <= compoMassLeft &&
		   (compoMassLeft-tc.getMass()) >= hull.getVolume()/3) {
		    compos.addElement(new Integer(weaponId));
		    compoMassLeft -= tc.getMass();
		    hasWeapon      = true;
		    debugPrint("+++ Found weapon \"" + tc.toString() + "\".");
		}
	    }
	}

	if(!hasWeapon)
	    return false;

	shipParts = materials.components();
	while(shipParts.hasMoreElements()) {
	    TransferableComponent tc = 
		(TransferableComponent) shipParts.nextElement();

	    if(tc instanceof ShipShield) {
		int shieldId      = tc.getId();
		if(tc.getMass() <= compoMassLeft) {
		    compos.addElement(new Integer(shieldId));
		    compoMassLeft -= tc.getMass();
		    hasShield      = true;
		    debugPrint("+++ Found shield \"" + tc.toString() + "\".");
		}
	    }
	}

	int weaponTech = p.getTechLevels().getWeaponTech();
	if(weaponTech > 0) {
	    ShipWeapon bestWeapon = new ShipWeapon(weaponTech);
	    if(compoMassLeft >= bestWeapon.getMass())
		return false;
	}

	debugPrint("++ Building new planet defense ship on planet " + 
		   p.getName() + ".");
	p.startBuildShip(hull.getId(), compos);

	return true;
    }

    protected boolean tryBuildAttackShip(Planet p) {
	debugPrint("++ Checking for ship components for attack ship");

	MaterialBundle materials = p.getMaterials();
	Enumeration shipParts;

	boolean hasWeapon = false;
	boolean hasShield = false;
	int enginePower   = 0;
	int consumption   = 0;

	ShipHull hull         = null;
	int     compoMassLeft = 0;
	Vector  compos        = new Vector();

	hull = phworld.ai.Toolkit.getHullInStock(p);
	if(hull == null)
	    return false;

	consumption += hull.getPowerConsumption();

	compoMassLeft = hull.getVolume();
	debugPrint("+++ Found hull \"" + hull.toString() + "\".");

	shipParts = materials.components();
	while(shipParts.hasMoreElements()) {
	    TransferableComponent tc = 
		(TransferableComponent) shipParts.nextElement();

	    if(tc instanceof ShipEngine) {
		compoMassLeft -= tc.getMass();
		compos.addElement(new Integer(tc.getId()));
		enginePower += ((ShipEngine) tc).getMaxPowerProduction();
		consumption += ((ShipEngine) tc).getPowerConsumption();
	    }

	    if(enginePower > (hull.getMass() + hull.getVolume())*3/2)
		break;
	}

	compoMassLeft -= consumption * 2;

	if(enginePower < (hull.getMass() + hull.getVolume()))
	    return false;

	int freeMass = compoMassLeft;

	shipParts = materials.components();
	while(shipParts.hasMoreElements()) {
	    TransferableComponent tc = 
		(TransferableComponent) shipParts.nextElement();

	    if(tc instanceof ShipWeapon) {
		int weaponId      = tc.getId();
		if(tc.getMass() <= compoMassLeft &&
		   (compoMassLeft-tc.getMass()) >= freeMass/3) {
		    compos.addElement(new Integer(weaponId));
		    compoMassLeft -= tc.getMass();
		    hasWeapon      = true;
		    debugPrint("+++ Found weapon \"" + tc.toString() + "\".");
		}
	    }
	}

	if(!hasWeapon)
	    return false;

	shipParts = materials.components();
	while(shipParts.hasMoreElements()) {
	    TransferableComponent tc = 
		(TransferableComponent) shipParts.nextElement();

	    if(tc instanceof ShipShield) {
		int shieldId      = tc.getId();
		if(tc.getMass() <= compoMassLeft) {
		    compos.addElement(new Integer(shieldId));
		    compoMassLeft -= tc.getMass();
		    hasShield      = true;
		    debugPrint("+++ Found shield \"" + tc.toString() + "\".");
		}
	    }
	}

	int weaponTech = p.getTechLevels().getWeaponTech();
	if(weaponTech > 0) {
	    ShipWeapon bestWeapon = new ShipWeapon(weaponTech);
	    if(compoMassLeft >= bestWeapon.getMass())
		return false;
	}

	debugPrint("++ Building new attack ship on planet " + 
		   p.getName() + ".");
	p.startBuildShip(hull.getId(), compos);

	return true;
    }


    protected boolean tryBuildScoutShip(Planet p) {
	debugPrint("++ Checking for ship components for scout");

	MaterialBundle materials = p.getMaterials();
	Enumeration shipParts;

	ShipHull hull         = phworld.ai.Toolkit.getHullInStock(p);
	int     compoMassLeft = 0;
	Vector  compos        = new Vector();

	if(hull == null)
	    return false;

	compoMassLeft = hull.getVolume();
	debugPrint("+++ Found hull \"" + hull.toString() + "\".");

	boolean hasEngine = false;
	shipParts = materials.components();
	while(shipParts.hasMoreElements()) {
	    TransferableComponent tc = 
		(TransferableComponent) shipParts.nextElement();

	    if(tc instanceof ShipEngine) {
		compoMassLeft -= tc.getMass();
		compos.addElement(new Integer(tc.getId()));
		hasEngine = true;
		break;
	    }
	}

	if(!hasEngine)
	    return false;

	debugPrint("++ Building a new scout on planet " + p.getName() + ".");
	p.startBuildShip(hull.getId(), compos);

	return true;
    }

    protected boolean tryBuildCargoShip(Planet p) {
	debugPrint("++ Checking for ship components for cargo ship");

	MaterialBundle materials = p.getMaterials();
	Enumeration shipParts;

	ShipHull hull         = phworld.ai.Toolkit.getHullInStock(p);
	int     compoMassLeft = 0;
	Vector  compos        = new Vector();
	int     enginePower   = 0;

	if(hull == null)
	    return false;

	compoMassLeft = hull.getVolume();
	debugPrint("+++ Found hull \"" + hull.toString() + "\".");

	shipParts = materials.components();
	while(shipParts.hasMoreElements()) {
	    TransferableComponent tc = 
		(TransferableComponent) shipParts.nextElement();

	    if(tc instanceof ShipEngine) {
		compoMassLeft -= tc.getMass();
		compos.addElement(new Integer(tc.getId()));
		enginePower += ((ShipEngine) tc).getMaxPowerProduction();
	    }

	    if(enginePower> (hull.getMass() + hull.getVolume()) * 2)
		break;
	}

	if(enginePower < (hull.getMass() + hull.getVolume()))
	    return false;

	debugPrint("++ Building a new cargo ship on planet " + 
		   p.getName() + ".");
	p.startBuildShip(hull.getId(), compos);

	return true;
    }


    protected boolean tryBuildComponents(Planet p) {
	debugPrint("++ Trying to build more ship components");

	TechLevel techLevels = p.getTechLevels();
	ShipHull hull = phworld.ai.Toolkit.getHullInStock(p);
	int requested_type = parentAI.getShipHandler().
	    getPreferredNewShipType(p);

	if(p.getActivityType() == HostPlanet.BUILD_COMPONENT)
	    p.startIdling();

	if(hull != null) {
	    if(requested_type == ShipHandler.SCOUT ||
	       requested_type == ShipHandler.CARGO)
		requested_type = ShipHandler.DEFENDER;

	    if(hull.getTech() == 1)
		requested_type = ShipHandler.SCOUT;

	    if(hull.getTech() == 3 ||
	       hull.getTech() == 6)
		requested_type = ShipHandler.CARGO;
	}

	if(hull == null) {
	    int buildLevel = 0;
	    
	    if(requested_type == ShipHandler.SCOUT) {
		if(techLevels.getHullTech() >= 1)
		    buildLevel = 1;
	    } else if(requested_type == ShipHandler.DEFENDER) {
		if(techLevels.getHullTech() >= 2)
		    buildLevel = 2;
		if(techLevels.getHullTech() >= 4)
		    buildLevel = 4;
		if(techLevels.getHullTech() >= 5)
		    buildLevel = 5;
		if(techLevels.getHullTech() >= 7)
		    buildLevel = 7;
	    } else if(requested_type == ShipHandler.CARGO) {
		if(techLevels.getHullTech() >= 3)
		    buildLevel = 3;
		if(techLevels.getHullTech() >= 6)
		    buildLevel = 6;
		if(techLevels.getEngineTech() <= 3)
		    buildLevel = 0;
	    } else if(requested_type == ShipHandler.ATTACK) {
		if(techLevels.getHullTech() >= 2)
		    buildLevel = 2;
		if(techLevels.getHullTech() >= 4)
		    buildLevel = 4;
		if(techLevels.getHullTech() >= 7)
		    buildLevel = 7;
	    }

	    if(buildLevel > 0) {
		startBuildComponent(p, "Hull", buildLevel);
		return true;
	    }
	    
	    return false;
	} 

	if(hull.getTech() != 5 && requested_type != ShipHandler.DEFENDER) {
	    int engine_power = 
	     ShipMath.calcEnginePower(phworld.ai.Toolkit.getEnginesInStock(p));
	    if(engine_power < (hull.getVolume() + hull.getMass())) {
		int buildLevel = techLevels.getEngineTech();

		if(buildLevel > 0) {
		    startBuildComponent(p, "Engine", buildLevel);
		    return true;
		} else {
		    return false;
		}
	    }
	}

	Enumeration shields = phworld.ai.Toolkit.getShieldsInStock(p);
	if(ShipMath.calculateMass(shields) < hull.getVolume()/3) {
	    int buildLevel = techLevels.getShieldTech();
	    if(buildLevel > 0) {
		startBuildComponent(p, "Shield", buildLevel);
		return true;
	    }
	}

	if(ShipMath.calculateMass(phworld.ai.
				  Toolkit.getWeaponsInStock(p).elements()) >=
	   hull.getVolume())
	    return false;

	int buildLevel = techLevels.getWeaponTech();
	if(buildLevel > 0) {
	    int hullKillPower = 
		ShipMath.calcWeaponHullPower(phworld.ai.
					     Toolkit.getWeaponsInStock(p));
	    int shieldKillPower = 
		ShipMath.calcWeaponShieldPower(phworld.ai.
					       Toolkit.getWeaponsInStock(p));

	    boolean buildHullKillers=false;

	    if(((hullKillPower*3)/2) < shieldKillPower)
		buildHullKillers=true;

	    if(buildLevel < 7) {
		if(buildHullKillers) {
		    if(buildLevel % 2 == 0)
			buildLevel -= 1;
		} else if(buildLevel > 1) {
		    if(buildLevel % 2 == 1)
			buildLevel -= 1;
		}
	    }

	    startBuildComponent(p, "Weapon", buildLevel);
	    return true;
	}

	return false;
    }
    
    protected void startBuildComponent(Planet p, 
				       String techName, 
				       int techLevel) {
	int techType = TechLevel.getTechNumber(techName);
	p.startBuildComponent(techType, techLevel);
	debugPrint("++ Building new " + 
		   TechLevel.getName(techType, techLevel));
    }

    protected boolean hasTechMaterials(Planet p, 
				       String techName) {

	int techType = TechLevel.getTechNumber(techName);

	MaterialBundle requirements =
	    TechLevel.getTechMaterials(techType, 
				       p.getTechLevels().getTech(techType)+1,
				       0);
	MaterialBundle materials    =
	    p.getMaterials();

	if(materials.subtractFullBundle(requirements)) {
	    materials.addFullBundle(requirements);
	    return true;
	}

	return false;
    }				      

    protected boolean tryImproveTech(Planet p) {
	String techName = null;
	int level = 0;
	TechLevel techs = p.getTechLevels();
	int preferredType = 
	    parentAI.getShipHandler().getPreferredNewShipType(p);

	if(p.getActivityType() == HostPlanet.IMPROVE_TECH) {
	    p.startIdling();
	}

	if(preferredType != ShipHandler.CARGO) {
	    if(techs.getWeaponTech() < 8 && 
	       hasTechMaterials(p, "Weapon")) {
		level    = techs.getWeaponTech();
		techName = "Weapon";
	    }

	    if(techs.getShieldTech() < 8 && 
	       hasTechMaterials(p, "Shield")) {
		level    = techs.getShieldTech();
		techName = "Shield";
	    }
	}

	if(preferredType != ShipHandler.DEFENDER) {
	    if(techs.getEngineTech() < 8 &&
	       hasTechMaterials(p, "Engine")) {
		level    = techs.getEngineTech();
		techName = "Engine";
	    }
	}

	if(techs.getHullTech() < 8 &&
	   hasTechMaterials(p, "Hull")) {
	    level    = techs.getHullTech();
	    techName = "Hull";
	}

	if(techName != null) {
	    p.startImproveTech(TechLevel.getTechNumber(techName));
	    debugPrint("++ Improving " + techName + " technology");
	    return true;
	}

	return false;
    }
}
