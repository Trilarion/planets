/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2001 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld.ai.defender;

import dsc.netgame.*;
import phworld.*;
import java.util.*;


public class AI implements dsc.netgame.AI {

    PHWorld world = null;

    ShipHandler shipHandler;
    PlanetHandler planetHandler;

    public AI() {
	shipHandler   = new ShipHandler(this);
	planetHandler = new PlanetHandler(this);
    }

    public void doRound(World world) {
	this.world = (PHWorld) world;

	shipHandler.doShipActions(this.world);
	planetHandler.doPlanetActions(this.world);

	world.endTurn();
    }

    void debugPrint(String s) {
	System.err.println("Defender AI debug: " + s);
    }

    protected Enumeration allOwnPlanets() {
	Enumeration e = world.getWorldComponents();
	Vector planets = new Vector();

	while(e.hasMoreElements()) {
	    WorldComponent wc = (WorldComponent) e.nextElement();

	    if(wc instanceof Planet &&
	       world.getController().playerNumber==wc.getOwner()) {
		planets.addElement(wc);
	    }
	}

	return planets.elements();
    }

    protected Enumeration allOwnShips() {
	Enumeration e = world.getWorldComponents();
	Vector ships = new Vector();

	while(e.hasMoreElements()) {
	    WorldComponent wc = (WorldComponent) e.nextElement();

	    if(wc instanceof Ship &&
	       world.getController().playerNumber==wc.getOwner()) {
		ships.addElement(wc);
	    }
	}

	return ships.elements();
    }

    protected Enumeration allOwnShipsAt(Location l) {
	Enumeration e = world.getWorldComponents();
	Vector ships = new Vector();

	while(e.hasMoreElements()) {
	    WorldComponent wc = (WorldComponent) e.nextElement();

	    if(wc instanceof Ship &&
	       world.getController().playerNumber==wc.getOwner() &&
	       wc.getLocation().equals(l)) {
		ships.addElement(wc);
	    }
	}

	return ships.elements();
    }

    protected Enumeration allOwnFleets() {
	Enumeration e = world.getWorldComponents();
	Vector fleets = new Vector();

	while(e.hasMoreElements()) {
	    WorldComponent wc = (WorldComponent) e.nextElement();

	    if(wc instanceof Fleet &&
	       world.getController().playerNumber==wc.getOwner()) {
		fleets.addElement(wc);
	    }
	}

	return fleets.elements();
    }


    protected Enumeration allOwnFleetsAt(Location l) {
	Enumeration e = world.getWorldComponents();
	Vector fleets = new Vector();

	while(e.hasMoreElements()) {
	    WorldComponent wc = (WorldComponent) e.nextElement();

	    if(wc instanceof Fleet &&
	       world.getController().playerNumber==wc.getOwner() &&
	       wc.getLocation().equals(l)) {
		fleets.addElement(wc);
	    }
	}

	return fleets.elements();
    }


    protected boolean canSeeAt(Location l) {
	Enumeration e = world.getWorldComponents();

	while(e.hasMoreElements()) {
	    WorldComponent wc = (WorldComponent) e.nextElement();

	    if(wc.getOwner() != world.getController().playerNumber)
		continue;

	    if(l.distance(wc.getLocation()) <= wc.getScanRange()) {
		return true;
	    }
	}

	return false;
    }

    ShipHandler getShipHandler(){
	return shipHandler;
    }

    PlanetHandler getPlanetHandler() {
	return planetHandler;
    }
}
