package phworld.ai;

import dsc.netgame.*;
import phworld.*;
import java.util.*;


public abstract class Toolkit {

    public static ShipHull getHullInStock(Planet p) {
	MaterialBundle materials = p.getMaterials();
	Enumeration shipParts = materials.components();

	ShipHull hull         = null;

	while(shipParts.hasMoreElements()) {
	    TransferableComponent tc = 
		(TransferableComponent) shipParts.nextElement();

	    if(tc instanceof ShipHull ) {
		return (ShipHull) tc;
	    }
	}

	return null;
    }

    public static Enumeration getShieldsInStock(Planet p) {
	MaterialBundle materials = p.getMaterials();
	Enumeration shipParts = materials.components();
	Vector shields = new Vector();

	while(shipParts.hasMoreElements()) {
	    TransferableComponent tc = 
		(TransferableComponent) shipParts.nextElement();

	    if(tc instanceof ShipShield) {
		shields.addElement(tc);
	    }
	}

	return shields.elements();
    }

    public static Vector getWeaponsInStock(Planet p) {
	MaterialBundle materials = p.getMaterials();
	Enumeration shipParts = materials.components();
	Vector weapons = new Vector();

	while(shipParts.hasMoreElements()) {
	    TransferableComponent tc = 
		(TransferableComponent) shipParts.nextElement();

	    if(tc instanceof ShipWeapon) {
		weapons.addElement(tc);
	    }
	}

	return weapons;
    }

    public static Vector getEnginesInStock(Planet p) {
	MaterialBundle materials = p.getMaterials();
	Enumeration shipParts = materials.components();
	Vector engines = new Vector();

	while(shipParts.hasMoreElements()) {
	    TransferableComponent tc = 
		(TransferableComponent) shipParts.nextElement();

	    if(tc instanceof ShipEngine) {
		engines.addElement(tc);
	    }
	}

	return engines;
    }

}
