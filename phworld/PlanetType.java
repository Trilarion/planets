/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import java.io.*;
import dsc.netgame.Location;

public class PlanetType {

    public static final int DODECHAEDRON = 0;
    public static final int GAIA = 1;
    public static final int MARS = 2;
    public static final int SEA = 3;
    public static final int VENUS = 4;
    public static final int ASTEROID = 5;
    public static final int JUPITER = 6;
    
    public static final int RUINED    = 7;
    public static final int MAGMA     = 8;
    public static final int ICE       = 9;
    public static final int COPPER    = 10;
    public static final int MOON      = 11;
    public static final int SATURN    = 12;

    public static final int HOME = 100;
    public static final int RANDOM = 666;

    public static final int MAX_NUMBER =  12;
    public static final int MIN_NUMBER =  0;

    public static final double SCALE=0.70;

    private static final int [] minFuel = {
	12, 10,  8,  3, 4,   2, 25,  8,  8,  3,  4,  4, 20 };
    private static final int [] maxFuel = {
	35, 25, 30, 12, 15,  5, 50, 20, 30, 12, 15, 10, 50 };

    private static final int [] minMetals = {
	5,   5,  6,  1, 20,  3,  1, 10,  5,  1, 30, 10, 3  };
    private static final int [] maxMetals = {
	18, 23, 23,  4, 45, 13,  4, 23, 12,  4, 45, 27, 13 };

    private static final int [] minMinerals = {
	5,   5, 15,  2,  5,  1,  5,  5, 23,  1,  4,  2, 7  };
    private static final int [] maxMinerals = {
	20, 25, 50, 10, 20,  3, 20, 25, 50,  4, 10,  6, 24 };

    private static final int [] minOil = {
	0, 10,   5, 25, 13,  0,  0,  9,  4, 30,  7,  3,  0 };
    private static final int [] maxOil = {
	0, 30,  10, 50, 17,  0,  0, 24, 10, 50, 12,  6,  0 };

    private static final int [] minTroops = {
	0, 10,   4,  8,  3,  0,  0,  5,  2,  3,  2,  1,  0 };
    private static final int [] maxTroops = {
	0, 42,  13, 27, 12,  2,  0, 18,  5, 12, 12,  6,  0 };

    private static final int [] minDodechaedron = {
	4,  0,   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 };


    private static int calcNumberInRange(int min, int max) {
	double d1 = (max - min) / 2;
	double d2 = (max - min +1 ) /2;

	int a = (int) (Math.random() * d1 + 0.5);
	int b = (int) (Math.random() * d2 + 0.5);
    
	double scaled_result =  SCALE * ((double) (min + a+b));

	return ((int) scaled_result);
    }

    public static MaterialBundle getInitialMaterials(int type) {
	if(type < MIN_NUMBER || type > MAX_NUMBER) 
	    throw (new IllegalArgumentException("Must be between " +
						MIN_NUMBER + " and " +
						MAX_NUMBER));

	MaterialBundle mb = new MaterialBundle(MaterialBundle.UNLIMITED);
	mb.putFuel(calcNumberInRange(70,150));

	return mb;
    }

    public static MaterialBundle getProductionMaterials(int type) {
	if(type < MIN_NUMBER || type > MAX_NUMBER) 
	    throw (new IllegalArgumentException("Must be between " +
						MIN_NUMBER + " and " +
						MAX_NUMBER));

	MaterialBundle prod = new MaterialBundle(MaterialBundle.UNLIMITED);
    
	prod.putFuel(calcNumberInRange(minFuel[type], maxFuel[type]));
	prod.putMetals(calcNumberInRange(minMetals[type], maxMetals[type]));
	prod.putMinerals(calcNumberInRange(minMinerals[type], maxMinerals[type]));
	prod.putOil(calcNumberInRange(minOil[type], maxOil[type]));
	prod.putTroops(calcNumberInRange(minTroops[type], maxTroops[type]));
	prod.putDodechadrium(minDodechaedron[type]);

	return prod;
    }

    public static int getRandomType() {
	int n = (int) (Math.random() * 12.0 + 1.0);
	if(n>MAX_NUMBER)
	    n=MAX_NUMBER;

	if(n == ASTEROID) {
		return getRandomType();
	} else {
		return n;
	}
    }

    public static String getImageFileName(int type) {
	return ("data" + File.separator + 
		"graph" + File.separator +
		"still" + File.separator +
		"components" + File.separator +
		"planets" + File.separator +
		"planet" + type +
		".gif");
    }

    public static String getPanelImageFileName(int type) {
	return ("data" + File.separator + 
		"graph" + File.separator +
		"still" + File.separator +
		"panel" + File.separator +
		"planet" + type +
		".gif");
    }

    public static HostPlanet getHomePlanet(PHHostWorld hw,
					   int player,
					   Location location) {
	HostPlanet hp;
	MaterialBundle initial = getHomePlanetMaterials(player);
	MaterialBundle prod = getHomePlanetProduction();

	hp=new HostPlanet(hw, player, location, prod, initial, 
			  PlanetType.HOME);
    
	hp.setTech(getHomePlanetTechLevel());
	hp.setOwnershipVisiblity(true);
	return hp;
    }

    public static MaterialBundle getHomePlanetProduction() {
	MaterialBundle prod = new MaterialBundle(MaterialBundle.UNLIMITED);

	prod.putFuel(40);
	prod.putMetals(30);
	prod.putMinerals(30);
	prod.putOil(40);
	prod.putTroops(50);

	return prod;
    }

    public static MaterialBundle getHomePlanetMaterials(int player) {
	MaterialBundle initial = new MaterialBundle(MaterialBundle.UNLIMITED);

	initial.putFuel(1000);
	initial.putMetals(400);
	initial.putMinerals(400);
	initial.putOil(400);
	initial.putTroops(1000);

	initial.addComponent(new ShipHull(player, 1));
	initial.addComponent(new ShipHull(player, 1));
	initial.addComponent(new ShipEngine(1));
	initial.addComponent(new ShipEngine(1));
	initial.addComponent(new ShipEngine(1));
	initial.addComponent(new ShipWeapon(1));
	initial.addComponent(new ShipShield(1));
	initial.addComponent(new ShipShield(1));

	return initial;
    }

    public static TechLevel getHomePlanetTechLevel() {
	TechLevel tl = new TechLevel();
	tl.setWeaponTech(1);
	tl.setShieldTech(1);
	tl.setHullTech(1);
	tl.setEngineTech(1);

	return tl;
    }

    public static int getCreditProduction(int type) {
	switch(type) {
	case DODECHAEDRON:
	    return 0;
	case HOME:
	    return 800;
	default:
	    return 35;
	}
    }

    public static int getPlanetTypeNumber(String s) {
	if(s.equalsIgnoreCase("RANDOM")) return getRandomType();

	if(s.equalsIgnoreCase("DODECHAEDRON")) return DODECHAEDRON;
	if(s.equalsIgnoreCase("GAIA")) return GAIA;
	if(s.equalsIgnoreCase("MARS")) return MARS;
	if(s.equalsIgnoreCase("SEA")) return SEA;
	if(s.equalsIgnoreCase("VENUS")) return VENUS;
	if(s.equalsIgnoreCase("ASTEROID")) return ASTEROID;
	if(s.equalsIgnoreCase("SATURN")) return SATURN;
	if(s.equalsIgnoreCase("HOME")) return HOME;
	if(s.equalsIgnoreCase("JUPITER")) return JUPITER;
	if(s.equalsIgnoreCase("RUINED")) return RUINED;
	if(s.equalsIgnoreCase("MAGMA")) return MAGMA;
	if(s.equalsIgnoreCase("ICE")) return ICE;
	if(s.equalsIgnoreCase("COPPER")) return COPPER;
	if(s.equalsIgnoreCase("MOON")) return MOON;
	
	return -1;
    }

    public static String getPlanetTypeName(int i) {
	switch(i) {
	    
	case GAIA:
	    return("GAIA");
	case RANDOM:
	    return("RANDOM");
	case DODECHAEDRON:
	    return("DODECHAEDRON");
	case MARS:
	    return("MARS");
	case SEA:
	    return("SEA");
	case VENUS:
	    return("VENUS");
	case ASTEROID:
	    return("ASTEROID");
	case SATURN:
	    return("SATURN");
	case HOME:
	    return("HOME");
	case JUPITER:
	    return("JUPITER");
	case RUINED:
	    return("RUINED");
	case MAGMA:
	    return("MAGMA");
	case ICE:
	    return("ICE");
	case COPPER:
	    return("COPPER");
	case MOON:
	    return("MOON");
	default:
	    return("NONAME");
	}
    }
}
