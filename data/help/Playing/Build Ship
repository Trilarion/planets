This is the Build Ship Control Panel where you can assemble previously-built components into previously-built hulls.

Use the choice list immediately below the shipyard picture to select a hull to be built on. The list will show you the technology level, name and condition of each hull.

Use the check list below the hull choice list to check on all components you want aboard your new ship. If there is not enough room aboard the ship for a component, it cannot be checked. The list will show you the type (E/W/S), technology level, component name, condition and mass of each component.

Below the check list, you see several statistics on the new ship. 

"Cargo" will show you the available cargo space versus the total space on the hull.

"Mass" will show you the mass of the ship without cargo versus the mass of the ship with its cargo space full.

"W/S" will show you the hull weapon power, shield weapon power, max shield power, and persistent shield power of the ship. See the manual for details.

"E/ES/EDT" will show you the engine power, estimated speed with full cargo space (in light years per turn), and maximum range of the ship (in light years, one-way and without refueling). See the manual for details.

The last line will display the estimated fuel consumption of the ship per turn, and the time required to build the ship.

If the ship has enough engine power to move, you can see a green circle in the map, centered on the planet which is building the ship. The circle's radius is the ship's maximum range, one-way and without refueling. In some cases, the circle may not be shown if it is so big that it will not fit on the map.

Click on the "Gallery" button to open the Technology Gallery and to receive details about hull and component types. Click "OK" to proceed with building the ship. "Cancel" will abort all changes.
